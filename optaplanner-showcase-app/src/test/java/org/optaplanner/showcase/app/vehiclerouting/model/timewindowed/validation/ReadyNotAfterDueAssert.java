package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.validation;

import java.util.Set;
import javax.validation.ConstraintViolation;

public class ReadyNotAfterDueAssert extends ConstraintViolationAssert<TimeWindowDTO> {

    private static final String VALIDATION_ERROR_MESSAGE = "Ready time must not be after due time.";

    public ReadyNotAfterDueAssert(Set<ConstraintViolation<TimeWindowDTO>> actual) {
        super(ReadyNotAfterDueAssert.class, actual);
    }

    public static ReadyNotAfterDueAssert assertThat(Set<ConstraintViolation<TimeWindowDTO>> actual) {
        return new ReadyNotAfterDueAssert(actual);
    }

    @Override
    protected String getErrorMessage() {
        return VALIDATION_ERROR_MESSAGE;
    }

}
