package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.impl;

import org.optaplanner.showcase.app.vehiclerouting.exception.MappingException;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedDelivery;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.testdata.PlanningTimeWindowedCustomerBuilder;
import org.optaplanner.showcase.app.vehiclerouting.testdata.PlanningTimeWindowedDepotBuilder;
import org.optaplanner.showcase.app.vehiclerouting.testdata.PlanningVehicleBuilder;
import org.optaplanner.showcase.app.vehiclerouting.testdata.TestModelData;
import org.optaplanner.showcase.app.vehiclerouting.testdata.TestScheduledPlanningBeans;
import org.optaplanner.showcase.app.vehiclerouting.util.datetime.PlanningUnitsToDateTimeTranslator;
import org.optaplanner.showcase.app.vehiclerouting.util.mapper.MappingUtils;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningDepot;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningBeanFactory;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.timewindowed.DefaultPlanningBeanFactory;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class DefaultTimedBeanMapperTest {

    public static final int INVALID_ARRIVAL_TIME = -1000;
    public static final DateTime ORIGIN = TestModelData.ORIGIN;

    private DefaultTimedBeanMapper timedBeanMapper;
    private PlanningBeanFactory planningBeanFactory;
    private PlanningUnitsToDateTimeTranslator timeTranslator;

    @Before
    public void setUp() {
        planningBeanFactory = DefaultPlanningBeanFactory.getInstanceForAirDistance();
        timeTranslator = new PlanningUnitsToDateTimeTranslator();
        timedBeanMapper = new DefaultTimedBeanMapper(planningBeanFactory, timeTranslator);
    }

    @Test
    public void map_PlanningCustomerGiven_ShouldReturnMappedCustomer() {
        PlanningTimeWindowedCustomer sourceCustomer = PlanningTimeWindowedCustomerBuilder
                .getBuilder(planningBeanFactory)
                .forDefaultCustomer()
                .build();

        TimeWindowedCustomer destinationCustomer = timedBeanMapper.map(sourceCustomer, ORIGIN);

        assertNotNull(destinationCustomer);
        assertEquals(null, destinationCustomer.getId());
        assertEquals(TestModelData.DEMAND, destinationCustomer.getDemand().intValue());
        assertEquals(TestModelData.LOCATION, destinationCustomer.getLocation());
        assertEquals(TestModelData.SERVICE_DURATION, destinationCustomer.getServiceDuration().intValue());
        assertEquals(TestModelData.createDefaultTimeWindow(), destinationCustomer.getTimeWindow());
        // Checks equality based on natural identity of problem domain customers
        assertEquals(createReferenceTimeWindowedCustomer(), destinationCustomer);
    }

    @Test
    public void map_PlanningCustomerWithNullIdGiven_ShouldThrowMappingException() {
        PlanningTimeWindowedCustomer sourceCustomer = PlanningTimeWindowedCustomerBuilder
                .getBuilder(planningBeanFactory)
                .forDefaultCustomer()
                .withId(null)
                .build();

        catchException(timedBeanMapper).map(sourceCustomer, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("planningCustomer", "Customer id can't be null"))
                .hasCauseInstanceOf(NullPointerException.class);
    }

    @Test
    public void map_PlanningCustomerWithNullLocationGiven_ShouldThrowMappingException() {
        PlanningTimeWindowedCustomer sourceCustomer = PlanningTimeWindowedCustomerBuilder
                .getBuilder(planningBeanFactory)
                .forDefaultCustomer()
                .withLocation(null)
                .build();

        catchException(timedBeanMapper).map(sourceCustomer, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("planningCustomer", "Customer location can't be null"))
                .hasCauseInstanceOf(NullPointerException.class);
    }

    @Test
    public void map_PlanningDepotGiven_ShouldReturnMappedDepot() {
        PlanningTimeWindowedDepot sourceDepot = PlanningTimeWindowedDepotBuilder
                .getBuilder(planningBeanFactory)
                .forDefaultDepot()
                .build();

        TimeWindowedDepot destinationDepot = timedBeanMapper.map(sourceDepot, ORIGIN);

        assertNotNull(destinationDepot);
        assertEquals(null, destinationDepot.getId());
        assertEquals(TestModelData.LOCATION, destinationDepot.getLocation());
        assertEquals(TestModelData.createDefaultTimeWindow(), destinationDepot.getTimeWindow());
        // Checks equality based on natural identity of problem domain depots
        assertEquals(createReferenceTimeWindowedDepot(), destinationDepot);
    }

    @Test
    public void map_PlanningDepotWithNullIdGiven_ShouldThrowMappingException() {
        PlanningTimeWindowedDepot sourceDepot = PlanningTimeWindowedDepotBuilder
                .getBuilder(planningBeanFactory)
                .forDefaultDepot()
                .withId(null)
                .build();

        catchException(timedBeanMapper).map(sourceDepot, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("planningDepot", "Depot id can't be null"))
                .hasCauseInstanceOf(NullPointerException.class);
    }

    @Test
    public void map_PlanningDepotWithNullLocationGiven_ShouldThrowMappingException() {
        PlanningTimeWindowedDepot sourceDepot = PlanningTimeWindowedDepotBuilder
                .getBuilder(planningBeanFactory)
                .forDefaultDepot()
                .withLocation(null)
                .build();

        catchException(timedBeanMapper).map(sourceDepot, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("planningDepot", "Depot location can't be null"))
                .hasCauseInstanceOf(NullPointerException.class);
    }

    @Test
    public void map_PlanningVehicleGiven_ShouldReturnMappedVehicle() {
        PlanningVehicle sourceVehicle = PlanningVehicleBuilder
                .getBuilder(planningBeanFactory)
                .forDefaultVehicle()
                .build();

        TimedVehicle destinationVehicle = timedBeanMapper.map(sourceVehicle, ORIGIN);

        assertNotNull(destinationVehicle);
        assertEquals(null, destinationVehicle.getId());
        assertEquals(TestModelData.CAPACITY, destinationVehicle.getCapacity().intValue());
        assertEquals(TestModelData.LICENSE_PLATE, destinationVehicle.getLicensePlate());
        assertEquals(createReferenceTimeWindowedDepot(), destinationVehicle.getDepot());
        // Checks equality based on natural identity of problem domain vehicles
        assertEquals(createReferenceTimedVehicle(), destinationVehicle);
    }

    @Test
    public void map_PlanningVehicleWithNullLicensePlate_ShouldReturnMappedVehicleWithGeneratedLicensePlate() {
        PlanningVehicle sourceVehicle = PlanningVehicleBuilder
                .getBuilder(planningBeanFactory)
                .forDefaultVehicle()
                .withLicensePlate(null)
                .build();

        TimedVehicle destinationVehicle = timedBeanMapper.map(sourceVehicle, ORIGIN);

        assertNotNull(destinationVehicle);
        assertEquals(null, destinationVehicle.getId());
        assertEquals(TestModelData.CAPACITY, destinationVehicle.getCapacity().intValue());
        // todo assert NotNull
        assertEquals(null, destinationVehicle.getLicensePlate());
        assertEquals(createReferenceTimeWindowedDepot(), destinationVehicle.getDepot());
        // If the planning vehicle does not have license plate set, it is not equal to problem domain vehicle
        assertNotEquals(createReferenceTimedVehicle(), destinationVehicle);
    }

    @Test
    public void map_PlanningVehicleWithIncompatibleDepotGiven_ShouldThrowMappingException() {
        PlanningDepot sourceDepot = TestModelData.createDummyPlanningDepot();
        PlanningVehicle sourceVehicle = PlanningVehicleBuilder
                .getBuilder(planningBeanFactory)
                .forDefaultVehicle()
                .withDepot(sourceDepot)
                .build();

        catchException(timedBeanMapper).map(sourceVehicle, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("planningVehicle",
                        MappingUtils.getTypeMismatchExceptionMessage(PlanningTimeWindowedDepot.class, sourceDepot)))
                .hasCauseInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void map_PlanningVehicleWithNullIdGiven_ShouldThrowMappingException() {
        PlanningVehicle sourceVehicle = PlanningVehicleBuilder
                .getBuilder(planningBeanFactory)
                .forDefaultVehicle()
                .withId(null)
                .build();

        catchException(timedBeanMapper).map(sourceVehicle, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("planningVehicle", "Vehicle id can't be null"))
                .hasCauseInstanceOf(NullPointerException.class);
    }

    @Test
    public void map_PlanningVehicleWithNullDepotGiven_ShouldThrowMappingException() {
        PlanningVehicle sourceVehicle = PlanningVehicleBuilder
                .getBuilder(planningBeanFactory)
                .forDefaultVehicle()
                .withDepot(null)
                .build();

        catchException(timedBeanMapper).map(sourceVehicle, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("planningVehicle", "Depot can't be null"))
                .hasCauseInstanceOf(NullPointerException.class);
    }

    @Test
    public void map_PlanningVehicleAndPlanningCustomerGiven_ShouldReturnMappedTimedDelivery() {
        PlanningVehicle sourceVehicle = PlanningVehicleBuilder
                .getBuilder(planningBeanFactory)
                .forDefaultVehicle()
                .build();
        int arrivalInUnits = TestModelData.PLANNING_ARRIVAL_TIME;
        PlanningTimeWindowedCustomer sourceCustomerMock = TestScheduledPlanningBeans
                .getScheduledTimeWindowedCustomer(TestModelData.PLANNING_LOCATION, arrivalInUnits, null);
        // Creates default arrivalTime w.r.t. origin
        DateTime defaultArrivalTime = timeTranslator.getDateTimeFromScaledShift(arrivalInUnits, ORIGIN);

        TimedDelivery delivery = timedBeanMapper.map(sourceVehicle, sourceCustomerMock, ORIGIN);

        assertNotNull(delivery);
        assertEquals(null, delivery.getId());
        assertEquals(createReferenceTimeWindowedCustomer(), delivery.getCustomer());
        assertEquals(createReferenceTimedVehicle(), delivery.getVehicle());
        assertEquals(defaultArrivalTime, delivery.getArrivalTime());
        verify(sourceCustomerMock, times(2)).getLocation();
    }

    @Test
    public void map_PlanningVehicleAndPlanningCustomerWithInvalidArrivalTimeGiven_ShouldThrowMappingException() {
        PlanningVehicle sourceVehicle = PlanningVehicleBuilder
                .getBuilder(planningBeanFactory)
                .forDefaultVehicle()
                .build();
        PlanningTimeWindowedCustomer sourceCustomerMock = TestScheduledPlanningBeans
                .getScheduledTimeWindowedCustomer(TestModelData.PLANNING_LOCATION, INVALID_ARRIVAL_TIME, null);

        catchException(timedBeanMapper).map(sourceVehicle, sourceCustomerMock, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("vehicle with customer to TimedDelivery",
                        "scheduledTime must be positive"));
    }

    @Test
    public void map_CustomerGiven_ShouldReturnMappedPlanningCustomer() {
        TimeWindowedCustomer sourceCustomer = TestModelData.createDefaultTimeWindowedCustomer();
        // Sets id since it is supposed to be generated
        sourceCustomer.setId(TestModelData.ID_1);

        PlanningTimeWindowedCustomer destinationCustomer = timedBeanMapper.map(sourceCustomer, ORIGIN);

        assertNotNull(destinationCustomer);
        assertEquals(TestModelData.ID_1, destinationCustomer.getId());
        assertEquals(TestModelData.ID_1, destinationCustomer.getLocation().getId());
        assertEquals(TestModelData.DEMAND, destinationCustomer.getDemand());
        assertEquals(TestModelData.PLANNING_LOCATION, destinationCustomer.getLocation());
        assertEquals(TestModelData.PLANING_SERVICE_DURATION, destinationCustomer.getServiceDuration());
        assertEquals(TestModelData.PLANNING_READY_TIME, destinationCustomer.getReadyTime());
        assertEquals(TestModelData.PLANNING_DUE_TIME, destinationCustomer.getDueTime());
    }

    @Test
    public void map_CustomerWithNullIdGiven_ShouldThrowMappingException() {
        TimeWindowedCustomer sourceCustomer = TestModelData.createDefaultTimeWindowedCustomer();

        catchException(timedBeanMapper).map(sourceCustomer, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("customer", "Customer id can't be null"))
                .hasCauseInstanceOf(NullPointerException.class);
    }

    @Test
    public void map_CustomerWithNullLocationGiven_ShouldThrowMappingException() {
        TimeWindowedCustomer sourceCustomer = TestModelData.createTimeWindowedCustomer(null,
                TestModelData.createDefaultTimeWindow());
        // Sets id since it is supposed to be generated
        sourceCustomer.setId(TestModelData.ID_1);

        catchException(timedBeanMapper).map(sourceCustomer, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("customer", "Customer location can't be null"))
                .hasCauseInstanceOf(NullPointerException.class);
    }

    @Test
    public void map_DepotGiven_ShouldReturnMappedPlanningDepot() {
        TimeWindowedDepot sourceDepot = TestModelData.createDefaultTimeWindowedDepot();
        // Sets id since it is supposed to be generated
        sourceDepot.setId(TestModelData.ID_1);

        PlanningTimeWindowedDepot destinationDepot = timedBeanMapper.map(sourceDepot, ORIGIN);

        assertNotNull(destinationDepot);
        assertEquals(TestModelData.ID_1, destinationDepot.getId());
        assertEquals(TestModelData.ID_1, destinationDepot.getLocation().getId());
        assertEquals(TestModelData.PLANNING_LOCATION, destinationDepot.getLocation());
        assertEquals(TestModelData.PLANNING_READY_TIME, destinationDepot.getReadyTime());
        assertEquals(TestModelData.PLANNING_DUE_TIME, destinationDepot.getDueTime());
    }

    @Test
    public void map_DepotWithNullIdGiven_ShouldThrowMappingException() {
        TimeWindowedDepot sourceDepot = TestModelData.createDefaultTimeWindowedDepot();

        catchException(timedBeanMapper).map(sourceDepot, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("depot", "Depot id can't be null"))
                .hasCauseInstanceOf(NullPointerException.class);
    }

    @Test
    public void map_DepotWithNullLocationGiven_ShouldThrowMappingException() {
        TimeWindowedDepot sourceDepot = TestModelData.createTimeWindowedDepot(null,
                TestModelData.createDefaultTimeWindow());
        // Sets id since it is supposed to be generated
        sourceDepot.setId(TestModelData.ID_1);

        catchException(timedBeanMapper).map(sourceDepot, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("depot", "Depot location can't be null"))
                .hasCauseInstanceOf(NullPointerException.class);
    }

    @Test
    public void map_VehicleGiven_ShouldReturnMappedPlanningVehicleWithDepot() {
        TimedVehicle sourceVehicle = TestModelData.createDefaultTimedVehicle();
        // Sets id since it is supposed to be generated
        sourceVehicle.setId(TestModelData.ID_1);
        sourceVehicle.getDepot().setId(TestModelData.ID_1);

        PlanningVehicle destinationVehicle = timedBeanMapper.map(sourceVehicle, ORIGIN);

        assertNotNull(destinationVehicle);
        assertEquals(TestModelData.ID_1, destinationVehicle.getId());
        assertEquals(TestModelData.CAPACITY, destinationVehicle.getCapacity());
        assertEquals(TestModelData.LICENSE_PLATE, destinationVehicle.getLicensePlate());
    }

    @Test
    public void map_VehicleWithNullIdGiven_ShouldThrowMappingException() {
        TimedVehicle sourceVehicle = TestModelData.createDefaultTimedVehicle();

        catchException(timedBeanMapper).map(sourceVehicle, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("vehicle", "Vehicle id can't be null"))
                .hasCauseInstanceOf(NullPointerException.class);
    }

    @Test
    public void map_VehicleWithNullLicensePlateGiven_ShouldThrowMappingException() {
        TimedVehicle sourceVehicle = TestModelData.createDefaultTimedVehicle();
        // Sets id since it is supposed to be generated
        sourceVehicle.setId(TestModelData.ID_1);
        sourceVehicle.getDepot().setId(TestModelData.ID_2);
        sourceVehicle.setLicensePlate(null);

        catchException(timedBeanMapper).map(sourceVehicle, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("vehicle", "Vehicle licensePlate can't be null"))
                .hasCauseInstanceOf(NullPointerException.class);
    }

    @Test
    public void map_VehicleWithNullDepotGiven_ShouldThrowMappingException() {
        TimedVehicle sourceVehicle = TestModelData.createDefaultTimedVehicle();
        // Sets id since it is supposed to be generated
        sourceVehicle.setId(TestModelData.ID_1);
        sourceVehicle.setDepot(null);

        catchException(timedBeanMapper).map(sourceVehicle, ORIGIN);

        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getExceptionMessage("vehicle", "Vehicle's depot reference can't be null"))
                .hasCauseInstanceOf(NullPointerException.class);
    }

    private TimeWindowedDepot createReferenceTimeWindowedDepot() {
        return TestModelData.createDefaultTimeWindowedDepot();
    }

    private TimeWindowedCustomer createReferenceTimeWindowedCustomer() {
        return TestModelData.createDefaultTimeWindowedCustomer();
    }

    private TimedVehicle createReferenceTimedVehicle() {
        return TestModelData.createDefaultTimedVehicle();
    }

}
