package org.optaplanner.showcase.app.vehiclerouting.web.timewindowed;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import org.optaplanner.showcase.app.vehiclerouting.event.modification.timewindowed.ProblemModificationEventSupport;
import org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException;
import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindow;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.service.persistence.timewindowed.TimeWindowedPersistenceService;
import org.optaplanner.showcase.app.vehiclerouting.testdata.ArbitraryTimeStamp;
import org.optaplanner.showcase.app.vehiclerouting.testdata.TestModelData;
import org.optaplanner.showcase.app.vehiclerouting.web.common.ErrorController;
import org.optaplanner.showcase.app.vehiclerouting.web.util.WebUtil;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/test-timewindowed-context-config.xml",
        "classpath:spring/test-mvc-config.xml"})
@WebAppConfiguration
@ActiveProfiles("timewindowed")
public class TimeWindowedCustomerControllerTest {

    public static final long ID = 1L;
    public static final long ID2 = 2L;
    public static final int MINUTES_LATER = 90;
    public static final String MODEL_ATTRIBUTE_CUSTOMER = TimeWindowedCustomerController.MODEL_ATTRIBUTE_CUSTOMER;

    public static final String LOCATION_PROPERTY = "location";
    public static final String DEMAND_PROPERTY = "demand";
    public static final String DURATION_PROPERTY = "serviceDuration";
    public static final String WINDOW_PROPERTY = "timeWindow";
    public static final String ID_PROPERTY = "id";

    public static final String CUSTOMERS_REQUEST = TimeWindowedCustomerController.CUSTOMERS_REQUEST;
    public static final String CUSTOMER_REQUEST = "/customers/{customerId}";
    public static final String CUSTOMER_EDIT_REQUEST = "/customers/{customerId}/edit";
    public static final String CUSTOMER_DELETE_REQUEST = "/customers/{customerId}/delete";

    private MockMvc mockMvc;

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TimeWindowedPersistenceService persistenceServiceMock;
    @Autowired
    private ProblemModificationEventSupport problemModificationEventSupportMock;

    @Before
    public void setUp() {
        Mockito.reset(persistenceServiceMock, problemModificationEventSupportMock);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    public String getMessage(String message) {
        return messageSource.getMessage(message, new Object[]{}, LocaleContextHolder.getLocale());
    }

    @Test
    public void listAllCustomers_AllCustomersFound_ShouldAddCustomersToModelAndReturnCustomerListView() throws Exception {
        TimeWindowedCustomer customerA = TestModelData.createDefaultTimeWindowedCustomerA();
        TimeWindowedCustomer customerB = TestModelData.createDefaultTimeWindowedCustomerB();

        when(persistenceServiceMock.getCustomers()).thenReturn(Arrays.asList(customerA, customerB));

        mockMvc.perform(get("/customers"))
                .andExpect(status().isOk())
                .andExpect(view().name(TimeWindowedCustomerController.VIEW_CUSTOMER_LIST))
                .andExpect(forwardedUrl("/WEB-INF/jsp/customers/list.jsp"))
                .andExpect(model().attribute(TimeWindowedCustomerController.MODEL_ATTRIBUTE_CUSTOMER_LIST, hasSize(2)))
                .andExpect(model().attribute(TimeWindowedCustomerController.MODEL_ATTRIBUTE_CUSTOMER_LIST, hasItem(
                        allOf(
                                hasProperty(DEMAND_PROPERTY, is(TestModelData.DEMAND)),
                                hasProperty(LOCATION_PROPERTY, is(TestModelData.createDefaultLocationA()))
                        )
                )))
                .andExpect(model().attribute(TimeWindowedCustomerController.MODEL_ATTRIBUTE_CUSTOMER_LIST, hasItem(
                        allOf(
                                hasProperty(DEMAND_PROPERTY, is(TestModelData.DEMAND)),
                                hasProperty(LOCATION_PROPERTY, is(TestModelData.createDefaultLocationB()))
                        )
                )));

        verify(persistenceServiceMock, times(1)).getCustomers();
        verifyNoMoreInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void viewCustomerDetails_CustomerFound_ShouldAddCustomerToModelAndReturnCustomerDetailsView() throws Exception {
        TimeWindowedCustomer customer = TestModelData.createDefaultTimeWindowedCustomer();

        when(persistenceServiceMock.getCustomer(ID)).thenReturn(customer);

        mockMvc.perform(get(CUSTOMER_REQUEST, ID))
                .andExpect(status().isOk())
                .andExpect(view().name(TimeWindowedCustomerController.VIEW_CUSTOMER_DETAILS))
                .andExpect(forwardedUrl("/WEB-INF/jsp/customers/details.jsp"))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(DEMAND_PROPERTY, is(TestModelData.DEMAND))))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(LOCATION_PROPERTY,
                        is(TestModelData.createDefaultLocationA()))));

        verify(persistenceServiceMock, times(1)).getCustomer(ID);
        verifyNoMoreInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void viewCustomerDetails_CustomerNotFound_ShouldReturn404View() throws Exception {
        when(persistenceServiceMock.getCustomer(ID)).thenThrow(new ResourceNotFoundException());

        mockMvc.perform(get(CUSTOMER_REQUEST, ID))
                .andExpect(status().isNotFound())
                .andExpect(view().name(ErrorController.VIEW_NOT_FOUND))
                .andExpect(forwardedUrl("/WEB-INF/jsp/error/status404.jsp"));

        verify(persistenceServiceMock, times(1)).getCustomer(ID);
        verifyNoMoreInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void displayCustomerEditForm_CustomerPlacedInSession_ShouldAddCustomerToModelAndReturnEditForm()
            throws Exception {
        TimeWindowedCustomer customer = TestModelData.createDefaultTimeWindowedCustomer();
        customer.setId(ID);

        mockMvc.perform(get(CUSTOMER_EDIT_REQUEST, ID)
                .sessionAttr(MODEL_ATTRIBUTE_CUSTOMER, customer))
                .andExpect(status().isOk())
                .andExpect(view().name(TimeWindowedCustomerController.VIEW_CUSTOMER_ADD_OR_EDIT_FORM))
                .andExpect(forwardedUrl("/WEB-INF/jsp/customers/addOrEditForm.jsp"))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(ID_PROPERTY, is(ID))))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(DEMAND_PROPERTY, is(TestModelData.DEMAND))))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(LOCATION_PROPERTY,
                        is(TestModelData.createDefaultLocationA()))));

        verifyZeroInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void displayCustomerEditForm_CustomerNotPlacedInSessionCustomerFound_ShouldAddCustomerToModelAndReturnEditForm()
            throws Exception {
        TimeWindowedCustomer customer = TestModelData.createDefaultTimeWindowedCustomer();
        customer.setId(ID);
        TimeWindowedCustomer customerInSession = TestModelData.createDefaultTimeWindowedCustomer();
        customerInSession.setId(ID2);

        when(persistenceServiceMock.getCustomer(ID)).thenReturn(customer);

        mockMvc.perform(get(CUSTOMER_EDIT_REQUEST, ID)
                .sessionAttr(MODEL_ATTRIBUTE_CUSTOMER, customerInSession))
                .andExpect(status().isOk())
                .andExpect(view().name(TimeWindowedCustomerController.VIEW_CUSTOMER_ADD_OR_EDIT_FORM))
                .andExpect(forwardedUrl("/WEB-INF/jsp/customers/addOrEditForm.jsp"))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(ID_PROPERTY, is(ID))))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(DEMAND_PROPERTY, is(TestModelData.DEMAND))))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(LOCATION_PROPERTY,
                        is(TestModelData.createDefaultLocationA()))));

        verify(persistenceServiceMock, times(1)).getCustomer(ID);
        verifyNoMoreInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void displayCustomerEditForm_CustomerNotPlacedInSessionCustomerNotFound_ShouldReturn404View() throws Exception {
        TimeWindowedCustomer customerInSession = TestModelData.createDefaultTimeWindowedCustomer();
        customerInSession.setId(ID2);

        when(persistenceServiceMock.getCustomer(ID)).thenThrow(new ResourceNotFoundException());

        mockMvc.perform(get(CUSTOMER_EDIT_REQUEST, ID)
                .sessionAttr(MODEL_ATTRIBUTE_CUSTOMER, customerInSession))
                .andExpect(status().isNotFound())
                .andExpect(view().name(ErrorController.VIEW_NOT_FOUND))
                .andExpect(forwardedUrl("/WEB-INF/jsp/error/status404.jsp"));

        verify(persistenceServiceMock, times(1)).getCustomer(ID);
        verifyNoMoreInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void processCustomerEditForm_ValidCustomerGiven_ShouldUpdateCustomerAndFireUpdateEventAndRedirect() throws
            Exception {
        TimeWindowedCustomer updatedCustomer = TestModelData.createDefaultTimeWindowedCustomer();
        updatedCustomer.setId(ID);

        when(persistenceServiceMock.saveCustomer(isA(TimeWindowedCustomer.class))).thenReturn(updatedCustomer);

        mockMvc.perform(post(CUSTOMER_EDIT_REQUEST, ID)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param(ID_PROPERTY, String.valueOf(ID))
                .param(LOCATION_PROPERTY, TestModelData.LOCATION.toString())
                .param(DEMAND_PROPERTY, String.valueOf(TestModelData.DEMAND))
                .param(DURATION_PROPERTY, String.valueOf(TestModelData.SERVICE_DURATION))
                .param(WINDOW_PROPERTY, TestModelData.createDefaultTimeWindow().toString())
                .sessionAttr(MODEL_ATTRIBUTE_CUSTOMER, updatedCustomer))
                .andExpect(status().isFound())
                .andExpect(view().name(WebUtil.getRedirectedURL(CUSTOMERS_REQUEST, "{customerId}")))
                .andExpect(flash().attribute(TimeWindowedCustomerController.FLASH_MESSAGE_KEY_FEEDBACK,
                        is(getMessage(TimeWindowedCustomerController.FEEDBACK_MESSAGE_KEY_CUSTOMER_UPDATED))));

        ArgumentCaptor<TimeWindowedCustomer> formCustomerObject = ArgumentCaptor.forClass(TimeWindowedCustomer.class);
        verify(persistenceServiceMock, times(1)).saveCustomer(formCustomerObject.capture());
        TimeWindowedCustomer returnedUpdatedCustomer = formCustomerObject.getValue();

        verifyNoMoreInteractions(persistenceServiceMock);
        verify(problemModificationEventSupportMock, times(1)).fireModificationEnded(null);
        verifyNoMoreInteractions(problemModificationEventSupportMock);
        assertEquals(TestModelData.LOCATION, returnedUpdatedCustomer.getLocation());
        assertEquals(TestModelData.DEMAND, returnedUpdatedCustomer.getDemand().intValue());
        assertEquals(TestModelData.SERVICE_DURATION, returnedUpdatedCustomer.getServiceDuration().intValue());
        // For now not checking time window equality because org.joda.time.DateTime does not equal itself after deserialization
        // assertEquals(TestModelData.createDefaultTimeWindow(), returnedUpdatedCustomer.getTimeWindow());
        assertEquals(updatedCustomer, returnedUpdatedCustomer);
    }

    @Test
    public void processCustomerEditForm_CustomerWithNullDemandGiven_ShouldContainValidationErrorAndForwardToForm()
            throws Exception {
        TimeWindowedCustomer updatedCustomer = TestModelData.createDefaultTimeWindowedCustomer();
        updatedCustomer.setId(ID);
        updatedCustomer.setDemand(null);

        mockMvc.perform(post(CUSTOMER_EDIT_REQUEST, ID)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param(ID_PROPERTY, String.valueOf(ID))
                .sessionAttr(MODEL_ATTRIBUTE_CUSTOMER, updatedCustomer))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("/WEB-INF/jsp/" + TimeWindowedCustomerController.VIEW_CUSTOMER_ADD_OR_EDIT_FORM
                        + ".jsp"))
                .andExpect(model().attributeHasFieldErrors(MODEL_ATTRIBUTE_CUSTOMER, DEMAND_PROPERTY))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(ID_PROPERTY, is(ID))));

        verifyZeroInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void processCustomerEditForm_CustomerWithZeroDemandGiven_ShouldContainValidationErrorAndForwardToForm()
            throws Exception {
        int zeroDemand = 0;
        TimeWindowedCustomer updatedCustomer = TestModelData.createDefaultTimeWindowedCustomer();
        updatedCustomer.setId(ID);
        updatedCustomer.setDemand(zeroDemand);

        mockMvc.perform(post(CUSTOMER_EDIT_REQUEST, ID)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param(ID_PROPERTY, String.valueOf(ID))
                .param(LOCATION_PROPERTY, TestModelData.LOCATION.toString())
                .param(DEMAND_PROPERTY, String.valueOf(zeroDemand))
                .param(DURATION_PROPERTY, String.valueOf(TestModelData.SERVICE_DURATION))
                .param(WINDOW_PROPERTY, TestModelData.createDefaultTimeWindow().toString())
                .sessionAttr(MODEL_ATTRIBUTE_CUSTOMER, updatedCustomer))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("/WEB-INF/jsp/" + TimeWindowedCustomerController.VIEW_CUSTOMER_ADD_OR_EDIT_FORM
                        + ".jsp"))
                .andExpect(model().attributeHasFieldErrors(MODEL_ATTRIBUTE_CUSTOMER, DEMAND_PROPERTY))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(ID_PROPERTY, is(ID))));

        verifyZeroInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void processCustomerEditForm_CustomerWithZeroServiceDurationGiven_ShouldContainValidationErrorAndForwardToForm()
            throws Exception {
        int zeroDuration = 0;
        TimeWindowedCustomer updatedCustomer = TestModelData.createDefaultTimeWindowedCustomer();
        updatedCustomer.setId(ID);
        updatedCustomer.setServiceDuration(zeroDuration);

        mockMvc.perform(post(CUSTOMER_EDIT_REQUEST, ID)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param(ID_PROPERTY, String.valueOf(ID))
                .param(LOCATION_PROPERTY, TestModelData.LOCATION.toString())
                .param(DEMAND_PROPERTY, String.valueOf(TestModelData.DEMAND))
                .param(DURATION_PROPERTY, String.valueOf(zeroDuration))
                .param(WINDOW_PROPERTY, TestModelData.createDefaultTimeWindow().toString())
                .sessionAttr(MODEL_ATTRIBUTE_CUSTOMER, updatedCustomer))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("/WEB-INF/jsp/" + TimeWindowedCustomerController.VIEW_CUSTOMER_ADD_OR_EDIT_FORM
                        + ".jsp"))
                .andExpect(model().attributeHasFieldErrors(MODEL_ATTRIBUTE_CUSTOMER, DURATION_PROPERTY))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(ID_PROPERTY, is(ID))));

        verifyZeroInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void processCustomerEditForm_CustomerReadyTimeAfterDueTimeGiven_ShouldContainValidationErrorAndForwardToForm()
            throws Exception {
        DateTime dueTime = ArbitraryTimeStamp.getDateTime();
        DateTime readyTime = dueTime.plusMinutes(MINUTES_LATER);
        TimeWindowedCustomer updatedCustomer = TestModelData.createDefaultTimeWindowedCustomer();
        updatedCustomer.setId(ID);

        mockMvc.perform(post(CUSTOMER_EDIT_REQUEST, ID)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param(ID_PROPERTY, String.valueOf(ID))
                .param(LOCATION_PROPERTY, TestModelData.LOCATION.toString())
                .param(DEMAND_PROPERTY, String.valueOf(TestModelData.DEMAND))
                .param(DURATION_PROPERTY, String.valueOf(TestModelData.SERVICE_DURATION))
                .param(WINDOW_PROPERTY, new TimeWindow(readyTime, dueTime).toString())
                .sessionAttr(MODEL_ATTRIBUTE_CUSTOMER, updatedCustomer))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("/WEB-INF/jsp/" + TimeWindowedCustomerController.VIEW_CUSTOMER_ADD_OR_EDIT_FORM
                        + ".jsp"))
                .andExpect(model().attributeHasFieldErrors(MODEL_ATTRIBUTE_CUSTOMER, "timeWindow.dueTime"))
                .andExpect(model().attributeHasFieldErrors(MODEL_ATTRIBUTE_CUSTOMER, "timeWindow.readyTime"))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(ID_PROPERTY, is(ID))));

        verifyZeroInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void displayCustomerAddForm_ShouldAddNewCustomerToModelAndReturnEditForm() throws Exception {
        Location emptyLocation = Location.getBuilder()
                .withLongitude(0)
                .withLatitude(0)
                .withName("")
                .build();

        mockMvc.perform(get("/customers/add"))
                .andExpect(status().isOk())
                .andExpect(view().name(TimeWindowedCustomerController.VIEW_CUSTOMER_ADD_OR_EDIT_FORM))
                .andExpect(forwardedUrl("/WEB-INF/jsp/customers/addOrEditForm.jsp"))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(ID_PROPERTY, nullValue())))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(DEMAND_PROPERTY, nullValue())))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(LOCATION_PROPERTY,
                        is(emptyLocation))));

        verifyZeroInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void processCustomerAddForm_ValidCustomerGiven_ShouldAddCustomerAndFireAddEventAndRedirect() throws Exception {
        TimeWindowedCustomer addedCustomer = TestModelData.createDefaultTimeWindowedCustomer();
        addedCustomer.setId(ID);

        when(persistenceServiceMock.saveCustomer(isA(TimeWindowedCustomer.class))).thenReturn(addedCustomer);

        mockMvc.perform(post("/customers/add")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param(ID_PROPERTY, String.valueOf(ID))
                .param(LOCATION_PROPERTY, TestModelData.LOCATION.toString())
                .param(DEMAND_PROPERTY, String.valueOf(TestModelData.DEMAND))
                .param(DURATION_PROPERTY, String.valueOf(TestModelData.SERVICE_DURATION))
                .param(WINDOW_PROPERTY, TestModelData.createDefaultTimeWindow().toString())
                .sessionAttr(MODEL_ATTRIBUTE_CUSTOMER, addedCustomer))
                .andExpect(status().isFound())
                .andExpect(view().name(WebUtil.getRedirectedURL(CUSTOMERS_REQUEST,
                        String.valueOf(addedCustomer.getId()))))
                .andExpect(flash().attribute(TimeWindowedCustomerController.FLASH_MESSAGE_KEY_FEEDBACK,
                        is(getMessage(TimeWindowedCustomerController.FEEDBACK_MESSAGE_KEY_CUSTOMER_ADDED))));

        ArgumentCaptor<TimeWindowedCustomer> formCustomerObject = ArgumentCaptor.forClass(TimeWindowedCustomer.class);
        verify(persistenceServiceMock, times(1)).saveCustomer(formCustomerObject.capture());
        TimeWindowedCustomer returnedAddedCustomer = formCustomerObject.getValue();

        verifyNoMoreInteractions(persistenceServiceMock);
        verify(problemModificationEventSupportMock, times(1)).fireModificationEnded(null);
        verifyNoMoreInteractions(problemModificationEventSupportMock);
        assertEquals(TestModelData.LOCATION, returnedAddedCustomer.getLocation());
        assertEquals(TestModelData.DEMAND, returnedAddedCustomer.getDemand().intValue());
        assertEquals(TestModelData.SERVICE_DURATION, returnedAddedCustomer.getServiceDuration().intValue());
        // For now not checking time window equality because org.joda.time.DateTime does not equal itself after deserialization
        // assertEquals(TestModelData.createDefaultTimeWindow(), returnedAddedCustomer.getTimeWindow());
        assertEquals(addedCustomer, returnedAddedCustomer);
    }

    @Test
    public void processCustomerAddForm_CustomerWithReadyTimeAfterDueTimeGiven_ShouldContainValidationErrorAndForwardToForm()
            throws Exception {
        DateTime dueTime = ArbitraryTimeStamp.getDateTime();
        DateTime readyTime = dueTime.plusMinutes(MINUTES_LATER);
        TimeWindowedCustomer updatedCustomer = TestModelData.createDefaultTimeWindowedCustomer();
        updatedCustomer.setId(ID);

        mockMvc.perform(post("/customers/add")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param(ID_PROPERTY, String.valueOf(ID))
                .param(LOCATION_PROPERTY, TestModelData.LOCATION.toString())
                .param(DEMAND_PROPERTY, String.valueOf(TestModelData.DEMAND))
                .param(DURATION_PROPERTY, String.valueOf(TestModelData.SERVICE_DURATION))
                .param(WINDOW_PROPERTY, new TimeWindow(readyTime, dueTime).toString())
                .sessionAttr(MODEL_ATTRIBUTE_CUSTOMER, updatedCustomer))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("/WEB-INF/jsp/" + TimeWindowedCustomerController.VIEW_CUSTOMER_ADD_OR_EDIT_FORM
                        + ".jsp"))
                .andExpect(model().attributeHasFieldErrors(MODEL_ATTRIBUTE_CUSTOMER, "timeWindow.dueTime"))
                .andExpect(model().attributeHasFieldErrors(MODEL_ATTRIBUTE_CUSTOMER, "timeWindow.readyTime"))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_CUSTOMER, hasProperty(ID_PROPERTY, is(ID))));

        verifyZeroInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    @Ignore
    public void deleteCustomerById_CustomerFound_ShouldDeleteCustomerAndFireDeleteEventAndRedirect() throws Exception {
        doNothing().when(persistenceServiceMock).deleteCustomer(ID);

        mockMvc.perform(get(CUSTOMER_DELETE_REQUEST, ID))
                .andExpect(status().isOk())
                .andExpect(view().name(TimeWindowedCustomerController.VIEW_CUSTOMER_LIST))
                .andExpect(forwardedUrl("/WEB-INF/jsp/customers/list.jsp"))
                .andExpect(flash().attribute(TimeWindowedCustomerController.FLASH_MESSAGE_KEY_FEEDBACK,
                        is(getMessage(TimeWindowedCustomerController.FEEDBACK_MESSAGE_KEY_CUSTOMER_DELETED))));

        verify(persistenceServiceMock, times(1)).getCustomer(ID);
        verifyNoMoreInteractions(persistenceServiceMock);
        verify(problemModificationEventSupportMock, times(1)).fireModificationEnded(null);
        verifyNoMoreInteractions(problemModificationEventSupportMock);
    }

    @Test
    @Ignore
    public void deleteCustomerById_CustomerNotFound_ShouldReturn404View() throws Exception {
        when(persistenceServiceMock.getCustomer(ID)).thenThrow(new ResourceNotFoundException());

        mockMvc.perform(get(CUSTOMER_DELETE_REQUEST, ID))
                .andExpect(status().isNotFound())
                .andExpect(view().name(ErrorController.VIEW_NOT_FOUND))
                .andExpect(forwardedUrl("/WEB-INF/jsp/error/status404.jsp"));

        verify(persistenceServiceMock, times(1)).getCustomer(ID);
        verifyNoMoreInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

}
