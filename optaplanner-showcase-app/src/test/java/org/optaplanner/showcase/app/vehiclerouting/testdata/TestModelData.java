package org.optaplanner.showcase.app.vehiclerouting.testdata;

import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindow;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.util.datetime.PlanningUnitsToDateTimeTranslator;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningDepot;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningBeanFactory;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.Depot;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.timewindowed.DefaultPlanningBeanFactory;

import org.joda.time.DateTime;

/**
 * Object Mother for holding cross-test constants and creating sample model objects.
 */
public final class TestModelData {

    public static final Long ID_1 = 1L;
    public static final Long ID_2 = 2L;

    public static final double LATITUDE_A = 50.55;
    public static final double LONGITUDE_A = 3.37;
    public static final String CITY_NAME_A = "GUIGNIES";

    public static final double LATITUDE_B = 50.62;
    public static final double LONGITUDE_B = 3.91;

    public static final int DEMAND = 100;
    public static final int SERVICE_DURATION = 250;
    public static final int READY_TIME = 1200;
    public static final int DUE_TIME = 1350;
    public static final int ARRIVAL_TIME = 1300;

    public static final int CAPACITY = 200;
    public static final String LICENSE_PLATE = "XXX-000";

    public static final int PLANING_SERVICE_DURATION = getScaledTimeUnits(SERVICE_DURATION);
    public static final int PLANNING_DUE_TIME = getScaledTimeUnits(DUE_TIME);
    public static final int PLANNING_READY_TIME = getScaledTimeUnits(READY_TIME);
    public static final int PLANNING_ARRIVAL_TIME = getScaledTimeUnits(ARRIVAL_TIME);
    public static final PlanningBeanFactory PLANNING_BEAN_FACTORY = DefaultPlanningBeanFactory.getInstanceForAirDistance();
    public static final PlanningLocation PLANNING_LOCATION = TestModelData.createDefaultPlanningLocationA();

    public static final DateTime ORIGIN = ArbitraryTimeStamp.getDateTime();
    public static final Location LOCATION = TestModelData.createDefaultLocationA();

    private TestModelData() {
    }

    public static int getScaledTimeUnits(int units) {
        return PlanningUnitsToDateTimeTranslator.getScaledTimeUnitsFromDuration(units);
    }

    public static PlanningBeanFactory getPlanningBeanFactory() {
        return PLANNING_BEAN_FACTORY;
    }

    public static TimeWindow createDefaultTimeWindow() {
        return createTimeWindow(READY_TIME, DUE_TIME, ORIGIN);
    }

    public static TimeWindow createTimeWindow(int readyTime, int dueTime, DateTime origin) {
        PlanningUnitsToDateTimeTranslator timeTranslator = new PlanningUnitsToDateTimeTranslator();
        return new TimeWindow(timeTranslator.getDateTimeFromShift(readyTime, origin),
                timeTranslator.getDateTimeFromShift(dueTime, origin));
    }

    // We don't use location-specific builder here to avoid having distance type dependency scattered all over the place

    public static PlanningLocation createDefaultPlanningLocationA() {
        PlanningLocation location = PLANNING_BEAN_FACTORY.createUniqueLocation(CITY_NAME_A, LATITUDE_A, LONGITUDE_A);
        location.setId(ID_1);
        return location;
    }

    public static PlanningLocation createDefaultPlanningLocationB() {
        PlanningLocation location = PLANNING_BEAN_FACTORY.createUniqueLocation("", LATITUDE_B, LONGITUDE_B);
        location.setId(ID_2);
        return location;
    }

    public static PlanningTimeWindowedCustomer createDefaultPlanningTimeWindowedCustomerA() {
        return PlanningTimeWindowedCustomerBuilder
                .getBuilder(PLANNING_BEAN_FACTORY)
                .forDefaultCustomer()
                .build();
    }

    public static PlanningTimeWindowedCustomer createDefaultPlanningTimeWindowedCustomerB() {
        return PlanningTimeWindowedCustomerBuilder
                .getBuilder(PLANNING_BEAN_FACTORY)
                .forDefaultCustomer()
                .withLocation(createDefaultPlanningLocationB())
                .build();
    }

    public static PlanningDepot createDummyPlanningDepot() {
        return new Depot();
    }

    public static PlanningTimeWindowedDepot createDefaultPlanningTimeWindowedDepot() {
        return PlanningTimeWindowedDepotBuilder
                .getBuilder(PLANNING_BEAN_FACTORY)
                .forDefaultDepot()
                .build();
    }

    public static PlanningVehicle createDefaultPlanningVehicle() {
        return createPlanningVehicle(LICENSE_PLATE);
    }

    public static PlanningVehicle createPlanningVehicle(String licensePlate) {
        return PlanningVehicleBuilder
                .getBuilder(PLANNING_BEAN_FACTORY)
                .forDefaultVehicle()
                .withLicensePlate(licensePlate)
                .build();
    }

    public static Location createDefaultLocationA() {
        return Location.getBuilder()
                .withLatitude(LATITUDE_A)
                .withLongitude(LONGITUDE_A)
                .withName(CITY_NAME_A)
                .build();
    }

    public static Location createDefaultLocationB() {
        return Location.getBuilder()
                .withLatitude(LATITUDE_B)
                .withLongitude(LONGITUDE_B)
                .build();
    }

    public static TimeWindowedCustomer createDefaultTimeWindowedCustomer() {
        return createTimeWindowedCustomer(LOCATION, createDefaultTimeWindow());
    }

    public static TimeWindowedCustomer createTimeWindowedCustomer(Location location, TimeWindow timeWindow) {
        return TimeWindowedCustomer.getBuilder()
                .withLocation(location)
                .withDemand(DEMAND)
                .withServiceDuration(SERVICE_DURATION)
                .withTimeWindow(timeWindow)
                .build();
    }

    public static TimeWindowedCustomer createDefaultTimeWindowedCustomerA() {
        return createDefaultTimeWindowedCustomer();
    }

    public static TimeWindowedCustomer createDefaultTimeWindowedCustomerB() {
        return createTimeWindowedCustomer(createDefaultLocationB(), createDefaultTimeWindow());
    }

    public static TimeWindowedDepot createDefaultTimeWindowedDepot() {
        return createTimeWindowedDepot(LOCATION, createDefaultTimeWindow());
    }

    public static TimeWindowedDepot createTimeWindowedDepot(Location location, TimeWindow timeWindow) {
        return TimeWindowedDepot.getBuilder()
                .withLocation(location)
                .withTimeWindow(timeWindow)
                .build();
    }

    public static TimeWindowedDepot createDefaultTimeWindowedDepotA() {
        return createDefaultTimeWindowedDepot();
    }

    public static TimeWindowedDepot createDefaultTimeWindowedDepotB() {
        return createTimeWindowedDepot(createDefaultLocationB(), createDefaultTimeWindow());
    }

    public static TimedVehicle createDefaultTimedVehicle() {
        return createTimedVehicle(LICENSE_PLATE, createDefaultTimeWindowedDepotA());
    }

    public static TimedVehicle createTimedVehicle(String licensePlate) {
        return createTimedVehicle(licensePlate, createDefaultTimeWindowedDepotA());
    }

    public static TimedVehicle createTimedVehicle(String licensePlate, TimeWindowedDepot depot) {
        return TimedVehicle.getBuilder()
                .withCapacity(CAPACITY)
                .withLicensePlate(licensePlate)
                .withDepot(depot)
                .build();
    }

}
