package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.validation;

import org.joda.time.DateTime;

@ReadyTimeNotAfterDueTime(
        readyTimeFieldName = "readyTime",
        dueTimeFieldName = "dueTime"
)
public class TimeWindowDTO {

    private DateTime readyTime;

    private DateTime dueTime;

    public static Builder getBuilder() {
        return new Builder();
    }

    public static class Builder {

        private TimeWindowDTO built;

        public Builder() {
            built = new TimeWindowDTO();
        }

        public TimeWindowDTO build() {
            return built;
        }

        public Builder withReadyTime(DateTime readyTime) {
            built.readyTime = readyTime;
            return this;
        }

        public Builder withDueTime(DateTime dueTime) {
            built.dueTime = dueTime;
            return this;
        }
    }

}
