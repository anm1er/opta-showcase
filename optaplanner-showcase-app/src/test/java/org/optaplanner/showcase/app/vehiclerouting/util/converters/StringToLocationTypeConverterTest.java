package org.optaplanner.showcase.app.vehiclerouting.util.converters;

import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.testdata.TestModelData;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringToLocationTypeConverterTest {

    private StringToLocationTypeConverter converter;

    @Before
    public void setUp() {
        converter = new StringToLocationTypeConverter();
    }

    @Test
    public void convert_ValidLocationStringGiven_ShouldReturnLocation() {
        Location location = TestModelData.LOCATION;
        String locationString = location.toString();

        Location convertedLocation = converter.convert(locationString);

        assertEquals(location, convertedLocation);
    }

}
