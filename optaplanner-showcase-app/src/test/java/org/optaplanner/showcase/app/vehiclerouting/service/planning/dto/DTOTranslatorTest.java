package org.optaplanner.showcase.app.vehiclerouting.service.planning.dto;

import java.util.List;

import org.optaplanner.showcase.app.vehiclerouting.testdata.TestSolutionGeneratorUtility;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DTOTranslatorTest {

    /**
     * Some hard-coded values for Solomon_025_C101.
     */
    public static final int CUSTOMERS_NUMBER = 25;
    public static final int VEHICLES_NUMBER = 25;
    public static final int SCHEDULED_VEHICLE_IDX_1 = 0;
    public static final int SCHEDULED_VEHICLE_IDX_2 = 5;
    public static final int SCHEDULED_VEHICLE_IDX_3 = 8;
    public static final int DELIVERIES_FOR_VEHICLE_1 = 6;
    public static final int DELIVERIES_FOR_VEHICLE_2 = 8;
    public static final int DELIVERIES_FOR_VEHICLE_3 = 11;

    // Note: The way AbstractSolutionDao is currently implemented requires that data directory is explicitly set
    @Before
    public void setUp() {
        TestSolutionGeneratorUtility.setUpDataDirProperty();
    }

    @Test
    public void getVehicleRoutingSolutionDTO_SolutionForSolomon_025_C101_Minus191815SoftGiven_ShouldReturnSolutionDTO()
            throws Exception {
        VehicleRoutingSolution solution = TestSolutionGeneratorUtility.getSolvedSolomon_025_C101();

        VehicleRoutingSolutionDTO solutionDTO = DTOTranslator.getVehicleRoutingSolutionDTO(solution);
        List<VehicleDTO> vehicleDTOs = solutionDTO.getVehicleDTOs();

        assertNotNull(solutionDTO);
        assertEquals(CUSTOMERS_NUMBER, solutionDTO.getCustomerDTOs().size());
        assertEquals(VEHICLES_NUMBER, vehicleDTOs.size());
        assertEquals(DELIVERIES_FOR_VEHICLE_1, vehicleDTOs.get(SCHEDULED_VEHICLE_IDX_1).getCustomerDTOs().size());
        assertEquals(DELIVERIES_FOR_VEHICLE_2, vehicleDTOs.get(SCHEDULED_VEHICLE_IDX_2).getCustomerDTOs().size());
        assertEquals(DELIVERIES_FOR_VEHICLE_3, vehicleDTOs.get(SCHEDULED_VEHICLE_IDX_3).getCustomerDTOs().size());
    }

    // todo: more tests needed

}
