package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.validation;

import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.validation.ReadyNotAfterDueAssert.assertThat;

public class ReadyTimeNotAfterDueTimeValidatorTest {

    public static final DateTime BEFORE_DATE_TIME = new DateTime("2014-01-01T00:00:00.801+04:00");
    public static final DateTime DATE_TIME = new DateTime("2014-02-01T00:00:00.801+04:00");


    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void readyTimeNotAfterDueTime_BothDateTimesAreNull_ShouldPassValidation() {
        TimeWindowDTO passesValidation = TimeWindowDTO.getBuilder().build();

        assertThat(validator.validate(passesValidation)).hasNoValidationErrors();
    }


    @Test
    public void readyTimeNotAfterDueTime_readyTimeIsNull_ShouldPassValidation() {
        TimeWindowDTO passesValidation = TimeWindowDTO.getBuilder()
                .withDueTime(DATE_TIME)
                .build();

        assertThat(validator.validate(passesValidation)).hasNoValidationErrors();
    }

    @Test
    public void readyTimeNotAfterDueTime_dueTimeIsNull_ShouldPassValidation() {
        TimeWindowDTO passesValidation = TimeWindowDTO.getBuilder()
                .withReadyTime(DATE_TIME)
                .build();

        assertThat(validator.validate(passesValidation)).hasNoValidationErrors();
    }

    @Test
    public void readyTimeNotAfterDueTime_readyTimeBeforeDueTime_ShouldPassValidation() {
        TimeWindowDTO passesValidation = TimeWindowDTO.getBuilder()
                .withReadyTime(BEFORE_DATE_TIME)
                .withDueTime(DATE_TIME)
                .build();

        assertThat(validator.validate(passesValidation)).hasNoValidationErrors();
    }

    @Test
    public void readyTimeNotAfterDueTime_readyTimeSameAsDueTime_ShouldPassValidation() {
        TimeWindowDTO passesValidation = TimeWindowDTO.getBuilder()
                .withReadyTime(DATE_TIME)
                .withDueTime(DATE_TIME)
                .build();

        assertThat(validator.validate(passesValidation)).hasNoValidationErrors();
    }

    @Test
    public void readyTimeNotAfterDueTime_readyTimeAfterDueTime_ShouldReturnValidationErrorsForBothFields() {
        TimeWindowDTO failsValidation = TimeWindowDTO.getBuilder()
                .withReadyTime(DATE_TIME)
                .withDueTime(BEFORE_DATE_TIME)
                .build();

        assertThat(validator.validate(failsValidation))
                .numberOfValidationErrorsIs(2)
                .hasValidationErrorForField("readyTime")
                .hasValidationErrorForField("dueTime");
    }

    @Test(expected = ValidationException.class)
    public void readyTimeNotAfterDueTime_InvalidReadyTimeField_ShouldThrowException() {
        InvalidReadyTimeFieldDTO invalid = new InvalidReadyTimeFieldDTO();

        validator.validate(invalid);
    }

    @Test(expected = ValidationException.class)
    public void readyTimeNotAfterDueTime_InvalidDueTimeField_ShouldThrowException() {
        InvalidDueTimeFieldDTO invalid = new InvalidDueTimeFieldDTO();

        validator.validate(invalid);
    }

    @ReadyTimeNotAfterDueTime(
            readyTimeFieldName = "readyTime",
            dueTimeFieldName = "dueTime"
    )
    private class InvalidReadyTimeFieldDTO {
        private DateTime dueTime;
    }

    @ReadyTimeNotAfterDueTime(
            readyTimeFieldName = "readyTime",
            dueTimeFieldName = "dueTime"
    )
    private class InvalidDueTimeFieldDTO {
        private DateTime readyTime;
    }

}
