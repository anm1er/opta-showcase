package org.optaplanner.showcase.app.vehiclerouting.web.planning;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import org.optaplanner.showcase.app.vehiclerouting.exception.SolvingDeniedException;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.ProblemDirector;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.SolverService;
import org.optaplanner.showcase.app.vehiclerouting.testdata.TestSolutionGeneratorUtility;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/test-planning-context-config.xml", "classpath:spring/test-mvc-config.xml"})
@WebAppConfiguration
@ActiveProfiles("planning")
public class PlannerControllerTest {

    public static final String ROOT = "/demo";

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    @Qualifier("domain")
    private SolverService solverServiceMock;

    @Autowired
    private ProblemDirector problemDirectorMock;

    private MockMvc mockMvc;

    // Note: The way AbstractSolutionDao is currently implemented requires that data directory is explicitly set
    @BeforeClass
    public static void setUpDataDirectoryProperty() {
        TestSolutionGeneratorUtility.setUpDataDirProperty();
    }

    @Before
    public void setUp() {
        Mockito.reset(solverServiceMock, problemDirectorMock);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void solve_NoSolverStartedForTheCurrentSession_ShouldStartSolver() throws Exception {
        doNothing().when(solverServiceMock).startSolving();

        mockMvc.perform(post(ROOT + "/start"))
                .andExpect(status().isOk());

        verify(solverServiceMock, times(1)).startSolving();
        verifyNoMoreInteractions(solverServiceMock);
        verifyZeroInteractions(problemDirectorMock);
    }

    @Test
    public void solve_SolverHasBeenStartedForTheCurrentSession_ShouldThrowException() throws Exception {
        doThrow(new SolvingDeniedException()).when(solverServiceMock).startSolving();

        mockMvc.perform(post(ROOT + "/start"))
                .andExpect(status().isBadRequest());

        verify(solverServiceMock, times(1)).startSolving();
        verifyNoMoreInteractions(solverServiceMock);
        verifyZeroInteractions(problemDirectorMock);
    }

    @Test
    public void terminateEarly_SolverStartedForTheCurrentSession_ShouldTerminateSolver() throws Exception {
        when(solverServiceMock.terminateSolving()).thenReturn(true);
        when(solverServiceMock.getCurrentSolution()).thenReturn(new VehicleRoutingSolution());

        mockMvc.perform(post(ROOT + "/terminate"))
                .andExpect(status().isOk());

        verify(solverServiceMock, times(1)).terminateSolving();
        verify(solverServiceMock).getCurrentSolution();
        verify(problemDirectorMock).setDomainSolution(isA(VehicleRoutingSolution.class));
        verifyNoMoreInteractions(solverServiceMock);
        verifyZeroInteractions(problemDirectorMock);
    }

}
