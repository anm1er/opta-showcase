package org.optaplanner.showcase.app.vehiclerouting.web.util;

public final class WebUtil {

    public static final String REDIRECT = "redirect:/";

    private WebUtil() { }

    public static String getRedirectedURL(String... components) {
        StringBuilder redirectedURL = new StringBuilder();
        redirectedURL.append(REDIRECT);
        for (String component : components) {
            redirectedURL.append(component);
            redirectedURL.append("/");
        }
        return redirectedURL.toString();
    }

}
