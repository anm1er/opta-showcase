package org.optaplanner.showcase.app.vehiclerouting.testdata;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;

import org.mockito.Mockito;

import static org.mockito.Mockito.when;

/**
 * Helper class to produce planning bean mocks with schedule-related properties set,
 * since they are only exposed through public API as read-only.
 */
public final class TestScheduledPlanningBeans {

    public static final PlanningLocation PLANNING_LOCATION = TestModelData.createDefaultPlanningLocationA();
    public static final int SCHEDULED_TIME = 100;

    private TestScheduledPlanningBeans() {
    }

    /**
     * Returns customer mock in the planning domain with all properties set to default values from {@link TestModelData}
     * except the property which makes the customer's natural identity - {@code location},
     * and the properties set by the solving engine.
     * @param location location to identify a customer
     * @param arrivalTime timed scheduled for arrival of a servicing vehicle
     * @param nextCustomer next customer in the delivery chain
     * @return customer mock with scheduled information
     */
    public static PlanningTimeWindowedCustomer getScheduledTimeWindowedCustomer(PlanningLocation location,
            int arrivalTime, PlanningTimeWindowedCustomer nextCustomer) {
        PlanningTimeWindowedCustomer customerMock = Mockito.mock(PlanningTimeWindowedCustomer.class);

        when(customerMock.getDemand()).thenReturn(TestModelData.DEMAND);
        when(customerMock.getLocation()).thenReturn(location);
        when(customerMock.getServiceDuration()).thenReturn(TestModelData.PLANING_SERVICE_DURATION);
        when(customerMock.getReadyTime()).thenReturn(TestModelData.PLANNING_READY_TIME);
        when(customerMock.getDueTime()).thenReturn(TestModelData.PLANNING_DUE_TIME);
        when(customerMock.getScheduledTime()).thenReturn(arrivalTime);
        when(customerMock.getNextCustomer()).thenReturn(nextCustomer);

        return customerMock;
    }

    /**
     * Returns customer mock in the planning domain with all properties set to default values,
     * except the next customer in chain.
     * @param nextCustomer next customer in the delivery chain
     * @return customer mock with scheduled information
     */
    public static PlanningTimeWindowedCustomer getScheduledTimeWindowedCustomer(PlanningTimeWindowedCustomer nextCustomer) {
        PlanningTimeWindowedCustomer customerMock = Mockito.mock(PlanningTimeWindowedCustomer.class);

        when(customerMock.getDemand()).thenReturn(TestModelData.DEMAND);
        when(customerMock.getLocation()).thenReturn(PLANNING_LOCATION);
        when(customerMock.getServiceDuration()).thenReturn(TestModelData.PLANING_SERVICE_DURATION);
        when(customerMock.getReadyTime()).thenReturn(TestModelData.READY_TIME);
        when(customerMock.getDueTime()).thenReturn(TestModelData.DUE_TIME);
        when(customerMock.getScheduledTime()).thenReturn(SCHEDULED_TIME);
        when(customerMock.getNextCustomer()).thenReturn(nextCustomer);

        return customerMock;
    }

    /**
     * Returns vehicle mock in the planning domain with all properties set to default values from {@link TestModelData}
     * except the property which makes the vehicle's natural identity - {@code licensePlate},
     * and the properties set by the solving engine.
     * @param licensePlate license plate to identify a vehicle
     * @param firstCustomer first customer to service
     * @return vehicle mock with scheduled information
     */
    public static PlanningVehicle getScheduledVehicle(String licensePlate, PlanningTimeWindowedCustomer firstCustomer) {
        PlanningVehicle vehicleMock = Mockito.mock(PlanningVehicle.class);
        when(vehicleMock.getCapacity()).thenReturn(TestModelData.CAPACITY);
        when(vehicleMock.getLicensePlate()).thenReturn(licensePlate);
        when(vehicleMock.getDepot()).thenReturn(TestModelData.createDefaultPlanningTimeWindowedDepot());
        when(vehicleMock.getFirstCustomerToService()).thenReturn(firstCustomer);

        return vehicleMock;
    }

    /**
     * Returns vehicle mock in the planning domain with all properties set to default values,
     * except the first customer to service.
     * @param firstCustomer first customer to service
     * @return vehicle mock with scheduled information
     */
    public static PlanningVehicle getScheduledVehicle(PlanningTimeWindowedCustomer firstCustomer) {
        PlanningVehicle vehicleMock = Mockito.mock(PlanningVehicle.class);
        when(vehicleMock.getCapacity()).thenReturn(TestModelData.CAPACITY);
        when(vehicleMock.getLicensePlate()).thenReturn(TestModelData.LICENSE_PLATE);
        when(vehicleMock.getDepot()).thenReturn(TestModelData.createDefaultPlanningTimeWindowedDepot());
        when(vehicleMock.getFirstCustomerToService()).thenReturn(firstCustomer);

        return vehicleMock;
    }

}
