package org.optaplanner.showcase.app.vehiclerouting.testdata;

import java.io.File;
import java.net.URL;

import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;
import org.optaplanner.showcase.model.vehiclerouting.impl.persistence.VehicleRoutingDao;
import org.optaplanner.showcase.model.vehiclerouting.impl.persistence.VehicleRoutingImporter;

public final class TestSolutionGeneratorUtility {

    public static final String DATA_DIR_PATH = "/org/optaplanner/showcase/app/vehiclerouting/data";
    public static final String SOLVED_025_C101_SOLUTION_PATH =
            DATA_DIR_PATH + "/vehiclerouting/solved/Solomon_025_C101.xml";
    public static final String UNSOLVED_025_C101_SOLUTION_PATH =
            DATA_DIR_PATH + "/vehiclerouting/import/airdistance/timewindowed/Solomon_025_C101.vrp";

    private TestSolutionGeneratorUtility() {
    }

    public static void setUpDataDirProperty() {
        URL dataDirURL = VehicleRoutingSolution.class.getResource(DATA_DIR_PATH);
        System.setProperty("org.optaplanner.examples.dataDir", dataDirURL.getPath());
    }

    public static VehicleRoutingSolution getSolvedSolomon_025_C101() throws Exception {
        URL solutionURL = VehicleRoutingSolution.class.getResource(SOLVED_025_C101_SOLUTION_PATH);
        File unsolvedSolutionFile = new File(solutionURL.toURI());
        return (VehicleRoutingSolution) new VehicleRoutingDao().readSolution(unsolvedSolutionFile);
    }

    public static VehicleRoutingSolution getUnsolvedSolomon_025_C101() throws Exception {
        URL solutionURL = VehicleRoutingSolution.class.getResource(UNSOLVED_025_C101_SOLUTION_PATH);
        File unsolvedSolutionFile = new File(solutionURL.toURI());
        return (VehicleRoutingSolution) new VehicleRoutingImporter(true).readSolution(unsolvedSolutionFile);
    }

}
