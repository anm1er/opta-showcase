package org.optaplanner.showcase.app.vehiclerouting.util.datetime;

import org.optaplanner.showcase.app.vehiclerouting.testdata.ArbitraryTimeStamp;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PlanningUnitsToDateTimeTranslatorTest {

    public static final int MINUTES_IN_HOUR = 60;
    public static final int HOURS_IN_DAY = 24;
    public static final int SECONDS_IN_MINUTES = 60;
    public static final int DAY_SHIFT_IN_MINUTES = MINUTES_IN_HOUR * HOURS_IN_DAY;

    public static final DateTime ORIGIN = ArbitraryTimeStamp.getDateTime();

    private PlanningUnitsToDateTimeTranslator dateTimeTranslator;

    @Before
    public void setUp() {
        dateTimeTranslator = new PlanningUnitsToDateTimeTranslator();
    }

    @Test
    public void getDateTimeFromShift_PositiveShiftAndDateTimeGiven_ShouldReturnShiftedDateTime() {
        DateTime afterDateTime = dateTimeTranslator.getDateTimeFromShift(DAY_SHIFT_IN_MINUTES, ORIGIN);
        assertEquals(ORIGIN.plusDays(1), afterDateTime);
    }

    @Test
    public void getDateTimeFromShift_NegativeShiftAndDateTimeGiven_ShouldReturnShiftedDateTime() {
        DateTime beforeDateTime = dateTimeTranslator.getDateTimeFromShift(-DAY_SHIFT_IN_MINUTES, ORIGIN);
        assertEquals(ORIGIN.minusDays(1), beforeDateTime);
    }

    @Test
    public void getShiftFromDateTime_DateTimeAndOriginGiven_ShouldReturnPositiveShift() {
        int shift = dateTimeTranslator.getShiftFromDateTime(ORIGIN.plusDays(1), ORIGIN);
        assertEquals(DAY_SHIFT_IN_MINUTES, shift);
    }


    @Test
    public void getDateTimeFromShiftgetShiftFromDateTime_RoundTripShiftGiven_ShouldReturnSameShift() {
        DateTime beforeDateTime = dateTimeTranslator.getDateTimeFromShift(DAY_SHIFT_IN_MINUTES, ORIGIN);
        int roundTripShift = dateTimeTranslator.getShiftFromDateTime(beforeDateTime, ORIGIN);

        assertEquals(DAY_SHIFT_IN_MINUTES, roundTripShift);
    }

}
