package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed;

import org.optaplanner.showcase.app.vehiclerouting.testdata.TestModelData;
import org.optaplanner.showcase.app.vehiclerouting.util.mapper.MappingUtils;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MappingValidatorTest {

    public static final long ID = 1L;
    public static final int TIME = 1000;

    // Fall back on rules to test static methods exceptions handling

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void getValidatedCustomer_ValidCustomerGiven_ShouldReturnSameInstance() {
        PlanningTimeWindowedCustomer customer = TestModelData.createDefaultPlanningTimeWindowedCustomerA();

        PlanningTimeWindowedCustomer resultingCustomer = MappingValidator.getValidatedCustomer(customer);

        assertSame(resultingCustomer, customer);
    }

    @Test
    public void validatePlanningBeanMappingRequest_ValidCustomerGiven_ShouldReturnSameInstance() {
        PlanningTimeWindowedCustomer customerMock = Mockito.mock(PlanningTimeWindowedCustomer.class);
        when(customerMock.getId()).thenReturn(ID);

        PlanningTimeWindowedCustomer resultingCustomer = MappingValidator.getValidatedCustomer(customerMock);

        assertSame(resultingCustomer, customerMock);
    }

    @Test
    public void getValidatedCustomer_IncompatibleCustomerTypeGiven_ShouldThrowMappingException() {
        PlanningCustomer customerMock = Mockito.mock(PlanningCustomer.class);

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(MappingUtils.getTypeMismatchExceptionMessage(PlanningTimeWindowedCustomer.class, customerMock));

        MappingValidator.getValidatedCustomer(customerMock);
    }

}
