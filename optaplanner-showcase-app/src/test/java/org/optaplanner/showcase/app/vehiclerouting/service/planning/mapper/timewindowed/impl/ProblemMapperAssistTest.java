package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.impl;

import java.util.Collections;
import java.util.List;

import org.optaplanner.showcase.app.vehiclerouting.exception.MappingException;
import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindow;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedDelivery;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.testdata.TestModelData;
import org.optaplanner.showcase.app.vehiclerouting.testdata.TestScheduledPlanningBeans;
import org.optaplanner.showcase.app.vehiclerouting.util.mapper.MappingUtils;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ProblemMapperAssistTest {

    public static final String NON_DEFAULT_LICENSE_PLATE = "ABC-123";

    public static final Location LOCATION_A = TestModelData.createDefaultLocationA();
    public static final Location LOCATION_B = TestModelData.createDefaultLocationB();
    public static final PlanningLocation PLANNING_LOCATION_A = TestModelData.createDefaultPlanningLocationA();
    public static final PlanningLocation PLANNING_LOCATION_B = TestModelData.createDefaultPlanningLocationB();
    public static final TimeWindow TIME_WINDOW = TestModelData.createDefaultTimeWindow();

    // Reuses constants from TestModelData
    public static final int DEMAND = TestModelData.DEMAND;
    public static final int SERVICE_DURATION = TestModelData.SERVICE_DURATION;
    public static final int PLANNING_SERVICE_DURATION = TestModelData.PLANING_SERVICE_DURATION;
    public static final int CAPACITY = TestModelData.CAPACITY;
    public static final String LICENSE_PLATE = TestModelData.LICENSE_PLATE;
    public static final int DUE_TIME = TestModelData.PLANNING_DUE_TIME;
    public static final int READY_TIME = TestModelData.PLANNING_READY_TIME;
    public static final Long ID = TestModelData.ID_1;
    public static final DateTime ORIGIN = TestModelData.ORIGIN;
    private static final DateTime ARRIVAL_DATE_TIME = ORIGIN.plusHours(12);

    private ProblemMapperAssist problemMapper;

    // This one is expected do all the heavy lifting
    @Mock
    private DefaultTimedBeanMapper timedBeanMapperMock;

    @Before
    public void setUp() {
        problemMapper = new ProblemMapperAssist(timedBeanMapperMock, ORIGIN);
    }

    @Test
    public void mapPlanningCustomers_ValidCustomerListGiven_ShouldReturnMappedCustomers() {
        PlanningTimeWindowedCustomer defaultCustomer = TestModelData.createDefaultPlanningTimeWindowedCustomerA();
        List<PlanningTimeWindowedCustomer> sourceCustomers = Collections.nCopies(2, defaultCustomer);

        when(timedBeanMapperMock.map(sourceCustomers.get(0), ORIGIN))
                .thenReturn(TestModelData.createDefaultTimeWindowedCustomerA())
                .thenReturn(TestModelData.createDefaultTimeWindowedCustomerB());

        List<TimeWindowedCustomer> destinationCustomers = problemMapper.mapPlanningCustomers(sourceCustomers);

        verify(timedBeanMapperMock, times(2)).map(defaultCustomer, ORIGIN);
        assertEquals(2, destinationCustomers.size());
        assertThat(destinationCustomers).extracting("id", "demand", "location", "serviceDuration", "timeWindow")
                .contains(tuple(null, DEMAND, LOCATION_A, SERVICE_DURATION, TIME_WINDOW))
                .contains(tuple(null, DEMAND, LOCATION_B, SERVICE_DURATION, TIME_WINDOW));
    }

    @Test
    public void mapPlanningDepots_ValidDepotListGiven_ShouldReturnMappedDepots() {
        PlanningTimeWindowedDepot defaultDepot = TestModelData.createDefaultPlanningTimeWindowedDepot();
        List<PlanningTimeWindowedDepot> sourceDepots = Collections.nCopies(2, defaultDepot);

        when(timedBeanMapperMock.map(defaultDepot, ORIGIN))
                .thenReturn(TestModelData.createDefaultTimeWindowedDepotA())
                .thenReturn(TestModelData.createDefaultTimeWindowedDepotB());

        List<TimeWindowedDepot> destinationDepots = problemMapper.mapPlanningDepots(sourceDepots);

        verify(timedBeanMapperMock, times(2)).map(defaultDepot, ORIGIN);
        assertEquals(2, destinationDepots.size());
        assertThat(destinationDepots).extracting("id", "location", "timeWindow")
                .contains(tuple(null, LOCATION_A, TIME_WINDOW))
                .contains(tuple(null, LOCATION_B, TIME_WINDOW));
    }

    @Test
    public void mapPlanningVehicles_ValidVehiclesListGiven_ShouldReturnMappedVehiclesWithDepots() {
        PlanningVehicle defaultVehicle = TestModelData.createDefaultPlanningVehicle();
        List<PlanningVehicle> sourceVehicles = Collections.nCopies(2, defaultVehicle);

        TimeWindowedDepot destinationDepot = TestModelData.createDefaultTimeWindowedDepotA();

        when(timedBeanMapperMock.map(defaultVehicle, ORIGIN))
                .thenReturn(TestModelData.createTimedVehicle(LICENSE_PLATE, destinationDepot))
                .thenReturn(TestModelData.createTimedVehicle(NON_DEFAULT_LICENSE_PLATE, destinationDepot));

        List<TimedVehicle> destinationVehicles = problemMapper.mapPlanningVehicles(sourceVehicles);

        verify(timedBeanMapperMock, times(2)).map(defaultVehicle, ORIGIN);
        assertEquals(2, destinationVehicles.size());
        assertThat(destinationVehicles).extracting("id", "capacity", "licensePlate", "depot")
                .contains(tuple(null, CAPACITY, LICENSE_PLATE, destinationDepot))
                .contains(tuple(null, CAPACITY, NON_DEFAULT_LICENSE_PLATE, destinationDepot));
        assertEquals(destinationDepot, destinationVehicles.get(0).getDepot());
    }

    @Test
    public void mapPlanningVehiclesToDeliveries_VehicleListGiven_ShouldReturnTimedDeliveryForNonChainedCustomer() {
        // Mocks customer to include positive arrival time, otherwise an exception will be thrown
        PlanningTimeWindowedCustomer sourceCustomerMock = TestScheduledPlanningBeans.getScheduledTimeWindowedCustomer(null);
        // Mocks vehicle to include the first customer to service
        PlanningVehicle sourceVehicleMock = TestScheduledPlanningBeans.getScheduledVehicle(sourceCustomerMock);
        List<PlanningVehicle> sourceVehicles = Collections.singletonList(sourceVehicleMock);

        TimeWindowedCustomer destinationCustomer = TestModelData.createDefaultTimeWindowedCustomerA();
        TimedVehicle destinationScheduledVehicle = TestModelData.createDefaultTimedVehicle();

        when(timedBeanMapperMock.map(sourceVehicleMock, sourceCustomerMock, ORIGIN))
                .thenReturn(TimedDelivery.getBuilder()
                        .withCustomer(destinationCustomer)
                        .withVehicle(destinationScheduledVehicle)
                        .withArrivalTime(ARRIVAL_DATE_TIME)
                        .build());

        List<TimedDelivery> deliveries = problemMapper.mapPlanningVehiclesToDeliveries(sourceVehicles);

        verify(timedBeanMapperMock, times(1)).map(sourceVehicleMock, sourceCustomerMock, ORIGIN);
        assertEquals(1, deliveries.size());
        assertThat(deliveries).extracting("id", "customer", "vehicle", "arrivalTime")
                .contains(tuple(null, destinationCustomer, destinationScheduledVehicle, ARRIVAL_DATE_TIME));
    }

    @Test
    public void mapPlanningVehiclesToDeliveries_VehicleListWithChainedCustomersGiven_ShouldReturn2TimedDeliveriesInOneList() {
        // Mocks customers to include arrival time and next customer in chain
        PlanningTimeWindowedCustomer sourceNextCustomerMock = TestScheduledPlanningBeans.getScheduledTimeWindowedCustomer(null);
        PlanningTimeWindowedCustomer sourceCustomerMock = TestScheduledPlanningBeans
                .getScheduledTimeWindowedCustomer(sourceNextCustomerMock);
        // Mocks vehicle to include the customer chain reference
        PlanningVehicle sourceVehicleMock = TestScheduledPlanningBeans.getScheduledVehicle(sourceCustomerMock);
        List<PlanningVehicle> sourceVehicles = Collections.singletonList(sourceVehicleMock);

        when(timedBeanMapperMock.map(sourceVehicleMock, sourceCustomerMock, ORIGIN))
                .thenReturn(TimedDelivery.getBuilder().build());

        List<TimedDelivery> deliveries = problemMapper.mapPlanningVehiclesToDeliveries(sourceVehicles);

        verify(timedBeanMapperMock, times(1)).map(sourceVehicleMock, sourceCustomerMock, ORIGIN);
        verify(timedBeanMapperMock, times(1)).map(sourceVehicleMock, sourceNextCustomerMock, ORIGIN);
        // The point is to verify that mapper iterates over the chain of connected customers
        assertEquals(2, deliveries.size());
    }

    @Test
    public void mapPlanningVehiclesToDeliveries_VehicleWithSelfReferencingCustomerGiven_ShouldThrowMappingException() {
        // Mocks customers to include arrival time and next customer in chain
        PlanningTimeWindowedCustomer sourceSelfReferencingCustomerMock = TestScheduledPlanningBeans
                .getScheduledTimeWindowedCustomer(null);
        when(sourceSelfReferencingCustomerMock.getNextCustomer()).thenReturn(sourceSelfReferencingCustomerMock);
        // Mocks vehicle to include the customer chain reference
        PlanningVehicle sourceVehicleMock = TestScheduledPlanningBeans.getScheduledVehicle(NON_DEFAULT_LICENSE_PLATE,
                sourceSelfReferencingCustomerMock);
        List<PlanningVehicle> sourceVehicles = Collections.singletonList(sourceVehicleMock);

        when(timedBeanMapperMock.map(sourceVehicleMock, sourceSelfReferencingCustomerMock, ORIGIN))
                .thenReturn(TimedDelivery.getBuilder().build());

        catchException(problemMapper).mapPlanningVehiclesToDeliveries(sourceVehicles);

        verify(timedBeanMapperMock, times(2)).map(sourceVehicleMock, sourceSelfReferencingCustomerMock, ORIGIN);
        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getCyclesDetectedExceptionMessage("vehicle-" + sourceVehicleMock.getId()
                        + " with customer-" + sourceSelfReferencingCustomerMock.getId()));
    }

    @Test
    public void mapPlanningVehiclesToDeliveries_VehicleWithCyclicCustomersGiven_ShouldThrowMappingException() {
        final Long nextCustomerId = 2L;
        // Mocks customers to include arrival time and next customer in chain
        PlanningTimeWindowedCustomer sourceNextCustomerMock = TestScheduledPlanningBeans
                .getScheduledTimeWindowedCustomer(null);
        when(sourceNextCustomerMock.getId()).thenReturn(nextCustomerId);
        PlanningTimeWindowedCustomer sourceCustomerMock = TestScheduledPlanningBeans
                .getScheduledTimeWindowedCustomer(sourceNextCustomerMock);
        when(sourceNextCustomerMock.getNextCustomer()).thenReturn(sourceCustomerMock);
        // Mocks vehicle to include the customer chain reference
        PlanningVehicle sourceVehicleMock = TestScheduledPlanningBeans.getScheduledVehicle(sourceCustomerMock);
        List<PlanningVehicle> sourceVehicles = Collections.singletonList(sourceVehicleMock);

        when(timedBeanMapperMock.map(sourceVehicleMock, sourceCustomerMock, ORIGIN))
                .thenReturn(TimedDelivery.getBuilder().build());

        catchException(problemMapper).mapPlanningVehiclesToDeliveries(sourceVehicles);

        verify(timedBeanMapperMock, times(2)).map(sourceVehicleMock, sourceCustomerMock, ORIGIN);
        assertThat(caughtException())
                .isExactlyInstanceOf(MappingException.class)
                .hasMessage(MappingUtils.getCyclesDetectedExceptionMessage("vehicle-" + sourceVehicleMock.getId()
                        + " with customer-" + sourceCustomerMock.getId()));
    }

    @Test
    public void mapCustomers_ValidCustomerListGiven_ShouldReturnMappedPlanningCustomers() {
        TimeWindowedCustomer defaultCustomer = TestModelData.createDefaultTimeWindowedCustomerA();
        List<TimeWindowedCustomer> sourceCustomers = Collections.nCopies(2, defaultCustomer);

        when(timedBeanMapperMock.map(defaultCustomer, ORIGIN))
                .thenReturn(TestModelData.createDefaultPlanningTimeWindowedCustomerA())
                .thenReturn(TestModelData.createDefaultPlanningTimeWindowedCustomerB());

        List<PlanningTimeWindowedCustomer> destinationCustomers = problemMapper.mapCustomers(sourceCustomers);

        verify(timedBeanMapperMock, times(2)).map(defaultCustomer, ORIGIN);
        assertEquals(2, destinationCustomers.size());
        assertThat(destinationCustomers).extracting("id", "demand", "location", "serviceDuration", "dueTime", "readyTime")
                .contains(tuple(ID, DEMAND, PLANNING_LOCATION_A, PLANNING_SERVICE_DURATION, DUE_TIME, READY_TIME))
                .contains(tuple(ID, DEMAND, PLANNING_LOCATION_B, PLANNING_SERVICE_DURATION, DUE_TIME, READY_TIME));
    }

    @Test
    public void mapDepots_ValidDepotListGiven_ShouldReturnMappedPlanningDepots() {
        TimeWindowedDepot defaultDepot = TestModelData.createDefaultTimeWindowedDepotA();
        List<TimeWindowedDepot> sourceDepots = Collections.singletonList(defaultDepot);

        when(timedBeanMapperMock.map(defaultDepot, ORIGIN))
                .thenReturn(TestModelData.createDefaultPlanningTimeWindowedDepot());

        List<PlanningTimeWindowedDepot> destinationDepots = problemMapper.mapDepots(sourceDepots);

        verify(timedBeanMapperMock, times(1)).map(defaultDepot, ORIGIN);
        assertEquals(1, destinationDepots.size());
        assertThat(destinationDepots).extracting("id", "location", "dueTime", "readyTime")
                .contains(tuple(ID, PLANNING_LOCATION_A, DUE_TIME, READY_TIME));
    }

    @Test
    public void mapVehicles_ValidVehiclesListGiven_ShouldReturnMappedPlanningVehiclesWithDepots() {
        TimedVehicle defaultVehicle = TestModelData.createDefaultTimedVehicle();
        List<TimedVehicle> sourceVehicles = Collections.nCopies(2, defaultVehicle);

        when(timedBeanMapperMock.map(defaultVehicle, ORIGIN))
                .thenReturn(TestModelData.createPlanningVehicle(LICENSE_PLATE))
                .thenReturn(TestModelData.createPlanningVehicle(NON_DEFAULT_LICENSE_PLATE));

        List<PlanningVehicle> destinationVehicles = problemMapper.mapVehicles(sourceVehicles);

        verify(timedBeanMapperMock, times(2)).map(defaultVehicle, ORIGIN);
        assertEquals(2, destinationVehicles.size());
        assertThat(destinationVehicles).extracting("id", "capacity", "licensePlate")
                .contains(tuple(ID, CAPACITY, LICENSE_PLATE))
                .contains(tuple(ID, CAPACITY, NON_DEFAULT_LICENSE_PLATE));

    }

}
