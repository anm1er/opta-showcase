package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed;

import org.optaplanner.showcase.app.vehiclerouting.testdata.TestModelData;
import org.optaplanner.showcase.app.vehiclerouting.model.Depot;
import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.testdata.ArbitraryTimeStamp;

import org.joda.time.DateTime;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TimeWindowedDepotTest {

    public static final DateTime DATE_TIME = ArbitraryTimeStamp.getDateTime();

    @Test
    public void build_ThisAndBaseClassFieldsGiven_ShouldBuildTimeWindowedDepot() {
        TimeWindowedDepot depot = TimeWindowedDepot.getBuilder()
                .withLocation(new Location(TestModelData.LATITUDE_A, TestModelData.LONGITUDE_A))
                .withTimeWindow(new TimeWindow(DATE_TIME, DATE_TIME))
                .build();

        assertNull(depot.getId());
        assertEquals(new Location(TestModelData.LATITUDE_A, TestModelData.LONGITUDE_A), depot.getLocation());
        assertEquals(new TimeWindow(DATE_TIME, DATE_TIME), depot.getTimeWindow());
    }

    @Test
    public void equals_IdenticalDepotsGiven_ShouldReturnTrue() {
        TimeWindowedDepot depot1 = TimeWindowedDepot.getBuilder()
                .withLocation(new Location(TestModelData.LATITUDE_A, TestModelData.LONGITUDE_A))
                .withTimeWindow(new TimeWindow(DATE_TIME, DATE_TIME))
                .build();

        TimeWindowedDepot depot2 = TimeWindowedDepot.getBuilder()
                .withLocation(new Location(TestModelData.LATITUDE_A, TestModelData.LONGITUDE_A))
                .withTimeWindow(new TimeWindow(DATE_TIME, DATE_TIME))
                .build();

        assertEquals(true, depot1.equals(depot2));
        assertEquals(true, depot2.equals(depot1));
    }

    @Test
    public void equals_NonIdenticalDepotsGiven_ShouldReturnFalse() {
        TimeWindowedDepot depot1 = TimeWindowedDepot.getBuilder()
                .withLocation(new Location(TestModelData.LATITUDE_A, TestModelData.LONGITUDE_A))
                .withTimeWindow(new TimeWindow(DATE_TIME, DATE_TIME))
                .build();

        TimeWindowedDepot depot2 = TimeWindowedDepot.getBuilder()
                .withLocation(new Location(TestModelData.LATITUDE_B, TestModelData.LONGITUDE_A))
                .withTimeWindow(new TimeWindow(DATE_TIME, DATE_TIME))
                .build();

        assertEquals(false, depot1.equals(depot2));
        assertEquals(false, depot2.equals(depot1));
    }

    @Test
    public void equals_ThisAndBaseDepotsGiven_ShouldReturnFalse() {
        TimeWindowedDepot depot = TimeWindowedDepot.getBuilder()
                .withLocation(new Location(TestModelData.LATITUDE_A, TestModelData.LONGITUDE_A))
                .withTimeWindow(new TimeWindow(DATE_TIME, DATE_TIME))
                .build();

        Depot baseDepot = Depot.getBuilder()
                .withLocation(new Location(TestModelData.LATITUDE_B, TestModelData.LONGITUDE_A))
                .build();

        assertEquals(false, depot.equals(baseDepot));
        assertEquals(false, baseDepot.equals(depot));
    }

}
