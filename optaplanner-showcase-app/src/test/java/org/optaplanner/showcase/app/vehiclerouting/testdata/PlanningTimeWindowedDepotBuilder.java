package org.optaplanner.showcase.app.vehiclerouting.testdata;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningBeanFactory;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;

public final class PlanningTimeWindowedDepotBuilder {

    private static final Long ID = TestModelData.ID_1;
    private static final PlanningLocation LOCATION = TestModelData.createDefaultPlanningLocationA();
    private static final int READY_TIME = TestModelData.PLANNING_READY_TIME;
    private static final int DUE_TIME = TestModelData.PLANNING_DUE_TIME;
    private PlanningTimeWindowedDepot built;

    private PlanningTimeWindowedDepotBuilder(PlanningBeanFactory planningBeanFactory) {
        this.built = planningBeanFactory.createDepot();
    }

    public static PlanningTimeWindowedDepotBuilder getBuilder(PlanningBeanFactory planningBeanFactory) {
        return new PlanningTimeWindowedDepotBuilder(planningBeanFactory);
    }

    public PlanningTimeWindowedDepotBuilder forDefaultDepot() {
        return withId(ID)
                .withLocation(LOCATION)
                .withReadyTime(READY_TIME)
                .withDueTime(DUE_TIME);
    }

    public PlanningTimeWindowedDepotBuilder withId(Long id) {
        built.setId(id);
        return this;
    }

    public PlanningTimeWindowedDepotBuilder withLocation(PlanningLocation location) {
        built.setLocation(location);
        return this;
    }

    public PlanningTimeWindowedDepotBuilder withReadyTime(int readyTime) {
        built.setReadyTime(readyTime);
        return this;
    }

    public PlanningTimeWindowedDepotBuilder withDueTime(int dueTime) {
        built.setDueTime(dueTime);
        return this;
    }

    public PlanningTimeWindowedDepot build() {
        return built;
    }

}
