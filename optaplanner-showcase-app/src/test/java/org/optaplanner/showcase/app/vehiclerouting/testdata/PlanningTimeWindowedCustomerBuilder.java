package org.optaplanner.showcase.app.vehiclerouting.testdata;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningBeanFactory;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;

public final class PlanningTimeWindowedCustomerBuilder {

    private static final Long ID = TestModelData.ID_1;
    private static final int DEMAND = TestModelData.DEMAND;
    private static final int SERVICE_DURATION = TestModelData.SERVICE_DURATION;
    private static final int PLANNING_SERVICE_DURATION = TestModelData.PLANING_SERVICE_DURATION;
    private static final int READY_TIME = TestModelData.PLANNING_READY_TIME;
    private static final int DUE_TIME = TestModelData.PLANNING_DUE_TIME;
    private PlanningTimeWindowedCustomer built;

    private PlanningTimeWindowedCustomerBuilder(PlanningBeanFactory planningBeanFactory) {
        this.built = planningBeanFactory.createCustomer();
    }

    public static PlanningTimeWindowedCustomerBuilder getBuilder(PlanningBeanFactory planningBeanFactory) {
        return new PlanningTimeWindowedCustomerBuilder(planningBeanFactory);
    }

    public PlanningTimeWindowedCustomerBuilder forDefaultCustomer() {
        return withId(ID)
                .withDemand(DEMAND)
                .withLocation(TestModelData.createDefaultPlanningLocationA())
                .withServiceDuration(PLANNING_SERVICE_DURATION)
                .withReadyTime(READY_TIME)
                .withDueTime(DUE_TIME);
    }

    public PlanningTimeWindowedCustomerBuilder withId(Long id) {
        built.setId(id);
        return this;
    }

    public PlanningTimeWindowedCustomerBuilder withDemand(int demand) {
        built.setDemand(demand);
        return this;
    }

    public PlanningTimeWindowedCustomerBuilder withLocation(PlanningLocation location) {
        built.setLocation(location);
        return this;
    }

    public PlanningTimeWindowedCustomerBuilder withServiceDuration(int serviceDuration) {
        built.setServiceDuration(serviceDuration);
        return this;
    }

    public PlanningTimeWindowedCustomerBuilder withReadyTime(int readyTime) {
        built.setReadyTime(readyTime);
        return this;
    }

    public PlanningTimeWindowedCustomerBuilder withDueTime(int dueTime) {
        built.setDueTime(dueTime);
        return this;
    }

    public PlanningTimeWindowedCustomer build() {
        return built;
    }

}
