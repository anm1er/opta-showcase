package org.optaplanner.showcase.app.vehiclerouting.testdata;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningDepot;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningBeanFactory;

public final class PlanningVehicleBuilder {

    private static final Long ID = TestModelData.ID_1;
    private static final int CAPACITY = TestModelData.CAPACITY;
    private static final String LICENSE_PLATE = TestModelData.LICENSE_PLATE;
    private PlanningVehicle built;

    private PlanningVehicleBuilder(PlanningBeanFactory planningBeanFactory) {
        this.built = planningBeanFactory.createVehicle();
    }

    public static PlanningVehicleBuilder getBuilder(PlanningBeanFactory planningBeanFactory) {
        return new PlanningVehicleBuilder(planningBeanFactory);
    }

    public PlanningVehicleBuilder forDefaultVehicle() {
        return withId(ID)
                .withCapacity(CAPACITY)
                .withLicensePlate(LICENSE_PLATE)
                .withDepot(TestModelData.createDefaultPlanningTimeWindowedDepot());
    }

    public PlanningVehicleBuilder withId(Long id) {
        built.setId(id);
        return this;
    }

    public PlanningVehicleBuilder withCapacity(int capacity) {
        built.setCapacity(capacity);
        return this;
    }

    public PlanningVehicleBuilder withLicensePlate(String licensePlate) {
        built.setLicensePlate(licensePlate);
        return this;
    }

    public PlanningVehicleBuilder withDepot(PlanningDepot depot) {
        built.setDepot(depot);
        return this;
    }

    public PlanningVehicle build() {
        return built;
    }

}
