package org.optaplanner.showcase.app.vehiclerouting.util.editors;


import org.optaplanner.showcase.app.vehiclerouting.testdata.ArbitraryTimeStamp;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DateTimeEditorTest {

    public static final DateTime DATE_TIME = ArbitraryTimeStamp.getDateTime();
    private static final String DATE_TIME_STRING = ArbitraryTimeStamp.getDateTimeString();

    private DateTimeEditor dateTimeEditor;

    @Before
    public void setUp() {
        dateTimeEditor = new DateTimeEditor();
    }

    @Test
    public void setAsText_ValidDateTimeString_ShouldSetDateTime() {
        dateTimeEditor.setAsText(DATE_TIME_STRING);
        assertEquals(DATE_TIME, dateTimeEditor.getValue());
    }

    @Test
    public void getAsText_DateTimeValueSet_ShouldGetDateTimeAsString() {
        dateTimeEditor.setValue(DATE_TIME);
        assertEquals(DATE_TIME_STRING, dateTimeEditor.getAsText());
    }

}
