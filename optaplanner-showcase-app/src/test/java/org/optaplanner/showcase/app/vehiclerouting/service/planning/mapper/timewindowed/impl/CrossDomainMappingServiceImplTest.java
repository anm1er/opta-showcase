package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.testdata.ArbitraryTimeStamp;
import org.optaplanner.showcase.app.vehiclerouting.testdata.TestModelData;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem.TimeWindowedVehicleRoutingProblem;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem.TimeWindowedVehicleRoutingProblemFactory;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.timewindowed.problem.DefaultVehicleRoutingProblemFactory;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;

public class CrossDomainMappingServiceImplTest {

    public static final DateTime ORIGIN = ArbitraryTimeStamp.getDateTime();

    private TimeWindowedVehicleRoutingProblemFactory problemFactory;
    private CrossDomainMappingServiceImpl mappingService;

    private DefaultTimedBeanMapper timedBeanMapperMock;

    @Before
    public void setUp() {
        timedBeanMapperMock = Mockito.mock(DefaultTimedBeanMapper.class);
        problemFactory = new DefaultVehicleRoutingProblemFactory();
        mappingService = new CrossDomainMappingServiceImpl(timedBeanMapperMock, problemFactory, ORIGIN);
    }

    @Test
    public void mapDomainEntitiesToPlanningProblem_EntityListsGiven_ShouldReturnProblem() {
        // Given domain entities
        TimeWindowedCustomer customerA = TestModelData.createDefaultTimeWindowedCustomerA();
        TimeWindowedCustomer customerB = TestModelData.createDefaultTimeWindowedCustomerB();
        List<TimeWindowedCustomer> customers = Arrays.asList(customerA, customerB);
        List<TimeWindowedDepot> depots = Arrays.asList(TestModelData.createDefaultTimeWindowedDepotA());
        List<TimedVehicle> vehicles = Arrays.asList(TestModelData.createDefaultTimedVehicle());
        // And expected planning problem
        PlanningTimeWindowedCustomer planningCustomerA = TestModelData.createDefaultPlanningTimeWindowedCustomerA();
        PlanningTimeWindowedCustomer planningCustomerB = TestModelData.createDefaultPlanningTimeWindowedCustomerB();
        PlanningTimeWindowedDepot planningDepot = TestModelData.createDefaultPlanningTimeWindowedDepot();
        PlanningVehicle destinationVehicle = TestModelData.createDefaultPlanningVehicle();
        TimeWindowedVehicleRoutingProblem expectedProblem = problemFactory.getProblemBuilder()
                .withCustomers(Arrays.asList(planningCustomerA, planningCustomerB))
                .withVehicles(Arrays.asList(destinationVehicle))
                .withDepots(Arrays.asList(planningDepot))
                .build();

        when(timedBeanMapperMock.map(isA(TimeWindowedCustomer.class), eq(ORIGIN)))
                .thenReturn(TestModelData.createDefaultPlanningTimeWindowedCustomerA())
                .thenReturn(TestModelData.createDefaultPlanningTimeWindowedCustomerB());
        when(timedBeanMapperMock.map(isA(TimeWindowedDepot.class), eq(ORIGIN)))
                .thenReturn(TestModelData.createDefaultPlanningTimeWindowedDepot());
        when(timedBeanMapperMock.map(isA(TimedVehicle.class), eq(ORIGIN)))
                .thenReturn(TestModelData.createDefaultPlanningVehicle());

        TimeWindowedVehicleRoutingProblem problem = mappingService.mapDomainEntitiesToPlanningProblem(customers, depots,
                vehicles);

        assertEquals(expectedProblem, problem);
        assertEquals(expectedProblem.getCustomersNumber() + expectedProblem.getDepotsNumber(), problem.getLocationsNumber());
        assertThat(problem.getLocationList()).contains(planningCustomerA.getLocation(), planningCustomerB.getLocation(),
                planningDepot.getLocation());
    }

    @Test
    public void mapDomainEntitiesToPlanningProblem_EntityListsWithDifferentOrderGiven_ShouldReturnProblem() {
        // Given domain entities
        TimeWindowedCustomer customerA = TestModelData.createDefaultTimeWindowedCustomerA();
        TimeWindowedCustomer customerB = TestModelData.createDefaultTimeWindowedCustomerB();
        List<TimeWindowedCustomer> customers = Arrays.asList(customerA, customerB);
        List<TimeWindowedDepot> depots = Arrays.asList(TestModelData.createDefaultTimeWindowedDepotA());
        List<TimedVehicle> vehicles = Arrays.asList(TestModelData.createDefaultTimedVehicle());
        // And expected planning problem
        PlanningTimeWindowedCustomer planningCustomerA = TestModelData.createDefaultPlanningTimeWindowedCustomerA();
        PlanningTimeWindowedCustomer planningCustomerB = TestModelData.createDefaultPlanningTimeWindowedCustomerB();
        PlanningTimeWindowedDepot planningDepot = TestModelData.createDefaultPlanningTimeWindowedDepot();
        PlanningVehicle planningVehicle = TestModelData.createDefaultPlanningVehicle();
        TimeWindowedVehicleRoutingProblem expectedProblem = problemFactory.getProblemBuilder()
                .withCustomers(Arrays.asList(planningCustomerB, planningCustomerA))
                .withVehicles(Arrays.asList(planningVehicle))
                .withDepots(Arrays.asList(planningDepot))
                .build();

        when(timedBeanMapperMock.map(isA(TimeWindowedCustomer.class), eq(ORIGIN)))
                .thenReturn(TestModelData.createDefaultPlanningTimeWindowedCustomerA())
                .thenReturn(TestModelData.createDefaultPlanningTimeWindowedCustomerB());
        when(timedBeanMapperMock.map(isA(TimeWindowedDepot.class), eq(ORIGIN)))
                .thenReturn(TestModelData.createDefaultPlanningTimeWindowedDepot());
        when(timedBeanMapperMock.map(isA(TimedVehicle.class), eq(ORIGIN)))
                .thenReturn(TestModelData.createDefaultPlanningVehicle());

        TimeWindowedVehicleRoutingProblem problem = mappingService.mapDomainEntitiesToPlanningProblem(customers, depots,
                vehicles);

        assertEquals(expectedProblem, problem);
        assertEquals(expectedProblem.getCustomersNumber() + expectedProblem.getDepotsNumber(), problem.getLocationsNumber());
        assertThat(problem.getLocationList()).contains(planningCustomerA.getLocation(), planningCustomerB.getLocation(),
                planningDepot.getLocation());
    }

    @Test
    public void mapDomainEntitiesToPlanningProblem_EmptyListsGiven_ShouldReturnProblemWithEmptyLists() {
        List<TimeWindowedCustomer> customers = new ArrayList<>();
        List<TimeWindowedDepot> depots = new ArrayList<>();
        List<TimedVehicle> vehicles = new ArrayList<>();
        TimeWindowedVehicleRoutingProblem expectedProblem = problemFactory.getProblemBuilder()
                .withCustomers(new ArrayList<PlanningTimeWindowedCustomer>())
                .withVehicles(new ArrayList<PlanningVehicle>())
                .withDepots(new ArrayList<PlanningTimeWindowedDepot>())
                .build();

        TimeWindowedVehicleRoutingProblem problem = mappingService.mapDomainEntitiesToPlanningProblem(customers, depots,
                vehicles);

        assertEquals(expectedProblem, problem);
    }

}
