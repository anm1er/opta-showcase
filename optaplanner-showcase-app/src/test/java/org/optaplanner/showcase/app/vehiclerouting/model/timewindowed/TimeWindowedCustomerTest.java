package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed;

import org.optaplanner.showcase.app.vehiclerouting.testdata.TestModelData;
import org.optaplanner.showcase.app.vehiclerouting.model.Customer;
import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.testdata.ArbitraryTimeStamp;

import org.joda.time.DateTime;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TimeWindowedCustomerTest {

    public static final DateTime DATE_TIME = ArbitraryTimeStamp.getDateTime();

    @Test
    public void build_ThisAndBaseClassFieldsGiven_ShouldBuildTimeWindowedCustomer() {
        TimeWindowedCustomer customer = TimeWindowedCustomer.getBuilder()
                .withDemand(TestModelData.DEMAND)
                .withLocation(new Location(TestModelData.LATITUDE_A, TestModelData.LONGITUDE_A))
                .withServiceDuration(TestModelData.SERVICE_DURATION)
                .withTimeWindow(new TimeWindow(DATE_TIME, DATE_TIME))
                .build();

        assertNull(customer.getId());
        assertTrue(customer.getTimedDeliveries().isEmpty());
        assertEquals(TestModelData.DEMAND, customer.getDemand().intValue());
        assertEquals(new Location(TestModelData.LATITUDE_A, TestModelData.LONGITUDE_A), customer.getLocation());
        assertEquals(TestModelData.SERVICE_DURATION, customer.getServiceDuration().intValue());
        assertEquals(new TimeWindow(DATE_TIME, DATE_TIME), customer.getTimeWindow());
    }

    @Test
    public void equals_IdenticalCustomersGiven_ShouldReturnTrue() {
        TimeWindowedCustomer customer1 = TimeWindowedCustomer.getBuilder()
                .withDemand(TestModelData.DEMAND)
                .withLocation(new Location(TestModelData.LATITUDE_A, TestModelData.LONGITUDE_A))
                .withServiceDuration(TestModelData.SERVICE_DURATION)
                .withTimeWindow(new TimeWindow(DATE_TIME, DATE_TIME))
                .build();

        TimeWindowedCustomer customer2 = TimeWindowedCustomer.getBuilder()
                .withDemand(TestModelData.DEMAND)
                .withLocation(new Location(TestModelData.LATITUDE_A, TestModelData.LONGITUDE_A))
                .withServiceDuration(TestModelData.SERVICE_DURATION)
                .withTimeWindow(new TimeWindow(DATE_TIME, DATE_TIME))
                .build();

        assertEquals(true, customer1.equals(customer2));
        assertEquals(true, customer2.equals(customer1));
    }

    @Test
    public void equals_NonIdenticalCustomersGiven_ShouldReturnFalse() {
        TimeWindowedCustomer customer1 = TimeWindowedCustomer.getBuilder()
                .withDemand(TestModelData.DEMAND)
                .withLocation(new Location(TestModelData.LATITUDE_A, TestModelData.LONGITUDE_A))
                .withServiceDuration(TestModelData.SERVICE_DURATION)
                .withTimeWindow(new TimeWindow(DATE_TIME, DATE_TIME))
                .build();

        TimeWindowedCustomer customer2 = TimeWindowedCustomer.getBuilder()
                .withDemand(TestModelData.DEMAND)
                .withLocation(new Location(TestModelData.LATITUDE_A, TestModelData.LONGITUDE_B))
                .withServiceDuration(TestModelData.SERVICE_DURATION)
                .withTimeWindow(new TimeWindow(DATE_TIME, DATE_TIME))
                .build();

        assertEquals(false, customer1.equals(customer2));
        assertEquals(false, customer2.equals(customer1));
    }

    @Test
    public void equals_ThisAndBaseCustomersGiven_ShouldReturnFalse() {
        TimeWindowedCustomer customer = TimeWindowedCustomer.getBuilder()
                .withDemand(TestModelData.DEMAND)
                .withLocation(new Location(TestModelData.LATITUDE_A, TestModelData.LONGITUDE_A))
                .withServiceDuration(TestModelData.SERVICE_DURATION)
                .withTimeWindow(new TimeWindow(DATE_TIME, DATE_TIME))
                .build();

        Customer baseCustomer = Customer.getBuilder()
                .withDemand(TestModelData.DEMAND)
                .withLocation(new Location(TestModelData.LATITUDE_A, TestModelData.LONGITUDE_B))
                .build();

        assertEquals(false, customer.equals(baseCustomer));
        assertEquals(false, baseCustomer.equals(customer));
    }

}
