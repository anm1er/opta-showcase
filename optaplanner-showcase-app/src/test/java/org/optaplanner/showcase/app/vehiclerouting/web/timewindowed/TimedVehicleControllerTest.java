package org.optaplanner.showcase.app.vehiclerouting.web.timewindowed;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import org.optaplanner.showcase.app.vehiclerouting.event.modification.timewindowed.ProblemModificationEventSupport;
import org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.service.persistence.timewindowed.TimeWindowedPersistenceService;
import org.optaplanner.showcase.app.vehiclerouting.testdata.TestModelData;
import org.optaplanner.showcase.app.vehiclerouting.web.common.ErrorController;
import org.optaplanner.showcase.app.vehiclerouting.web.util.WebUtil;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/test-timewindowed-context-config.xml",
        "classpath:spring/test-mvc-config.xml"})
@WebAppConfiguration
@ActiveProfiles("timewindowed")
public class TimedVehicleControllerTest {

    public static final long ID = 1L;
    public static final long ID2 = 2L;
    public static final String MODEL_ATTRIBUTE_VEHICLE = TimedVehicleController.MODEL_ATTRIBUTE_VEHICLE;
    public static final String NON_DEFAULT_LICENSE_PLATE = "XXX-000";
    public static final String INVALID_LICENSE_PLATE = "aa-123";

    public static final String LICENSE_PROPERTY = "licensePlate";
    public static final String CAPACITY_PROPERTY = "capacity";
    public static final String ID_PROPERTY = "id";
    public static final String DEPOT_PROPERTY = "depot";

    public static final String VEHICLES_REQUEST = TimedVehicleController.VEHICLES_REQUEST;
    public static final String VEHICLE_REQUEST = "/vehicles/{vehicleId}";
    public static final String VEHICLE_EDIT_REQUEST = "/vehicles/{vehicleId}/edit";

    private MockMvc mockMvc;

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TimeWindowedPersistenceService persistenceServiceMock;
    @Autowired
    private ProblemModificationEventSupport problemModificationEventSupportMock;

    @Before
    public void setUp() {
        Mockito.reset(persistenceServiceMock, problemModificationEventSupportMock);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    public String getMessage(String message) {
        return messageSource.getMessage(message, new Object[]{}, LocaleContextHolder.getLocale());
    }

    @Test
    public void listAllVehicles_AllVehiclesFound_ShouldAddVehiclesToModelAndReturnVehicleListView() throws Exception {
        TimedVehicle vehicleA = TestModelData.createDefaultTimedVehicle();
        TimedVehicle vehicleB = TestModelData.createTimedVehicle(NON_DEFAULT_LICENSE_PLATE);

        when(persistenceServiceMock.getVehicles()).thenReturn(Arrays.asList(vehicleA, vehicleB));

        mockMvc.perform(get("/vehicles"))
                .andExpect(status().isOk())
                .andExpect(view().name(TimedVehicleController.VIEW_VEHICLE_LIST))
                .andExpect(forwardedUrl("/WEB-INF/jsp/vehicles/list.jsp"))
                .andExpect(model().attribute(TimedVehicleController.MODEL_ATTRIBUTE_VEHICLE_LIST, hasSize(2)))
                .andExpect(model().attribute(TimedVehicleController.MODEL_ATTRIBUTE_VEHICLE_LIST, hasItem(
                        allOf(
                                hasProperty(LICENSE_PROPERTY, is(TestModelData.LICENSE_PLATE)),
                                hasProperty(CAPACITY_PROPERTY, is(TestModelData.CAPACITY))
                        )
                )))
                .andExpect(model().attribute(TimedVehicleController.MODEL_ATTRIBUTE_VEHICLE_LIST, hasItem(
                        allOf(
                                hasProperty(LICENSE_PROPERTY, is(NON_DEFAULT_LICENSE_PLATE)),
                                hasProperty(CAPACITY_PROPERTY, is(TestModelData.CAPACITY))
                        )
                )));

        verify(persistenceServiceMock, times(1)).getVehicles();
        verifyNoMoreInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

   @Test
    public void viewVehicleDetails_VehicleFound_ShouldAddVehicleToModelAndReturnVehicleDetailsView() throws Exception {
       TimedVehicle vehicle = TestModelData.createDefaultTimedVehicle();

       when(persistenceServiceMock.getVehicle(ID)).thenReturn(vehicle);

        mockMvc.perform(get(VEHICLE_REQUEST, ID))
                .andExpect(status().isOk())
                .andExpect(view().name(TimedVehicleController.VIEW_VEHICLE_DETAILS))
                .andExpect(forwardedUrl("/WEB-INF/jsp/vehicles/details.jsp"))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(CAPACITY_PROPERTY,
                        is(TestModelData.CAPACITY))))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(LICENSE_PROPERTY,
                        is(TestModelData.LICENSE_PLATE))));

        verify(persistenceServiceMock, times(1)).getVehicle(ID);
        verifyNoMoreInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void viewVehicleDetails_VehicleNotFound_ShouldReturn404View() throws Exception {
        when(persistenceServiceMock.getVehicle(ID)).thenThrow(new ResourceNotFoundException());

        mockMvc.perform(get(VEHICLE_REQUEST, ID))
                .andExpect(status().isNotFound())
                .andExpect(view().name(ErrorController.VIEW_NOT_FOUND))
                .andExpect(forwardedUrl("/WEB-INF/jsp/error/status404.jsp"));

        verify(persistenceServiceMock, times(1)).getVehicle(ID);
        verifyNoMoreInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void displayVehicleEditForm_VehiclePlacedInSession_ShouldAddVehicleToModelAndReturnEditForm()
            throws Exception {
        TimedVehicle vehicle = TestModelData.createDefaultTimedVehicle();
        vehicle.setId(ID);

        mockMvc.perform(get(VEHICLE_EDIT_REQUEST, ID)
                .sessionAttr(MODEL_ATTRIBUTE_VEHICLE, vehicle))
                .andExpect(status().isOk())
                .andExpect(view().name(TimedVehicleController.VIEW_VEHICLE_ADD_OR_EDIT_FORM))
                .andExpect(forwardedUrl("/WEB-INF/jsp/vehicles/addOrEditForm.jsp"))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(ID_PROPERTY, is(ID))))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(CAPACITY_PROPERTY,
                        is(TestModelData.CAPACITY))))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(LICENSE_PROPERTY,
                        is(TestModelData.LICENSE_PLATE))));

        verifyZeroInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void displayVehicleEditForm_VehicleNotPlacedInSessionVehicleFound_ShouldAddVehicleToModelAndReturnEditForm()
            throws Exception {
        TimedVehicle vehicle = TestModelData.createDefaultTimedVehicle();
        vehicle.setId(ID);
        TimedVehicle vehicleInSession = TestModelData.createDefaultTimedVehicle();
        vehicleInSession.setId(ID2);

        when(persistenceServiceMock.getVehicle(ID)).thenReturn(vehicle);

        mockMvc.perform(get(VEHICLE_EDIT_REQUEST, ID)
                .sessionAttr(MODEL_ATTRIBUTE_VEHICLE, vehicleInSession))
                .andExpect(status().isOk())
                .andExpect(view().name(TimedVehicleController.VIEW_VEHICLE_ADD_OR_EDIT_FORM))
                .andExpect(forwardedUrl("/WEB-INF/jsp/vehicles/addOrEditForm.jsp"))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(ID_PROPERTY, is(ID))))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(CAPACITY_PROPERTY,
                        is(TestModelData.CAPACITY))))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(LICENSE_PROPERTY,
                        is(TestModelData.LICENSE_PLATE))));

        verify(persistenceServiceMock, times(1)).getVehicle(ID);
        verifyNoMoreInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void displayVehicleEditForm_VehicleNotPlacedInSessionVehicleNotFound_ShouldReturn404View() throws Exception {
        TimedVehicle vehicleInSession = TestModelData.createDefaultTimedVehicle();
        vehicleInSession.setId(ID2);

        when(persistenceServiceMock.getVehicle(ID)).thenThrow(new ResourceNotFoundException());

        mockMvc.perform(get(VEHICLE_EDIT_REQUEST, ID)
                .sessionAttr(MODEL_ATTRIBUTE_VEHICLE, vehicleInSession))
                .andExpect(status().isNotFound())
                .andExpect(view().name(ErrorController.VIEW_NOT_FOUND))
                .andExpect(forwardedUrl("/WEB-INF/jsp/error/status404.jsp"));

        verify(persistenceServiceMock, times(1)).getVehicle(ID);
        verifyNoMoreInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void processVehicleEditForm_ValidVehicleGiven_ShouldUpdateVehicleAndFireUpdateEventAndRedirect()
            throws Exception {
        TimedVehicle updatedVehicle = TestModelData.createDefaultTimedVehicle();
        updatedVehicle.setId(ID);

        when(persistenceServiceMock.saveVehicle(isA(TimedVehicle.class))).thenReturn(updatedVehicle);

        mockMvc.perform(post(VEHICLE_EDIT_REQUEST, ID)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param(ID_PROPERTY, String.valueOf(ID))
                .param(LICENSE_PROPERTY, TestModelData.LICENSE_PLATE)
                .param(CAPACITY_PROPERTY, String.valueOf(TestModelData.CAPACITY))
                .sessionAttr(MODEL_ATTRIBUTE_VEHICLE, updatedVehicle))
                .andExpect(status().isFound())
                .andExpect(view().name(WebUtil.getRedirectedURL(VEHICLES_REQUEST, "{vehicleId}")))
                .andExpect(flash().attribute(TimedVehicleController.FLASH_MESSAGE_KEY_FEEDBACK,
                        is(getMessage(TimedVehicleController.FEEDBACK_MESSAGE_KEY_VEHICLE_UPDATED))));

        ArgumentCaptor<TimedVehicle> formVehicleObject = ArgumentCaptor.forClass(TimedVehicle.class);
        verify(persistenceServiceMock, times(1)).saveVehicle(formVehicleObject.capture());
        TimedVehicle returnedUpdatedVehicle = formVehicleObject.getValue();

        verifyNoMoreInteractions(persistenceServiceMock);
        verify(problemModificationEventSupportMock, times(1)).fireModificationEnded(null);
        verifyNoMoreInteractions(problemModificationEventSupportMock);
        assertEquals(TestModelData.LICENSE_PLATE, returnedUpdatedVehicle.getLicensePlate());
        assertEquals(TestModelData.CAPACITY, returnedUpdatedVehicle.getCapacity().intValue());
        // For now not checking time window equality because org.joda.time.DateTime does not equal itself after deserialization
        // assertEquals(TestModelData.createDefaultTimeWindow(), returnedUpdatedCustomer.getTimeWindow());
        assertEquals(updatedVehicle, returnedUpdatedVehicle);
    }

    @Test
    public void processVehicleEditForm_VehicleWithNullLicensePlateGiven_ShouldContainValidationErrorAndForwardToForm()
            throws Exception {
        TimedVehicle updatedVehicle = TestModelData.createDefaultTimedVehicle();
        updatedVehicle.setId(ID);
        updatedVehicle.setLicensePlate(null);

        mockMvc.perform(post(VEHICLE_EDIT_REQUEST, ID)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param(ID_PROPERTY, String.valueOf(ID))
                .sessionAttr(MODEL_ATTRIBUTE_VEHICLE, updatedVehicle))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("/WEB-INF/jsp/" + TimedVehicleController.VIEW_VEHICLE_ADD_OR_EDIT_FORM  + ".jsp"))
                .andExpect(model().attributeHasFieldErrors(MODEL_ATTRIBUTE_VEHICLE, LICENSE_PROPERTY))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(ID_PROPERTY, is(ID))));

        verifyZeroInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void processVehicleEditForm_VehicleWithInvalidLicensePlateGiven_ShouldContainValidationErrorAndForwardToForm()
            throws Exception {
        TimedVehicle updatedVehicle = TestModelData.createDefaultTimedVehicle();
        updatedVehicle.setId(ID);
        updatedVehicle.setLicensePlate(INVALID_LICENSE_PLATE);

        mockMvc.perform(post(VEHICLE_EDIT_REQUEST, ID)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param(ID_PROPERTY, String.valueOf(ID))
                .sessionAttr(MODEL_ATTRIBUTE_VEHICLE, updatedVehicle))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("/WEB-INF/jsp/" + TimedVehicleController.VIEW_VEHICLE_ADD_OR_EDIT_FORM  + ".jsp"))
                .andExpect(model().attributeHasFieldErrors(MODEL_ATTRIBUTE_VEHICLE, LICENSE_PROPERTY))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(ID_PROPERTY, is(ID))));

        verifyZeroInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void processVehicleEditForm_VehicleWithNullCapacityGiven_ShouldContainValidationErrorAndForwardToForm()
            throws Exception {
        TimedVehicle updatedVehicle = TestModelData.createDefaultTimedVehicle();
        updatedVehicle.setId(ID);
        updatedVehicle.setCapacity(null);

        mockMvc.perform(post(VEHICLE_EDIT_REQUEST, ID)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param(ID_PROPERTY, String.valueOf(ID))
                .sessionAttr(MODEL_ATTRIBUTE_VEHICLE, updatedVehicle))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("/WEB-INF/jsp/" + TimedVehicleController.VIEW_VEHICLE_ADD_OR_EDIT_FORM  + ".jsp"))
                .andExpect(model().attributeHasFieldErrors(MODEL_ATTRIBUTE_VEHICLE, CAPACITY_PROPERTY))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(ID_PROPERTY, is(ID))));

        verifyZeroInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void processVehicleEditForm_VehicleWithZeroCapacityGiven_ShouldContainValidationErrorAndForwardToForm()
            throws Exception {
        TimedVehicle updatedVehicle = TestModelData.createDefaultTimedVehicle();
        updatedVehicle.setId(ID);
        updatedVehicle.setCapacity(0);

        mockMvc.perform(post(VEHICLE_EDIT_REQUEST, ID)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param(ID_PROPERTY, String.valueOf(ID))
                .sessionAttr(MODEL_ATTRIBUTE_VEHICLE, updatedVehicle))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("/WEB-INF/jsp/" + TimedVehicleController.VIEW_VEHICLE_ADD_OR_EDIT_FORM  + ".jsp"))
                .andExpect(model().attributeHasFieldErrors(MODEL_ATTRIBUTE_VEHICLE, CAPACITY_PROPERTY))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(ID_PROPERTY, is(ID))));

        verifyZeroInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void displayVehicleAddForm_DepotRetrieved_ShouldSetDepotOfANewVehicleAndAddVehicleToModelAndReturnEditForm()
            throws Exception {
        TimeWindowedDepot depot = TestModelData.createDefaultTimeWindowedDepot();
        when(persistenceServiceMock.getDepot()).thenReturn(depot);

        mockMvc.perform(get("/vehicles/add"))
                .andExpect(status().isOk())
                .andExpect(view().name(TimedVehicleController.VIEW_VEHICLE_ADD_OR_EDIT_FORM))
                .andExpect(forwardedUrl("/WEB-INF/jsp/vehicles/addOrEditForm.jsp"))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(ID_PROPERTY, nullValue())))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(LICENSE_PROPERTY, nullValue())))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(CAPACITY_PROPERTY, Matchers.nullValue())))
                .andExpect(model().attribute(MODEL_ATTRIBUTE_VEHICLE, hasProperty(DEPOT_PROPERTY, is(depot))));

        verify(persistenceServiceMock, times(1)).getDepot();
        verifyNoMoreInteractions(persistenceServiceMock);
        verifyZeroInteractions(problemModificationEventSupportMock);
    }

    @Test
    public void processVehicleAddForm_ValidVehicleGiven_ShouldAddVehicleAndFireAddEventAndRedirect() throws Exception {
        TimedVehicle addedVehicle = TestModelData.createDefaultTimedVehicle();
        addedVehicle.setId(ID);

        when(persistenceServiceMock.saveVehicle(isA(TimedVehicle.class))).thenReturn(addedVehicle);

        mockMvc.perform(post("/vehicles/add")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param(ID_PROPERTY, String.valueOf(ID))
                .param(LICENSE_PROPERTY, TestModelData.LICENSE_PLATE)
                .param(CAPACITY_PROPERTY, String.valueOf(TestModelData.CAPACITY))
                .sessionAttr(MODEL_ATTRIBUTE_VEHICLE, addedVehicle))
                .andExpect(status().isFound())
                .andExpect(view().name(WebUtil.getRedirectedURL(VEHICLES_REQUEST, String.valueOf(addedVehicle.getId()))))
                .andExpect(flash().attribute(TimedVehicleController.FLASH_MESSAGE_KEY_FEEDBACK,
                        is(getMessage(TimedVehicleController.FEEDBACK_MESSAGE_KEY_VEHICLE_ADDED))));

        ArgumentCaptor<TimedVehicle> formVehicleObject = ArgumentCaptor.forClass(TimedVehicle.class);
        verify(persistenceServiceMock, times(1)).saveVehicle(formVehicleObject.capture());
        TimedVehicle returnedUpdatedVehicle = formVehicleObject.getValue();

        verifyNoMoreInteractions(persistenceServiceMock);
        verify(problemModificationEventSupportMock, times(1)).fireModificationEnded(null);
        verifyNoMoreInteractions(problemModificationEventSupportMock);
        assertEquals(TestModelData.LICENSE_PLATE, returnedUpdatedVehicle.getLicensePlate());
        assertEquals(TestModelData.CAPACITY, returnedUpdatedVehicle.getCapacity().intValue());
        // For now not checking time window equality because org.joda.time.DateTime does not equal itself after deserialization
        // assertEquals(TestModelData.createDefaultTimeWindow(), returnedUpdatedCustomer.getTimeWindow());
        assertEquals(addedVehicle, returnedUpdatedVehicle);
    }

}
