package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed;

import org.optaplanner.showcase.app.vehiclerouting.testdata.TestModelData;
import org.optaplanner.showcase.app.vehiclerouting.model.Vehicle;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TimedVehicleTest {

    public static final String ALTERNATIVE_LICENSE_PLATE = "ABC-999";

    @Test
    public void build_ThisAndBaseClassFieldsGiven_ShouldBuildTimedVehicleWithDepot() {
        TimeWindowedDepot timeWindowedDepot = TimeWindowedDepot.getBuilder()
                .createDepot();

        TimedVehicle vehicle = TimedVehicle.getBuilder()
                .withCapacity(TestModelData.CAPACITY)
                .withLicensePlate(TestModelData.LICENSE_PLATE)
                .withDepot(timeWindowedDepot)
                .build();

        assertNull(vehicle.getId());
        assertTrue(vehicle.getTimedDeliveries().isEmpty());
        assertEquals(Integer.valueOf(TestModelData.CAPACITY), vehicle.getCapacity());
        assertEquals(TestModelData.LICENSE_PLATE, vehicle.getLicensePlate());
        assertEquals(timeWindowedDepot, vehicle.getDepot());
    }

    @Test
    public void equals_IdenticalVehiclesGiven_ShouldReturnTrue() {
        TimeWindowedDepot timeWindowedDepot = TimeWindowedDepot.getBuilder()
                .createDepot();

        TimedVehicle vehicle1 = TimedVehicle.getBuilder()
                .withCapacity(TestModelData.CAPACITY)
                .withLicensePlate(TestModelData.LICENSE_PLATE)
                .withDepot(timeWindowedDepot)
                .build();

        TimedVehicle vehicle2 = TimedVehicle.getBuilder()
                .withCapacity(TestModelData.CAPACITY)
                .withLicensePlate(TestModelData.LICENSE_PLATE)
                .withDepot(timeWindowedDepot)
                .build();

        assertEquals(true, vehicle1.equals(vehicle2));
        assertEquals(true, vehicle2.equals(vehicle1));
    }

    @Test
    public void equals_NonIdenticalVehiclesGiven_ShouldReturnFalse() {
        TimeWindowedDepot timeWindowedDepot = TimeWindowedDepot.getBuilder()
                .createDepot();

        TimedVehicle vehicle1 = TimedVehicle.getBuilder()
                .withCapacity(TestModelData.CAPACITY)
                .withLicensePlate(TestModelData.LICENSE_PLATE)
                .withDepot(timeWindowedDepot)
                .build();

        TimedVehicle vehicle2 = TimedVehicle.getBuilder()
                .withCapacity(TestModelData.CAPACITY)
                .withLicensePlate(ALTERNATIVE_LICENSE_PLATE)
                .withDepot(timeWindowedDepot)
                .build();

        assertEquals(false, vehicle1.equals(vehicle2));
        assertEquals(false, vehicle2.equals(vehicle1));
    }

    @Test
    public void equals_ThisAndBaseVehiclesGiven_ShouldReturnFalse() {
        TimeWindowedDepot timeWindowedDepot = TimeWindowedDepot.getBuilder()
                .createDepot();

        TimedVehicle vehicle = TimedVehicle.getBuilder()
                .withCapacity(TestModelData.CAPACITY)
                .withLicensePlate(TestModelData.LICENSE_PLATE)
                .withDepot(timeWindowedDepot)
                .build();

        Vehicle baseVehicle = Vehicle.getBuilder()
                .withCapacity(TestModelData.CAPACITY)
                .withLicensePlate(TestModelData.LICENSE_PLATE)
                .withDepot(timeWindowedDepot)
                .build();

        assertEquals(false, vehicle.equals(baseVehicle));
        assertEquals(false, baseVehicle.equals(vehicle));
    }

}
