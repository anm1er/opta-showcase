package org.optaplanner.showcase.app.vehiclerouting.web.planning;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.ProblemDirector;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.SolverService;
import org.optaplanner.showcase.app.vehiclerouting.testdata.TestSolutionGeneratorUtility;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/test-planning-context-config.xml", "classpath:spring/test-mvc-config.xml"})
@WebAppConfiguration
@ActiveProfiles("planning")
public class RestPlannerControllerTest /*extends PlannerControllerTest*/ {

    public static final String ROOT = "/demo";
    public static final String APPLICATION_JSON_CONTENT_TYPE = "application/json";
    public static final int VEHICLES_NUMBER = 25;
    public static final int CUSTOMERS_NUMBER = 25;
    public static final int DELIVERIES_FOR_VEHICLE_1 = 6;
    public static final float DEPOT_LATITUDE = 40.0f;
    public static final float DEPOT_LONGITUDE = 50.0f;
    public static final float FIRST_CUSTOMER_LATITUDE = 45.0f;
    public static final float FIRST_CUSTOMER_LONGITUDE = 68.0f;
    public static final int FIRST_CUSTOMER_DEMAND = 10;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    @Qualifier("domain")
    private SolverService solverServiceMock;

    @Autowired
    private ProblemDirector problemDirectorMock;

    // Note: The way AbstractSolutionDao is currently implemented requires that data directory is explicitly set
    @BeforeClass
    public static void setUpDataDirectoryProperty() {
        TestSolutionGeneratorUtility.setUpDataDirProperty();
    }

    @Before
    public void setUp() {
        Mockito.reset(solverServiceMock, problemDirectorMock);
        RestAssuredMockMvc.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getScheduledData_UnsolvedSolutionGiven_ShouldSetUnsolvedSolution() throws Exception {
        VehicleRoutingSolution solution = TestSolutionGeneratorUtility.getUnsolvedSolomon_025_C101();
        when(solverServiceMock.getCurrentSolution()).thenReturn(solution);

        // The fluent api style is different (probably it should not be?) from the one used by Jayway guys
        given()
        .when()
                .get(ROOT + "/schedule")
        .then()
                .statusCode(HttpServletResponse.SC_OK)
                .contentType(APPLICATION_JSON_CONTENT_TYPE)
                .body("size()", equalTo(2))
                .body("vehicleDTOs.size()", equalTo(VEHICLES_NUMBER))
                .body("vehicleDTOs[0].depotLatitude", equalTo(DEPOT_LATITUDE))
                .body("vehicleDTOs[0].depotLongitude", equalTo(DEPOT_LONGITUDE))
                // checks that the customer list assigned by the planner is empty
                .body("vehicleDTOs[0].customerDTOs.size()", equalTo(0))
                .body("customerDTOs.size()", equalTo(CUSTOMERS_NUMBER))
                .body("customerDTOs[0].latitude", equalTo(FIRST_CUSTOMER_LATITUDE))
                .body("customerDTOs[0].longitude", equalTo(FIRST_CUSTOMER_LONGITUDE))
                .body("customerDTOs[0].demand", equalTo(FIRST_CUSTOMER_DEMAND));

        verify(solverServiceMock, times(1)).getCurrentSolution();
        verifyZeroInteractions(problemDirectorMock);
    }

    @Test
    public void getScheduledData_SolvedSolutionGiven_ShouldSetSolvedSolution() throws Exception {
        VehicleRoutingSolution solution = TestSolutionGeneratorUtility.getSolvedSolomon_025_C101();
        when(solverServiceMock.getCurrentSolution()).thenReturn(solution);

        given()
        .when()
                .get(ROOT + "/schedule")
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .contentType(APPLICATION_JSON_CONTENT_TYPE)
                .body("size()", equalTo(2))
                .body("vehicleDTOs.size()", equalTo(VEHICLES_NUMBER))
                .body("vehicleDTOs[0].depotLatitude", equalTo(DEPOT_LATITUDE))
                .body("vehicleDTOs[0].depotLongitude", equalTo(DEPOT_LONGITUDE))
                .body("vehicleDTOs[0].customerDTOs.size()", equalTo(DELIVERIES_FOR_VEHICLE_1))
                .body("customerDTOs.size()", equalTo(CUSTOMERS_NUMBER))
                .body("customerDTOs[0].latitude", equalTo(FIRST_CUSTOMER_LATITUDE))
                .body("customerDTOs[0].longitude", equalTo(FIRST_CUSTOMER_LONGITUDE))
                .body("customerDTOs[0].demand", equalTo(FIRST_CUSTOMER_DEMAND));

        verify(solverServiceMock, times(1)).getCurrentSolution();
        verifyZeroInteractions(problemDirectorMock);
    }

}
