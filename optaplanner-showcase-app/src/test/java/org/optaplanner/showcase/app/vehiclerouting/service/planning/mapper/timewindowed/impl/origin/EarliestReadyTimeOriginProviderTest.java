package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.impl.origin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.testdata.ArbitraryTimeStamp;
import org.optaplanner.showcase.app.vehiclerouting.testdata.TestModelData;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;

public class EarliestReadyTimeOriginProviderTest {

    public static final int READY_TIME = 1200;
    public static final int BEFORE_READY_TIME = 900;
    public static final int DUE_TIME = 1350;
    public static final DateTime ORIGIN = ArbitraryTimeStamp.getDateTime();

    private EarliestReadyTimeOriginProvider originProvider;

    @Before
    public void setUp() {
        originProvider = new EarliestReadyTimeOriginProvider();
    }

    @Test
    public void getTimeOriginFromDomainObjects_TimeWindowedObjectsGiven_ShouldReturnEarliestReadyTime() {
        TimeWindowedDepot depotA = TimeWindowedDepot.getBuilder()
                .withLocation(TestModelData.createDefaultLocationA())
                .withTimeWindow(TestModelData.createTimeWindow(READY_TIME, DUE_TIME, ORIGIN))
                .build();
        TimeWindowedDepot depotB = TimeWindowedDepot.getBuilder()
                .withLocation(TestModelData.createDefaultLocationB())
                .withTimeWindow(TestModelData.createTimeWindow(BEFORE_READY_TIME, DUE_TIME, ORIGIN))
                .build();
        List<TimeWindowedDepot> depots = Arrays.asList(depotA, depotB);

        DateTime origin = originProvider.getTimeOriginFromDomainObjects(depots);

        assertNotNull(origin);
        assertEquals(depotB.getTimeWindow().getReadyTime(), origin);
        assertSame(depotB.getTimeWindow().getReadyTime(), origin);
    }

    @Test
    public void getTimeOriginFromDomainObjects_TimeWindowedObjectsWithDuplicateReadyTimeGiven_ShouldReturnEither() {
        TimeWindowedDepot depotA = TimeWindowedDepot.getBuilder()
                .withLocation(TestModelData.createDefaultLocationA())
                .withTimeWindow(TestModelData.createTimeWindow(READY_TIME, DUE_TIME, ORIGIN))
                .build();
        TimeWindowedDepot depotB = TimeWindowedDepot.getBuilder()
                .withLocation(TestModelData.createDefaultLocationB())
                .withTimeWindow(TestModelData.createTimeWindow(BEFORE_READY_TIME, DUE_TIME, ORIGIN))
                .build();
        TimeWindowedDepot depotC = TimeWindowedDepot.getBuilder()
                .withLocation(TestModelData.createDefaultLocationB())
                .withTimeWindow(TestModelData.createTimeWindow(BEFORE_READY_TIME, DUE_TIME, ORIGIN))
                .build();
        List<TimeWindowedDepot> depots = Arrays.asList(depotA, depotB, depotC);

        DateTime origin = originProvider.getTimeOriginFromDomainObjects(depots);

        assertNotNull(origin);
        assertThat(origin, allOf(is(depotB.getTimeWindow().getReadyTime()),
                is(depotC.getTimeWindow().getReadyTime())));
        assertThat(origin, anyOf(sameInstance(depotB.getTimeWindow().getReadyTime()),
                sameInstance(depotC.getTimeWindow().getReadyTime())));
    }

    @Test
    public void getTimeOriginFromDomainObjects_SingleTimeWindowedObjectGiven_ShouldReturnReadyTime() {
        TimeWindowedDepot depot = TimeWindowedDepot.getBuilder()
                .withLocation(TestModelData.createDefaultLocationA())
                .withTimeWindow(TestModelData.createTimeWindow(READY_TIME, DUE_TIME, ORIGIN))
                .build();
        List<TimeWindowedDepot> depots = Arrays.asList(depot);

        DateTime origin = originProvider.getTimeOriginFromDomainObjects(depots);

        assertNotNull(origin);
        assertEquals(depot.getTimeWindow().getReadyTime(), origin);
        assertSame(depot.getTimeWindow().getReadyTime(), origin);
    }

    @Test
    public void getTimeOriginFromDomainObjects_EmptyCollectionGiven_ShouldReturnNull() {
        List<TimeWindowedDepot> depots = new ArrayList<>();

        DateTime origin = originProvider.getTimeOriginFromDomainObjects(depots);

        assertNull(origin);
    }

}
