package org.optaplanner.showcase.app.vehiclerouting.testdata;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Holds arbitrary time stamp in either {@link DateTime} or string format representation.
 */
public final class ArbitraryTimeStamp {

    public static final int MONTH = 12;
    public static final int DAY = 10;
    public static final int HOUR = 21;
    public static final int MINUTE = 20;
    private static final int YEAR = 2014;

    private static final String PATTERN = "yyyy/MM/dd H:mm";
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormat.forPattern(PATTERN);

    private static final DateTime DATE_TIME = new DateTime(YEAR, MONTH, DAY, HOUR, MINUTE);

    private ArbitraryTimeStamp() {
    }

    public static String getDateTimeString() {
        return TIME_FORMATTER.print(DATE_TIME);
    }

    public static DateTime getDateTime() {
        return DATE_TIME;
    }

}
