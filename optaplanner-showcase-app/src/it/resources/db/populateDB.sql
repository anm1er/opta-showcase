-- customers

insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (80, 53.12, 20.08, '2014-07-01 15:00:00', '2014-07-01 16:00:00', 55);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (65, 58.44, 20.60, '2014-07-01 11:00:00', '2014-07-01 13:30:00', 40);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (70, 55.40, 21.55, '2014-07-01 19:00:00', '2014-07-02 10:00:00', 60);

-- depot

insert into depots (latitude, longitude, ready_time, due_time) values (53.22, 20.25, '2014-07-01 10:00:00', '2014-07-02 22:00:00');

-- vehicles

insert into vehicles (capacity, license_plate, depot_id) values (150, 'ABZ-075', 1);
insert into vehicles (capacity, license_plate, depot_id) values (150, 'YRI-298', 1);

-- pre-existing solution

insert into deliveries (arrival_time, vehicle_id, customer_id) values ('2014-07-01 15:30:00', 1, 1);
insert into deliveries (arrival_time, vehicle_id, customer_id) values ('2014-07-01 11:40:00', 2, 2);
insert into deliveries (arrival_time, vehicle_id, customer_id) values ('2014-07-01 19:00:00', 1, 3);
