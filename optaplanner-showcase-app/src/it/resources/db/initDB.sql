
drop table customers if exists;

drop table deliveries if exists;

drop table depots if exists;

drop table vehicles if exists;

create table customers (
    id bigint generated by default as identity,
    demand integer not null check (demand>=1),
    latitude double not null,
    longitude double not null,
    name varchar(255),
    service_duration integer not null check (service_duration>=1),
    due_time timestamp not null,
    ready_time timestamp not null,
    primary key (id)
);

create table deliveries (
    id bigint generated by default as identity,
    arrival_time timestamp,
    customer_id bigint not null,
    vehicle_id bigint not null,
    primary key (id)
);

create table depots (
    id bigint generated by default as identity,
    latitude double not null,
    longitude double not null,
    name varchar(255),
    due_time timestamp not null,
    ready_time timestamp not null,
    primary key (id)
);

create table vehicles (
    id bigint generated by default as identity,
    capacity integer not null check (capacity>=50),
    license_plate varchar(7) not null,
    depot_id bigint not null,
    primary key (id)
);

alter table customers
    add constraint UK_n6yy53finm6kwtjsuehcx9fqx unique (latitude, longitude);

alter table deliveries
    add constraint UK_ssshbn2psyhuqeo3ast9ibjfq unique (vehicle_id, customer_id);

alter table depots
    add constraint UK_hfro3oxhdbk7vv7kc66lr7o8a unique (latitude, longitude);

alter table vehicles
    add constraint UK_9vovnbiegxevdhqfcwvp2g8pj unique (license_plate);

alter table deliveries
    add constraint FK_mkbasoaamsg6wc8kwstf8gcoh
    foreign key (customer_id)
    references customers;

alter table deliveries
    add constraint FK_oh8fxxr81iuhkq3jt6gwogiww
    foreign key (vehicle_id)
    references vehicles;

alter table vehicles
    add constraint FK_3h3c65441pu8mgyqpxrqqe44e
    foreign key (depot_id)
    references depots;
