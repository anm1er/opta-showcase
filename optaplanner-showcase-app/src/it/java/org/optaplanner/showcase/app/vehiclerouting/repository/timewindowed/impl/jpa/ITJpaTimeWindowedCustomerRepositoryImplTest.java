package org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.impl.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedDelivery;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.TimeWindowedCustomerRepository;

import org.hibernate.LazyInitializationException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/test-persistence-config.xml"})
public class ITJpaTimeWindowedCustomerRepositoryImplTest {

    public static final long CUSTOMER_ID_1 = 1L;
    public static final double CUSTOMER_LATITUDE_1 = 53.12;
    public static final double CUSTOMER_LONGITUDE_1 = 20.08;
    public static final String CUSTOMER_ARRIVAL_TIME_1 = "01/07/2014 15:30:00";
    public static final String CUSTOMER_1_SERVICING_VEHICLE_LIC = "ABZ-075";

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");

    @Autowired
    private TimeWindowedCustomerRepository repository;

    @Test(expected = LazyInitializationException.class)
    public void findOne_ScheduleDataOfDetachedCustomerAccessed_ShouldThrowLazyInitializationException() {
        // Given
        TimeWindowedCustomer customer = repository.findOne(CUSTOMER_ID_1);
        repository.clearPersistenceContext();
        // When
        customer.getTimedDeliveries().size();
        // Then
        // LazyInitializationException thrown
    }

    @Test
    public void findOneWithScheduledData_ScheduleDataOfDetachedCustomerAccessed_AssociationShouldSurviveClearingContext() {
        // Given
        TimedDelivery scheduledDelivery = TimedDelivery.getBuilder()
                .withArrivalTime(DateTime.parse(CUSTOMER_ARRIVAL_TIME_1, DATE_TIME_FORMATTER))
                .withCustomer(TimeWindowedCustomer.getBuilder()
                        .withLocation(Location.getBuilder()
                                .withLatitude(CUSTOMER_LATITUDE_1)
                                .withLongitude(CUSTOMER_LONGITUDE_1)
                                .build())
                        .build())
                .withVehicle(TimedVehicle.getBuilder()
                        .withLicensePlate(CUSTOMER_1_SERVICING_VEHICLE_LIC)
                        .build())
                .build();
        TimeWindowedCustomer customer = repository.findOneWithScheduledData(CUSTOMER_ID_1);
        repository.clearPersistenceContext();
        // When
        int deliveriesSize = customer.getTimedDeliveries().size();
        // Then
        assertEquals(1, deliveriesSize);
        assertThat(customer, allOf(
                hasProperty("id", is(CUSTOMER_ID_1)),
                hasProperty("location", is(Location.getBuilder()
                        .withLatitude(CUSTOMER_LATITUDE_1)
                        .withLongitude(CUSTOMER_LONGITUDE_1)
                        .build())),
                hasProperty("timedDeliveries", hasItem(scheduledDelivery))
        ));
    }

}
