package org.optaplanner.showcase.app.vehiclerouting.service.persistence.timewindowed.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException;
import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindow;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedDelivery;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.service.persistence.timewindowed.TimeWindowedPersistenceService;
import org.optaplanner.showcase.app.vehiclerouting.testdata.TestModelData;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/test-persistence-config.xml"})
public class ITTimeWindowedPersistenceServiceImplTest {

    public static final long NON_EXISTING_ID = 10L;
    public static final long ADDED_CUSTOMER_ID = 4L;
    public static final long ADDED_VEHICLE_ID = 3L;
    public static final long ADDED_DEPOT_ID = 2L;
    public static final int NUMBER_OF_CUSTOMERS = 3;
    public static final int NUMBER_OF_VEHICLES = 2;
    public static final int NUMBER_OF_DEPOTS = 1;

    public static final long CUSTOMER_ID_1 = 1L;
    public static final int CUSTOMER_DEMAND_1 = 80;
    public static final double CUSTOMER_LATITUDE_1 = 53.12;
    public static final double CUSTOMER_LONGITUDE_1 = 20.08;
    public static final int CUSTOMER_SERVICE_DURATION_1 = 55;
    public static final String CUSTOMER_READY_TIME_1 = "01/07/2014 15:00:00";
    public static final String CUSTOMER_DUE_TIME_1 = "01/07/2014 16:00:00";
    public static final String CUSTOMER_ARRIVAL_TIME_1 = "01/07/2014 15:30:00";
    public static final String CUSTOMER_ALT_ARRIVAL_TIME_1 = "01/07/2014 15:45:00";
    public static final String CUSTOMER_1_SERVICING_VEHICLE_LIC = "ABZ-075";

    public static final long CUSTOMER_ID_2 = 2L;
    public static final int CUSTOMER_DEMAND_2 = 65;
    public static final double CUSTOMER_LATITUDE_2 = 58.44;
    public static final double CUSTOMER_LONGITUDE_2 = 20.60;
    private static final String CUSTOMER_ARRIVAL_TIME_2 = "01/07/2014 11:40:00";
    private static final String CUSTOMER_ALT_ARRIVAL_TIME_2 = "01/07/2014 12:00:00";

    public static final long CUSTOMER_ID_3 = 3L;
    public static final int CUSTOMER_DEMAND_3 = 70;
    public static final double CUSTOMER_LATITUDE_3 = 55.40;
    public static final double CUSTOMER_LONGITUDE_3 = 21.55;
    private static final String CUSTOMER_ARRIVAL_TIME_3 = "01/07/2014 19:00:00";
    private static final String CUSTOMER_ALT_ARRIVAL_TIME_3 = "01/07/2014 21:00:00";

    public static final long VEHICLE_ID_1 = 1L;
    public static final String VEHICLE_LIC_1 = "ABZ-075";
    public static final int VEHICLE_CAPACITY_1 = 150;

    public static final long VEHICLE_ID_2 = 2L;
    public static final String VEHICLE_LIC_2 = "YRI-298";
    public static final int VEHICLE_CAPACITY_2 = 150;

    public static final long DEPOT_ID_1 = 1L;
    public static final double DEPOT_LATITUDE_1 = 53.22;
    public static final double DEPOT_LONGITUDE_1 = 20.25;
    public static final String DEPOT_READY_TIME_1 = "01/07/2014 10:00:00";
    public static final String DEPOT_DUE_TIME_1 = "02/07/2014 22:00:00";

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");

    @Autowired
    private TimeWindowedPersistenceService persistenceService;

    @Test
    public void getCustomer_NonExistingCustomerIdGiven_ShouldThrowResourceNotFoundException() {
        // When
        catchException(persistenceService).getCustomer(NON_EXISTING_ID);
        // Then
        assertThat(caughtException())
                .isExactlyInstanceOf(ResourceNotFoundException.class)
                .hasMessage("No customer found with id: " + NON_EXISTING_ID);
    }

    @Test
    public void getCustomer_CustomerIdGiven_ShouldReturnCustomerWithScheduleData() {
        // Given
        TimedDelivery scheduledDelivery = TimedDelivery.getBuilder()
                .withArrivalTime(DateTime.parse(CUSTOMER_ARRIVAL_TIME_1, DATE_TIME_FORMATTER))
                .withCustomer(TimeWindowedCustomer.getBuilder()
                        .withLocation(Location.getBuilder()
                                .withLatitude(CUSTOMER_LATITUDE_1)
                                .withLongitude(CUSTOMER_LONGITUDE_1)
                                .build())
                        .build())
                .withVehicle(TimedVehicle.getBuilder()
                        .withLicensePlate(CUSTOMER_1_SERVICING_VEHICLE_LIC)
                        .build())
                .build();
        // When
        TimeWindowedCustomer customer = persistenceService.getCustomer(CUSTOMER_ID_1);
        // Then
        assertThat(customer, allOf(
                hasProperty("id", is(CUSTOMER_ID_1)),
                hasProperty("location", is(Location.getBuilder()
                        .withLatitude(CUSTOMER_LATITUDE_1)
                        .withLongitude(CUSTOMER_LONGITUDE_1)
                        .build())),
                hasProperty("timedDeliveries", hasItem(scheduledDelivery))
        ));
    }

    @Test
    public void getCustomer_CustomerLocationGiven_ShouldReturnCustomerWithScheduleData() {
        // Given
        Location customerLocation = Location.getBuilder()
                .withLatitude(CUSTOMER_LATITUDE_1)
                .withLongitude(CUSTOMER_LONGITUDE_1)
                .build();
        TimedDelivery scheduledDelivery = TimedDelivery.getBuilder()
                .withArrivalTime(DateTime.parse(CUSTOMER_ARRIVAL_TIME_1, DATE_TIME_FORMATTER))
                .withCustomer(TimeWindowedCustomer.getBuilder()
                        .withLocation(customerLocation)
                        .build())
                .withVehicle(TimedVehicle.getBuilder()
                        .withLicensePlate(CUSTOMER_1_SERVICING_VEHICLE_LIC)
                        .build())
                .build();
        // When
        TimeWindowedCustomer customer = persistenceService.getCustomerByLocation(customerLocation);
        // Then
        assertThat(customer, allOf(
                hasProperty("id", is(CUSTOMER_ID_1)),
                hasProperty("location", is(customerLocation)),
                hasProperty("timedDeliveries", hasItem(scheduledDelivery))
        ));
    }

    @Test
    public void getCustomers_ThreeCustomersFound_ShouldReturnAListOfThreeCustomers() {
        // When
        List<TimeWindowedCustomer> customers = persistenceService.getCustomers();
        // Then
        assertThat(customers.size(), is(NUMBER_OF_CUSTOMERS));
        assertThat(customers, contains(
                allOf(
                        hasProperty("id", is(CUSTOMER_ID_1)),
                        hasProperty("demand", is(CUSTOMER_DEMAND_1)),
                        hasProperty("location", is(Location.getBuilder()
                                .withLatitude(CUSTOMER_LATITUDE_1)
                                .withLongitude(CUSTOMER_LONGITUDE_1)
                                .build()))
                ),
                allOf(
                        hasProperty("id", is(CUSTOMER_ID_2)),
                        hasProperty("demand", is(CUSTOMER_DEMAND_2)),
                        hasProperty("location", is(Location.getBuilder()
                                .withLatitude(CUSTOMER_LATITUDE_2)
                                .withLongitude(CUSTOMER_LONGITUDE_2)
                                .build()))
                ),
                allOf(
                        hasProperty("id", is(CUSTOMER_ID_3)),
                        hasProperty("demand", is(CUSTOMER_DEMAND_3)),
                        hasProperty("location", is(Location.getBuilder()
                                .withLatitude(CUSTOMER_LATITUDE_3)
                                .withLongitude(CUSTOMER_LONGITUDE_3)
                                .build()))
                )
        ));
    }

    @Test
    public void saveCustomer_NewCustomerGiven_ShouldSaveCustomerAndDeleteAllDeliveries() {
        // Given
        TimeWindowedCustomer customer = TestModelData.createDefaultTimeWindowedCustomer();
        // When
        TimeWindowedCustomer savedCustomer = persistenceService.saveCustomer(customer);
        // Then
        assertEquals(customer, savedCustomer);
        assertNotNull(savedCustomer.getId());
        assertEquals(ADDED_CUSTOMER_ID, savedCustomer.getId().longValue());
        assertEquals(persistenceService.getCustomers().size(), NUMBER_OF_CUSTOMERS + 1);
        assertTrue(persistenceService.getDeliveries().isEmpty());
    }

    @Test
    public void deleteCustomer_CustomerIdGiven_ShouldDeleteCustomerAndAllDeliveries() {
        // When
        persistenceService.deleteCustomer(CUSTOMER_ID_1);
        // Then
        assertEquals(persistenceService.getCustomers().size(), NUMBER_OF_CUSTOMERS - 1);
        assertTrue(persistenceService.getDeliveries().isEmpty());
    }

    @Test
    public void deleteCustomer_NonExistingCustomerIdGiven_ShouldThrowResourceNotFoundException() {
        // When
        catchException(persistenceService).deleteCustomer(NON_EXISTING_ID);
        // Then
        assertThat(caughtException())
                .isExactlyInstanceOf(ResourceNotFoundException.class)
                .hasMessage("No customer found with id: " + NON_EXISTING_ID);
    }

    @Test
    public void deleteCustomers_ShouldDeleteAllCustomersAndDeliveries() {
        // When
        persistenceService.deleteCustomers();
        // Then
        assertTrue(persistenceService.getCustomers().isEmpty());
        assertTrue(persistenceService.getDeliveries().isEmpty());
    }

    @Test
    public void getVehicle_NonExistingVehicleIdGiven_ShouldThrowResourceNotFoundException() {
        // When
        catchException(persistenceService).getVehicle(NON_EXISTING_ID);
        // Then
        assertThat(caughtException())
                .isExactlyInstanceOf(ResourceNotFoundException.class)
                .hasMessage("No vehicle found with id: " + NON_EXISTING_ID);
    }

    @Test
    public void getVehicle_VehicleIdGiven_ShouldReturnVehicleWithScheduleData() {
        // Given
        TimedVehicle servicingVehicle = TimedVehicle.getBuilder()
                .withLicensePlate(VEHICLE_LIC_1)
                .build();
        // When
        TimedVehicle vehicle = persistenceService.getVehicle(VEHICLE_ID_1);
        // Then
        assertThat(vehicle, allOf(
                hasProperty("id", is(VEHICLE_ID_1)),
                hasProperty("licensePlate", is(VEHICLE_LIC_1)),
                hasProperty("timedDeliveries", contains(
                        allOf(
                                hasProperty("customer", is(TimeWindowedCustomer.getBuilder()
                                        .withLocation(Location.getBuilder()
                                                .withLatitude(CUSTOMER_LATITUDE_1)
                                                .withLongitude(CUSTOMER_LONGITUDE_1)
                                                .build())
                                        .build())),
                                hasProperty("vehicle", is(servicingVehicle)),
                                hasProperty("arrivalTime", is(DateTime.parse(CUSTOMER_ARRIVAL_TIME_1, DATE_TIME_FORMATTER)))
                        ),
                        allOf(
                                hasProperty("customer", is(TimeWindowedCustomer.getBuilder()
                                        .withLocation(Location.getBuilder()
                                                .withLatitude(CUSTOMER_LATITUDE_3)
                                                .withLongitude(CUSTOMER_LONGITUDE_3)
                                                .build())
                                        .build())),
                                hasProperty("vehicle", is(servicingVehicle)),
                                hasProperty("arrivalTime", is(DateTime.parse(CUSTOMER_ARRIVAL_TIME_3, DATE_TIME_FORMATTER)))
                        )
                ))
        ));
    }

    @Test
    public void getVehicle_VehicleLicGiven_ShouldReturnVehicleWithScheduleData() {
        // Given
        TimedVehicle servicingVehicle = TimedVehicle.getBuilder()
                .withLicensePlate(VEHICLE_LIC_1)
                .build();
        // When
        TimedVehicle vehicle = persistenceService.getVehicleByLicensePlate(VEHICLE_LIC_1);
        // Then
        assertThat(vehicle, allOf(
                hasProperty("id", is(VEHICLE_ID_1)),
                hasProperty("licensePlate", is(VEHICLE_LIC_1)),
                hasProperty("timedDeliveries", contains(
                        allOf(
                                hasProperty("customer", is(TimeWindowedCustomer.getBuilder()
                                        .withLocation(Location.getBuilder()
                                                .withLatitude(CUSTOMER_LATITUDE_1)
                                                .withLongitude(CUSTOMER_LONGITUDE_1)
                                                .build())
                                        .build())),
                                hasProperty("vehicle", is(servicingVehicle)),
                                hasProperty("arrivalTime", is(DateTime.parse(CUSTOMER_ARRIVAL_TIME_1, DATE_TIME_FORMATTER)))
                        ),
                        allOf(
                                hasProperty("customer", is(TimeWindowedCustomer.getBuilder()
                                        .withLocation(Location.getBuilder()
                                                .withLatitude(CUSTOMER_LATITUDE_3)
                                                .withLongitude(CUSTOMER_LONGITUDE_3)
                                                .build())
                                        .build())),
                                hasProperty("vehicle", is(servicingVehicle)),
                                hasProperty("arrivalTime", is(DateTime.parse(CUSTOMER_ARRIVAL_TIME_3, DATE_TIME_FORMATTER)))
                        )
                ))
        ));
    }

    @Test
    public void getVehicles_TwoVehiclesFound_ShouldReturnAListOfTwoVehicles() {
        // Given
        TimeWindowedDepot depot = TimeWindowedDepot.getBuilder()
                .withLocation(Location.getBuilder()
                        .withLatitude(DEPOT_LATITUDE_1)
                        .withLongitude(DEPOT_LONGITUDE_1)
                        .build())
                .build();
        // When
        List<TimedVehicle> vehicles = persistenceService.getVehicles();
        // Then
        assertThat(vehicles.size(), is(NUMBER_OF_VEHICLES));
        assertThat(vehicles, contains(
                allOf(
                        hasProperty("id", is(VEHICLE_ID_1)),
                        hasProperty("licensePlate", is(VEHICLE_LIC_1)),
                        hasProperty("capacity", is(VEHICLE_CAPACITY_1)),
                        hasProperty("depot", is(depot))
                ),
                allOf(
                        hasProperty("id", is(VEHICLE_ID_2)),
                        hasProperty("licensePlate", is(VEHICLE_LIC_2)),
                        hasProperty("capacity", is(VEHICLE_CAPACITY_2)),
                        hasProperty("depot", is(depot))
                )
        ));
    }

    @Test
    @Ignore
    public void saveVehicle_NewVehicleWithPersistentDepotGiven_ShouldSaveVehicleAndDeleteAllDeliveries() {
        // Given
        List<TimeWindowedDepot> depots = persistenceService.getDepots();
        assertTrue(depots.size() > 0);
        TimedVehicle vehicle = TimedVehicle.getBuilder()
                .withLicensePlate(TestModelData.LICENSE_PLATE)
                .withCapacity(VEHICLE_CAPACITY_1)
                .withDepot(depots.get(0))
                .build();
        // When
        TimedVehicle savedVehicle = persistenceService.saveVehicle(vehicle);
        // Then
        assertEquals(vehicle, savedVehicle);
        assertNotNull(savedVehicle.getId());
        assertEquals(ADDED_VEHICLE_ID, savedVehicle.getId().longValue());
        assertEquals(persistenceService.getVehicles().size(), NUMBER_OF_VEHICLES + 1);
        assertTrue(persistenceService.getDeliveries().isEmpty());
    }

    @Test
    public void deleteVehicle_VehicleIdGiven_ShouldDeleteVehicleAndAllDeliveries() {
        // When
        persistenceService.deleteVehicle(VEHICLE_ID_1);
        // Then
        assertEquals(persistenceService.getVehicles().size(), NUMBER_OF_VEHICLES - 1);
        assertTrue(persistenceService.getDeliveries().isEmpty());
    }

    @Test
    public void deleteVehicle_NonExistingVehicleIdGiven_ShouldThrowResourceNotFoundException() {
        // When
        catchException(persistenceService).deleteVehicle(NON_EXISTING_ID);
        // Then
        assertThat(caughtException())
                .isExactlyInstanceOf(ResourceNotFoundException.class)
                .hasMessage("No vehicle found with id: " + NON_EXISTING_ID);
    }

    @Test
    public void deleteVehicles_ShouldDeleteAllVehiclesAndDeliveries() {
        // When
        persistenceService.deleteVehicles();
        // Then
        assertTrue(persistenceService.getVehicles().isEmpty());
        assertTrue(persistenceService.getDeliveries().isEmpty());
    }

    @Test
    public void getDepot_DepotIdGiven_ShouldReturnDepot() {
        // When
        TimeWindowedDepot depot = persistenceService.getDepot(DEPOT_ID_1);
        // Then
        assertThat(depot, allOf(
                hasProperty("id", is(DEPOT_ID_1)),
                hasProperty("location", is(Location.getBuilder()
                        .withLatitude(DEPOT_LATITUDE_1)
                        .withLongitude(DEPOT_LONGITUDE_1)
                        .build())),
                hasProperty("timeWindow", is(new TimeWindow(DateTime.parse(DEPOT_READY_TIME_1, DATE_TIME_FORMATTER),
                        DateTime.parse(DEPOT_DUE_TIME_1, DATE_TIME_FORMATTER))))
        ));
    }

    @Test
    public void getDepot_NonExistingDepotIdGiven_ShouldThrowResourceNotFoundException() {
        // When
        catchException(persistenceService).getDepot(NON_EXISTING_ID);
        // Then
        assertThat(caughtException())
                .isExactlyInstanceOf(ResourceNotFoundException.class)
                .hasMessage("No depot found with id: " + NON_EXISTING_ID);
    }

    @Test
    public void getDepots_OneDepotFound_ShouldReturnAListOfOneDepot() {
        // When
        List<TimeWindowedDepot> depots = persistenceService.getDepots();
        // Then
        assertThat(depots.size(), is(NUMBER_OF_DEPOTS));
        assertThat(depots, contains(
                allOf(
                        hasProperty("id", is(DEPOT_ID_1)),
                        hasProperty("location", is(Location.getBuilder()
                                .withLatitude(DEPOT_LATITUDE_1)
                                .withLongitude(DEPOT_LONGITUDE_1)
                                .build())),
                        hasProperty("timeWindow", is(new TimeWindow(DateTime.parse(DEPOT_READY_TIME_1, DATE_TIME_FORMATTER),
                                DateTime.parse(DEPOT_DUE_TIME_1, DATE_TIME_FORMATTER))))

                )));
    }

    @Test
    public void saveDepot_NewDepotGiven_ShouldSaveDepotAndDeleteAllDeliveries() {
        // Given
        TimeWindowedDepot depot = TestModelData.createDefaultTimeWindowedDepot();
        // When
        TimeWindowedDepot savedDepot = persistenceService.saveDepot(depot);
        // Then
        assertEquals(depot, savedDepot);
        assertNotNull(savedDepot.getId());
        assertEquals(ADDED_DEPOT_ID, savedDepot.getId().longValue());
        assertEquals(persistenceService.getDepots().size(), NUMBER_OF_DEPOTS + 1);
        assertTrue(persistenceService.getDeliveries().isEmpty());
    }

    @Test
    public void deleteDepot_DepotIdGiven_ShouldDeleteDepotAndAllDeliveries() {
        // When
        persistenceService.deleteDepot(DEPOT_ID_1);
        // Then
        assertTrue(persistenceService.getDeliveries().isEmpty());
        assertTrue(persistenceService.getDepots().isEmpty());
    }

    @Test
    public void deleteDepot_NonExistingDepotIdGiven_ShouldThrowResourceNotFoundException() {
        // When
        catchException(persistenceService).deleteDepot(NON_EXISTING_ID);
        // Then
        assertThat(caughtException())
                .isExactlyInstanceOf(ResourceNotFoundException.class)
                .hasMessage("No depot found with id: " + NON_EXISTING_ID);
    }

    @Test
    public void deleteDepots_ShouldDeleteAllDepotsAndDeliveries() {
        // When
        persistenceService.deleteCustomers();
        // Then
        assertTrue(persistenceService.getCustomers().isEmpty());
        assertTrue(persistenceService.getDeliveries().isEmpty());
    }

    @Test
    @Ignore
    public void getDeliveries_ThreeDeliveriesFound_ShouldReturnAListOfThreeDeliveries() {
        // Given
        TimedVehicle servicingVehicle = TimedVehicle.getBuilder()
                .withLicensePlate(VEHICLE_LIC_1)
                .build();
        // When
        List<TimedDelivery> deliveries = persistenceService.getDeliveries();
        // Then
        assertThat(deliveries.size(), is(NUMBER_OF_CUSTOMERS));
        assertThat(deliveries, contains(
                allOf(
                        hasProperty("customer", is(TimeWindowedCustomer.getBuilder()
                                .withLocation(Location.getBuilder()
                                        .withLatitude(CUSTOMER_LATITUDE_1)
                                        .withLongitude(CUSTOMER_LONGITUDE_1)
                                        .build())
                                .build())),
                        hasProperty("vehicle", is(servicingVehicle)),
                        hasProperty("arrivalTime", is(DateTime.parse(CUSTOMER_ARRIVAL_TIME_1, DATE_TIME_FORMATTER)))
                ),
                allOf(
                        hasProperty("customer", is(TimeWindowedCustomer.getBuilder()
                                .withLocation(Location.getBuilder()
                                        .withLatitude(CUSTOMER_LATITUDE_2)
                                        .withLongitude(CUSTOMER_LONGITUDE_2)
                                        .build())
                                .build())),
                        hasProperty("vehicle", is(TimedVehicle.getBuilder()
                                .withLicensePlate(VEHICLE_LIC_2)
                                .build())),
                        hasProperty("arrivalTime", is(DateTime.parse(CUSTOMER_ARRIVAL_TIME_2, DATE_TIME_FORMATTER)))
                ),
                allOf(
                        hasProperty("customer", is(TimeWindowedCustomer.getBuilder()
                                .withLocation(Location.getBuilder()
                                        .withLatitude(CUSTOMER_LATITUDE_3)
                                        .withLongitude(CUSTOMER_LONGITUDE_3)
                                        .build())
                                .build())),
                        hasProperty("vehicle", is(servicingVehicle)),
                        hasProperty("arrivalTime", is(DateTime.parse(CUSTOMER_ARRIVAL_TIME_3, DATE_TIME_FORMATTER)))
                )
        ));
    }

    @Test
    public void saveAllDeliveries_TimedDeliveriesListGiven_ShouldSaveDeliveriesReferencingPersistentEntities() {
        // Given
        persistenceService.deleteDeliveries();
        assertTrue(persistenceService.getDeliveries().isEmpty());
        List<TimedDelivery> deliveries = Arrays.asList(
                buildTimedDelivery(CUSTOMER_LATITUDE_1, CUSTOMER_LONGITUDE_1, VEHICLE_LIC_1, CUSTOMER_ALT_ARRIVAL_TIME_1),
                buildTimedDelivery(CUSTOMER_LATITUDE_2, CUSTOMER_LONGITUDE_2, VEHICLE_LIC_1, CUSTOMER_ALT_ARRIVAL_TIME_2),
                buildTimedDelivery(CUSTOMER_LATITUDE_3, CUSTOMER_LONGITUDE_3, VEHICLE_LIC_2, CUSTOMER_ALT_ARRIVAL_TIME_3)
        );
        TimedDelivery newDeliveryForCustomer3 = buildTimedDelivery(
                CUSTOMER_LATITUDE_3, CUSTOMER_LONGITUDE_3, VEHICLE_LIC_2, CUSTOMER_ALT_ARRIVAL_TIME_2);
        // When
        List<TimedDelivery> savedDeliveries = persistenceService.saveAllDeliveries(deliveries);
        TimeWindowedCustomer customer = persistenceService.getCustomer(CUSTOMER_ID_3);
        TimedVehicle vehicle = persistenceService.getVehicle(VEHICLE_ID_2);
        // Then
        assertEquals(deliveries.size(), savedDeliveries.size());
        assertThat(customer.getTimedDeliveries(), contains(newDeliveryForCustomer3));
        assertThat(vehicle.getTimedDeliveries(), contains(newDeliveryForCustomer3));
    }

    private TimedDelivery buildTimedDelivery(double latitude, double longitude, String license, String arrivalTimeString) {
        return TimedDelivery.getBuilder()
                .withCustomer(TimeWindowedCustomer.getBuilder()
                        .withLocation(Location.getBuilder()
                                .withLatitude(latitude)
                                .withLongitude(longitude)
                                .build())
                        .build())
                .withVehicle(TimedVehicle.getBuilder()
                        .withLicensePlate(license)
                        .build())
                .withArrivalTime(DateTime.parse(arrivalTimeString, DATE_TIME_FORMATTER))
                .build();
    }

}
