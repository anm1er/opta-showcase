package org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.impl.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.TimedVehicleRepository;

import org.hibernate.LazyInitializationException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/test-persistence-config.xml"})
public class ITJpaTimedVehicleCustomerRepositoryImplTest {

    public static final long VEHICLE_ID_1 = 1L;
    public static final String VEHICLE_LIC_1 = "ABZ-075";

    public static final double CUSTOMER_LATITUDE_1 = 53.12;
    public static final double CUSTOMER_LONGITUDE_1 = 20.08;
    public static final String CUSTOMER_ARRIVAL_TIME_1 = "01/07/2014 15:30:00";

    public static final double CUSTOMER_LATITUDE_3 = 55.40;
    public static final double CUSTOMER_LONGITUDE_3 = 21.55;
    private static final String CUSTOMER_ARRIVAL_TIME_3 = "01/07/2014 19:00:00";

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");

    @Autowired
    private TimedVehicleRepository repository;

    @Test(expected = LazyInitializationException.class)
    public void findOne_ScheduledDataOfDetachedVehicleAccessed_ShouldThrowLazyInitializationException() {
        // When
        TimedVehicle vehicle = repository.findOne(VEHICLE_ID_1);
        repository.clearPersistenceContext();
        // When
        vehicle.getTimedDeliveries().size();
        // Then
        // LazyInitializationException thrown
    }

    @Test
    public void findOneWithScheduledData_ScheduledDataOfDetachedVehicleAccessed_AssociationShouldSurviveClearingContext() {
        // Given
        TimedVehicle servicingVehicle = TimedVehicle.getBuilder()
                .withLicensePlate(VEHICLE_LIC_1)
                .build();
        TimedVehicle vehicle = repository.findOneWithScheduledData(VEHICLE_ID_1);
        repository.clearPersistenceContext();
        // When
        int deliveriesSize = vehicle.getTimedDeliveries().size();
        // Then
        assertEquals(2, deliveriesSize);
        assertThat(vehicle, allOf(
                hasProperty("id", is(VEHICLE_ID_1)),
                hasProperty("licensePlate", is(VEHICLE_LIC_1)),
                hasProperty("timedDeliveries", contains(
                        allOf(
                                hasProperty("customer", is(TimeWindowedCustomer.getBuilder()
                                        .withLocation(Location.getBuilder()
                                                .withLatitude(CUSTOMER_LATITUDE_1)
                                                .withLongitude(CUSTOMER_LONGITUDE_1)
                                                .build())
                                        .build())),
                                hasProperty("vehicle", is(servicingVehicle)),
                                hasProperty("arrivalTime", is(DateTime.parse(CUSTOMER_ARRIVAL_TIME_1, DATE_TIME_FORMATTER)))
                        ),
                        allOf(
                                hasProperty("customer", is(TimeWindowedCustomer.getBuilder()
                                        .withLocation(Location.getBuilder()
                                                .withLatitude(CUSTOMER_LATITUDE_3)
                                                .withLongitude(CUSTOMER_LONGITUDE_3)
                                                .build())
                                        .build())),
                                hasProperty("vehicle", is(servicingVehicle)),
                                hasProperty("arrivalTime", is(DateTime.parse(CUSTOMER_ARRIVAL_TIME_3, DATE_TIME_FORMATTER)))
                        )
                ))
        ));
    }

}
