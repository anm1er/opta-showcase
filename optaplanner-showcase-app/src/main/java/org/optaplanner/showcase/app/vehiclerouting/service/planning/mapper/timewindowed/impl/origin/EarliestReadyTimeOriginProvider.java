package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.impl.origin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowed;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.comparator.ReadyTimeComparator;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.origin.TimeOriginProvider;

import org.joda.time.DateTime;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Provides time origin based on the {@code readyTime} of a {@code TimeWindowed} domain object.
 * This complies with the logic of the standard Solomon and Homberger data sets,
 * where '0' refers to the single depot's ready time.
 */
@Component
public class EarliestReadyTimeOriginProvider implements TimeOriginProvider {

    @Override
    public <T extends TimeWindowed> DateTime getTimeOriginFromDomainObject(T domainObject) {
        checkNotNull(domainObject, "domainObject can't be null");
        checkNotNull(domainObject.getTimeWindow(), "timeWindow can't be null");
        return domainObject.getTimeWindow().getReadyTime();
    }

    @Override
    public DateTime getTimeOriginFromDomainObjects(Collection<? extends TimeWindowed> domainObjects) {
        checkNotNull(domainObjects, "domainObjects can't be null");
        DateTime dateTime = null;
        if (!domainObjects.isEmpty()) {
            if (domainObjects.size() == 1) {
                return getTimeOriginFromDomainObject(domainObjects.iterator().next());
            }
            List<TimeWindowed> timeWindowedList = new ArrayList<>(domainObjects);
            // Picks the earliest ready time
            Collections.sort(timeWindowedList, new ReadyTimeComparator());
            TimeWindowed earliest = timeWindowedList.get(0);
            checkNotNull(earliest.getTimeWindow(), "timeWindow can't be null");
            dateTime = earliest.getTimeWindow().getReadyTime();
        }
        return dateTime;
    }

}
