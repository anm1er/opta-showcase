package org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.impl;

import java.util.concurrent.atomic.AtomicBoolean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.api.solver.event.BestSolutionChangedEvent;
import org.optaplanner.core.api.solver.event.SolverEventListener;
import org.optaplanner.core.config.solver.termination.TerminationConfig;
import org.optaplanner.showcase.app.vehiclerouting.exception.InvalidProblemInstanceException;
import org.optaplanner.showcase.app.vehiclerouting.exception.SolvingDeniedException;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.SolverService;
import org.optaplanner.showcase.app.vehiclerouting.util.planning.PlannerConfigDetailsExtractor;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.DistanceType;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.problem.VehicleRoutingProblem;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

// Note: The qualifier and id thing is just a quick solution to allow for separate playground for trying sample instances
@Service("domain")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.INTERFACES)
public class SolverServiceImpl implements SolverService {

    public static final String SOLVER_ALREADY_STARTED_MESSAGE = "Solver has already been started within the current session";

    private static final Logger LOGGER = LoggerFactory.getLogger(SolverServiceImpl.class);

    private final SolverFactory solverFactory;

    private Solver solver;
    private AtomicBoolean isSolutionOutdated;
    private VehicleRoutingSolution solution;

    private TaskExecutor taskExecutor;
    private String solverConfigurationXmlResource;

    @Autowired
    public SolverServiceImpl(@Value("${solverConfigFile}") String solverConfigFile, TaskExecutor taskExecutor,
            TerminationConfig solverTerminationConfig) {
        solverConfigurationXmlResource = solverConfigFile;
        this.taskExecutor = taskExecutor;
        solverFactory = SolverFactory.createFromXmlResource(solverConfigurationXmlResource);
        solverFactory.getSolverConfig().setTerminationConfig(solverTerminationConfig);
        isSolutionOutdated = new AtomicBoolean(true);
    }

    @Override
    public void setSolutionOutdated() {
        isSolutionOutdated.set(true);
    }

    @Override
    public boolean trySetSolutionUpdated() {
        return isSolutionOutdated.compareAndSet(true, false);
    }

    @Override
    public void startSolving(final VehicleRoutingProblem problem) {
        checkNotNull(checkNotNull(problem).getPlannerSolution(), "Problem can't be null");
        verifySolution(problem.getPlannerSolution());
        LOGGER.debug("Starting solving problem {}", problem);
        if (!initializeSolver().isSolving()) {
            setSolution(problem.getPlannerSolution());
            taskExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    solver.solve(solution);
                    setSolution((VehicleRoutingSolution) solver.getBestSolution());
                }
            });
        } else {
            throw new SolvingDeniedException(SOLVER_ALREADY_STARTED_MESSAGE);
        }
    }

    @Override
    public void startSolving() {
        verifySolution(solution);
        LOGGER.debug("Starting solving problem");
        checkNotNull(solution, "Unsolved solution should be set before solving");
        if (!initializeSolver().isSolving()) {
            taskExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    solver.solve(solution);
                    setSolution((VehicleRoutingSolution) solver.getBestSolution());
                }
            });
        } else {
            throw new SolvingDeniedException(SOLVER_ALREADY_STARTED_MESSAGE);
        }
    }

    @Override
    public synchronized boolean terminateSolving() {
        LOGGER.debug("Terminating solver");
        if (solver.isSolving()) {
            solver.terminateEarly();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isBusy() {
        return solver.isSolving();
    }

    @Override
    public Solver getSolver() {
        return solver;
    }

    @Override
    public void setProblem(final VehicleRoutingProblem problem) {
        checkNotNull(problem, "Problem instance can't be null");
        checkNotNull(problem.getPlannerSolution(), "The problem's solution can't be null");
        setSolution(problem.getPlannerSolution());
        LOGGER.debug("Set problem for solver service: {}", problem);
    }

    @Override
    public VehicleRoutingSolution getCurrentSolution() {
        return solution;
    }

    /**
     * Performs sanity checks for the {@code solution}.
     * @return
     */
    private void verifySolution(final VehicleRoutingSolution solution) {
        if (solution != null) {
            assertState(!solution.getCustomerList().isEmpty(), "customerList can't be empty");
            assertState(!solution.getDepotList().isEmpty(), "depotList can't be empty");
            assertState(solution.getDepotList().size() == 1, "depotList can't contain more than one item");
            assertState(!solution.getVehicleList().isEmpty(), "vehiclesList can't be empty");
            assertState(solution.getLocationList().size() == solution.getCustomerList().size() + solution.getDepotList().size(),
                    "locations number not equal to customers + depots number");
            assertState(solution.getDistanceType() != DistanceType.UNDETERMINED, "distance type should be determined");
        }
    }

    /**
     * Sets or resets solution for the current session.
     * @param newSolution
     */
    private synchronized void setSolution(final VehicleRoutingSolution newSolution) {
        // Verification has been moved to the start solving phase
        solution = newSolution;
    }

    private synchronized Solver initializeSolver() {
        if (solver == null) {
            LOGGER.debug("Building Solver using {} for score configuration: {}", PlannerConfigDetailsExtractor
                    .getScoreConfigurationDetails(solverFactory.getSolverConfig().getScoreDirectorFactoryConfig()));
            solver = solverFactory.buildSolver();
            registerEventListeners(solver);
            LOGGER.debug("Built new solver instance");
        }
        return solver;
    }

    private void registerEventListeners(final Solver solver) {
        // Registers event listener to listen to best solution change
        solver.addEventListener(new SolverEventListener() {
            @Override
            public void bestSolutionChanged(BestSolutionChangedEvent event) {
                if (event.isNewBestSolutionInitialized()) {
                    VehicleRoutingSolution bestSolution = (VehicleRoutingSolution) event.getNewBestSolution();
                    setSolution(bestSolution);
                }
            }
        });
    }

    private void assertState(boolean expression, String message) {
        if (!expression) {
            throw new InvalidProblemInstanceException("Invalid problem instance submitted: " + message);
        }
    }

}
