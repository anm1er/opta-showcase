package org.optaplanner.showcase.app.vehiclerouting.web.planning;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import org.optaplanner.showcase.app.vehiclerouting.service.planning.dto.DTOTranslator;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.dto.VehicleRoutingSolutionDTO;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.ProblemDirector;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.SolverService;
import org.optaplanner.showcase.app.vehiclerouting.util.io.SolutionIOManager;
import org.optaplanner.showcase.app.vehiclerouting.web.common.FlashMessageController;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.problem.VehicleRoutingProblem;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.problem.VehicleRoutingProblemFactory;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides a way to try test problem sets separately from domain.
 */
@Controller
@RequestMapping(value = "/samples")
public class SampleProblemController extends FlashMessageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SampleProblemController.class);

    private static final String FEEDBACK_MESSAGE_KEY_ERROR_IMPORTING_PROBLEM = "error.import";
    private static final String VIEW_SAMPLE_SOLUTION = "samples/samples";

    private final SolverService solverService;
    private final ProblemDirector problemDirector;

    private final VehicleRoutingProblemFactory problemFactory;
    private final SolutionIOManager ioManager;

    @Autowired
    public SampleProblemController(@Qualifier("benchmark") SolverService solverService,
            ProblemDirector problemDirector, VehicleRoutingProblemFactory problemFactory) {
        this.solverService = solverService;
        this.problemDirector = problemDirector;
        this.problemFactory = problemFactory;
        this.ioManager = new SolutionIOManager();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String displayBenchmarkProblem() {
        LOGGER.debug("GET request handled for /samples");
        return VIEW_SAMPLE_SOLUTION;
    }

    @RequestMapping(value = "/schedule", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public VehicleRoutingSolutionDTO getScheduledData(HttpSession httpSession) {
        VehicleRoutingSolution solution = solverService.getCurrentSolution();
        if (solution == null) {
            // The default problem will be set from domain
            VehicleRoutingProblem problem = problemDirector.getCurrentDomainProblem();
            solverService.setProblem(problem);
            return DTOTranslator.getVehicleRoutingSolutionDTO(problem.getPlannerSolution());
        }
        return DTOTranslator.getVehicleRoutingSolutionDTO(solution);
    }

    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public String importProblem(@RequestPart("file") MultipartFile multipartFile, RedirectAttributes attributes,
            HttpSession httpSession) {
        LOGGER.debug("Setting new problem instance from imported dataset for session id:{}", httpSession.getCreationTime());
        try {
            VehicleRoutingSolution solution;
            String filename = multipartFile.getOriginalFilename();
            if (filename.endsWith(".vrp")) {
                solution = ioManager.importSolution(multipartFile);
            } else if (filename.endsWith(".xml")) {
                solution = ioManager.openSolution(multipartFile);
            } else {
                throw new IllegalArgumentException("Incompatible file extension");
            }
            solverService.setProblem(problemFactory.createProblem(solution));
        } catch (Exception e) {
            addFlashMessage(attributes, FEEDBACK_MESSAGE_KEY_ERROR_IMPORTING_PROBLEM);
        }
        return "redirect:/samples";
    }

    @RequestMapping(value = "/start", method = RequestMethod.POST)
    @ResponseBody
    public String solve(HttpSession httpSession, HttpServletResponse response) {
        LOGGER.debug("Starting solver for for session id:{}", httpSession.getId());
        try {
            solverService.startSolving();
        } catch (Exception e) {
            LOGGER.warn("Error launching solver: {}", e.getMessage());
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return e.getMessage();
        }
        return "Solving started";
    }

    @RequestMapping(value = "/terminate", method = RequestMethod.POST)
    @ResponseBody
    public String terminateEarly(HttpSession httpSession) {
        LOGGER.debug("Terminating solver for session id:{}", httpSession.getId());
        boolean terminated = solverService.terminateSolving();
        return terminated ? "Solving terminated" : "";
    }

}
