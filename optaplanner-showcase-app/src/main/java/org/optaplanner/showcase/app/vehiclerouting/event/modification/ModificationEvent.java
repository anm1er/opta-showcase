package org.optaplanner.showcase.app.vehiclerouting.event.modification;

import java.util.Objects;

import org.optaplanner.showcase.app.vehiclerouting.model.BaseEntity;

public class ModificationEvent {

    private BaseEntity domainObject;
    private ModificationEventType eventType;

    public ModificationEvent(BaseEntity domainObject, ModificationEventType eventType) {
        this.domainObject = domainObject;
        this.eventType = eventType;
    }

    public BaseEntity getDomainObject() {
        return domainObject;
    }

    public void setDomainObject(BaseEntity domainObject) {
        this.domainObject = domainObject;
    }

    public ModificationEventType getEventType() {
        return eventType;
    }

    public void setEventType(ModificationEventType eventType) {
        this.eventType = eventType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ModificationEvent that = (ModificationEvent) o;
        return Objects.equals(domainObject, that.domainObject) &&
                Objects.equals(eventType, that.eventType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(domainObject, eventType);
    }

}
