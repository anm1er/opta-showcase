package org.optaplanner.showcase.app.vehiclerouting.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;

import org.springframework.core.style.ToStringCreator;

/**
 * A reusable representation of location.
 */
@SuppressWarnings("serial")
@Embeddable
public class Location implements Serializable {

    private double latitude;

    private double longitude;

    private String name;

    public Location() {
    }

    public Location(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Location(double latitude, double longitude, String name) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Location location = (Location) o;
        return Objects.equals(latitude, location.latitude)
                && Objects.equals(longitude, location.longitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("latitude", latitude)
                .append("longitude", longitude)
                .toString();
    }

    public static class Builder {

        private Location built;

        public Builder() {
            built = new Location();
        }

        public Location build() {
            return built;
        }

        public Builder withLatitude(double latitude) {
            built.setLatitude(latitude);
            return this;
        }

        public Builder withLongitude(double longitude) {
            built.setLongitude(longitude);
            return this;
        }

        public Builder withName(String name) {
            built.setName(name);
            return this;
        }
    }

}
