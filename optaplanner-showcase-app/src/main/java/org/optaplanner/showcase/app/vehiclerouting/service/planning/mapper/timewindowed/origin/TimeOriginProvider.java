package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.origin;

import java.util.Collection;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowed;

import org.joda.time.DateTime;

/**
 * Provides time origin for date time translations based on some invariable domain data.
 */
public interface TimeOriginProvider {

    <T extends TimeWindowed> DateTime getTimeOriginFromDomainObject(T domainObject);

    /**
     * Returns date time extracted from any non-empty {@code domainObjects} collection
     * or {@code null} if the passed collection is empty.
     * @param domainObjects
     * @return
     */
    DateTime getTimeOriginFromDomainObjects(Collection<? extends TimeWindowed> domainObjects);

}
