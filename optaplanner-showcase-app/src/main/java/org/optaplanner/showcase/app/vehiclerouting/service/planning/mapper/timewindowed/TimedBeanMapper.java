package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed;

import org.optaplanner.showcase.app.vehiclerouting.exception.MappingException;
import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedDelivery;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;

import org.joda.time.DateTime;

public interface TimedBeanMapper {

    // Passing over origin may not look like optimal solution (?), but at least it is a way to avoid
    // the 'deceptive api' and the statefulness of time translator

    /**
     * Maps customer in planning domain to customer in problem domain.
     * @param planningCustomer planning bean to map from
     * @param origin arbitrary point in time for date time translations
     * @return resulting problem entity
     * @throws MappingException if exception occurs on mapping
     */
    TimeWindowedCustomer map(PlanningTimeWindowedCustomer planningCustomer, DateTime origin);

    /**
     * Maps depot in planning domain to depot in problem domain.
     * @param planningDepot planning bean to map from
     * @param origin arbitrary point in time for date time translations
     * @return resulting problem entity
     * @throws MappingException if exception occurs on mapping
     */
    TimeWindowedDepot map(PlanningTimeWindowedDepot planningDepot, DateTime origin);

    /**
     * Maps vehicle with its depot reference in planning domain to vehicle with the corresponding depot in problem domain.
     * <p/>
     * Note: In OptaPlanner model vehicle is a time windows-agnostic <em>problem fact</em>, while in the problem domain model
     * vehicles have to reference scheduling information. Hence, if the passed planningVehicle object references depot, whose
     * type is not compatible with {@code PlanningTimeWindowedDepot}, MappingException is thrown.
     * @param planningVehicle planning bean to map from
     * @param origin arbitrary point in time for date time translations
     * @return resulting problem entity
     * @throws MappingException if exception occurs on mapping
     */
    TimedVehicle map(PlanningVehicle planningVehicle, DateTime origin);

    /**
     * Maps vehicle and customer in planning domain to {@code TimedDelivery}.
     * @param vehicle planning vehicle to map from
     * @param customer planning customer to map from
     * @param origin arbitrary point in time for date time translations
     * @return {@code TimedDelivery} object
     * @throws MappingException if exception occurs on mapping
     */
    TimedDelivery map(PlanningVehicle vehicle, PlanningTimeWindowedCustomer customer, DateTime origin);

    /**
     * Maps customer in problem domain to customer in planning domain.
     * @param customer problem entity to map from
     * @param origin arbitrary point in time for date time translations
     * @return resulting planning bean
     * @throws MappingException if exception occurs on mapping
     */
    PlanningTimeWindowedCustomer map(TimeWindowedCustomer customer, DateTime origin);

    /**
     * Maps depot in problem domain to depot in planning domain.
     * @param depot problem entity to map from
     * @param origin arbitrary point in time for date time translations
     * @return resulting planning bean
     * @throws MappingException if exception occurs on mapping
     */
    PlanningTimeWindowedDepot map(TimeWindowedDepot depot, DateTime origin);

    /**
     * Maps vehicle in problem domain to vehicle in planning domain.
     * @param vehicle problem entity to map from
     * @param origin arbitrary point in time for date time translations
     * @return resulting planning bean
     * @throws MappingException if exception occurs on mapping
     */
    PlanningVehicle map(TimedVehicle vehicle, DateTime origin);

    /**
     * Maps location in problem domain to location in planning domain.
     * @param location problem entity to map from
     * @return resulting planning bean
     */
    PlanningLocation map(Location location, Long id);

    /**
     * Maps location in planning domain to location in problem domain.
     * @param planningLocation planning bean to map from
     * @return resulting problem entity
     */
    Location map(PlanningLocation planningLocation);

}
