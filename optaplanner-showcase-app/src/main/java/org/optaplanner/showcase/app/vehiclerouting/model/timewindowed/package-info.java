
/**
 * <p>
 *     The classes in this package represent the business layer of the sample vehicle routing problem with <b>time windows</b>.
 * </p>
 *
 * <p>
 *     VRP with Time Windows (VRPTW) is a problem with a fixed fleet of delivery vehicles servicing known customer demands for
 *     a single commodity from a common depot at minimum transit cost, with the additional restriction that a time window
 *     is associated with each customer and the depot.
 * </p>
 *
 * <p>
 *     <em>Note: For now {@code TimedDelivery}, {@code TimedVehicle}, {@code TimeWindowedCustomer} and {@code TimeWindowedDepot}
 *     are the only expected delivery, vehicle, customer and depot types respectively. Yet we would like to avoid extra prefixes
 *     in schema. This may change as we choose to implement some other VRP flavors (e.g. VRPBH - VRP with Backhauls).</em>
 * </p>
 */
package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed;

