package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindow;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedDelivery;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.MappingValidator;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.TimedBeanMapper;
import org.optaplanner.showcase.app.vehiclerouting.util.datetime.PlanningUnitsToDateTimeTranslator;
import org.optaplanner.showcase.app.vehiclerouting.util.mapper.MappingUtils;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningBeanFactory;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class DefaultTimedBeanMapper implements TimedBeanMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultTimedBeanMapper.class);

    private final PlanningUnitsToDateTimeTranslator timeTranslator;
    private final PlanningBeanFactory planningBeanFactory;

    @Autowired
    public DefaultTimedBeanMapper(PlanningBeanFactory planningBeanFactory) {
        this.planningBeanFactory = planningBeanFactory;
        this.timeTranslator = new PlanningUnitsToDateTimeTranslator();
    }

    public DefaultTimedBeanMapper(PlanningBeanFactory planningBeanFactory, PlanningUnitsToDateTimeTranslator timeTranslator) {
        this.timeTranslator = timeTranslator;
        this.planningBeanFactory = planningBeanFactory;
    }

    public PlanningUnitsToDateTimeTranslator getTimeTranslator() {
        return timeTranslator;
    }

    @Override
    public TimeWindowedCustomer map(PlanningTimeWindowedCustomer planningCustomer, DateTime origin) {
        TimeWindowedCustomer destinationCustomer = null;
        try {
            MappingValidator.validatePlanningBeanMappingRequest(planningCustomer, "Customer");
            MappingUtils.assertNotNull(planningCustomer.getLocation(), "Customer location");
            destinationCustomer = TimeWindowedCustomer.getBuilder()
                    .withDemand(planningCustomer.getDemand())
                    .withLocation(map(planningCustomer.getLocation()))
                    .withServiceDuration(PlanningUnitsToDateTimeTranslator
                            .getDurationFromScaledTimeUnits(planningCustomer.getServiceDuration()))
                    .withTimeWindow(map(planningCustomer.getReadyTime(),
                            planningCustomer.getDueTime(), origin))
                    .build();
        } catch (Exception e) {
            MappingUtils.throwMappingException("planningCustomer", e);
        }
        return destinationCustomer;
    }

    @Override
    public TimeWindowedDepot map(PlanningTimeWindowedDepot planningDepot, DateTime origin) {
        TimeWindowedDepot destinationDepot = null;
        try {
            MappingValidator.validatePlanningBeanMappingRequest(planningDepot, "Depot");
            MappingUtils.assertNotNull(planningDepot.getLocation(), "Depot location");
            destinationDepot = TimeWindowedDepot.getBuilder()
                    .withLocation(map(planningDepot.getLocation()))
                    .withTimeWindow(map(planningDepot.getReadyTime(),
                            planningDepot.getDueTime(), origin))
                    .build();
        } catch (Exception e) {
            MappingUtils.throwMappingException("planningDepot", e);
        }
        return destinationDepot;
    }

    @Override
    public TimedVehicle map(PlanningVehicle planningVehicle, DateTime origin) {
        // todo generate unique license plate if null
        TimedVehicle destinationVehicle = null;
        try {
            MappingValidator.validatePlanningBeanMappingRequest(planningVehicle, "Vehicle");
            PlanningTimeWindowedDepot planningDepot = MappingValidator.getValidatedDepot(planningVehicle.getDepot());
            destinationVehicle = TimedVehicle.getBuilder()
                    .withCapacity(planningVehicle.getCapacity())
                    .withLicensePlate(planningVehicle.getLicensePlate())
                    .withDepot(map(planningDepot, origin))
                    .build();
        } catch (Exception e) {
            MappingUtils.throwMappingException("planningVehicle", e);
        }
        return destinationVehicle;
    }

    @Override
    public TimedDelivery map(PlanningVehicle vehicle, PlanningTimeWindowedCustomer customer, DateTime origin) {
        TimedDelivery delivery = null;
        try {
            MappingValidator.validatePlanningBeanMappingRequest(vehicle, "Vehicle");
            MappingValidator.validatePlanningBeanMappingRequest(customer, "Customer");
            MappingValidator.validateScheduledTime(customer.getScheduledTime());
            delivery = TimedDelivery.getBuilder()
                    .withCustomer(map(customer, origin))
                    .withVehicle(map(vehicle, origin))
                    .withArrivalTime(timeTranslator.getDateTimeFromScaledShift(customer.getScheduledTime(), origin))
                    .build();
        } catch (Exception e) {
            MappingUtils.throwMappingException("vehicle with customer to TimedDelivery", e);
        }
        return delivery;
    }

    // The following methods rely on the original planning model API to create planning beans

    @Override
    public PlanningTimeWindowedCustomer map(TimeWindowedCustomer customer, DateTime origin) {
        PlanningTimeWindowedCustomer planningCustomer = null;
        try {
            MappingValidator.validateEntityBeanMappingRequest(customer, "Customer");
            MappingUtils.assertNotNull(customer.getLocation(), "Customer location");
            planningCustomer = planningBeanFactory.createCustomer();
            planningCustomer.setId(customer.getId());
            planningCustomer.setDemand(customer.getDemand());
            // Each customer has the corresponding id of his location
            planningCustomer.setLocation(map(customer.getLocation(), customer.getId()));
            planningCustomer.setServiceDuration(PlanningUnitsToDateTimeTranslator
                    .getScaledTimeUnitsFromDuration(customer.getServiceDuration()));
            planningCustomer.setReadyTime(timeTranslator.getScaledShiftFromDateTime(
                    customer.getTimeWindow().getReadyTime(), origin));
            planningCustomer.setDueTime(timeTranslator.getScaledShiftFromDateTime(
                    customer.getTimeWindow().getDueTime(), origin));
        } catch (Exception e) {
            MappingUtils.throwMappingException("customer", e);
        }
        return planningCustomer;
    }

    @Override
    public PlanningTimeWindowedDepot map(TimeWindowedDepot depot, DateTime origin) {
        PlanningTimeWindowedDepot planningDepot = null;
        try {
            MappingValidator.validateEntityBeanMappingRequest(depot, "Depot");
            MappingUtils.assertNotNull(depot.getLocation(), "Depot location");
            planningDepot = planningBeanFactory.createDepot();
            planningDepot.setId(depot.getId());
            // Depot has the corresponding id of its location
            planningDepot.setLocation(map(depot.getLocation(), depot.getId()));
            planningDepot.setReadyTime(timeTranslator.getScaledShiftFromDateTime(
                    depot.getTimeWindow().getReadyTime(), origin));
            planningDepot.setDueTime(timeTranslator.getScaledShiftFromDateTime(
                    depot.getTimeWindow().getDueTime(), origin));
        } catch (Exception e) {
            MappingUtils.throwMappingException("depot", e);
        }
        return planningDepot;
    }

    @Override
    public PlanningVehicle map(TimedVehicle vehicle, DateTime origin) {
        PlanningVehicle planningVehicle = null;
        try {
            MappingValidator.validateEntityBeanMappingRequest(vehicle, "Vehicle");
            MappingUtils.assertNotNull(vehicle.getDepot(), "Vehicle's depot reference");
            MappingUtils.assertNotNull(vehicle.getLicensePlate(), "Vehicle licensePlate");
            planningVehicle = planningBeanFactory.createVehicle();
            planningVehicle.setId(vehicle.getId());
            planningVehicle.setCapacity(vehicle.getCapacity());
            planningVehicle.setLicensePlate(vehicle.getLicensePlate());
            planningVehicle.setDepot(map(vehicle.getDepot(), origin));
        } catch (Exception e) {
            MappingUtils.throwMappingException("vehicle", e);
        }
        return planningVehicle;
    }

    @Override
    public PlanningLocation map(Location location, Long id) {
        PlanningLocation planningLocation = planningBeanFactory.createUniqueLocation(location.getName(),
                location.getLatitude(), location.getLongitude());
        planningLocation.setId(id);
        return planningLocation;
    }

    @Override
    public Location map(PlanningLocation planningLocation) {
        return Location.getBuilder()
                .withLatitude(planningLocation.getLatitude())
                .withLongitude(planningLocation.getLongitude())
                .withName(planningLocation.getName())
                .build();
    }

    private TimeWindow map(int readyTime, int dueTime, DateTime origin) {
        return new TimeWindow(timeTranslator.getDateTimeFromScaledShift(readyTime, origin),
                timeTranslator.getDateTimeFromScaledShift(dueTime, origin));
    }

}
