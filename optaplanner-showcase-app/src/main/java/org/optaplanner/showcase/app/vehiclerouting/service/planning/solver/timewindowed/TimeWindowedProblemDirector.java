package org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.timewindowed;

import org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.ProblemDirector;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem.TimeWindowedVehicleRoutingProblem;

import org.joda.time.DateTime;

/**
 * Handles operations on the current domain problem instance.
 */
public interface TimeWindowedProblemDirector extends ProblemDirector {

    /**
     * Returns a valid problem instance which matches the current domain state
     * or {@code null} if no matching valid domain data can be obtained.
     * @return problem instance based on the domain data
     */
    TimeWindowedVehicleRoutingProblem getCurrentDomainProblem();

    /**
     * Returns a valid problem instance mapped from domain w.r.t. {@code timeOrigin}
     * or {@code null} if no matching valid domain data can be obtained.
     * @return problem instance based on the domain data
     */
    TimeWindowedVehicleRoutingProblem getProblemForOrigin(DateTime timeOrigin);

}
