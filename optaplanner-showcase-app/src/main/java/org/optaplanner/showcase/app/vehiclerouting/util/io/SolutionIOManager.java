package org.optaplanner.showcase.app.vehiclerouting.util.io;

import java.io.File;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;
import org.optaplanner.showcase.model.vehiclerouting.impl.persistence.VehicleRoutingDao;
import org.optaplanner.showcase.model.vehiclerouting.impl.persistence.VehicleRoutingImporter;

public class SolutionIOManager {

    public static final boolean WITHOUT_DAO = true;

    private final VehicleRoutingDao solutionDao;
    private final VehicleRoutingImporter importer;

    public SolutionIOManager() {
        this.solutionDao = new VehicleRoutingDao();
        this.importer = new VehicleRoutingImporter(WITHOUT_DAO);
    }

    public VehicleRoutingSolution importSolution(MultipartFile file) throws IOException {
        return importSolution(getTemporaryFile(file));
    }

    public VehicleRoutingSolution importSolution(File file) {
        return (VehicleRoutingSolution) importer.readSolution(file);
    }

    public VehicleRoutingSolution openSolution(MultipartFile file) throws IOException {
        return openSolution(getTemporaryFile(file));
    }

    public VehicleRoutingSolution openSolution(File file) {
        return (VehicleRoutingSolution) solutionDao.readSolution(file);
    }

    public void saveSolution(VehicleRoutingSolution solution, File file) {
        solutionDao.writeSolution(solution, file);
    }

    /**
     * Returns created temporary file for the {@link MultipartFile} or {@code null} if the multipartFile is empty.
     * @param multipartFile
     * @return
     * @throws IOException
     */
    public File getTemporaryFile(MultipartFile multipartFile) throws IOException {
        File tmpFile = null;
        if (!multipartFile.isEmpty()) {
            tmpFile = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator")
                    + multipartFile.getOriginalFilename());
            multipartFile.transferTo(tmpFile);
        }
        return tmpFile;
    }

}
