package org.optaplanner.showcase.app.vehiclerouting.service.planning.dto;

import java.util.List;

public class VehicleDTO {

    // depot location-related data
    private double depotLatitude;
    private double depotLongitude;
    private String depotName;

    private List<CustomerDTO> customerDTOs;

    public VehicleDTO() {
    }

    public VehicleDTO(double depotLatitude, double depotLongitude, String depotName,
            List<CustomerDTO> customerDTOs) {
        this.depotLatitude = depotLatitude;
        this.depotLongitude = depotLongitude;
        this.depotName = depotName;
        this.customerDTOs = customerDTOs;
    }

    public double getDepotLatitude() {
        return depotLatitude;
    }

    public void setDepotLatitude(double depotLatitude) {
        this.depotLatitude = depotLatitude;
    }

    public double getDepotLongitude() {
        return depotLongitude;
    }

    public void setDepotLongitude(double depotLongitude) {
        this.depotLongitude = depotLongitude;
    }

    public String getDepotName() {
        return depotName;
    }

    public void setDepotName(String depotName) {
        this.depotName = depotName;
    }

    public List<CustomerDTO> getCustomerDTOs() {
        return customerDTOs;
    }

    public void setCustomerDTOs(List<CustomerDTO> customerDTOs) {
        this.customerDTOs = customerDTOs;
    }

}
