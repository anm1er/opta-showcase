package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.comparator;

import java.io.Serializable;
import java.util.Comparator;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowed;

public class ReadyTimeComparator implements Comparator<TimeWindowed>, Serializable {

    @Override
    public int compare(TimeWindowed a, TimeWindowed b) {
        return a.getTimeWindow().getReadyTime().
                compareTo(b.getTimeWindow().getReadyTime());
    }

}
