package org.optaplanner.showcase.app.vehiclerouting.service.persistence.timewindowed.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException;
import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedDelivery;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.TimeWindowedCustomerRepository;
import org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.TimeWindowedDepotRepository;
import org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.TimedDeliveryRepository;
import org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.TimedVehicleRepository;
import org.optaplanner.showcase.app.vehiclerouting.service.persistence.timewindowed.TimeWindowedPersistenceService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * An implementation of the service interface {@link TimeWindowedPersistenceService}.
 */
@Service
@Transactional(readOnly = true)
public class TimeWindowedPersistenceServiceImpl implements TimeWindowedPersistenceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeWindowedPersistenceServiceImpl.class);

    private TimeWindowedCustomerRepository customerRepository;
    private TimedVehicleRepository vehicleRepository;
    private TimeWindowedDepotRepository depotRepository;
    private TimedDeliveryRepository deliveryRepository;

    @Autowired
    public TimeWindowedPersistenceServiceImpl(TimeWindowedCustomerRepository customerRepository, TimedVehicleRepository
            vehicleRepository, TimeWindowedDepotRepository depotRepository, TimedDeliveryRepository deliveryRepository) {
        this.customerRepository = customerRepository;
        this.vehicleRepository = vehicleRepository;
        this.depotRepository = depotRepository;
        this.deliveryRepository = deliveryRepository;
    }

    @Override
    public TimeWindowedCustomer getCustomer(Long id) {
        checkNotNull(id, "id can't be null");
        LOGGER.debug("Finding a customer with id: {}", id);
        TimeWindowedCustomer found = customerRepository.findOne(id);
        LOGGER.debug("Found customer: {}", found);
        if (found == null) {
            throw new ResourceNotFoundException("No customer found with id: " + id);
        }
        return found;
    }

    @Override
    public TimeWindowedCustomer getCustomerByLocation(Location location) {
        checkNotNull(location, "location can't be null");
        LOGGER.debug("Finding a customer with location: {}", location);
        TimeWindowedCustomer found = customerRepository.findOneByLocation(location);
        LOGGER.debug("Found customer: {}", found);
        if (found == null) {
            throw new ResourceNotFoundException("No customer found with location: " + location);
        }
        return found;
    }

    @Override
    public TimeWindowedCustomer getCustomerWithScheduleData(Long id) {
        checkNotNull(id, "id can't be null");
        LOGGER.debug("Finding a customer with id: {}", id);
        TimeWindowedCustomer found = customerRepository.findOneWithScheduledData(id);
        LOGGER.debug("Found customer: {}", found);
        if (found == null) {
            throw new ResourceNotFoundException("No customer found with id: " + id);
        }
        return found;
    }

    @Override
    public List<TimeWindowedCustomer> getCustomers() {
        LOGGER.debug("Finding all customers");
        List<TimeWindowedCustomer> allCustomers = customerRepository.findAll();
        if (allCustomers.isEmpty()) {
            LOGGER.warn("No customers retrieved from the store");
        } else {
            LOGGER.debug("Found {} customers", allCustomers.size());
        }
        return allCustomers;
    }

    @Override
    @Transactional(readOnly = false)
    public TimeWindowedCustomer saveCustomer(TimeWindowedCustomer customer) {
        checkNotNull(customer, "customer can't be null");
        LOGGER.debug("Saving customer with information: {}", customer);
        deleteDeliveries();
        return customerRepository.save(customer);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteCustomer(Serializable id) {
        checkNotNull(id, "id can't be null");
        LOGGER.debug("Deleting a customer with id: {}", id);
        deleteDeliveries();
        TimeWindowedCustomer deleted = getCustomer((Long) id);
        customerRepository.delete(deleted);
        LOGGER.debug("Deleted customer: {}", deleted);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteCustomers() {
        LOGGER.debug("Deleting all customers");
        deleteDeliveries();
        customerRepository.deleteAll();
        LOGGER.debug("Deleted all customers");
    }

    @Override
    public TimedVehicle getVehicle(Long id) {
        checkNotNull(id, "id can't be null");
        LOGGER.debug("Finding a vehicle with id: {}", id);
        TimedVehicle found = vehicleRepository.findOne(id);
        LOGGER.debug("Found vehicle: {}", found);
        if (found == null) {
            throw new ResourceNotFoundException("No vehicle found with id: " + id);
        }
        return found;
    }

    @Override
    public TimedVehicle getVehicleWithScheduleData(Long id) {
        checkNotNull(id, "id can't be null");
        LOGGER.debug("Finding a vehicle with id with schedule data: {}", id);
        TimedVehicle found = vehicleRepository.findOneWithScheduledData(id);
        LOGGER.debug("Found vehicle: {}", found);
        if (found == null) {
            throw new ResourceNotFoundException("No vehicle found with id: " + id);
        }
        return found;
    }

    @Override
    public TimedVehicle getVehicleByLicensePlate(String licensePlate) {
        checkNotNull(licensePlate, "licensePlate can't be null");
        LOGGER.debug("Finding a vehicle with id: {}", licensePlate);
        TimedVehicle found = vehicleRepository.findOneByPlate(licensePlate);
        LOGGER.debug("Found vehicle: {}", found);
        if (found == null) {
            throw new ResourceNotFoundException("No vehicle found with id: " + licensePlate);
        }
        return found;
    }

    @Override
    public List<TimedVehicle> getVehicles() {
        LOGGER.debug("Finding all vehicles");
        List<TimedVehicle> allVehicles = vehicleRepository.findAll();
        if (allVehicles.isEmpty()) {
            LOGGER.warn("No vehicles retrieved from the store");
        } else {
            LOGGER.debug("Found {} vehicles", allVehicles.size());
        }
        return allVehicles;
    }

    @Override
    @Transactional(readOnly = false)
    public TimedVehicle saveVehicle(TimedVehicle vehicle) {
        checkNotNull(vehicle, "vehicle can't be null");
        LOGGER.debug("Saving vehicle with information: {}", vehicle);
        deleteDeliveries();
        return vehicleRepository.save(vehicle);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteVehicle(Serializable id) {
        checkNotNull(id, "id can't be null");
        LOGGER.debug("Deleting a vehicle with id: {}", id);
        deleteDeliveries();
        TimedVehicle deleted = getVehicle((Long) id);
        vehicleRepository.delete(deleted);
        LOGGER.debug("Deleted vehicle: {}", deleted);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteVehicles() {
        LOGGER.debug("Deleting all vehicles");
        deleteDeliveries();
        vehicleRepository.deleteAll();
        LOGGER.debug("Deleted all vehicles");
    }

    @Override
    public TimeWindowedDepot getDepot(Long id) {
        checkNotNull(id, "id can't be null");
        LOGGER.debug("Finding a depot with id: {}", id);
        TimeWindowedDepot found = depotRepository.findOne(id);
        LOGGER.debug("Found depot: {}", found);
        if (found == null) {
            throw new ResourceNotFoundException("No depot found with id: " + id);
        }
        return found;
    }

    @Override
    public List<TimeWindowedDepot> getDepots() {
        LOGGER.debug("Finding all depots");
        List<TimeWindowedDepot> allDepots = depotRepository.findAll();
        if (allDepots.isEmpty()) {
            LOGGER.warn("No depots retrieved from the store");
        } else if (allDepots.size() > 1) {
            LOGGER.warn("Single depot expected for the problem, but retrieved {}", allDepots.size());
            throw new IllegalStateException("VRPTW should have a single depot");
        }
        return allDepots;
    }

    @Override
    public TimeWindowedDepot getDepot() {
        LOGGER.debug("Finding the depot");
        List<TimeWindowedDepot> allDepots = depotRepository.findAll();
        if (allDepots.isEmpty()) {
            LOGGER.warn("No depots retrieved from the store");
        } else if (allDepots.size() > 1) {
            LOGGER.warn("Single depot expected for the problem, but retrieved {}", allDepots.size());
            throw new IllegalStateException("VRPTW should have a single depot");
        }
        return allDepots.get(0);
    }

    @Override
    @Transactional(readOnly = false)
    public TimeWindowedDepot saveDepot(TimeWindowedDepot depot) {
        checkNotNull(depot, "depot can't be null");
        LOGGER.debug("Saving depot with information: {}", depot);
        deleteDeliveries();
        return depotRepository.save(depot);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteDepot(Serializable id) {
        checkNotNull(id, "id can't be null");
        LOGGER.debug("Deleting a depot with id: {}", id);
        deleteDeliveries();
        TimeWindowedDepot deleted = getDepot((Long) id);
        depotRepository.delete(deleted);
        LOGGER.debug("Deleted depot: {}", deleted);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteDepots() {
        LOGGER.debug("Deleting all depots");
        deleteDeliveries();
        depotRepository.deleteAll();
        LOGGER.debug("Deleted all depots");
    }

    @Override public List<TimedDelivery> getDeliveries() {
        LOGGER.debug("Finding all deliveries");
        List<TimedDelivery> allDeliveries = deliveryRepository.findAll();
        if (allDeliveries.isEmpty()) {
            LOGGER.warn("No deliveries retrieved from the store");
        } else {
            LOGGER.debug("Found {} deliveries", allDeliveries.size());
        }
        return allDeliveries;
    }

    @Override
    @Transactional(readOnly = false)
    public List<TimedDelivery> saveAllDeliveries(List<TimedDelivery> deliveries) {
        checkNotNull(deliveries, "deliveries can't be null");
        LOGGER.debug("Saving {} deliveries", deliveries.size());
        if (!deliveries.isEmpty()) {
            if (deliveries.get(0).isTransient()) {
                makeDeliveriesReferencePersistentEntities(deliveries);
            }
        }
        List<TimedDelivery> savedDeliveries = deliveryRepository.saveAll(deliveries);
        LOGGER.debug("Saved {} deliveries", savedDeliveries.size());
        return savedDeliveries;
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteDeliveries() {
        LOGGER.debug("Deleting all deliveries");
        deliveryRepository.deleteAll();
        LOGGER.debug("Deleted all deliveries");
    }

    private void makeDeliveriesReferencePersistentEntities(List<TimedDelivery> deliveries) {
        List<TimeWindowedCustomer> customers = customerRepository.findAll();
        List<TimedVehicle> vehicles = vehicleRepository.findAll();
        for (TimedDelivery delivery : deliveries) {
            for (TimeWindowedCustomer customer : customers) {
                if (delivery.getCustomer().equals(customer)) {
                    delivery.setCustomer(customer);
                }
            }
            for (TimedVehicle vehicle : vehicles) {
                if (delivery.getVehicle().equals(vehicle)) {
                    delivery.setVehicle(vehicle);
                }
            }
        }
    }

}
