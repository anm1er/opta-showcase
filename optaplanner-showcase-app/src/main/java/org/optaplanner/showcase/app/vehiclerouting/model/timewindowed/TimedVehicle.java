package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.optaplanner.showcase.app.vehiclerouting.model.Vehicle;

/**
 * A {@code TimedVehicle} is a vehicle servicing {@link TimeWindowedCustomer}s and having {@link TimeWindowedDepot} as a
 * starting point for the route.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "vehicles")
public class TimedVehicle extends Vehicle<TimeWindowedDepot> {

    /**
     * All timed deliveries scheduled for this vehicle.
     */
    @OneToMany(mappedBy = "vehicle")
    @Column(name = "deliveries")
    @OrderBy("arrivalTime")
    private Set<TimedDelivery> timedDeliveries = new HashSet<>();

    public static Builder getBuilder() {
        return new Builder();
    }

    public Set<TimedDelivery> getTimedDeliveries() {
        return timedDeliveries;
    }

    public void setTimedDeliveries(Set<TimedDelivery> timedDeliveries) {
        this.timedDeliveries = timedDeliveries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode());
    }

    public static class Builder extends Vehicle.Builder<TimeWindowedDepot, TimedVehicle, Builder> {

        @Override
        protected TimedVehicle createVehicle() {
            return new TimedVehicle();
        }

        @Override
        protected Builder getBuilder() {
            return this;
        }
    }

}
