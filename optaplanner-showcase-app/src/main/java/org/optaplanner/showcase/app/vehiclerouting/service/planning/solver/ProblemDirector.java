package org.optaplanner.showcase.app.vehiclerouting.service.planning.solver;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.problem.VehicleRoutingProblem;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

/**
 * Obtains and updates domain problem instance.
 */
public interface ProblemDirector {

    /**
     * Returns a valid problem instance which matches the current domain state
     * or {@code null} if no matching valid domain data can be obtained.
     * @return problem instance based on the domain data
     */
    VehicleRoutingProblem getCurrentDomainProblem();

    /**
     * Updates the the current domain state so that it matches the {@code solution}.
     * @param solution
     */
    void setDomainSolution(VehicleRoutingSolution solution);

}
