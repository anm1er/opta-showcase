package org.optaplanner.showcase.app.vehiclerouting.util.datetime;

import org.joda.time.DateTime;
import org.joda.time.Duration;

/**
 * Most available standard problem sets (like those suggested by Solomon and Homberger) make use of integer values to express
 * due time and ready time in VRPTW instances.
 * <p/>
 * For convenience, it is assumed that '0' is an arbitrary starting point which can be fed to
 * {@code PlanningUnitsToDateTimeTranslator}, and any subsequent positive integer value is a shift in time (either in seconds,
 * minutes or hours) w.r.t. that point.
 * <p/>
 * It seems most reasonable to consider the integer values as minutes, as those would also correspond to service duration.
 */
public class PlanningUnitsToDateTimeTranslator {

    /**
     * Scale factor for planning time units.
     */
    public static final int FACTOR = 1000;

    private final PlanningTimeUnit planningTimeUnit;

    public PlanningUnitsToDateTimeTranslator() {
        this(PlanningTimeUnit.MINUTES);
    }

    public PlanningUnitsToDateTimeTranslator(PlanningTimeUnit planningTimeUnit) {
        this.planningTimeUnit = planningTimeUnit;
    }

    public static int getScaledTimeUnitsFromDuration(int duration) {
        return duration * FACTOR;
    }

    public static int getDurationFromScaledTimeUnits(int scaledUnits) {
        return scaledUnits / FACTOR;
    }

    public Duration getForwardDuration(DateTime dateTime, DateTime origin) {
        return new Duration(origin, dateTime);
    }

    public DateTime getDateTimeFromScaledShift(int scaledShift, DateTime origin) {
        scaledShift /= FACTOR;
        return getDateTimeFromShift(scaledShift, origin);
    }

    public int getScaledShiftFromDateTime(DateTime dateTime, DateTime origin) {
        return FACTOR * getShiftFromDateTime(dateTime, origin);
    }

    /**
     * Returns date time by forward translations (past {@code shift} w.r.t. origin)
     * @param shift
     * @return
     */
    public DateTime getDateTimeFromShift(int shift, DateTime origin) {
        if (planningTimeUnit == PlanningTimeUnit.SECONDS) {
            return origin.plusSeconds(shift);
        } else if (planningTimeUnit == PlanningTimeUnit.MINUTES) {
            return origin.plusMinutes(shift);
        } else if (planningTimeUnit == PlanningTimeUnit.HOURS) {
            return origin.plusHours(shift);
        } else {
            throw new IllegalStateException("Incompatible planningTimeUnit: " + planningTimeUnit);
        }
    }

    /**
     * Returns integer shift by forward translations (positive if {@code dateTime} comes AFTER origin,
     * negative if if {@code dateTime} comes BEFORE origin).
     * @param dateTime
     * @return
     */
    public int getShiftFromDateTime(DateTime dateTime, DateTime origin) {
        Duration forwardDuration = getForwardDuration(dateTime, origin);
        if (planningTimeUnit == PlanningTimeUnit.SECONDS) {
            return forwardDuration.toStandardSeconds().getSeconds();
        } else if (planningTimeUnit == PlanningTimeUnit.MINUTES) {
            return forwardDuration.toStandardMinutes().getMinutes();
        } else if (planningTimeUnit == PlanningTimeUnit.HOURS) {
            return forwardDuration.toStandardHours().getHours();
        } else {
            throw new IllegalStateException("Incompatible planningTimeUnit: " + planningTimeUnit);
        }
    }

}
