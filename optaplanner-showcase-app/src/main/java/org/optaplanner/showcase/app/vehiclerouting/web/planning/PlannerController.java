package org.optaplanner.showcase.app.vehiclerouting.web.planning;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import org.optaplanner.showcase.app.vehiclerouting.service.planning.dto.DTOTranslator;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.dto.VehicleRoutingSolutionDTO;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.ProblemDirector;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.SolverService;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.problem.VehicleRoutingProblem;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
@RequestMapping(value = "/demo")
public class PlannerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlannerController.class);

    private final SolverService solverService;
    private final ProblemDirector problemDirector;

    @Autowired
    public PlannerController(@Qualifier("domain") SolverService solverService,
            ProblemDirector problemDirector) {
        this.solverService = solverService;
        this.problemDirector = problemDirector;
    }

    @RequestMapping(value = "/schedule", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public VehicleRoutingSolutionDTO getScheduledData(HttpSession httpSession) {
        VehicleRoutingSolution solution = solverService.getCurrentSolution();
        // If solution is outdated it should be reinitialized
        if (solverService.trySetSolutionUpdated()) {
            VehicleRoutingProblem problem = problemDirector.getCurrentDomainProblem();
            solverService.setProblem(problem);
            return DTOTranslator.getVehicleRoutingSolutionDTO(problem.getPlannerSolution());
        }
        return DTOTranslator.getVehicleRoutingSolutionDTO(solution);
    }

    @RequestMapping(value = "/start", method = RequestMethod.POST)
    @ResponseBody
    public String solve(HttpSession httpSession, HttpServletResponse response) {
        LOGGER.debug("Starting solver for for session id:{}", httpSession.getId());
        try {
            solverService.startSolving();
        } catch (Exception e) {
            LOGGER.warn("Error launching solver: {}", e.getMessage());
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return e.getMessage();
        }
        return "Solving started";
    }

    @RequestMapping(value = "/terminate", method = RequestMethod.POST)
    @ResponseBody
    public String terminateEarly(HttpSession httpSession) {
        LOGGER.debug("Terminating solver for session id:{}", httpSession.getId());
        if (solverService.terminateSolving()) {
            problemDirector.setDomainSolution(solverService.getCurrentSolution());
            return "Solving terminated";
        } else {
            return "Solver has not been started";
        }
    }

}
