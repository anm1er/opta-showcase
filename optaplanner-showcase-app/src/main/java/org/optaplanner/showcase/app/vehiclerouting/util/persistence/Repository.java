package org.optaplanner.showcase.app.vehiclerouting.util.persistence;

import java.io.Serializable;
import java.util.List;

/**
 * Repository service for domain objects.
 * @param <T> Generic domain object type.
 */
public interface Repository<T extends Identifiable> {

    /**
     * Retrieves a domain object from the data store by id.
     * @param id the id to search for
     * @return the requested domain object if found or null if none were found
     */
    T findOne(Serializable id);

    /**
     * Checks if a domain object specified by the id exists in the data store.
     * @param id id of the domain object
     * @return {@code true} if the domain object with the specified id exists;
     * {@code false} otherwise.
     */
    boolean exists(Serializable id);

    /**
     * Returns the number of domain objects available in the data store.
     * @return the number of available domain objects
     */
    long count();

    /**
     * Retrieves all domain objects from the data store.
     * @return a {@code Collection} of domain objects or an empty {@code Collection} if none were found.
     */
    List<T> findAll();

    /**
     * Saves a domain object to the data store, either inserting or updating it.
     * @param entity the domain object to save
     * @return saved domain object
     */
    T save(T entity);

    /**
     * Removes a domain object from the data store.
     * @param entity the domain object to remove
     */
    void delete(T entity);

    /**
     * Removes all domain objects from the data store.
     */
    void deleteAll();

    /**
     * Method used for debugging purposes:
     * flushes and clears the persistence context, causing all managed entities to become detached.
     */
    void clearPersistenceContext();

}
