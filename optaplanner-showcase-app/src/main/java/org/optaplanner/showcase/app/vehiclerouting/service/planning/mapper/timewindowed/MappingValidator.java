package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed;

import org.optaplanner.showcase.app.vehiclerouting.util.mapper.MappingUtils;
import org.optaplanner.showcase.app.vehiclerouting.util.persistence.Identifiable;
import org.optaplanner.showcase.model.common.PlannerIdentifiable;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningDepot;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.problem.VehicleRoutingProblem;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;

public final class MappingValidator {

    private MappingValidator() {
    }

    public static void validateSolutionMappingRequest(final VehicleRoutingProblem solution) {
        MappingUtils.assertNotNull(solution, "solution");
        MappingUtils.assertNotNull(solution.getName(), "name");
        MappingUtils.assertNotNull(solution.getDistanceType(), "distanceType");
        MappingUtils.assertNotNull(solution.getLocationList(), "locations");
        MappingUtils.assertNotNull(solution.getCustomerList(), "customers");
        MappingUtils.assertNotNull(solution.getDepotList(), "depots");
        MappingUtils.assertNotNull(solution.getVehicleList(), "vehicles");
    }

    public static void validatePlanningBeanMappingRequest(PlannerIdentifiable planningBean, String planningBeanName) {
        MappingUtils.assertNotNull(planningBean, planningBeanName);
        MappingUtils.assertNotNull(planningBean.getId(), planningBeanName + " id");
    }

    public static void validateEntityBeanMappingRequest(Identifiable planningBean, String beanName) {
        MappingUtils.assertNotNull(planningBean, beanName);
        MappingUtils.assertNotNull(planningBean.getId(), beanName + " id");
    }

    public static void validateScheduledTime(int scheduledTime) {
        MappingUtils.assertPositive(scheduledTime, "scheduledTime");
    }

    /**
     * Verifies that the passed customer complies with VRPTW problem format.
     * @param customer
     * @return
     */
    public static PlanningTimeWindowedCustomer getValidatedCustomer(final PlanningCustomer customer) {
        MappingUtils.assertNotNull(customer, "Customer");
        MappingUtils.assertIsInstanceOf(PlanningTimeWindowedCustomer.class, customer);
        PlanningTimeWindowedCustomer timeWindowedCustomer = (PlanningTimeWindowedCustomer) customer;
        return timeWindowedCustomer;
    }

    /**
     * Verifies that the passed destination may serve as a valid depot for VRPTW problem.
     * @param depot
     * @return
     */
    public static PlanningTimeWindowedDepot getValidatedDepot(final PlanningDepot depot) {
        MappingUtils.assertNotNull(depot, "Depot");
        MappingUtils.assertIsInstanceOf(PlanningTimeWindowedDepot.class, depot);
        PlanningTimeWindowedDepot timeWindowedDepot = (PlanningTimeWindowedDepot) depot;
        return timeWindowedDepot;
    }

}
