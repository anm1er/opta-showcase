package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.CrossDomainMappingService;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.MappingServiceFactory;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.TimedBeanMapper;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem.TimeWindowedVehicleRoutingProblemFactory;

import org.joda.time.DateTime;

@Service
public class DefaultMappingServiceFactory implements MappingServiceFactory {

    private final TimedBeanMapper beanMapper;
    private final TimeWindowedVehicleRoutingProblemFactory problemFactory;

    @Autowired
    public DefaultMappingServiceFactory(TimedBeanMapper beanMapper, TimeWindowedVehicleRoutingProblemFactory problemFactory) {
        this.beanMapper = beanMapper;
        this.problemFactory = problemFactory;
    }

    @Override
    public CrossDomainMappingService getMappingServiceWithOrigin(DateTime dateTime) {
        return new CrossDomainMappingServiceImpl(beanMapper, problemFactory, dateTime);
    }

}
