package org.optaplanner.showcase.app.vehiclerouting.event.modification.timewindowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.optaplanner.showcase.app.vehiclerouting.event.EventSupport;
import org.optaplanner.showcase.app.vehiclerouting.event.modification.ModificationEvent;
import org.optaplanner.showcase.app.vehiclerouting.event.modification.ModificationEventType;
import org.optaplanner.showcase.app.vehiclerouting.event.modification.ProblemModificationListener;
import org.optaplanner.showcase.app.vehiclerouting.model.BaseEntity;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;

@Component
public class ProblemModificationEventSupport extends EventSupport<ProblemModificationListener> {

    @Autowired
    public ProblemModificationEventSupport(DelayedProblemModifier delayedProblemModifier) {
        addEventListener(delayedProblemModifier);
    }

    public void fireModificationStarted(ModificationEvent modificationEvent) {
        for (ProblemModificationListener listener : eventListeners) {
            if (modificationEvent == null) {
                listener.beforeModificationStarted();
            } else {
                BaseEntity domainObject = modificationEvent.getDomainObject();
                ModificationEventType eventType = modificationEvent.getEventType();
                if (domainObject instanceof TimeWindowedCustomer) {
                    listener.beforeCustomerModificationStarted((TimeWindowedCustomer) domainObject, eventType);
                } else if (domainObject instanceof TimeWindowedDepot) {
                    listener.beforeDepotModificationStarted((TimeWindowedDepot) domainObject, eventType);
                } else if (domainObject instanceof TimedVehicle) {
                    listener.beforeVehicleModificationStarted((TimedVehicle) domainObject, eventType);
                } else {
                    throw new IllegalArgumentException("Invalid domain entity type");
                }
            }
        }
    }

    public void fireModificationEnded(ModificationEvent modificationEvent) {
        for (ProblemModificationListener listener : eventListeners) {
            if (modificationEvent == null) {
                listener.afterModificationPerformed();
            } else {
                BaseEntity domainObject = modificationEvent.getDomainObject();
                ModificationEventType eventType = modificationEvent.getEventType();
                if (domainObject instanceof TimeWindowedCustomer) {
                    listener.afterCustomerModificationPerformed((TimeWindowedCustomer) domainObject, eventType);
                } else if (domainObject instanceof TimeWindowedDepot) {
                    listener.afterDepotModificationPerformed((TimeWindowedDepot) domainObject, eventType);
                } else if (domainObject instanceof TimedVehicle) {
                    listener.afterVehicleModificationPerformed((TimedVehicle) domainObject, eventType);
                } else {
                    throw new IllegalArgumentException("Invalid domain entity type");
                }
            }
        }
    }

}
