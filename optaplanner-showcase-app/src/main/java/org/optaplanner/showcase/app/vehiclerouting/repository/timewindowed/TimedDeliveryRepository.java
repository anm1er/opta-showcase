package org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedDelivery;
import org.optaplanner.showcase.app.vehiclerouting.util.persistence.BulkInsertRepository;
import org.optaplanner.showcase.app.vehiclerouting.util.persistence.Repository;

/**
 * Repository service for {@link TimedDelivery} problem domain entities.
 */
public interface TimedDeliveryRepository extends Repository<TimedDelivery>, BulkInsertRepository<TimedDelivery> {

}
