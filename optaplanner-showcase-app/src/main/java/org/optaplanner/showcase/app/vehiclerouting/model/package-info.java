/**
 *
 * The classes in this package represent the business layer of the sample vehicle routing problem.
 * Most classes' names mirror the domain model suggested for the vehicle routing problem in <em>Optalanner</em>.
 *
 * @see <a href="http://www.optaplanner.org">http://www.optaplanner.org</a>
 *
 */
package org.optaplanner.showcase.app.vehiclerouting.model;

