package org.optaplanner.showcase.app.vehiclerouting.service.planning.dto;

import java.util.ArrayList;
import java.util.List;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class DTOTranslator {

    private static final Logger LOGGER = LoggerFactory.getLogger(DTOTranslator.class);

    private DTOTranslator() {
    }

    public static VehicleRoutingSolutionDTO getVehicleRoutingSolutionDTO(final VehicleRoutingSolution solution) {
        LOGGER.debug("Mapping planning solution to VehicleRoutingSolutionDTO ");
        List<VehicleDTO> vehicleDTOs = new ArrayList<>(solution.getVehicleList().size());
        List<CustomerDTO> customerDTOs = new ArrayList<>(solution.getCustomerList().size());
        // Populates customers
        for (PlanningCustomer customer : solution.getCustomerList()) {
            PlanningLocation location = customer.getLocation();
            customerDTOs.add(new CustomerDTO(location.getLatitude(), location.getLongitude(),
                    location.getName(), customer.getDemand()));
        }
        // Populates vehicles
        for (PlanningVehicle vehicle : solution.getVehicleList()) {
            VehicleDTO vehicleDTO = new VehicleDTO();
            PlanningLocation depotLocation = vehicle.getDepot().getLocation();
            vehicleDTO.setDepotLatitude(depotLocation.getLatitude());
            vehicleDTO.setDepotLongitude(depotLocation.getLongitude());
            vehicleDTO.setDepotName(depotLocation.getName());
            List<CustomerDTO> scheduledCustomerDTOs = new ArrayList<>();
            PlanningCustomer customer = vehicle.getFirstCustomerToService();
            while (customer != null) {
                CustomerDTO customerDTO = new CustomerDTO();
                setScheduleDetails(customer, customerDTO);
                customerDTO.setDemand(customer.getDemand());
                PlanningLocation customerLocation = customer.getLocation();
                customerDTO.setLatitude(customerLocation.getLatitude());
                customerDTO.setLongitude(customerLocation.getLongitude());
                customerDTO.setName(customerLocation.getName());
                scheduledCustomerDTOs.add(customerDTO);
                customer = customer.getNextCustomer();
            }
            vehicleDTO.setCustomerDTOs(scheduledCustomerDTOs);
            vehicleDTOs.add(vehicleDTO);
        }
        return new VehicleRoutingSolutionDTO(vehicleDTOs, customerDTOs);
    }

    private static void setScheduleDetails(PlanningCustomer customer, CustomerDTO customerDTO) {
        if (customer instanceof PlanningTimeWindowedCustomer) {
            PlanningTimeWindowedCustomer timeWindowedCustomer = (PlanningTimeWindowedCustomer) customer;
            int offUnits = 0;
            int scheduledTime = timeWindowedCustomer.getScheduledTime();
            int readyTime = timeWindowedCustomer.getReadyTime();
            if (scheduledTime > 0) {
                int dueTime = timeWindowedCustomer.getDueTime();
                if (scheduledTime > dueTime) {
                    offUnits = scheduledTime - dueTime;
                } else if (scheduledTime < readyTime) {
                    offUnits = scheduledTime - readyTime;
                }
            }
            customerDTO.setOffUnits(offUnits);
        }
    }

}
