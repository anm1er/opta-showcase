package org.optaplanner.showcase.app.vehiclerouting.exception;

@SuppressWarnings("serial")
public class SolvingDeniedException extends RuntimeException {

    public SolvingDeniedException() {
    }

    public SolvingDeniedException(String message) {
        super(message);
    }

    public SolvingDeniedException(Throwable cause) {
        super(cause);
    }

    public SolvingDeniedException(String message, Throwable cause) {
        super(message, cause);
    }

}
