package org.optaplanner.showcase.app.vehiclerouting.service.persistence.timewindowed;

import java.io.Serializable;
import java.util.List;

import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedDelivery;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.service.persistence.PersistenceService;

/**
 * A service to manage persistence operations for domain entities of the vehicle routing problem with <b>time windows</b>.
 * Note: any operations that make the current solution invalid would result in all deliveries' removal within the same tx.
 */
public interface TimeWindowedPersistenceService extends PersistenceService {

    /**
     * Retrieves a {@code TimeWindowedCustomer} object by id.
     * @param id the id to search for
     * @return the {@code TimeWindowedCustomer} object if found
     * @throws org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException if not found
     */
    TimeWindowedCustomer getCustomer(Long id);

    /**
     * Retrieves a {@code TimeWindowedCustomer} object by id together with the schedule details for VRPTW problem.
     * @param id the id to search for
     * @return the {@code TimeWindowedCustomer} object if found
     * @throws org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException if not found
     */
    TimeWindowedCustomer getCustomerWithScheduleData(Long id);

    /**
     * Retrieves a {@code TimeWindowedCustomer} object by location together with the schedule details for VRPTW problem.
     * @param location the location to search for
     * @return the {@code TimeWindowedCustomer} object if found
     * @throws org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException if not found
     */
    TimeWindowedCustomer getCustomerByLocation(Location location);

    /**
     * Retrieves all {@code TimeWindowedCustomer} objects available.
     * @return a {@code List} of {@code TimeWindowedCustomer} objects or an empty {@code List} if none were found.
     */
    List<TimeWindowedCustomer> getCustomers();

    /**
     * Save changes to the {@code TimeWindowedCustomer} object and removes deliveries if any.
     * @param customer the {@code TimeWindowedCustomer} object to save
     * @return saved domain object
     */
    TimeWindowedCustomer saveCustomer(TimeWindowedCustomer customer);

    /**
     * Removes the {@code TimeWindowedCustomer} object from the data store by id and deliveries if any.
     * @param id id of the {@code TimeWindowedCustomer} object
     * @throws org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException if not found
     */
    void deleteCustomer(Serializable id);

    /**
     * Retrieves a {@code TimedVehicle} object by id.
     * @param id the id to search for
     * @return the {@code TimedVehicle} object if found
     * @throws org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException if not found
     */
    TimedVehicle getVehicle(Long id);

    /**
     * Retrieves a {@code TimedVehicle} object by id together with the schedule details for VRPTW problem.
     * @param id the id to search for
     * @return the {@code TimedVehicle} object if found
     * @throws org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException if not found
     */
    TimedVehicle getVehicleWithScheduleData(Long id);

    /**
     * Retrieves {@code TimedVehicle} object from the data store by plate number.
     * @param licensePlate the plate number to search for
     * @return the {@code TimedVehicle} object if found and {@code null} if not found
     */
    TimedVehicle getVehicleByLicensePlate(String licensePlate);

    /**
     * Retrieves all {@code TimedVehicle} objects available.
     * @return a {@code List} of {@code TimedVehicle} objects or an empty {@code List} if none were found.
     */
    List<TimedVehicle> getVehicles();

    /**
     * Save changes to the {@code TimedVehicle} object and removes deliveries if any.
     * @param vehicle the {@code TimedVehicle} to save
     * @return saved domain object
     */
    TimedVehicle saveVehicle(TimedVehicle vehicle);

    /**
     * Removes the {@code TimedVehicle} object from the data store by id and deliveries if any.
     * @param id id of the {@code TimedVehicle} object
     * @throws org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException if  not found
     */
    void deleteVehicle(Serializable id);

    /**
     * Retrieves a {@code TimeWindowedDepot} object by id.
     * @param id the id to search for
     * @return the {@code TimeWindowedDepot} object if found
     * @throws org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException if not found
     */
    TimeWindowedDepot getDepot(Long id);


    /**
     * Retrieves the single {@code TimeWindowedDepot} for the problem.
     * @return the {@code TimeWindowedDepot} object
     */
    TimeWindowedDepot getDepot();

    /**
     * Retrieves a list of presumably one {@code TimeWindowedDepot} object.
     * @return a {@code List} of a single {@code TimeWindowedDepot} object
     * @throws IllegalStateException if more than one {@code TimeWindowedDepot} objects were retrieved
     */
    List<TimeWindowedDepot> getDepots();

    /**
     * Save changes to the {@code TimeWindowedDepot} object and removes deliveries if any.
     * @param depot the {@code TimeWindowedDepot} to save
     * @return saved domain object
     */
    TimeWindowedDepot saveDepot(TimeWindowedDepot depot);

    /**
     * Removes the {@code TimeWindowedDepot} object from the data store by id and deliveries if any.
     * @param id id of the {@code TimeWindowedDepot} object
     * @throws org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException if  not found
     */
    void deleteDepot(Serializable id);

    /**
     * Retrieves all {@code TimedDelivery} objects available.
     * @return a {@code List} of {@code Delivery} objects or an empty {@code List} if none were found.
     */
    List<TimedDelivery> getDeliveries();

    /**
     * Saves all {@code Delivery} objects.
     * @param deliveries deliveries to save
     * @return a {@code List} of saved {@code Delivery} objects or an empty {@code List} if none were saved.
     */
    List<TimedDelivery> saveAllDeliveries(List<TimedDelivery> deliveries);

}
