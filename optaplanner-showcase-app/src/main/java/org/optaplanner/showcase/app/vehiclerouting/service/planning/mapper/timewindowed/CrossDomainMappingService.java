package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed;

import java.util.List;

import org.optaplanner.showcase.app.vehiclerouting.exception.MappingException;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedDelivery;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem.TimeWindowedVehicleRoutingProblem;

import org.joda.time.DateTime;

/**
 * Represents service to do cross-domain mapping.
 */
public interface CrossDomainMappingService {

    DateTime getTimeOrigin();

    /**
     * Maps problem with scheduled planning facts to the List of {@code Delivery} objects.
     * @param problem with scheduled planning facts
     * @return list of {@code Delivery} objects
     * @throws MappingException if exception occurs on mapping
     */
    List<TimedDelivery> mapSolvedPlanningProblemToDeliveryList(final TimeWindowedVehicleRoutingProblem problem);

    /**
     * Maps domain entities to problem with set planning facts.
     * @param customers domain customers
     * @param depots domain depots
     * @param vehicles domain vehicles
     * @return {@code TimeWindowedVehicleRoutingProblem}with set planning facts
     * @throws MappingException if exception occurs on mapping
     */
    TimeWindowedVehicleRoutingProblem mapDomainEntitiesToPlanningProblem(List<? extends TimeWindowedCustomer> customers,
            List<? extends TimeWindowedDepot> depots, List<? extends TimedVehicle> vehicles);

    /**
     * Maps deliveries to solved problem.
     * @param deliveries
     * @throws MappingException if exception occurs on mapping
     */
    TimeWindowedVehicleRoutingProblem mapDeliveryListToSolvedPlanningProblem(List<? extends TimedDelivery> deliveries);

    /**
     * Maps customer in planning domain to customer in problem domain.
     * @param planningCustomer planning bean to map from
     * @return resulting problem entity
     * @throws MappingException if exception occurs on mapping
     */
    TimeWindowedCustomer map(PlanningTimeWindowedCustomer planningCustomer);

    /**
     * Maps depot in planning domain to depot in problem domain.
     * @param planningDepot planning bean to map from
     * @return resulting problem entity
     * @throws MappingException if exception occurs on mapping
     */
    TimeWindowedDepot map(PlanningTimeWindowedDepot planningDepot);

    /**
     * Maps vehicle with its depot reference in planning domain to vehicle with the corresponding depot in problem domain.
     * <p/>
     * Note: In OptaPlanner model vehicle is a time windows-agnostic <em>problem fact</em>, while in the problem domain model
     * vehicles have to reference scheduling information. Hence, if the passed planningVehicle object references depot, whose
     * type is not compatible with {@code PlanningTimeWindowedDepot}, MappingException is thrown.
     * @param planningVehicle planning bean to map from
     * @return resulting problem entity
     * @throws MappingException if exception occurs on mapping
     */
    TimedVehicle map(PlanningVehicle planningVehicle);

    /**
     * Maps vehicle and customer in planning domain to {@code TimedDelivery}.
     * @param vehicle planning vehicle to map from
     * @param customer planning customer to map from
     * @return {@code TimedDelivery} object.
     */
    TimedDelivery map(PlanningVehicle vehicle, PlanningTimeWindowedCustomer customer);

    /**
     * Maps customer in problem domain to customer in planning domain.
     * @param customer problem entity to map from
     * @return resulting planning bean
     * @throws MappingException if exception occurs on mapping
     */
    PlanningTimeWindowedCustomer map(TimeWindowedCustomer customer);

    /**
     * Maps depot in problem domain to depot in planning domain.
     * @param depot problem entity to map from
     * @return resulting planning bean
     * @throws MappingException if exception occurs on mapping
     */
    PlanningTimeWindowedDepot map(TimeWindowedDepot depot);

    /**
     * Maps vehicle in problem domain to vehicle in planning domain.
     * @param vehicle problem entity to map from
     * @return resulting planning bean
     * @throws MappingException if exception occurs on mapping
     */
    PlanningVehicle map(TimedVehicle vehicle);

}
