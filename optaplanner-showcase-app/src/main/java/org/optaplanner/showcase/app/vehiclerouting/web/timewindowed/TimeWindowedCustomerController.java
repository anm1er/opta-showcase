package org.optaplanner.showcase.app.vehiclerouting.web.timewindowed;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import org.optaplanner.showcase.app.vehiclerouting.event.modification.ProblemModificationListener;
import org.optaplanner.showcase.app.vehiclerouting.event.modification.timewindowed.ProblemModificationEventSupport;
import org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.service.persistence.timewindowed.TimeWindowedPersistenceService;
import org.optaplanner.showcase.app.vehiclerouting.util.editors.DateTimeEditor;
import org.optaplanner.showcase.app.vehiclerouting.web.common.FlashMessageController;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Spring Web MVC controller exposing interface for customer-related operations.
 */
@Controller
@SessionAttributes("customer")
public class TimeWindowedCustomerController extends FlashMessageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeWindowedCustomerController.class);

    public static final String FEEDBACK_MESSAGE_KEY_CUSTOMER_ADDED = "feedback.message.customer.added";
    public static final String FEEDBACK_MESSAGE_KEY_CUSTOMER_UPDATED = "feedback.message.customer.updated";
    public static final String FEEDBACK_MESSAGE_KEY_CUSTOMER_DELETED = "feedback.message.customer.deleted";

    public static final String MODEL_ATTRIBUTE_CUSTOMER = "customer";
    public static final String MODEL_ATTRIBUTE_CUSTOMER_LIST = "customers";

    public static final String VIEW_CUSTOMER_LIST = "customers/list";
    public static final String VIEW_CUSTOMER_DETAILS = "customers/details";
    public static final String VIEW_CUSTOMER_ADD_OR_EDIT_FORM = "customers/addOrEditForm";

    public static final String CUSTOMERS_REQUEST = "customers";

    private final TimeWindowedPersistenceService persistenceService;
    private final ProblemModificationEventSupport problemModificationEventSupport;

    @Autowired
    public TimeWindowedCustomerController(TimeWindowedPersistenceService persistenceService, ProblemModificationEventSupport
            problemModificationEventSupport) {
        this.persistenceService = persistenceService;
        this.problemModificationEventSupport = problemModificationEventSupport;
    }

    @InitBinder
    public void setAllowedFields(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id", "timedDeliveries");
        // Converts empty string to null, since most validation rules fire only if the field isn't null
        dataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        // Converts the string to the DateTime using the registered editor
        dataBinder.registerCustomEditor(DateTime.class, new DateTimeEditor());
    }

    /**
     * Retrieves all customers from storage and adds a list containing all customers to the passed model.
     * @param model model
     * @return customer list view name
     */
    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public String listAllCustomers(Model model) {
        LOGGER.debug("Displaying customer list.");
        List<TimeWindowedCustomer> customers = persistenceService.getCustomers();
        model.addAttribute(MODEL_ATTRIBUTE_CUSTOMER_LIST, customers);
        LOGGER.debug("Added to model attribute: {}", MODEL_ATTRIBUTE_CUSTOMER_LIST);
        return VIEW_CUSTOMER_LIST;
    }

    /**
     * Retrieves the customer with id {@code id} and adds the customer to the passed model.
     * @param id customer ID
     * @param model model
     * @return customer details view name
     * @throws ResourceNotFoundException if the customer cannot be retrieved from storage
     */
    @RequestMapping(value = "/customers/{customerId}", method = RequestMethod.GET)
    public String viewCustomerDetails(@PathVariable("customerId") Long id, Model model) {
        LOGGER.debug("Displaying customer details for customer with id: {}", id);
        TimeWindowedCustomer requestedCustomer = persistenceService.getCustomer(id);
        model.addAttribute(MODEL_ATTRIBUTE_CUSTOMER, requestedCustomer);
        LOGGER.debug("Added to model attribute: {}", MODEL_ATTRIBUTE_CUSTOMER);
        return VIEW_CUSTOMER_DETAILS;
    }

    /**
     * Retrieves the customer with id {@code id} if it has not been already placed within session context
     * and adds the customer to the passed model.
     * @param id customer ID
     * @param customer customer added to model
     * @param model model
     * @return customer edit form view
     * @throws ResourceNotFoundException if the customer cannot be retrieved from storage
     */
    @RequestMapping(value = "/customers/{customerId}/edit", method = RequestMethod.GET)
    public String displayCustomerEditForm(@PathVariable("customerId") Long id,
            @Valid @ModelAttribute(MODEL_ATTRIBUTE_CUSTOMER) TimeWindowedCustomer customer, Model model) {
        LOGGER.debug("Displaying edit customer form for customer with id: {}", id);
        LOGGER.debug("Customer found in session scope has id {} ", customer.getId());
        TimeWindowedCustomer requestedCustomer;
        if (id.equals(customer.getId())) {
            LOGGER.debug("No hitting database required - the customer already stored in the session scope");
            requestedCustomer = customer;
        } else {
            LOGGER.debug("Hitting database for customer retrieval with id {} ", id);
            requestedCustomer = persistenceService.getCustomer(id);
        }
        model.addAttribute(MODEL_ATTRIBUTE_CUSTOMER, requestedCustomer);
        LOGGER.debug("Added to model attribute: {}", MODEL_ATTRIBUTE_CUSTOMER);
        return VIEW_CUSTOMER_ADD_OR_EDIT_FORM;
    }

    /**
     * Saves customer form object if it has no validation errors, adds flash message, fires modification event
     * for {@link ProblemModificationListener}s and redirects to the view with customer details.
     * @param customer customer form object
     * @param result a result holder for validation error registration
     * @param attributes attributes for a redirect scenario
     * @param status session status
     * @return redirected URL
     */
    @RequestMapping(value = "/customers/{customerId}/edit", method = RequestMethod.POST)
    public String processCustomerEditForm(@Valid @ModelAttribute(MODEL_ATTRIBUTE_CUSTOMER) TimeWindowedCustomer customer,
            BindingResult result, RedirectAttributes attributes, SessionStatus status) {
        LOGGER.debug("Updating customer");
        if (result.hasErrors()) {
            LOGGER.debug("Edit customer form was submitted with validation errors.");
            return VIEW_CUSTOMER_ADD_OR_EDIT_FORM;
        }
        TimeWindowedCustomer savedCustomer = persistenceService.saveCustomer(customer);
        LOGGER.debug("Updated the information of customer: {}", savedCustomer);
        status.setComplete();
        addFlashMessage(attributes, FEEDBACK_MESSAGE_KEY_CUSTOMER_UPDATED);
        problemModificationEventSupport.fireModificationEnded(null);
        return getRedirectedURL(CUSTOMERS_REQUEST, "{customerId}");
    }

    @RequestMapping(value = "/customers/{customerId}/cancel", method = RequestMethod.GET)
    public String viewCustomerDetailsAfterCancel(@PathVariable("customerId") Long id) {
        LOGGER.debug("Displaying customer details after cancelling update for customer {}", id);
        // Track cancels
        return VIEW_CUSTOMER_DETAILS;
    }

    /**
     * Adds new customer object to model.
     * @param model model
     * @return customer add form view
     */
    @RequestMapping(value = "/customers/add", method = RequestMethod.GET)
    public String displayCustomerAddForm(Model model) {
        LOGGER.debug("Displaying customer add form.");
        TimeWindowedCustomer formCustomerObject = new TimeWindowedCustomer();
        model.addAttribute(MODEL_ATTRIBUTE_CUSTOMER, formCustomerObject);
        LOGGER.debug("Added to model attribute: {}", MODEL_ATTRIBUTE_CUSTOMER);
        return VIEW_CUSTOMER_ADD_OR_EDIT_FORM;
    }

    /**
     * Saves customer form object if it has no validation errors, adds flash message, fires modification event
     * for {@link ProblemModificationListener}s and redirects to the view with customer details.
     * @param customer customer form object
     * @param result a result holder for validation error registration
     * @param attributes attributes for a redirect scenario
     * @param status session status
     * @return redirected URL
     */
    @RequestMapping(value = "/customers/add", method = RequestMethod.POST)
    public String processCustomerAddForm(@Valid @ModelAttribute(MODEL_ATTRIBUTE_CUSTOMER) TimeWindowedCustomer customer,
            BindingResult result, RedirectAttributes attributes, SessionStatus status) {
        LOGGER.debug("Adding new customer: {}", customer);
        if (result.hasErrors()) {
            LOGGER.debug("Add customer form was submitted with validation errors.");
            return VIEW_CUSTOMER_ADD_OR_EDIT_FORM;
        }
        TimeWindowedCustomer saved = persistenceService.saveCustomer(customer);
        LOGGER.debug("Added customer: {}", saved);
        status.setComplete();
        addFlashMessage(attributes, FEEDBACK_MESSAGE_KEY_CUSTOMER_ADDED);
        problemModificationEventSupport.fireModificationEnded(null);
        return getRedirectedURL(CUSTOMERS_REQUEST, String.valueOf(customer.getId()));
    }

    /**
     * Deletes customer by {@code id}.
     * @param id customer ID
     * @param customer requested customer
     * @param attributes attributes for a redirect scenario
     * @return redirected URL
     * @throws ResourceNotFoundException if the customer cannot be retrieved from storage
     */
    @RequestMapping(value = "/customers/{customerId}/delete", method = RequestMethod.GET)
    public String deleteCustomer(@PathVariable("customerId") Long id,
            @ModelAttribute(MODEL_ATTRIBUTE_CUSTOMER) TimeWindowedCustomer customer, RedirectAttributes attributes) {
        LOGGER.debug("Deleting customer with id: {}", id);
        persistenceService.deleteCustomer(id);
        addFlashMessage(attributes, FEEDBACK_MESSAGE_KEY_CUSTOMER_DELETED);
        problemModificationEventSupport.fireModificationEnded(null);
        return getRedirectedURL(CUSTOMERS_REQUEST);
    }

}
