package org.optaplanner.showcase.app.vehiclerouting.util.mapper;

import java.util.List;

import org.optaplanner.showcase.app.vehiclerouting.exception.MappingException;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.timewindowed.TimeWindowedCustomer;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public final class MappingUtils {

    private static final String CYCLES_DETECTED_MESSAGE = "cycles detected: customer has already been placed on delivery chain";

    private MappingUtils() {
    }

    public static void throwMappingException(String msg) {
        throw new MappingException(msg);
    }

    public static void throwMappingException(String source, Throwable cause) {
        throw new MappingException(getExceptionMessage(source, cause.getMessage()), cause);
    }

    public static void throwCyclesDetectedMappingException(String source) {
        throw new MappingException(getCyclesDetectedExceptionMessage(source));
    }

    public static void assertNotZero(int srcValue, String srcValueName) {
        checkArgument(srcValue != 0, srcValueName + " can't be zero");
    }

    public static void assertPositive(int srcValue, String srcValueName) {
        checkArgument(srcValue > 0, srcValueName + " must be positive");
    }

    public static void assertNotNull(Object srcObject, String srcObjectName) {
        checkNotNull(srcObject, srcObjectName + " can't be null");
    }

    public static void assertNotEmpty(List<?> srcList) {
        checkArgument(srcList.isEmpty(), "List can't be empty");
    }

    public static void assertIsInstanceOf(Class<?> clazz, Object obj) {
        checkArgument(clazz.isInstance(obj), getTypeMismatchExceptionMessage(clazz, obj));
    }

    public static String getExceptionMessage(String source, String cause) {
        return String.format("Error mapping %s: %s", source, cause);
    }

    public static String getCyclesDetectedExceptionMessage(String source) {
        return String.format("Error mapping %s: %s", source, CYCLES_DETECTED_MESSAGE);
    }

    public static String getTypeMismatchExceptionMessage(Class<?> clazz, Object obj) {
        return String.format("Object of class %s must be an instance of %s",
                (obj != null ? obj.getClass().getName() : "null"), clazz);
    }

    public static void printType(Class<?> clazz) {
        System.out.println(clazz);

    }

    public static void main(String[] args) {
        MappingUtils.printType(PlanningTimeWindowedCustomer.class);
        MappingUtils.printType(TimeWindowedCustomer.class);
    }

}
