package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed;

import org.joda.time.DateTime;

public interface MappingServiceFactory {

    CrossDomainMappingService getMappingServiceWithOrigin(DateTime dateTime);

}
