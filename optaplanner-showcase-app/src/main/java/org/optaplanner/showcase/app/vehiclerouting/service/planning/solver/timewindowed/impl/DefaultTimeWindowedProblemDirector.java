package org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.timewindowed.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedDelivery;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.service.persistence.timewindowed.TimeWindowedPersistenceService;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.MappingServiceFactory;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.origin.TimeOriginProvider;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.timewindowed.TimeWindowedProblemDirector;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem.TimeWindowedVehicleRoutingProblem;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem.TimeWindowedVehicleRoutingProblemFactory;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.INTERFACES)
public class DefaultTimeWindowedProblemDirector implements TimeWindowedProblemDirector {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultTimeWindowedProblemDirector.class);
    public static final int FACTOR = 1000;

    private final MappingServiceFactory mappingServiceFactory;
    private final TimeWindowedVehicleRoutingProblemFactory problemFactory;
    private final TimeWindowedPersistenceService persistenceService;
    private final TimeOriginProvider originProvider;

    @Autowired
    public DefaultTimeWindowedProblemDirector(MappingServiceFactory mappingServiceFactory, TimeOriginProvider originProvider,
            TimeWindowedVehicleRoutingProblemFactory problemFactory, TimeWindowedPersistenceService persistenceService) {
        this.mappingServiceFactory = mappingServiceFactory;
        this.problemFactory = problemFactory;
        this.persistenceService = persistenceService;
        this.originProvider = originProvider;
    }

    @Override
    public TimeWindowedVehicleRoutingProblem getCurrentDomainProblem() {
        return initializeProblemInstance();
    }

    @Override
    public TimeWindowedVehicleRoutingProblem getProblemForOrigin(DateTime timeOrigin) {
        checkNotNull(timeOrigin, "timeOrigin can't be null");
        return initializeProblemInstance(timeOrigin);
    }

    /**
     * Initializes a new problem instance with all dates mapped w.r.t. the time origin extracted from the depot object.
     * @return
     */
    public TimeWindowedVehicleRoutingProblem initializeProblemInstance() {
        return initializeProblemInstance(getDefaultTimeOrigin());
    }

    public TimeWindowedVehicleRoutingProblem initializeProblemInstance(DateTime timeOrigin) {
        LOGGER.debug("Setting new problem instance from domain data with time origin {}", timeOrigin);
        List<TimeWindowedCustomer> domainCustomers = persistenceService.getCustomers();
        List<TimeWindowedDepot> domainDepots = persistenceService.getDepots();
        List<TimedVehicle> domainVehicles = persistenceService.getVehicles();
        LOGGER.debug("Initialized mapping service for date time origin: {}", timeOrigin);
        return mappingServiceFactory.getMappingServiceWithOrigin(timeOrigin)
                .mapDomainEntitiesToPlanningProblem(domainCustomers, domainDepots, domainVehicles);
    }

    @Override
    public void setDomainSolution(VehicleRoutingSolution solution) {
        checkNotNull(solution, "solution can't be null");
        TimeWindowedVehicleRoutingProblem problem = problemFactory.createProblem(solution);
        LOGGER.debug("Saving solution to problem {}", problem);
        List<TimedDelivery> deliveries = mappingServiceFactory
                .getMappingServiceWithOrigin(getDefaultTimeOrigin())
                .mapSolvedPlanningProblemToDeliveryList(problem);
        List<TimedDelivery> savedDeliveries = persistenceService.saveAllDeliveries(deliveries);
        LOGGER.debug("Saved {} deliveries", savedDeliveries.size());
    }

    private DateTime getDefaultTimeOrigin() {
        return originProvider.getTimeOriginFromDomainObject(persistenceService.getDepot());
    }

    // This is a workaround to handle time windows as expected
    private TimeWindowedVehicleRoutingProblem getFixedProblem(TimeWindowedVehicleRoutingProblem problem) {
        for (PlanningCustomer customer : problem.getCustomerList()) {
            if (customer instanceof PlanningTimeWindowedCustomer) {
                PlanningTimeWindowedCustomer timeWindowedCustomer = (PlanningTimeWindowedCustomer) customer;
                timeWindowedCustomer.setReadyTime(timeWindowedCustomer.getReadyTime() * FACTOR);
                timeWindowedCustomer.setDueTime(timeWindowedCustomer.getDueTime() * FACTOR);
                timeWindowedCustomer.setServiceDuration(timeWindowedCustomer.getServiceDuration() * FACTOR);
            }
        }
        PlanningTimeWindowedDepot timeWindowedDepot = (PlanningTimeWindowedDepot) problem.getDepotList().get(0);
        timeWindowedDepot.setReadyTime(timeWindowedDepot.getReadyTime() * FACTOR);
        timeWindowedDepot.setDueTime(timeWindowedDepot.getDueTime() * FACTOR);
        return problem;
    }

    private TimeWindowedVehicleRoutingProblem getProblemCopy(TimeWindowedVehicleRoutingProblem problem) {
        return problemFactory.getProblemBuilder()
                .from(problem)
                .build();
    }

}
