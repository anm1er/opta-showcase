package org.optaplanner.showcase.app.vehiclerouting.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.core.style.ToStringCreator;

@SuppressWarnings("serial")
@MappedSuperclass
public class Vehicle<T extends Depot> extends BaseEntity {

    public static final int LICENSE_PLATE_LENGTH = 7;
    public static final int MINIMUM_CAPACITY = 50;

    /**
     * The vehicle's capacity (in product items): <em>A vehicle cannot carry more product items then its capacity allows.</em>
     */
    @NotNull
    @Min(MINIMUM_CAPACITY)
    private Integer capacity;

    /**
     * A vehicle's registration plate number represents its natural identity.
     * <p/>
     * Note: <em>The pattern of the plate number is arbitrarily chosen to serve demonstrative purposes only</em>.
     */
    @Column(unique = true)
    @NotNull
    @Size(min = LICENSE_PLATE_LENGTH, max = LICENSE_PLATE_LENGTH)
    @Pattern(regexp = "^[A-Z]{3}-[0-9]{3}$")
    private String licensePlate;

    /**
     * A single depot serves as the starting point for any <em>feasible</em> vehicle-specific route.
     */
    @ManyToOne
    @JoinColumn(name = "depot_id")
    @NotNull
    private T depot;

    public static Builder getBuilder() {
        return new DefaultBuilder();
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public T getDepot() {
        return depot;
    }

    public void setDepot(T depot) {
        this.depot = depot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Vehicle<?> vehicle = (Vehicle<?>) o;
        return Objects.equals(licensePlate, vehicle.licensePlate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(licensePlate);
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("licensePlate", licensePlate)
                .toString();
    }

    public abstract static class Builder<D extends Depot, T extends Vehicle<D>,
            B extends Builder<D, T, B>> {

        protected T built;

        private B builder;

        protected Builder() {
            built = createVehicle();
            builder = getBuilder();
        }

        public B withCapacity(Integer capacity) {
            built.setCapacity(capacity);
            return builder;
        }

        public B withLicensePlate(String licensePlate) {
            built.setLicensePlate(licensePlate);
            return builder;
        }

        public B withDepot(D depot) {
            built.setDepot(depot);
            return builder;
        }

        public T build() {
            return built;
        }

        protected abstract T createVehicle();

        protected abstract B getBuilder();
    }

    private static class DefaultBuilder extends Vehicle.Builder<Depot, Vehicle<Depot>, DefaultBuilder> {

        @Override
        protected Vehicle createVehicle() {
            return new Vehicle();
        }

        @Override
        protected DefaultBuilder getBuilder() {
            return this;
        }
    }

}
