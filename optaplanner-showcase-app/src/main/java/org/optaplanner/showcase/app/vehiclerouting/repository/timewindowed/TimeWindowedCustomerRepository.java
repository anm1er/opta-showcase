package org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed;

import java.io.Serializable;

import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.util.persistence.Repository;

/**
 * Repository service for {@link TimeWindowedCustomer} problem domain entities to retrieve
 * {@code TimeWindowedCustomer} instances together with the schedule details for VRPTW problem,
 * (the schedule details resulting from planning activities and encapsulated inside {@code timedDeliveries}).
 */
public interface TimeWindowedCustomerRepository extends Repository<TimeWindowedCustomer> {

    /**
     * Retrieves {@code TimeWindowedCustomer} object from the data store by location.
     * @param location the location to search for
     * @return the {@code TimeWindowedCustomer} object if found {@code null} if not found
     */
    TimeWindowedCustomer findOneByLocation(Location location);

    /**
     * Retrieves {@code TimeWindowedCustomer} object from the data store by id together with the schedule details.
     * @param id the id to search for
     * @return the requested {@code TimeWindowedCustomer} object if found and {@code null} if not found
     */
    TimeWindowedCustomer findOneWithScheduledData(Serializable id);

}
