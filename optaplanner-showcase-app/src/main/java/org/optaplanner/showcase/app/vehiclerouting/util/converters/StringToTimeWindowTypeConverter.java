package org.optaplanner.showcase.app.vehiclerouting.util.converters;

import java.util.Properties;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindow;

import org.joda.time.DateTime;

public class StringToTimeWindowTypeConverter implements Converter<String, TimeWindow> {

    public static final String FIRST_PROPERTY = "readyTime";
    public static final String SECOND_PROPERTY = "dueTime";

    @Override
    public TimeWindow convert(String s) {
        String windowString = StringUtils.deleteAny(s, "[]");
        String[] propertyStrings = StringUtils.split(windowString.substring(windowString.indexOf(FIRST_PROPERTY)), ",");
        Properties properties = StringUtils.splitArrayElementsIntoProperties(propertyStrings, "=");
        TimeWindow timeWindow;
        try {
            DateTime readyTime = DateTime.parse(properties.getProperty(FIRST_PROPERTY));
            DateTime dueTime = DateTime.parse(properties.getProperty(SECOND_PROPERTY));
            timeWindow = new TimeWindow(readyTime, dueTime);
        } catch (Exception e) {
            throw new ConversionFailedException(TypeDescriptor.valueOf(String.class),
                    TypeDescriptor.valueOf(TimeWindow.class), s, null);
        }
        return timeWindow;
    }

}
