package org.optaplanner.showcase.app.vehiclerouting.event.modification.timewindowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import org.optaplanner.core.impl.solver.ProblemFactChange;
import org.optaplanner.showcase.app.vehiclerouting.event.modification.ModificationEventType;
import org.optaplanner.showcase.app.vehiclerouting.event.modification.ProblemModificationListener;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.SolverService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Presents a simplified approach to handle problem modifications so that the whole problem mappings can be reduced.
 * It uses some assumptions about user workflow and problem consistency and is not meant for contrived scenarios.
 * <p/>
 * Note: <i>Delayed</i> refers to the fact that modifications are only made for the next solving procedure
 * iff no solving is already in process (solver has been terminated or hasn't been started).
 * Changes with solving already launched should be handled via {@link ProblemFactChange} interface.
 * In fact, this would cause Solver stop, do the ProblemFactChange, <strong>restart</strong> and run each phase again.
 */
@Component
public class DelayedProblemModifier implements ProblemModificationListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(DelayedProblemModifier.class);

    //private final TimeWindowedProblemDirector problemDirector;
    private final SolverService solverService;

    @Autowired
    public DelayedProblemModifier(@Qualifier("domain") SolverService solverService) {
        this.solverService = solverService;
    }

    @Override
    public void afterModificationPerformed() {
        LOGGER.debug("Handling afterModificationPerformed");
        solverService.setSolutionOutdated();
        LOGGER.debug("Solution is set to sync with persistent state");
    }

    @Override
    public void beforeModificationStarted() {
        // No action
    }

    @Override
    public void beforeCustomerModificationStarted(TimeWindowedCustomer customer, ModificationEventType eventType) {
        // No action
    }

    @Override
    public void beforeDepotModificationStarted(TimeWindowedDepot depot, ModificationEventType eventType) {
        // No action
    }

    @Override
    public void beforeVehicleModificationStarted(TimedVehicle vehicle, ModificationEventType eventType) {
        // No action
    }

    @Override
    public void afterDepotModificationPerformed(TimeWindowedDepot depot, ModificationEventType eventType) {
        // No action
    }

    @Override
    public void afterCustomerModificationPerformed(TimeWindowedCustomer customer, ModificationEventType eventType) {
        // No action
    }

    @Override
    public void afterVehicleModificationPerformed(TimedVehicle vehicle, ModificationEventType eventType) {
        // No action
    }

}
