package org.optaplanner.showcase.app.vehiclerouting.util.persistence;

import java.io.Serializable;

/**
 * Guarantees that any persistent entity provides ID.
 */
public interface Identifiable {

    /**
     * @return The primary key, or ID, of this entity
     */
    Serializable getId();

}
