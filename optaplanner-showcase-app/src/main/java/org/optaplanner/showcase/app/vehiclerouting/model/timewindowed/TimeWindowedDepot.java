package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.optaplanner.showcase.app.vehiclerouting.model.Depot;

/**
 * A {@code TimeWindowedDepot} is a depot with a <b>time window</b> associated with it,
 * defining the <em>scheduling horizon</em> for the given instance of VRPTW.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "depots", uniqueConstraints = @UniqueConstraint(columnNames = {"latitude", "longitude"}))
public class TimeWindowedDepot extends Depot implements TimeWindowed {

    /**
     * The time window wherein all customers have to be supplied.
     */
    @NotNull
    @Valid
    private TimeWindow timeWindow = new TimeWindow();

    @OneToMany(mappedBy = "depot", cascade = CascadeType.ALL)
    private Set<TimedVehicle> vehicles = new HashSet<>();

    public static Builder getBuilder() {
        return new Builder();
    }

    public TimeWindow getTimeWindow() {
        return timeWindow;
    }

    public void setTimeWindow(TimeWindow timeWindow) {
        this.timeWindow = timeWindow;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode());
    }

    public static class Builder extends Depot.Builder<TimeWindowedDepot, Builder> {

        public Builder withTimeWindow(TimeWindow timewindow) {
            built.setTimeWindow(timewindow);
            return this;
        }

        @Override
        protected TimeWindowedDepot createDepot() {
            return new TimeWindowedDepot();
        }

        @Override
        protected Builder getBuilder() {
            return this;
        }
    }

}
