package org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.impl.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedDelivery;
import org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.TimedDeliveryRepository;
import org.optaplanner.showcase.app.vehiclerouting.util.persistence.AbstractJpaRepository;

/**
 * JPA implementation of the interface {@link TimedDeliveryRepository}.
 */
@Repository
public class JpaTimedDeliveryRepositoryImpl extends AbstractJpaRepository<TimedDelivery>
        implements TimedDeliveryRepository {

    @Value("${hibernate.jdbc.batch_size}")
    private int batchSize;

    public JpaTimedDeliveryRepositoryImpl() {
        super(TimedDelivery.class);
    }

    @Override
    public List<TimedDelivery> saveAll(List<TimedDelivery> entities) {
        final List<TimedDelivery> entitiesToSave = new ArrayList<>(entities.size());
        EntityManager entityManager = getEntityManager();
        int i = 0;
        for (TimedDelivery timedDelivery : entities) {
            entitiesToSave.add(save(timedDelivery));
            i++;
            if (i % batchSize == 0) {
                // Flushes a batch of inserts and releases memory
                entityManager.flush();
                entityManager.clear();
            }
        }
        return entitiesToSave;
    }

}
