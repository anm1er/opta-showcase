/**
 *
 * The classes in this package represent flat dtos for rendering basic scheduled data.
 *
 */
package org.optaplanner.showcase.app.vehiclerouting.service.planning.dto;

