package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.impl;

import java.util.ArrayList;
import java.util.List;

import org.optaplanner.showcase.app.vehiclerouting.exception.MappingException;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedDelivery;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.MappingValidator;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.TimedBeanMapper;
import org.optaplanner.showcase.app.vehiclerouting.util.mapper.MappingUtils;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A utility class to assist in problem mappings.
 */
public class ProblemMapperAssist {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProblemMapperAssist.class);

    private final TimedBeanMapper timedBeanMapper;
    private final DateTime origin;

    public ProblemMapperAssist(TimedBeanMapper defaultTimedBeanMapper, DateTime origin) {
        this.timedBeanMapper = defaultTimedBeanMapper;
        this.origin = origin;
    }

    /**
     * Maps customers in planning domain to customers in problem domain.
     * @param sourceCustomers customers in planning domain
     * @return customers in problem domain
     * @throws MappingException if exception occurs on mapping
     */
    public List<TimeWindowedCustomer> mapPlanningCustomers(List<? extends PlanningTimeWindowedCustomer> sourceCustomers) {
        LOGGER.debug("Mapping {} source (planning facts) to destination (domain entities) customers.", sourceCustomers.size());
        MappingUtils.assertNotNull(sourceCustomers, "sourceCustomers");
        List<TimeWindowedCustomer> destinationCustomers;
        destinationCustomers = new ArrayList<>(sourceCustomers.size());
        for (PlanningTimeWindowedCustomer sourceCustomer : sourceCustomers) {
            TimeWindowedCustomer customer = timedBeanMapper.map(sourceCustomer, origin);
            LOGGER.trace("Mapped planning customer-{} to domain customer.", sourceCustomer.getId());
            destinationCustomers.add(customer);
        }
        LOGGER.debug("Created {} problem domain customers.", destinationCustomers.size());
        return destinationCustomers;
    }

    /**
     * Maps depots in planning domain to depots in problem domain.
     * @param sourceDepots depots in planning domain
     * @return depots in problem domain
     * @throws MappingException if exception occurs on mapping
     */
    public List<TimeWindowedDepot> mapPlanningDepots(List<? extends PlanningTimeWindowedDepot> sourceDepots) {
        LOGGER.debug("Mapping {} source (planning facts) to destination (domain entities) depots", sourceDepots.size());
        MappingUtils.assertNotNull(sourceDepots, "sourceDepots");
        List<TimeWindowedDepot> destinationDepots;
        destinationDepots = new ArrayList<>(sourceDepots.size());
        for (PlanningTimeWindowedDepot sourceDepot : sourceDepots) {
            TimeWindowedDepot depot = timedBeanMapper.map(sourceDepot, origin);
            LOGGER.trace("Mapped planning depot-{} to domain depot.", sourceDepot.getId());
            destinationDepots.add(depot);
        }
        LOGGER.debug("Created {} problem domain depots.", destinationDepots.size());
        return destinationDepots;
    }

    /**
     * Maps vehicles in planning domain to vehicles in problem domain together with their corresponding depot references.
     * <p/>
     * Note: In OptaPlanner model vehicle is a time windows-agnostic <em>problem fact</em>, while in the problem domain model
     * vehicles have to reference scheduling information. Hence, if the passed planningVehicle object references depot, whose
     * type is not compatible with {@code PlanningTimeWindowedDepot}, MappingException is thrown.
     * @param sourceVehicles vehicles in planning domain
     * @return vehicles in problem domain
     * @throws MappingException when not receiving list of PlanningTimedWindowedCustomer
     */
    public List<TimedVehicle> mapPlanningVehicles(List<? extends PlanningVehicle> sourceVehicles) {
        LOGGER.debug("Mapping {} source (planning facts) to destination (domain entities) vehicles"
                + " together with their corresponding depot references.", sourceVehicles.size());
        MappingUtils.assertNotNull(sourceVehicles, "sourceVehicles");
        List<TimedVehicle> destinationVehicles;
        destinationVehicles = new ArrayList<>(sourceVehicles.size());
        for (PlanningVehicle sourceVehicle : sourceVehicles) {
            TimedVehicle vehicle = timedBeanMapper.map(sourceVehicle, origin);
            LOGGER.trace("Mapped planning vehicle-{} to domain vehicle.", sourceVehicle.getId());
            destinationVehicles.add(vehicle);
        }
        LOGGER.debug("Created {} problem domain vehicles.", destinationVehicles.size());
        return destinationVehicles;
    }

    /**
     * Maps vehicles to a list of {@code TimedDelivery} objects iff the passed vehicles have the reference to the
     * customer chain, in which each customer has valid time value scheduled by the planner for the corresponding vehicle
     * arrival.
     * <p/>
     * @param vehicles vehicle list with the reference to customer chain to service
     * @return {@code TimedDelivery} list with the scheduled vehicle-customer matches
     * @throws MappingException if exception occurs on mapping
     */
    public List<TimedDelivery> mapPlanningVehiclesToDeliveries(List<? extends PlanningVehicle> vehicles) {
        LOGGER.debug("Mapping {} planning vehicles to TimedDelivery list", vehicles.size());
        MappingUtils.assertNotNull(vehicles, "sourceVehicles");
        List<TimedDelivery> deliveries = new ArrayList<>();
        TimedDelivery delivery;
        for (PlanningVehicle vehicle : vehicles) {
            PlanningCustomer customer = vehicle.getFirstCustomerToService();
            while (customer != null) {
                PlanningTimeWindowedCustomer timeWindowedCustomer;
                // Verifies that the referenced customer complies with VRPTW format and has nonnull time scheduled for service
                timeWindowedCustomer = MappingValidator.getValidatedCustomer(customer);
                delivery = timedBeanMapper.map(vehicle, timeWindowedCustomer, origin);
                // Tracks cycles in the delivery chain
                if (deliveries.contains(delivery)) {
                    MappingUtils.throwCyclesDetectedMappingException("vehicle-" + vehicle.getId()
                            + " with customer-" + customer.getId());
                }
                LOGGER.trace("Mapped vehicle-{} with customer-{} to TimedDelivery.", vehicle.getId(), customer.getId());
                deliveries.add(delivery);
                customer = customer.getNextCustomer();
            }
        }
        LOGGER.debug("Created {} TimedDelivery instances", deliveries.size());
        return deliveries;
    }

    /**
     * Maps customers in problem domain to customers in planning domain.
     * @param sourceCustomers customers in problem domain
     * @return customers in planning domain
     * @throws MappingException if exception occurs on mapping
     */
    public List<PlanningTimeWindowedCustomer> mapCustomers(List<? extends TimeWindowedCustomer> sourceCustomers) {
        LOGGER.debug("Mapping {} source (domain entities) to destination (planning facts) customers", sourceCustomers.size());
        MappingUtils.assertNotNull(sourceCustomers, "sourceCustomers");
        //checkAndInitializeDateTimeTranslations(sourceCustomers);
        List<PlanningTimeWindowedCustomer> destinationCustomers;
        destinationCustomers = new ArrayList<>(sourceCustomers.size());
        for (TimeWindowedCustomer sourceCustomer : sourceCustomers) {
            PlanningTimeWindowedCustomer planningCustomer = timedBeanMapper.map(sourceCustomer, origin);
            LOGGER.trace("Mapped problem domain customer-{} to planning customer.", sourceCustomer.getId());
            destinationCustomers.add(planningCustomer);
        }
        LOGGER.debug("Created {} planning customers.", destinationCustomers.size());
        return destinationCustomers;
    }

    /**
     * Maps depots in problem domain to customers in planning domain.
     * @param sourceDepots depots in problem domain
     * @return depots in planning domain
     * @throws MappingException if exception occurs on mapping
     */
    public List<PlanningTimeWindowedDepot> mapDepots(List<? extends TimeWindowedDepot> sourceDepots) {
        LOGGER.debug("Mapping {} source (domain entities) to destination (planning facts) depots.", sourceDepots.size());
        MappingUtils.assertNotNull(sourceDepots, "sourceDepots");
        //checkAndInitializeDateTimeTranslations(sourceDepots);
        List<PlanningTimeWindowedDepot> destinationDepots;
        destinationDepots = new ArrayList<>(sourceDepots.size());
        for (TimeWindowedDepot sourceDepot : sourceDepots) {
            PlanningTimeWindowedDepot planningDepot = timedBeanMapper.map(sourceDepot, origin);
            LOGGER.trace("Mapped domain depot-{} to planning depot.", sourceDepot.getId());
            destinationDepots.add(planningDepot);
        }
        LOGGER.debug("Created {} planning depots.", destinationDepots.size());
        return destinationDepots;
    }

    /**
     * Maps vehicles in problem domain to customers in planning domain.
     * @param sourceVehicles vehicles in problem domain
     * @return vehicles in planning domain
     * @throws MappingException if exception occurs on mapping
     * @throws IllegalArgumentException if the passed vehicles do not comply with VR problem with time windows format
     */
    public List<PlanningVehicle> mapVehicles(List<? extends TimedVehicle> sourceVehicles) {
        LOGGER.debug("Mapping {} source (domain entities) to destination (planning facts) vehicles.", sourceVehicles.size());
        MappingUtils.assertNotNull(sourceVehicles, "sourceVehicles");
        //checkAndInitializeDateTimeTranslationsForVehicles(sourceVehicles);
        List<PlanningVehicle> destinationVehicles;
        destinationVehicles = new ArrayList<>(sourceVehicles.size());
        for (TimedVehicle sourceVehicle : sourceVehicles) {
            PlanningVehicle planningVehicle = timedBeanMapper.map(sourceVehicle, origin);
            LOGGER.trace("Mapped problem domain vehicle-{} to planning vehicle.", sourceVehicle.getId());
            destinationVehicles.add(planningVehicle);
        }
        LOGGER.debug("Created {} planning vehicles.", destinationVehicles.size());
        return destinationVehicles;
    }

}

