package org.optaplanner.showcase.app.vehiclerouting.service.planning.solver;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.showcase.app.vehiclerouting.exception.InvalidProblemInstanceException;
import org.optaplanner.showcase.app.vehiclerouting.exception.SolvingDeniedException;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.problem.VehicleRoutingProblem;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

public interface SolverService {

    /**
     * Marks the solution outdated (not synced with the current domain state).
     */
    void setSolutionOutdated();

    /**
     * Marks the solution updated (synced with the current domain state).
     * @return true if solution has been marked updated, false if solution is up-to-date.
     */
    boolean trySetSolutionUpdated();

    /**
     * Starts solving a provided instance of vehicle routing problem.
     * @param problem vehicle routing problem instance
     * @throws InvalidProblemInstanceException if none of the valid problem instances have been provided
     * @throws SolvingDeniedException if solving has been requested within the same session
     */
    void startSolving(VehicleRoutingProblem problem);

    /**
     * Starts solving an instance of vehicle routing problem}.
     * @throws InvalidProblemInstanceException if none of the valid problem instances have been provided
     * @throws SolvingDeniedException if solving has been requested within the same session
     */
    void startSolving();

    /**
     * Returns {@code true} if solver has been successfully terminated.
     * @return  {@code true} if solver has been terminated
     */
    boolean terminateSolving();

    /**
     * Returns {@code true} if solving is in process for the session identified by {@code sessionId}.
     * @return {@code true} if solving is in process
     */
    boolean isBusy();

    /**
     * Returns {@code VehicleRoutingSolution} either populated with scheduled planning facts,
     * or <i>empty</i> solution if solver has not been triggered for the corresponding problem instance.
     * <p/>
     * Empty solution contains only the planning facts that are related to problem definition, and, e.g.
     * no customer references another customer in delivery chain or no vehicle references the first customer to service.
     * @return {@code VehicleRoutingSolution} instance, or {@code null} if no solution has been set.
     */
    VehicleRoutingSolution getCurrentSolution();

    /**
     * Returns solver.
     * @return
     */
    Solver getSolver();

    /**
     * Sets {@code VehicleRoutingProblem}.
     * @param problem
     */
    void setProblem(VehicleRoutingProblem problem);

}

