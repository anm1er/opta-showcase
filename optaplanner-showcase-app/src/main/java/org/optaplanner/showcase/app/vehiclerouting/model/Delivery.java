package org.optaplanner.showcase.app.vehiclerouting.model;

import java.util.Objects;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.springframework.core.style.ToStringCreator;

/**
 * A {@code Delivery} is the process of transporting product items by a distinct vehicle to a distinct customer (on the route
 * from depot to final customer).
 * @param <C> Customer scheduled for delivery
 * @param <V> Vehicle scheduled for delivery
 */
@SuppressWarnings("serial")
@MappedSuperclass
public class Delivery<C extends Customer, V extends Vehicle> extends BaseEntity {

    /**
     * A particular vehicle scheduled for the delivery.
     */
    @ManyToOne
    @JoinColumn(name = "vehicle_id")
    @NotNull
    private V vehicle;

    /**
     * A particular customer scheduled for the delivery.
     * <p/>
     * <p/>
     * Note: In real life the demand of a customer is unlikely to exceed the capacity of any vehicle in a fleet,
     * so that in most cases only a single {@code Customer} will be mapped to a single {@code TimedDelivery}.
     */
    @ManyToOne
    @JoinColumn(name = "customer_id")
    @NotNull
    private C customer;

    public V getVehicle() {
        return vehicle;
    }

    public void setVehicle(V vehicle) {
        this.vehicle = vehicle;
    }

    public C getCustomer() {
        return customer;
    }

    public void setCustomer(C customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Delivery<?, ?> delivery = (Delivery<?, ?>) o;
        return Objects.equals(vehicle, delivery.vehicle)
                && Objects.equals(customer, delivery.customer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vehicle, customer);
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("customer", customer)
                .append("vehicle", vehicle)
                .toString();
    }

}
