package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.comparator;

import java.io.Serializable;
import java.util.Comparator;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;

public class ReadyTimeCustomerComparator implements Comparator<TimeWindowedCustomer>, Serializable {

    @Override
    public int compare(TimeWindowedCustomer a, TimeWindowedCustomer b) {
        return a.getTimeWindow().getReadyTime().
                compareTo(b.getTimeWindow().getReadyTime());
    }

}
