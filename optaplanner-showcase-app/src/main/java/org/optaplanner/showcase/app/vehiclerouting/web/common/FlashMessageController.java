package org.optaplanner.showcase.app.vehiclerouting.web.common;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public abstract class FlashMessageController {

    public static final String REDIRECT = "redirect:/";
    public static final String FLASH_MESSAGE_KEY_FEEDBACK = "feedbackMessage";

    private static final Logger LOGGER = LoggerFactory.getLogger(FlashMessageController.class);

    @Autowired
    private MessageSource messageSource;

    protected void addFlashMessage(RedirectAttributes attributes, String messageCode, Object... messageParameters) {
        LOGGER.debug("Adding feedback message with code: {} and params: {}", messageCode, messageParameters);
        String localizedFeedbackMessage = getMessage(messageCode, messageParameters);
        LOGGER.debug("Localized message is: {}", localizedFeedbackMessage);
        attributes.addFlashAttribute(FLASH_MESSAGE_KEY_FEEDBACK, localizedFeedbackMessage);
    }

    protected String getRedirectedURL(String... components) {
        StringBuilder redirectedURL = new StringBuilder();
        redirectedURL.append(REDIRECT);
        for (String component : components) {
            redirectedURL.append(component);
            redirectedURL.append("/");
        }
        return redirectedURL.toString();
    }

    private String getMessage(String messageCode, Object... messageParameters) {
        Locale current = LocaleContextHolder.getLocale();
        LOGGER.debug("Current locale is {}", current);
        return messageSource.getMessage(messageCode, messageParameters, current);
    }

}
