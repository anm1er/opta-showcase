package org.optaplanner.showcase.app.vehiclerouting.event.modification;

/**
 * Represents CUD operation-specific event type.
 */
public enum ModificationEventType {
    CREATE,
    UPDATE,
    DELETE
}
