package org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed;

import java.io.Serializable;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.util.persistence.Repository;

/**
 * Repository service for {@link TimedVehicle} problem domain entities to retrieve
 * {@code TimedVehicle} instances together with the schedule details for VRPTW problem,
 * (the schedule details resulting from planning activities and encapsulated inside {@code timedDeliveries}).
 */
public interface TimedVehicleRepository extends Repository<TimedVehicle> {

    /**
     * Retrieves {@code TimedVehicle} object from the data store by plate number.
     * @param licensePlate the plate number to search for
     * @return the {@code TimedVehicle} object if found and {@code null} if not found
     */
    TimedVehicle findOneByPlate(String licensePlate);

    /**
     * Retrieves {@code TimedVehicle} object from the data store by id together with the schedule details.
     * @param id the id to search for
     * @return the requested {@code TimedVehicle} object if found and {@code null} if not found
     */
    TimedVehicle findOneWithScheduledData(Serializable id);

}
