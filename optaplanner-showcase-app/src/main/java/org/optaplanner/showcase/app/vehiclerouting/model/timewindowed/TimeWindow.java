package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.validation.ReadyTimeNotAfterDueTime;

import org.springframework.core.style.ToStringCreator;

/**
 * A reusable representation of time window - a time interval between {@code readyTime} and {@code dueTime}.
 */
@SuppressWarnings("serial")
@Embeddable
@ReadyTimeNotAfterDueTime(
        readyTimeFieldName = "readyTime",
        dueTimeFieldName = "dueTime"
)
public class TimeWindow implements Serializable {

    /**
     * Ready time is the <em>time, <b>before</b> which a vehicle <b>may</b> arrive
     * at the customer's location or at the depot, but <b>must not</b> start servicing.</em>
     */
    @NotNull
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime readyTime = new DateTime();

    /**
     * Due time is the <em>time, <b>before</b> which a vehicle
     * <b>must</b> arrive at the customer's location or at the depot.</em>
     */
    @NotNull
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime dueTime = new DateTime();

    public TimeWindow() {
    }

    public TimeWindow(DateTime readyTime, DateTime dueTime) {
        this.readyTime = readyTime;
        this.dueTime = dueTime;
    }

    public DateTime getReadyTime() {
        return readyTime;
    }

    public void setReadyTime(DateTime readyTime) {
        this.readyTime = readyTime;
    }

    public DateTime getDueTime() {
        return dueTime;
    }

    public void setDueTime(DateTime dueTime) {
        this.dueTime = dueTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TimeWindow that = (TimeWindow) o;
        return Objects.equals(readyTime, that.readyTime)
                && Objects.equals(dueTime, that.dueTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(readyTime, dueTime);
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("readyTime", readyTime)
                .append("dueTime", dueTime)
                .toString();
    }

}
