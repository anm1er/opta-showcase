package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = ReadyTimeNotAfterDueTimeValidator.class)
@Documented
public @interface ReadyTimeNotAfterDueTime {

    String message() default "{javax.validation.constraints.ReadyTimeNotAfterDueTime.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String readyTimeFieldName() default "";

    String dueTimeFieldName() default "";

}
