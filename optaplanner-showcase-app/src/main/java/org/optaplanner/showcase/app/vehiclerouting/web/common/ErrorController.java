package org.optaplanner.showcase.app.vehiclerouting.web.common;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ControllerAdvice
public class ErrorController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorController.class);

    public static final String VIEW_DEBUG_PAGE = "error/debug";
    public static final String VIEW_INTERNAL_SERVER_ERROR = "error/status500";
    public static final String VIEW_NOT_FOUND = "error/status404";

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String resourceNotFoundExceptionHandler() {
        return VIEW_NOT_FOUND;
    }

    @ExceptionHandler(Exception.class)
    public String resourceInternalServerExceptionHandler(Exception e) {
        if (e.getCause() instanceof ConstraintViolationException) {
            return VIEW_DEBUG_PAGE;
        } else {
            return VIEW_INTERNAL_SERVER_ERROR;
        }
    }

}
