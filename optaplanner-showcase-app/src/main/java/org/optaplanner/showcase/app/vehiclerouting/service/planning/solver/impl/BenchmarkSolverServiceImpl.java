package org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import org.optaplanner.core.config.solver.termination.TerminationConfig;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.SolverService;

/**
 * This service presents a quick and simple solution to try test problem sets separately from domain problem.
 */
@Service("benchmark")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.INTERFACES)
public class BenchmarkSolverServiceImpl extends SolverServiceImpl implements SolverService {

    @Autowired
    public BenchmarkSolverServiceImpl(@Value("${solverConfigFile}") String solverConfigFile, TaskExecutor taskExecutor,
            TerminationConfig solverTerminationConfig) {
        super(solverConfigFile, taskExecutor, solverTerminationConfig);
    }

}
