package org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.util.persistence.Repository;

/**
 * Repository service for {@link TimeWindowedDepot} problem domain entities.
 */
public interface TimeWindowedDepotRepository extends Repository<TimeWindowedDepot> {

}
