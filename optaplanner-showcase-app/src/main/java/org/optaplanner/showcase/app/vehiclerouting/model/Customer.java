package org.optaplanner.showcase.app.vehiclerouting.model;

import javax.persistence.MappedSuperclass;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.core.style.ToStringCreator;


// Note: <em>We are currently not concerned with "plain" customers, but we still would like to match the planning domain
// model hierarchy</em>.
@SuppressWarnings("serial")
@MappedSuperclass
public class Customer extends BaseEntity {

    /**
     * The demand of a customer (in product items) is a positive value.
     */
    @NotNull
    @Min(1)
    private Integer demand;

    /**
     * The location of a customer within a region.
     */
    @NotNull
    @Valid
    private Location location = new Location();

    public static Builder getBuilder() {
        return new DefaultBuilder();
    }

    public Integer getDemand() {
        return demand;
    }

    public void setDemand(Integer demand) {
        this.demand = demand;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Customer customer = (Customer) o;
        return java.util.Objects.equals(location, customer.location);
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(location);
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("location", location)
                .toString();
    }

    public abstract static class Builder<T extends Customer,
            B extends Builder<T, B>> {

        protected T built;

        private B builder;

        protected Builder() {
            built = createCustomer();
            builder = getBuilder();
        }

        public B withDemand(Integer demand) {
            built.setDemand(demand);
            return builder;
        }

        public B withLocation(Location location) {
            built.setLocation(location);
            return builder;
        }

        public T build() {
            return built;
        }

        protected abstract T createCustomer();

        protected abstract B getBuilder();
    }

    private static class DefaultBuilder extends Customer.Builder<Customer, DefaultBuilder> {

        @Override
        protected Customer createCustomer() {
            return new Customer();
        }

        @Override
        protected DefaultBuilder getBuilder() {
            return this;
        }
    }

}
