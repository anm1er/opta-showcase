package org.optaplanner.showcase.app.vehiclerouting.util.persistence;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Basic abstract JPA implementation of {@link Repository} interface.
 * @param <T> Generic domain object type
 */
public abstract class AbstractJpaRepository<T extends Identifiable> implements Repository<T> {

    @PersistenceContext
    private EntityManager entityManager;

    private Class<T> domainClass;

    public AbstractJpaRepository(Class<T> domainClass) {
        this.domainClass = domainClass;
    }

    @Override
    public T findOne(Serializable id) {
        checkNotNull(id, "id can't be null");
        return entityManager.find(domainClass, id);
    }

    @Override
    public boolean exists(Serializable id) {
        checkNotNull(id, "id can't be null");
        return (entityManager.find(domainClass, id) != null);
    }

    @Override
    public long count() {
        return (Long) entityManager.createQuery("select count(*) from " + getDomainClassName())
                .getSingleResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<T> findAll() {
        return entityManager.createQuery("from " + getDomainClassName())
                .getResultList();
    }

    @Override
    public T save(T entity) {
        checkNotNull(entity, getDomainClassName() + " entity can't be null");
        if (entity.getId() == null) {
            entityManager.persist(entity);
            return entity;
        } else {
            return entityManager.merge(entity);
        }
    }

    @Override
    public void delete(T entity) {
        checkNotNull(entity, getDomainClassName() + " entity can't be null");
        entityManager.remove(entityManager.merge(entity));
    }

    @Override
    public void deleteAll() {
        entityManager.createQuery("delete " + getDomainClassName())
                .executeUpdate();
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @SuppressWarnings("unchecked")
    private Class<T> getDomainClass() {
        if (domainClass == null) {
            ParameterizedType thisType = (ParameterizedType) getClass().getGenericSuperclass();
            this.domainClass = (Class<T>) thisType.getActualTypeArguments()[0];
        }
        return domainClass;
    }

    private String getDomainClassName() {
        return getDomainClass().getName();
    }

    @Override
    public void clearPersistenceContext() {
        entityManager.flush();
        entityManager.clear();
    }
}
