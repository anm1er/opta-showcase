package org.optaplanner.showcase.app.vehiclerouting.event;

import java.util.EventListener;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class EventSupport<L extends EventListener> {

    protected Set<L> eventListeners = new CopyOnWriteArraySet();

    public void addEventListener(L eventListener) {
        eventListeners.add(eventListener);
    }

    public void removeEventListener(L eventListener) {
        eventListeners.remove(eventListener);
    }

}
