package org.optaplanner.showcase.app.vehiclerouting.exception;

@SuppressWarnings("serial")
public class InvalidProblemInstanceException extends RuntimeException {

    public InvalidProblemInstanceException() {
    }

    public InvalidProblemInstanceException(String message) {
        super(message);
    }

    public InvalidProblemInstanceException(Throwable cause) {
        super(cause);
    }

    public InvalidProblemInstanceException(String message, Throwable cause) {
        super(message, cause);
    }

}
