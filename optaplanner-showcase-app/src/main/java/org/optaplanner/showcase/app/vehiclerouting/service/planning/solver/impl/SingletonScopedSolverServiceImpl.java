package org.optaplanner.showcase.app.vehiclerouting.service.planning.solver.impl;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.api.solver.event.BestSolutionChangedEvent;
import org.optaplanner.core.api.solver.event.SolverEventListener;
import org.optaplanner.core.config.solver.termination.TerminationConfig;
import org.optaplanner.showcase.app.vehiclerouting.exception.InvalidProblemInstanceException;
import org.optaplanner.showcase.app.vehiclerouting.exception.SolvingDeniedException;
import org.optaplanner.showcase.app.vehiclerouting.util.planning.PlannerConfigDetailsExtractor;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.DistanceType;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.problem.VehicleRoutingProblem;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

@Deprecated
public class SingletonScopedSolverServiceImpl /*implements SolverService*/ {

    private static final Logger LOGGER = LoggerFactory.getLogger(SingletonScopedSolverServiceImpl.class);

    private final SolverFactory solverFactory;
    private final ConcurrentMap<String, Solver> requestSolvers = new ConcurrentHashMap<>();
    private final ConcurrentMap<String, VehicleRoutingSolution> solutions = new ConcurrentHashMap<>();

    private TaskExecutor taskExecutor;

    private String solverConfigurationXmlResource;

    @Autowired
    public SingletonScopedSolverServiceImpl(@Value("${solverConfigFile}") String solverConfigFile, TaskExecutor taskExecutor,
            TerminationConfig solverTerminationConfig) {
        solverConfigurationXmlResource = solverConfigFile;
        this.taskExecutor = taskExecutor;
        solverFactory = SolverFactory.createFromXmlResource(solverConfigurationXmlResource);
        solverFactory.getSolverConfig().setTerminationConfig(solverTerminationConfig);
    }

    public void startSolving(final VehicleRoutingProblem problem, final String sessionId) {
        checkNotNull(checkNotNull(problem).getPlannerSolution(), "Problem instance can't be null");
        checkNotNull(sessionId, "sessionId can't be null");
        LOGGER.debug("Starting solving problem {} for sessionId {}", problem, sessionId);
        final Solver solver = initializeSolver(sessionId);
        registerEventListeners(sessionId, solver);
        setSolution(problem.getPlannerSolution(), sessionId);
        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                solver.solve(getCheckedCurrentSolution(sessionId));
                solutions.put(sessionId, (VehicleRoutingSolution) solver.getBestSolution());
            }
        });
    }

    public void startSolving(final String sessionId) {
        checkNotNull(sessionId, "sessionId can't be null");
        LOGGER.debug("Starting solving problem with sessionId {}", sessionId);
        final Solver solver = initializeSolver(sessionId);
        registerEventListeners(sessionId, solver);

        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                solver.solve(getCheckedCurrentSolution(sessionId));
                solutions.put(sessionId, (VehicleRoutingSolution) solver.getBestSolution());
            }
        });
    }

    public synchronized boolean terminateSolving(String sessionId) {
        checkNotNull(sessionId, "sessionId can't be null");
        LOGGER.debug("Terminating solver for sessionId {}", sessionId);
        Solver solver = requestSolvers.remove(sessionId);
        if (solver != null) {
            solver.terminateEarly();
            return true;
        } else {
            return false;
        }
    }

    public synchronized boolean isSolving(String sessionId) {
        return requestSolvers.get(sessionId).isSolving();
    }

    public void setProblem(final VehicleRoutingProblem problem, String sessionId) {
        checkNotNull(problem, "Problem instance can't be null");
        checkNotNull(problem.getPlannerSolution(), "The problem's solution can't be null");
        checkNotNull(sessionId, "sessionId can't be null");
        LOGGER.debug("Setting problem {} for sessionId {}", problem, sessionId);
        setSolution(problem.getPlannerSolution(), sessionId);
    }

    public VehicleRoutingSolution getCurrentSolution(String sessionId) {
        checkNotNull(sessionId, "sessionId can't be null");
        return solutions.get(sessionId);
    }

    /**
     * Performs sanity checks for the solution associated with the {@code sessionId}.
     * @param sessionId
     * @return
     */
    public VehicleRoutingSolution getCheckedCurrentSolution(String sessionId) {
        checkNotNull(sessionId, "sessionId can't be null");
        VehicleRoutingSolution solution = solutions.get(sessionId);
        if (solution != null) {
            synchronized (solutions) {
                assertState(!solution.getCustomerList().isEmpty(), "customerList can't be empty");
                assertState(!solution.getDepotList().isEmpty(), "depotList can't be empty");
                assertState(!solution.getVehicleList().isEmpty(), "vehiclesList can't be empty");
                assertState(solution.getLocationList().size() == solution.getCustomerList().size() + solution.getDepotList()
                                .size(),
                        "locations number not equal to customers + depots number");
                assertState(solution.getDistanceType() != DistanceType.UNDETERMINED, "distance type should be determined");
            }
        }
        return solution;
    }

    /**
     * Sets or resets solution for the passed {@code sessionId}.
     * @param newSolution
     * @param sessionId
     * @return {@code true} if solution for the {@code sessionId} has been REset
     */
    private boolean setSolution(final VehicleRoutingSolution newSolution, String sessionId) {
        VehicleRoutingSolution oldSolution = solutions.putIfAbsent(sessionId, newSolution);
        if (oldSolution == null) {
            return false;
        }
        return (solutions.replace(sessionId, oldSolution, newSolution));
    }

    private Solver initializeSolver(final String sessionId) {
        LOGGER.debug("Building Solver using {} for score configuration and sessionId: {}", PlannerConfigDetailsExtractor
                .getScoreConfigurationDetails(solverFactory.getSolverConfig().getScoreDirectorFactoryConfig()), sessionId);
        Solver solver;
        if (requestSolvers.containsKey(sessionId)) {
            // Checking key and throwing is not atomic
            synchronized (requestSolvers) {
                if (requestSolvers.containsKey(sessionId)) {
                    throw new SolvingDeniedException("Cannot trigger new solving phase for the same sessionId" + sessionId);
                } else {
                    requestSolvers.put(sessionId, solverFactory.buildSolver());
                    solver = requestSolvers.get(sessionId);
                }
            }
        } else {
            requestSolvers.putIfAbsent(sessionId, solverFactory.buildSolver());
            solver = requestSolvers.get(sessionId);
        }
        return solver;
    }

    private void registerEventListeners(final String sessionId, final Solver solver) {
        // Registers event listener to listen to best solution change
        solver.addEventListener(new SolverEventListener() {
            @Override
            public void bestSolutionChanged(BestSolutionChangedEvent event) {
                if (event.isNewBestSolutionInitialized()) {
                    VehicleRoutingSolution bestSolution = (VehicleRoutingSolution) event.getNewBestSolution();
                    solutions.put(sessionId, bestSolution);
                }
            }
        });
    }

    private void assertState(boolean expression, String message) {
        if (!expression) {
            throw new InvalidProblemInstanceException(message);
        }
    }

}
