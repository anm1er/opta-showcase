package org.optaplanner.showcase.app.vehiclerouting.service.planning.dto;

import java.util.List;

public class VehicleRoutingSolutionDTO {

    private List<VehicleDTO> vehicleDTOs;
    private List<CustomerDTO> customerDTOs;

    public VehicleRoutingSolutionDTO() {
    }

    public VehicleRoutingSolutionDTO(List<VehicleDTO> vehicleDTOs, List<CustomerDTO> customerDTOs) {
        this.vehicleDTOs = vehicleDTOs;
        this.customerDTOs = customerDTOs;
    }

    public List<VehicleDTO> getVehicleDTOs() {
        return vehicleDTOs;
    }

    public void setVehicleDTOs(List<VehicleDTO> vehicleDTOs) {
        this.vehicleDTOs = vehicleDTOs;
    }

    public List<CustomerDTO> getCustomerDTOs() {
        return customerDTOs;
    }

    public void setCustomerDTOs(List<CustomerDTO> customerDTOs) {
        this.customerDTOs = customerDTOs;
    }

}
