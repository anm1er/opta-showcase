package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.optaplanner.showcase.app.vehiclerouting.model.Delivery;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

/**
 * A {@code TimedDelivery} is the process of transporting product items by a distinct vehicle to a distinct customer
 * <b>within a predefined time interval</b> (on the route from depot to final customer) scheduled for that particular vehicle.
 * <p/>
 * Provided that timed deliveries scheduled by a planner correspond to a <em>feasible</em> VRP route, those deliveries that
 * relate to a single vehicle (more rarely, a single customer) form an <em>ordered</em> sequence w.r.t. {@code arrivalTime} -
 * the time scheduled for the arrival of a vehicle to service a particular customer.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "deliveries", uniqueConstraints = @UniqueConstraint(columnNames = {"vehicle_id", "customer_id"}))
public class TimedDelivery extends Delivery<TimeWindowedCustomer, TimedVehicle> {

    /**
     * Represents the time scheduled for the arrival of a given vehicle to a particular customer.
     */
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime arrivalTime = new DateTime();

    public static Builder getBuilder() {
        return new Builder();
    }

    public DateTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(DateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return super.equals(o);
    }

    @Override public int hashCode() {
        return Objects.hash(super.hashCode());
    }

    public static final class Builder {

        private TimedDelivery built;

        private Builder() {
            built = new TimedDelivery();
        }

        public TimedDelivery build() {
            return built;
        }

        public Builder withVehicle(TimedVehicle vehicle) {
            built.setVehicle(vehicle);
            return this;
        }

        public Builder withCustomer(TimeWindowedCustomer customer) {
            built.setCustomer(customer);
            return this;
        }

        public Builder withArrivalTime(DateTime arrivalTime) {
            built.arrivalTime = arrivalTime;
            return this;
        }
    }

}
