package org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.impl.jpa;

import java.io.Serializable;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import org.optaplanner.showcase.app.vehiclerouting.model.Location;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.TimeWindowedCustomerRepository;
import org.optaplanner.showcase.app.vehiclerouting.util.persistence.AbstractJpaRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * JPA implementation of the interface {@link TimeWindowedCustomerRepository}.
 */
@Repository
public class JpaTimeWindowedCustomerRepositoryImpl extends AbstractJpaRepository<TimeWindowedCustomer>
        implements TimeWindowedCustomerRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(JpaTimeWindowedCustomerRepositoryImpl.class);

    public JpaTimeWindowedCustomerRepositoryImpl() {
        super(TimeWindowedCustomer.class);
    }

    @Override
    public TimeWindowedCustomer findOneByLocation(Location location) {
        checkNotNull(location, "location can't be null");
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();
        TypedQuery<TimeWindowedCustomer> query = getEntityManager()
                .createQuery("from TimeWindowedCustomer c WHERE c.location.longitude=:longitude"
                                + " and c.location.latitude=:latitude", TimeWindowedCustomer.class)
                .setParameter("longitude", longitude)
                .setParameter("latitude", latitude);
        TimeWindowedCustomer found = null;
        try {
            found = query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.warn("No customer found with the specified location: ", e);
        }
        return found;
    }

    @Override
    public TimeWindowedCustomer findOneWithScheduledData(Serializable id) {
        checkNotNull(id, "id can't be null");
        TypedQuery<TimeWindowedCustomer> query = getEntityManager()
                .createQuery("from TimeWindowedCustomer c left join fetch c.timedDeliveries WHERE c.id=:id",
                        TimeWindowedCustomer.class)
                .setParameter("id", id);
        TimeWindowedCustomer found = null;
        try {
            found = query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.warn("No customer found with the specified id: ", e);
        }
        return found;
    }

}
