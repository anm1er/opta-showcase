package org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.impl.jpa;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.TimeWindowedDepotRepository;
import org.optaplanner.showcase.app.vehiclerouting.util.persistence.AbstractJpaRepository;

import org.springframework.stereotype.Repository;

/**
 * JPA implementation of the interface {@link TimeWindowedDepotRepository} interface.
 */
@Repository
public class JpaTimeWindowedDepotRepositoryImpl extends AbstractJpaRepository<TimeWindowedDepot>
        implements TimeWindowedDepotRepository {

    public JpaTimeWindowedDepotRepositoryImpl() {
        super(TimeWindowedDepot.class);
    }

}
