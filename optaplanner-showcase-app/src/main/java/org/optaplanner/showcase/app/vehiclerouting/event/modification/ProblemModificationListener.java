package org.optaplanner.showcase.app.vehiclerouting.event.modification;

import java.util.EventListener;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;

public interface ProblemModificationListener extends EventListener {

    void beforeModificationStarted();

    /**
     *
     * @param customer domain entity which is about to change
     * @param eventType
     */
    void beforeCustomerModificationStarted(TimeWindowedCustomer customer, ModificationEventType eventType);

    /**
     *
     * @param depot domain entity which is about to change
     * @param eventType
     */
    void beforeDepotModificationStarted(TimeWindowedDepot depot, ModificationEventType eventType);

    /**
     *
     * @param vehicle domain entity which is about to change
     * @param eventType
     */
    void beforeVehicleModificationStarted(TimedVehicle vehicle, ModificationEventType eventType);

    void afterModificationPerformed();

    /**
     *
     * @param customer modified domain entity
     * @param eventType
     */
    void afterCustomerModificationPerformed(TimeWindowedCustomer customer, ModificationEventType eventType);

    /**
     *
     * @param depot modified domain entity
     * @param eventType
     */
    void afterDepotModificationPerformed(TimeWindowedDepot depot, ModificationEventType eventType);

    /**
     *
     * @param vehicle updated domain entity
     * @param eventType
     */
    void afterVehicleModificationPerformed(TimedVehicle vehicle, ModificationEventType eventType);

}
