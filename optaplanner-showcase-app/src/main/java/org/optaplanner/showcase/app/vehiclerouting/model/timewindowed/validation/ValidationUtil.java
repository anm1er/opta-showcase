package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.validation;

import java.lang.reflect.Field;

import javax.validation.ConstraintValidatorContext;

public final class ValidationUtil {

    private ValidationUtil() {
    }

    public static void addValidationError(String field, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
                .addPropertyNode(field)
                .addConstraintViolation();
    }

    public static Object getFieldValue(Object object, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        Field f = object.getClass().getDeclaredField(fieldName);
        f.setAccessible(true);
        return f.get(object);
    }

}
