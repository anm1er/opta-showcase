package org.optaplanner.showcase.app.vehiclerouting.util.persistence;

import java.util.List;

public interface BulkInsertRepository<T extends Identifiable> {

    /**
     * Saves all entities.
     * @param entities entities to save
     * @return a {@code List} of saved entities or an empty {@code List} if none were saved.
     */
    List<T> saveAll(List<T> entities);

}
