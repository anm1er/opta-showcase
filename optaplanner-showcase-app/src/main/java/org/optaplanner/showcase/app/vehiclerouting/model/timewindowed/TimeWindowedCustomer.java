package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.optaplanner.showcase.app.vehiclerouting.model.Customer;

/**
 * A {@code TimeWindowedCustomer} is a customer with a <b>time window</b>,
 * a time interval wherein the customer has to be supplied:
 * <ul>
 * <li>A solution becomes infeasible if a customer is supplied <em>after</em> the upper bound of its time window.</li>
 * <li>A vehicle arriving <em>before</em> the lower limit of the time window causes additional waiting time.</li>
 * </ul>
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "customers", uniqueConstraints = @UniqueConstraint(columnNames = {"latitude", "longitude"}))
public class TimeWindowedCustomer extends Customer implements TimeWindowed {

    /**
     * The time window wherein the customer has to be supplied.
     */
    @NotNull
    @Valid
    private TimeWindow timeWindow = new TimeWindow();

    /**
     * The customer's service duration is the <em>time (in full minutes), <b>during</b> which a vehicle
     * <b>is guaranteed</b> to be available at the customer's location.</em>
     */
    @NotNull
    @Min(1)
    private Integer serviceDuration;

    @OneToMany(mappedBy = "customer")
    @Column(name = "deliveries")
    private Set<TimedDelivery> timedDeliveries = new HashSet<>();

    public static Builder getBuilder() {
        return new Builder();
    }

    @Override
    public TimeWindow getTimeWindow() {
        return timeWindow;
    }

    @Override
    public void setTimeWindow(TimeWindow timeWindow) {
        this.timeWindow = timeWindow;
    }

    public Set<TimedDelivery> getTimedDeliveries() {
        return timedDeliveries;
    }

    public void setTimedDeliveries(Set<TimedDelivery> timedDeliveries) {
        this.timedDeliveries = timedDeliveries;
    }

    public Integer getServiceDuration() {
        return serviceDuration;
    }

    public void setServiceDuration(Integer serviceDuration) {
        this.serviceDuration = serviceDuration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode());
    }

    public static class Builder extends Customer.Builder<TimeWindowedCustomer, Builder> {

        public Builder withServiceDuration(Integer duration) {
            built.setServiceDuration(duration);
            return this;
        }

        public Builder withTimeWindow(TimeWindow timewindow) {
            built.setTimeWindow(timewindow);
            return this;
        }

        @Override
        protected TimeWindowedCustomer createCustomer() {
            return new TimeWindowedCustomer();
        }

        @Override
        protected Builder getBuilder() {
            return this;
        }
    }

}
