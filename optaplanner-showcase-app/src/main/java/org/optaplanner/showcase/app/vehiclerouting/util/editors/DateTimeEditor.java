package org.optaplanner.showcase.app.vehiclerouting.util.editors;

import java.beans.PropertyEditorSupport;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class DateTimeEditor extends PropertyEditorSupport {

    /**
     * The desired date format pattern.
     */
    private static final String PATTERN = "yyyy/MM/dd H:mm";

    /**
     * Returns the formatted string representation of a date.
     */
    @Override
    public String getAsText() {
        return ((DateTime) getValue()).toString(PATTERN);
    }

    /**
     * Sets the date from its string representation.
     * @param date string representation of a date
     */
    @Override
    public void setAsText(String date) {
        setValue(DateTime.parse(date, DateTimeFormat.forPattern(PATTERN)));
    }

}
