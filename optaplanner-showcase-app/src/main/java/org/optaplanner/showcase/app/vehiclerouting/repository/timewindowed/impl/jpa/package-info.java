
/**
 * The current JPA implementation follows the strategy suggested by Spring Data JPA:
 * we let the service layer decide whether or not query for non-existing elements should be treated as exceptional behaviour.
 * <p/>
 * We end up tracking the cases of NoResultException thrown, the rest being translated to DataAccessException hierarchy.
 */
package org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.impl.jpa;

