package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.joda.time.DateTime;

public class ReadyTimeNotAfterDueTimeValidator implements ConstraintValidator<ReadyTimeNotAfterDueTime, Object> {

    private String readyTimeFieldName;

    private String dueTimeFieldName;

    @Override
    public void initialize(final ReadyTimeNotAfterDueTime constraintAnnotation) {
        readyTimeFieldName = constraintAnnotation.readyTimeFieldName();
        dueTimeFieldName = constraintAnnotation.dueTimeFieldName();
    }

    /**
     * Verifies that dueTime of the {@code TimeWindow} does not precede readyTime.
     * Assumes that non-equality to null should be guaranteed by {@link @NotNull} constraint.
     * @param value
     * @param context
     * @return
     */
    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {
        try {
            Object readyTime = ValidationUtil.getFieldValue(value, readyTimeFieldName);
            Object dueTime = ValidationUtil.getFieldValue(value, dueTimeFieldName);

            // Leave null-checking to @NotNull on individual parameters
            if (readyTime == null || dueTime == null) {
                return true;
            }

            if (!(readyTime instanceof DateTime) || !(dueTime instanceof DateTime)) {
                throw new IllegalArgumentException(
                        "Expected two parameters of type" + DateTime.class.getName()
                );
            }

            if (((DateTime) readyTime).isAfter((DateTime) dueTime)) {
                ValidationUtil.addValidationError(readyTimeFieldName, context);
                ValidationUtil.addValidationError(dueTimeFieldName, context);

                return false;
            }
        } catch (Exception ex) {
            throw new RuntimeException("Exception occurred during validation", ex);
        }
        return true;
    }

}
