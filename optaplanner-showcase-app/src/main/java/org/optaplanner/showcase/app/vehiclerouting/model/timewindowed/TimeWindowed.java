package org.optaplanner.showcase.app.vehiclerouting.model.timewindowed;

public interface TimeWindowed {

    TimeWindow getTimeWindow();

    void setTimeWindow(TimeWindow timeWindow);

}
