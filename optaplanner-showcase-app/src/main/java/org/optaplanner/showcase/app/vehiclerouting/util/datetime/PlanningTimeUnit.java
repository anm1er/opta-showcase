package org.optaplanner.showcase.app.vehiclerouting.util.datetime;

public enum PlanningTimeUnit {
    SECONDS,
    MINUTES,
    HOURS
}
