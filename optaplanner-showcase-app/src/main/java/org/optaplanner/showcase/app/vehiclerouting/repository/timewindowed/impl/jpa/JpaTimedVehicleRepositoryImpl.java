package org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.impl.jpa;

import java.io.Serializable;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.repository.timewindowed.TimedVehicleRepository;
import org.optaplanner.showcase.app.vehiclerouting.util.persistence.AbstractJpaRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * JPA implementation of the interface {@link TimedVehicleRepository}.
 */
@Repository
public class JpaTimedVehicleRepositoryImpl extends AbstractJpaRepository<TimedVehicle>
        implements TimedVehicleRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(JpaTimedVehicleRepositoryImpl.class);

    public JpaTimedVehicleRepositoryImpl() {
        super(TimedVehicle.class);
    }

    @Override
    public TimedVehicle findOneByPlate(String licensePlate) {
        checkNotNull(licensePlate, "licensePlate can't be null");
        TypedQuery<TimedVehicle> query = getEntityManager()
                .createQuery("from TimedVehicle v WHERE v.licensePlate=:licensePlate",
                        TimedVehicle.class)
                .setParameter("licensePlate", licensePlate);
        TimedVehicle found = null;
        try {
            found = query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.warn("No vehicle found with the specified license plate: ", e);
        }
        return found;
    }

    @Override
    public TimedVehicle findOneWithScheduledData(Serializable id) {
        checkNotNull(id, "id can't be null");
        TypedQuery<TimedVehicle> query = getEntityManager()
                .createQuery("from TimedVehicle v left join fetch v.timedDeliveries WHERE v.id=:id", TimedVehicle.class)
                .setParameter("id", id);
        TimedVehicle found = null;
        try {
            found = query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.warn("No vehicle found with the specified id", e);
        }
        return found;
    }

}
