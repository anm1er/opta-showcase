package org.optaplanner.showcase.app.vehiclerouting.service.persistence;

import java.util.List;

import org.optaplanner.showcase.app.vehiclerouting.model.Customer;
import org.optaplanner.showcase.app.vehiclerouting.model.Delivery;
import org.optaplanner.showcase.app.vehiclerouting.model.Depot;
import org.optaplanner.showcase.app.vehiclerouting.model.Vehicle;

public interface PersistenceService {

    /**
     * Retrieves all {@code Customer} objects available.
     * @return a {@code List} of {@code Customer} objects or an empty {@code List} if none were found.
     */
    List<? extends Customer> getCustomers();

    /**
     * Retrieves all {@code Vehicle} objects available.
     * @return a {@code List} of {@code Vehicle} objects or an empty {@code List} if none were found.
     */
    List<? extends Vehicle> getVehicles();

    /**
     * Retrieves all {@code Depot} objects available.
     * @return a {@code List} of {@code Depot} objects or an empty {@code List} if none were found.
     */
    List<? extends Depot> getDepots();

    /**
     * Retrieves all {@code Delivery} objects available.
     * @return a {@code List} of {@code Delivery} objects or an empty {@code List} if none were found.
     */
    List<? extends Delivery> getDeliveries();

    /**
     * Deletes all {@code Customer} objects.
     */
    void deleteCustomers();

    /**
     * Deletes all {@code Vehicle} objects.
     */
    void deleteVehicles();

    /**
     * Deletes all {@code Depot} objects.
     */
    void deleteDepots();

    /**
     * Deletes all {@code Delivery} objects.
     */
    void deleteDeliveries();

}
