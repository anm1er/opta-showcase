package org.optaplanner.showcase.app.vehiclerouting.web.timewindowed;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import org.optaplanner.showcase.app.vehiclerouting.event.modification.ProblemModificationListener;
import org.optaplanner.showcase.app.vehiclerouting.event.modification.timewindowed.ProblemModificationEventSupport;
import org.optaplanner.showcase.app.vehiclerouting.exception.ResourceNotFoundException;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.service.persistence.timewindowed.TimeWindowedPersistenceService;
import org.optaplanner.showcase.app.vehiclerouting.web.common.FlashMessageController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Spring Web MVC controller exposing interface for vehicle-related operations.
 */
@Controller
@SessionAttributes("vehicle")
public class TimedVehicleController extends FlashMessageController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TimedVehicleController.class);

    public static final String FEEDBACK_MESSAGE_KEY_VEHICLE_ADDED = "feedback.message.vehicle.added";
    public static final String FEEDBACK_MESSAGE_KEY_VEHICLE_UPDATED = "feedback.message.vehicle.updated";
    public static final String FEEDBACK_MESSAGE_KEY_VEHICLE_DELETED = "feedback.message.vehicle.deleted";

    public static final String MODEL_ATTRIBUTE_VEHICLE = "vehicle";
    public static final String MODEL_ATTRIBUTE_VEHICLE_LIST = "vehicles";

    public static final String VIEW_VEHICLE_LIST = "vehicles/list";
    public static final String VIEW_VEHICLE_DETAILS = "vehicles/details";
    public static final String VIEW_DELIVERIES = "vehicles/deliveries";
    public static final String VIEW_VEHICLE_ADD_OR_EDIT_FORM = "vehicles/addOrEditForm";

    public static final String VEHICLES_REQUEST = "vehicles";

    private final TimeWindowedPersistenceService persistenceService;
    private final ProblemModificationEventSupport problemModificationEventSupport;

    @Autowired
    public TimedVehicleController(TimeWindowedPersistenceService persistenceService, ProblemModificationEventSupport
            problemModificationEventSupport) {
        this.persistenceService = persistenceService;
        this.problemModificationEventSupport = problemModificationEventSupport;
    }

    @InitBinder
    public void setAllowedFields(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id", "timedDeliveries");
        // Converts empty string to null, since most validation rules fire only if the field isn't null
        dataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    /**
     * Retrieves all vehicles from storage and adds a list containing all vehicles to the passed model.
     * @param model model
     * @return vehicle list view name
     */
    @RequestMapping(value = "/vehicles", method = RequestMethod.GET)
    public String listAllVehicles(Model model) {
        LOGGER.debug("Displaying vehicle list.");
        List<TimedVehicle> vehicles = persistenceService.getVehicles();
        model.addAttribute(MODEL_ATTRIBUTE_VEHICLE_LIST, vehicles);
        LOGGER.debug("Added to model attribute: {}", MODEL_ATTRIBUTE_VEHICLE_LIST);
        return VIEW_VEHICLE_LIST;
    }

    /**
     * Retrieves the vehicle with id {@code id} together with the associated schedule data, which will survive closing
     * the tx-scoped persistence context.
     * @param id vehicle ID
     * @param model model
     * @return deliveries details view name
     * @throws ResourceNotFoundException if the vehicle cannot be retrieved from storage
     */
    @RequestMapping(value = "/vehicles/{vehicleId}/deliveries", method = RequestMethod.GET)
    public String viewDeliveriesDetails(@PathVariable("vehicleId") Long id, Model model) {
        LOGGER.debug("Displaying deliveries for vehicle with id: {}", id);
        TimedVehicle requestedVehicle = persistenceService.getVehicleWithScheduleData(id);
        model.addAttribute(MODEL_ATTRIBUTE_VEHICLE, requestedVehicle);
        LOGGER.debug("Added to model attribute: {}", MODEL_ATTRIBUTE_VEHICLE);
        return VIEW_DELIVERIES;
    }

    /**
     * Retrieves the vehicle with id {@code id} and adds the vehicle to the passed model.
     * @param id vehicle ID
     * @param model model
     * @return vehicle details view name
     * @throws ResourceNotFoundException if the vehicle cannot be retrieved from storage
     */
    @RequestMapping(value = "/vehicles/{vehicleId}", method = RequestMethod.GET)
    public String viewVehicleDetails(@PathVariable("vehicleId") Long id, Model model) {
        LOGGER.debug("Displaying vehicle details for vehicle with id: {}", id);
        TimedVehicle requestedVehicle = persistenceService.getVehicle(id);
        model.addAttribute(MODEL_ATTRIBUTE_VEHICLE, requestedVehicle);
        LOGGER.debug("Added to model attribute: {}", MODEL_ATTRIBUTE_VEHICLE);
        return VIEW_VEHICLE_DETAILS;
    }

    /**
     * Retrieves the vehicle with id {@code id} if it has not been already placed within session context
     * and adds the vehicle to the passed model.
     * @param id vehicle ID
     * @param vehicle vehicle added to model
     * @param model model
     * @return vehicle edit form view
     * @throws ResourceNotFoundException if the vehicle cannot be retrieved from storage
     */
    @RequestMapping(value = "/vehicles/{vehicleId}/edit", method = RequestMethod.GET)
    public String displayVehicleEditForm(@PathVariable("vehicleId") Long id,
            @Valid @ModelAttribute(MODEL_ATTRIBUTE_VEHICLE) TimedVehicle vehicle, Model model) {
        LOGGER.debug("Displaying edit vehicle form for vehicle with id: {}", id);
        LOGGER.debug("Vehicle found in session scope has id {} ", vehicle.getId());
        TimedVehicle requestedVehicle;
        if (id.equals(vehicle.getId())) {
            LOGGER.debug("No hitting database required - the vehicle already stored in the session scope");
            requestedVehicle = vehicle;
        } else {
            LOGGER.debug("Hitting database for vehicle retrieval with id {} ", id);
            requestedVehicle = persistenceService.getVehicle(id);
        }
        model.addAttribute(MODEL_ATTRIBUTE_VEHICLE, requestedVehicle);
        LOGGER.debug("Added to model attribute: {}", MODEL_ATTRIBUTE_VEHICLE);
        return VIEW_VEHICLE_ADD_OR_EDIT_FORM;
    }

    /**
     * Saves vehicle form object if it has no validation errors, adds flash message, fires modification event
     * for {@link ProblemModificationListener}s and redirects to the view with vehicle details.
     * @param vehicle vehicle form object
     * @param result a result holder for validation error registration
     * @param attributes attributes for a redirect scenario
     * @param status session status
     * @return redirected URL
     */
    @RequestMapping(value = "/vehicles/{vehicleId}/edit", method = RequestMethod.POST)
    public String processVehicleEditForm(@Valid @ModelAttribute(MODEL_ATTRIBUTE_VEHICLE) TimedVehicle vehicle,
            BindingResult result, RedirectAttributes attributes, SessionStatus status) {
        LOGGER.debug("Updating vehicle");
        if (result.hasErrors()) {
            LOGGER.debug("Edit vehicle form was submitted with validation errors.");
            return VIEW_VEHICLE_ADD_OR_EDIT_FORM;
        }
        TimedVehicle savedVehicle = persistenceService.saveVehicle(vehicle);
        LOGGER.debug("Updated the information of vehicle: {}", savedVehicle);
        status.setComplete();
        addFlashMessage(attributes, FEEDBACK_MESSAGE_KEY_VEHICLE_UPDATED);
        problemModificationEventSupport.fireModificationEnded(null);
        return getRedirectedURL(VEHICLES_REQUEST, "{vehicleId}");
    }

    @RequestMapping(value = "/vehicles/{vehicleId}/cancel", method = RequestMethod.GET)
    public String viewVehicleDetailsAfterCancel(@PathVariable("vehicleId") Long id) {
        LOGGER.debug("Displaying vehicle details after cancelling update for vehicle {}", id);
        return VIEW_VEHICLE_DETAILS;
    }

    /**
     * Adds new vehicle object to model.
     * @param model model
     * @return vehicle add form view
     */
    @RequestMapping(value = "/vehicles/add", method = RequestMethod.GET)
    public String displayVehicleAddForm(Model model) {
        LOGGER.debug("Displaying vehicle add form.");
        TimedVehicle formVehicleObject = new TimedVehicle();
        formVehicleObject.setDepot(persistenceService.getDepot());
        model.addAttribute(MODEL_ATTRIBUTE_VEHICLE, formVehicleObject);
        LOGGER.debug("Added to model attribute: {}", MODEL_ATTRIBUTE_VEHICLE);
        return VIEW_VEHICLE_ADD_OR_EDIT_FORM;
    }

    /**
     * Saves vehicle form object if it has no validation errors, adds flash message, fires modification event
     * for {@link ProblemModificationListener}s and redirects to redirects to the view with vehicle details.
     * @param vehicle vehicle form object
     * @param result a result holder for validation error registration
     * @param attributes attributes for a redirect scenario
     * @param status session status
     * @return redirected URL
     */
    @RequestMapping(value = "/vehicles/add", method = RequestMethod.POST)
    public String processVehicleAddForm(@Valid @ModelAttribute(MODEL_ATTRIBUTE_VEHICLE) TimedVehicle vehicle,
            BindingResult result, RedirectAttributes attributes, SessionStatus status) {
        LOGGER.debug("Adding new vehicle: {}", vehicle);
        if (result.hasErrors()) {
            LOGGER.warn("Add vehicle form was submitted with validation errors.");
            return VIEW_VEHICLE_ADD_OR_EDIT_FORM;
        }
        TimedVehicle savedVehicle = persistenceService.saveVehicle(vehicle);
        LOGGER.debug("Added vehicle: {}", savedVehicle);
        status.setComplete();
        addFlashMessage(attributes, FEEDBACK_MESSAGE_KEY_VEHICLE_ADDED);
        problemModificationEventSupport.fireModificationEnded(null);
        return getRedirectedURL(VEHICLES_REQUEST, String.valueOf(vehicle.getId()));
    }

    /**
     * Deletes vehicle by {@code id}.
     * @param id vehicle ID
     * @param vehicle requested vehicle
     * @param attributes attributes for a redirect scenario
     * @return redirected URL
     * @throws ResourceNotFoundException if the vehicle cannot be retrieved from storage
     */
    @RequestMapping(value = "/vehicles/{vehicleId}/delete", method = RequestMethod.GET)
    public String deleteVehicle(@PathVariable("vehicleId") Long id,
            @ModelAttribute(MODEL_ATTRIBUTE_VEHICLE) TimedVehicle vehicle, RedirectAttributes attributes) {
        LOGGER.debug("Deleting vehicle with id: {}", id);
        persistenceService.deleteVehicle(id);
        addFlashMessage(attributes, FEEDBACK_MESSAGE_KEY_VEHICLE_DELETED);
        problemModificationEventSupport.fireModificationEnded(null);
        return getRedirectedURL(VEHICLES_REQUEST);
    }

}
