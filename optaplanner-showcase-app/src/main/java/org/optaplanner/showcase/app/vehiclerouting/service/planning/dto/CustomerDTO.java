package org.optaplanner.showcase.app.vehiclerouting.service.planning.dto;

public class CustomerDTO {

    // location-related data
    private double latitude;
    private double longitude;
    private String name;

    //customer-specific data
    private int demand;
    private int offUnits;

    public CustomerDTO() {
    }

    public CustomerDTO(double latitude, double longitude, String locationName, int demand) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = locationName;
        this.demand = demand;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDemand() {
        return demand;
    }

    public void setDemand(int demand) {
        this.demand = demand;
    }

    public int getOffUnits() {
        return offUnits;
    }

    public void setOffUnits(int offUnits) {
        this.offUnits = offUnits;
    }

}
