package org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.impl;

import java.util.ArrayList;
import java.util.List;

import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimeWindowedDepot;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedDelivery;
import org.optaplanner.showcase.app.vehiclerouting.model.timewindowed.TimedVehicle;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.CrossDomainMappingService;
import org.optaplanner.showcase.app.vehiclerouting.service.planning.mapper.timewindowed.TimedBeanMapper;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.DistanceType;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem.TimeWindowedVehicleRoutingProblem;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem.TimeWindowedVehicleRoutingProblemFactory;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implementation of {@link CrossDomainMappingService} interface.
 */
public class CrossDomainMappingServiceImpl implements CrossDomainMappingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CrossDomainMappingServiceImpl.class);

    private static final String NAME_DOMAIN_CREATED = "Domain-";

    /**
     * Time origin for date time translations.
     */
    private final DateTime timeOrigin;

    private final TimedBeanMapper beanMapper;
    private final ProblemMapperAssist problemMapperAssist;
    private final TimeWindowedVehicleRoutingProblemFactory problemFactory;

    @Override
    public DateTime getTimeOrigin() {
        return timeOrigin;
    }

    public CrossDomainMappingServiceImpl(TimedBeanMapper beanMapper, TimeWindowedVehicleRoutingProblemFactory problemFactory,
            DateTime timeOrigin) {
        this.beanMapper = beanMapper;
        this.problemFactory = problemFactory;
        this.timeOrigin = timeOrigin;
        this.problemMapperAssist = new ProblemMapperAssist(beanMapper, timeOrigin);
    }

    @Override
    public TimeWindowedCustomer map(PlanningTimeWindowedCustomer planningCustomer) {
        return beanMapper.map(planningCustomer, timeOrigin);
    }

    @Override
    public TimeWindowedDepot map(PlanningTimeWindowedDepot planningDepot) {
        return beanMapper.map(planningDepot, timeOrigin);
    }

    @Override
    public TimedVehicle map(PlanningVehicle planningVehicle) {
        return beanMapper.map(planningVehicle, timeOrigin);
    }

    @Override
    public TimedDelivery map(PlanningVehicle vehicle, PlanningTimeWindowedCustomer customer) {
        return beanMapper.map(vehicle, customer, timeOrigin);
    }

    @Override
    public PlanningTimeWindowedCustomer map(TimeWindowedCustomer customer) {
        return beanMapper.map(customer, timeOrigin);
    }

    @Override
    public PlanningTimeWindowedDepot map(TimeWindowedDepot depot) {
        return beanMapper.map(depot, timeOrigin);
    }

    @Override
    public PlanningVehicle map(TimedVehicle vehicle) {
        return beanMapper.map(vehicle, timeOrigin);
    }

    // The domain model is not that rich, so this method accepts different entities' lists as arguments
    @Override
    public TimeWindowedVehicleRoutingProblem mapDomainEntitiesToPlanningProblem(List<? extends TimeWindowedCustomer> customers,
            List<? extends TimeWindowedDepot> depots, List<? extends TimedVehicle> vehicles) {
        LOGGER.debug("Setting planning problem from domain.");
        // Locations and customers could be set in one pass if we wouldn't depend on mapper for this
        List<PlanningTimeWindowedCustomer> destinationCustomers = problemMapperAssist.mapCustomers(customers);
        // Looking up depots separately from vehicles seems cleaner in case there are depots not related to any vehicles
        List<PlanningTimeWindowedDepot> destinationDepots = problemMapperAssist.mapDepots(depots);
        List<PlanningVehicle> destinationVehicles = problemMapperAssist.mapVehicles(vehicles);
        List<PlanningLocation> destinationLocations = new ArrayList<>(destinationCustomers.size());
        // By contract, mapper is to set non-null locations whenever non-null source locations are provided
        for (PlanningCustomer customer : destinationCustomers) {
            destinationLocations.add(customer.getLocation());
        }
        for (PlanningTimeWindowedDepot depot : destinationDepots) {
            destinationLocations.add(depot.getLocation());
        }
        // Sets generic properties
        String name = NAME_DOMAIN_CREATED + "-" + destinationCustomers.size();
        DistanceType distanceType = (!destinationLocations.isEmpty())
                ? destinationLocations.get(0).getDistanceType() : DistanceType.UNDETERMINED;
        TimeWindowedVehicleRoutingProblem problem = problemFactory.getProblemBuilder()
                .withCustomers(destinationCustomers)
                .withVehicles(destinationVehicles)
                .withDepots(destinationDepots)
                .withLocations(destinationLocations)
                .withDistanceType(distanceType)
                .withName(name)
                .build();
        LOGGER.debug("Set new problem instance from domain: {}", problem);
        return problem;
    }

    @Override
    public List<TimedDelivery> mapSolvedPlanningProblemToDeliveryList(final TimeWindowedVehicleRoutingProblem problem) {
        checkNotNull(problem, "Problem instance can't be null");
        LOGGER.debug("Setting linear deliveries from planning problem {}", problem);
        return problemMapperAssist.mapPlanningVehiclesToDeliveries(problem.getVehicleList());
    }

    @Override
    public TimeWindowedVehicleRoutingProblem mapDeliveryListToSolvedPlanningProblem(List<? extends TimedDelivery> deliveries) {
        // todo
        return problemFactory.getProblemBuilder().build();
    }

}
