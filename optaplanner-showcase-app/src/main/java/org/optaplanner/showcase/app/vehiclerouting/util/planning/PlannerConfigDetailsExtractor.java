package org.optaplanner.showcase.app.vehiclerouting.util.planning;

import java.util.ArrayList;
import java.util.List;

import org.optaplanner.core.config.score.director.ScoreDirectorFactoryConfig;

public final class PlannerConfigDetailsExtractor {

    private PlannerConfigDetailsExtractor() {
    }

    public static List<String> getScoreConfigurationDetails(ScoreDirectorFactoryConfig scoreConfig) {
        List<String> scoreConfigResources = new ArrayList<String>();
        if (scoreConfig.getEasyScoreCalculatorClass() != null) {
            scoreConfigResources.add(scoreConfig.getEasyScoreCalculatorClass().getName());
        } else if (scoreConfig.getIncrementalScoreCalculatorClass() != null) {
            scoreConfigResources.add(scoreConfig.getIncrementalScoreCalculatorClass().getName());
        } else if (!scoreConfig.getScoreDrlList().isEmpty()) {
            scoreConfigResources.addAll(scoreConfig.getScoreDrlList());
        }
        return scoreConfigResources;
    }

}
