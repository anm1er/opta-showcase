package org.optaplanner.showcase.app.vehiclerouting.model;

import java.util.Objects;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.springframework.core.style.ToStringCreator;

@SuppressWarnings("serial")
@MappedSuperclass
public class Depot extends BaseEntity {

    /**
     * The location of a depot (central stock inventory) is the starting point for any scheduled route of a vehicle.
     */
    @NotNull
    private Location location = new Location();

    public static Builder getBuilder() {
        return new DefaultBuilder();
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Depot depot = (Depot) o;
        return Objects.equals(location, depot.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(location);
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("location", location)
                .toString();
    }

    public abstract static class Builder<T extends Depot,
            B extends Builder<T, B>> {

        protected T built;

        private B builder;

        protected Builder() {
            built = createDepot();
            builder = getBuilder();
        }

        public B withLocation(Location location) {
            built.setLocation(location);
            return builder;
        }

        public T build() {
            return built;
        }

        protected abstract T createDepot();

        protected abstract B getBuilder();
    }

    private static class DefaultBuilder extends Depot.Builder<Depot, DefaultBuilder> {

        @Override
        protected Depot createDepot() {
            return new Depot();
        }

        @Override
        protected DefaultBuilder getBuilder() {
            return this;
        }
    }

}
