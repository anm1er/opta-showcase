package org.optaplanner.showcase.app.vehiclerouting.util.converters;

import java.util.Properties;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

import org.optaplanner.showcase.app.vehiclerouting.model.Location;

public class StringToLocationTypeConverter implements Converter<String, Location> {

    public static final String FIRST_PROPERTY = "latitude";
    public static final String SECOND_PROPERTY = "longitude";

    @Override
    public Location convert(String s) {
        String locationString = StringUtils.deleteAny(s, "[]");
        String[] propertyStrings = StringUtils.split(locationString.substring(locationString.indexOf(FIRST_PROPERTY)), ",");
        Properties properties = StringUtils.splitArrayElementsIntoProperties(propertyStrings, "=");
        Location location;
        try {
            location = Location.getBuilder()
                    .withLatitude(Double.parseDouble(properties.getProperty(FIRST_PROPERTY)))
                    .withLongitude(Double.parseDouble(properties.getProperty(SECOND_PROPERTY)))
                    .build();
        } catch (NumberFormatException e) {
            throw new ConversionFailedException(TypeDescriptor.valueOf(String.class),
                    TypeDescriptor.valueOf(Location.class), s, null);
        }
        return location;
    }

}
