-- Solomon-25-C101 - based data set.

--  customers

insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (10, 45.00, 68.00, '2015-08-01 15:12:00', '2015-08-01 16:07:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (30, 45.00, 70.00, '2015-08-01 13:45:00', '2015-08-01 14:30:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (10, 42.00, 66.00, '2015-08-01 01:05:00', '2015-08-01 02:26:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (10, 42.00, 68.00, '2015-08-01 12:07:00', '2015-08-01 13:02:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (10, 42.00, 65.00, '2015-08-01 00:15:00', '2015-08-01 01:07:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (20, 40.00, 69.00, '2015-08-01 10:21:00', '2015-08-01 11:42:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (20, 40.00, 66.00, '2015-08-01 02:50:00', '2015-08-01 03:45:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (20, 38.00, 68.00, '2015-08-01 04:15:00', '2015-08-01 05:24:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (10, 38.00, 70.00, '2015-08-01 08:54:00', '2015-08-01 10:05:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (10, 35.00, 66.00, '2015-08-01 05:57:00', '2015-08-01 06:50:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (10, 35.00, 69.00, '2015-08-01 07:28:00', '2015-08-01 08:25:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (20, 25.00, 85.00, '2015-08-01 10:52:00', '2015-08-01 12:01:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (30, 22.00, 75.00, '2015-08-01 00:30:00', '2015-08-01 01:32:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (10, 22.00, 85.00, '2015-08-01 09:27:00', '2015-08-01 10:20:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (40, 20.00, 80.00, '2015-08-01 06:24:00', '2015-08-01 07:09:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (40, 20.00, 85.00, '2015-08-01 07:55:00', '2015-08-01 08:48:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (20, 18.00, 75.00, '2015-08-01 01:39:00', '2015-08-01 02:28:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (20, 15.00, 75.00, '2015-08-01 02:59:00', '2015-08-01 04:14:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (10, 15.00, 80.00, '2015-08-01 04:38:00', '2015-08-01 05:45:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (10, 30.00, 50.00, '2015-08-01 00:10:00', '2015-08-01 01:13:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (20, 30.00, 52.00, '2015-08-01 15:14:00', '2015-08-01 16:05:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (20, 28.00, 52.00, '2015-08-01 13:32:00', '2015-08-01 14:43:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (20, 28.00, 55.00, '2015-08-01 12:12:00', '2015-08-01 12:57:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (10, 25.00, 50.00, '2015-08-01 01:05:00', '2015-08-01 02:24:00', 90);
insert into customers (demand, latitude, longitude, ready_time, due_time, service_duration) values (40, 25.00, 52.00, '2015-08-01 02:49:00', '2015-08-01 03:44:00', 90);


-- depot

insert into depots (latitude, longitude, ready_time, due_time) values (40.00, 50.00, '2015-08-01 00:00:00', '2015-08-01 20:36:00');


-- vehicles

insert into vehicles (capacity, license_plate, depot_id) values (200, 'AAS-301', 1);
insert into vehicles (capacity, license_plate, depot_id) values (200, 'FRY-075', 1);
insert into vehicles (capacity, license_plate, depot_id) values (200, 'PEU-641', 1);
insert into vehicles (capacity, license_plate, depot_id) values (200, 'FSW-339', 1);
insert into vehicles (capacity, license_plate, depot_id) values (200, 'NBZ-099', 1);
