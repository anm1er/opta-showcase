$(function () {
    // Makes all controls in the form-group get the validation style for error messages
    addValidationErrorClassesToForm();

    function addValidationErrorClassesToForm() {
        $("form").find(".form-group").each(function () {
            var errorMessage = $(this).find(".help-block").text();

            if (errorMessage) {
                $(this).addClass("has-error");
            }
        });
    }
});

$(function () {
    $("#ready-time").datetimepicker();
});

$(function () {
    $("#due-time").datetimepicker(/*{lang:'ru'}*/);
});
