function initAutocomplete() {
    var currentLat = document.getElementById('location-latitude').value;
    var currentLng = document.getElementById('location-longitude').value;
    var latLng = new google.maps.LatLng(currentLat,currentLng);
    var centerOfMap = new google.maps.LatLng(currentLat, currentLng);
    var map = new google.maps.Map(document.getElementById('map'), {
        center: centerOfMap,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var originalMarker = new google.maps.Marker({
        position: latLng,
        map: map,
        draggable: true
    });
    google.maps.event.addListener(originalMarker, 'dragend', function (event) {
        setMarkerLocation(event.latLng);
    });

    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();
        if (places.length == 0) {
            return;
        }
        markers.forEach(function(marker) {
            marker.setMap(null);
        });
        markers = [];
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            var marker = new google.maps.Marker({
                map: map,
                title: place.name,
                position: place.geometry.location,
                draggable: true
            });
            markers.push(marker);
            setMarkerLocation(marker.getPosition());
            google.maps.event.addListener(marker, 'dragend', function (event) {
                setMarkerLocation(event.latLng);
            });
            if (place.geometry.viewport) {
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });
}

function setMarkerLocation(location) {
    document.getElementById('location-latitude').value = location.lat().toFixed(2);
    document.getElementById('location-longitude').value = location.lng().toFixed(2);
}
