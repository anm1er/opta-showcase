<div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="container-fluid">
        <a class="optaplanner-logo" href="http://www.optaplanner.org/">
          <img alt="OptaPlanner Logo" src="<spring:url value="/resources/images/logo.png" htmlEscape="true" />"
               style="padding: 5px 3px 0px 3px;">
        </a>
        <a class="navbar-brand" href="#">OptaPlanner Showcase</a>
      </div>

    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="navbar-link"><a href="<spring:url value="/" htmlEscape="true" />">Home</a></li>
        <li><a href="#">About</a></li>
        <li class="navbar-link"><a href="<spring:url value="/demo" htmlEscape="true" />">Demo</a></li>
        <li class="navbar-link"><a href="<spring:url value="/admin" htmlEscape="true" />">Administration</a></li>
        <li class="navbar-link"><a href="<spring:url value="/samples" htmlEscape="true" />">Test problems</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <p class="navbar-text pull-right">
          Logged in as <a href="#" class="navbar-link">Username</a>
        </p>
      </ul>
    </div>
    <!-- /.nav-collapse -->
  </div>
  <!-- /.container -->
</div>
