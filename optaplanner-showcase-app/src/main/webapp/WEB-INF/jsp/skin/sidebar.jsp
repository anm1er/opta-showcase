<%-- sidebar --%>
<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
  <div class="list-group">
    <a href="<spring:url value="/customers.html" htmlEscape="true"/>" class="list-group-item">
      Customers
    </a>
    <a href="<spring:url value="/vehicles.html" htmlEscape="true"/>" class="list-group-item">
      Vehicles
    </a>
    <a href="#" class="list-group-item">Depots</a>
  </div>
</div>
