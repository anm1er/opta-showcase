<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="dec" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><dec:title default="VRPTW Optaplanner Showcase"/></title>

  <spring:url value="/webjars/bootstrap/3.2.0/css/bootstrap.min.css" var="bootstrapCss"/>
  <link href="${bootstrapCss}" rel="stylesheet"/>
  <spring:url value="/webjars/datetimepicker/2.3.4/jquery.datetimepicker.css" var="jQueryDatetimePickerCss"/>
  <link href="${jQueryDatetimePickerCss}" rel="stylesheet"/>
  <spring:url value="/resources/css/main.css" var="mainCss"/>
  <link href="${mainCss}" rel="stylesheet"/>

  <spring:url value="/webjars/jquery/2.1.1/jquery.js" var="jQuery"/>
  <script src="${jQuery}"></script>
  <spring:url value="/webjars/datetimepicker/2.3.4/jquery.datetimepicker.js" var="jQueryDatetimePicker"/>
  <script src="${jQueryDatetimePicker}"></script>
  <spring:url value="/webjars/bootstrap/3.2.0/js/bootstrap.min.js" var="bootstrapJs"/>
  <script src="${bootstrapJs}"></script>
  <!-- Off-canvas navigation toggling support -->
  <spring:url value="/resources/js/offcanvas.js" var="offcanvasJs"/>
  <script src="${offcanvasJs}"></script>

  <dec:head/>
</head>

<body>

<%@include file="navbar.jsp" %>

<div class="container outer-container">

  <!--Body content-->
  <dec:body/>

  <footer>
    <p>&copy; Acme 2015</p>
    <%-- Some footer --%>
  </footer>

</div>

</body>
</html>
