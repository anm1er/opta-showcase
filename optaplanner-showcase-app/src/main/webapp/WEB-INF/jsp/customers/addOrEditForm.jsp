<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ include file="/WEB-INF/jsp/admin/beanProperties.jspf" %>

<spring:url value="/resources/js/form.js" var="formJsUrl"/>
<spring:url value="/resources/js/changelocation.js" var="changeLocationJsUrl"/>
<spring:url value="/resources/js/addlocation.js" var="addLocationJsUrl"/>

<html>
<head>
  <style type="text/css">
    #map {
      height: 400px;
      width: 90%;
      margin: 2% 5%;
    }

    .controls {
      margin-top: 10px;
      border: 1px solid transparent;
      border-radius: 2px 0 0 2px;
      box-sizing: border-box;
      -moz-box-sizing: border-box;
      height: 32px;
      outline: none;
      box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
      background-color: #fff;
      font-size: 15px;
      font-weight: 300;
      margin-left: 12px;
      padding: 0 11px 0 13px;
      text-overflow: ellipsis;
      width: 300px;
    }
  </style>
  <title>Customer</title>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?language=en"></script>
  <script type="text/javascript" src="${formJsUrl}"></script>
</head>
<body>

<div class="container">
  <div class="row row-offcanvas row-offcanvas-right">

    <%@include file="../admin/sidebar.jsp" %>

    <div class="col-xs-12 col-sm-9">
      <p class="pull-right visible-xs">
        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
      </p>
      <%-- If entity has persistent representation, it should be edited; otherwise it should be added --%>
      <c:choose>
        <c:when test="${customer['transient']}"><c:set var="method" value="post"/></c:when>
        <c:otherwise><c:set var="method" value="put"/></c:otherwise>
      </c:choose>
      <h2>
        <c:choose>
          <c:when test="${customer['transient']}"><spring:message code="customer.add"/></c:when>
          <c:otherwise><spring:message code="customer.edit"/></c:otherwise>
        </c:choose>
      </h2>

      <div class="well">
        <div class="alert alert-info">
          Use the search box or drag marker to pick location.
        </div>
        <input id="pac-input" class="controls" type="text" placeholder="Search Box">

        <div id="map"></div>
        <%--For now only post is used to get expected validation behaviour, but there should be a workaround for this.--%>
        <%--<form:form modelAttribute="customer" method="${method}" class="form-horizontal" id="customer-form">--%>
        <form:form modelAttribute="customer" method="post" class="form-horizontal" id="customer-form">

          <div id="control-group-latitude" class="form-group">
            <label class="col-xs-2 control-label" for="location-latitude">${latitudeMessage}:</label>
            <div class="col-xs-8">
              <form:input id="location-latitude" class="form-control"
                          placeholder="Latitude" path="location.latitude"/>
              <form:errors id="error-latitude" path="location.latitude" cssClass="help-block"/>
            </div>
          </div>
          <div id="control-group-longitude" class="form-group">
            <label class="col-xs-2 control-label" for="location-longitude">${longitudeMessage}:</label>
            <div class="col-xs-8">
              <form:input id="location-longitude" class="form-control"
                          placeholder="Longitude" path="location.longitude"/>
              <form:errors id="error-longitude" path="location.longitude" cssClass="help-block"/>
            </div>
          </div>
          <div id="control-group-demand" class="form-group">
            <label class="col-xs-2 control-label" for="demand">${demandMessage}:</label>
            <div class="col-xs-8">
              <%-- Sets HTML5 type to number to have fully styled integer-valued inputs--%>
              <form:input type="number" id="demand" class="form-control"
                          placeholder="Demand" path="demand"/>
              <form:errors id="error-demand" path="demand" cssClass="help-block"/>
            </div>
          </div>
          <div id="control-group-service-duration" class="form-group">
            <label class="col-xs-2 control-label" for="service-duration">${serviceDurationMessage}:</label>
            <div class="col-xs-8">
              <form:input type="number" id="service-duration" class="form-control"
                          placeholder="Service duration" path="serviceDuration"/>
              <form:errors id="error-service-duration" path="serviceDuration" cssClass="help-block"/>
            </div>
          </div>
          <div id="control-ready-time" class="form-group">
            <label class="col-xs-2 control-label" for="ready-time">${readyTimeMessage}:</label>
            <div class="col-xs-8">
              <form:input id="ready-time" class="form-control"
                          placeholder="Ready Time" path="timeWindow.readyTime"/>
              <form:errors id="error-ready-time" path="timeWindow.readyTime" cssClass="help-block"/>
            </div>
          </div>
          <div id="control-due-time" class="form-group">
            <label class="col-xs-2 control-label" for="due-time">${dueTimeMessage}:</label>
            <div class="col-xs-8">
              <form:input id="due-time" class="form-control"
                          placeholder="Due Time" path="timeWindow.dueTime"/>
              <form:errors id="error-due-time" path="timeWindow.dueTime" cssClass="help-block"/>
            </div>
          </div>

          <div class="action-buttons">
            <button id="save-button" type="submit" class="btn btn-primary"><spring:message code="label.save"/></button>
              <%-- Cancelled editing should be handled differently --%>
            <c:choose>
              <c:when test="${method == 'post'}"><c:set var="cancelUrl" value="${customer.id}"/></c:when>
              <c:otherwise><c:set var="cancelUrl" value="${customer.id}/cancel"/></c:otherwise>
            </c:choose>
            <a href='<spring:url value="/customers/${cancelUrl}" htmlEscape="true"/>' class="btn btn-default">
              <spring:message code="label.cancel"/>
            </a>
          </div>
        </form:form>

        <c:choose>
          <c:when test="${customer['transient']}"><c:set var="pickLocationJsUrl" value="${addLocationJsUrl}"/></c:when>
          <c:otherwise><c:set var="pickLocationJsUrl" value="${changeLocationJsUrl}"/></c:otherwise>
        </c:choose>

      </div>
      <!--/well-->

    </div>
    <!--/span-->
  </div>
  <!--/row-->
</div>
<!--/container-->

<script type="text/javascript" src="${pickLocationJsUrl}"></script>
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initAutocomplete&language=en"
        async defer></script>

</body>

</html>
