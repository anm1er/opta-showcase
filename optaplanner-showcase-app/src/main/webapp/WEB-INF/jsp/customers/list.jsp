<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="datatables" uri="http://github.com/dandelion/datatables" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ include file="/WEB-INF/jsp/admin/beanProperties.jspf" %>

<html>
<head>
  <title>Customers</title>
</head>

<body>

<div class="container">
  <div class="row row-offcanvas row-offcanvas-right">

    <%@include file="../admin/sidebar.jsp" %>

    <div class="col-xs-12 col-sm-9">
      <p class="pull-right visible-xs">
        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
      </p>
      <c:if test="${not empty feedbackMessage}">
        <div class="alert alert-danger">${feedbackMessage}</div>
      </c:if>
      <h1><spring:message code="customer.header"/></h1>
      <p>
        <a href='<spring:url value="/customers/add" htmlEscape="true"/>' class="btn btn-success">
          <spring:message code="label.add.button"/>
        </a>
      </p>

      <datatables:table id="customers" data="${customers}" cdn="true" row="customer" theme="bootstrap2"
                        cssClass="table table-striped">
      <%-- Single link for all columns to refernce --%>
      <spring:url value="/customers/{customerId}.html" var="customerUrl">
        <spring:param name="customerId" value="${customer.id}"/>
      </spring:url>

        <%-- Columns --%>
        <datatables:column title="${locationMessage}" cssStyle="width: 150px;" display="html">
          <a href="${customerUrl}"><c:out value="${customer.location.latitude} ${customer.location.longitude}"/></a>
        </datatables:column>
        <datatables:column title="${demandMessage}">
          <a href="${customerUrl}"><c:out value="${customer.demand}"/></a>
        </datatables:column>
        <datatables:column title="${serviceDurationMessage}">
          <a href="${customerUrl}"><c:out value="${customer.serviceDuration}"/></a>
        </datatables:column>
        <datatables:column title="${readyTimeMessage}">
          <a href="${customerUrl}">
                <joda:format value="${customer.timeWindow.readyTime}" pattern="yyyy/MM/dd H:mm"/>
          </a>
        </datatables:column>
        <datatables:column title="${dueTimeMessage}">
          <a href="${customerUrl}">
                <joda:format value="${customer.timeWindow.dueTime}" pattern="yyyy/MM/dd H:mm"/>
          </a>
        </datatables:column>

      </datatables:table>
    </div>
    <!--/span-->
  </div>
  <!--/row-->
</div>
<!--/container-->

</body>

</html>
