<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
  <title>Administration</title>
</head>
<body style="padding-top: 70px">

<div class="container">
  <div class="row row-offcanvas row-offcanvas-right">

    <%@include file="sidebar.jsp" %>

    <div class="col-xs-12 col-sm-9">
      <p class="pull-right visible-xs">
        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
      </p>

      <div class="jumbotron">
        <h1>Main administration page..</h1>
      </div>
      <div class="alert alert-warning">
        <strong>Note:</strong> Any modifications made to problem instance will result in invalid solution.
      </div>
      <div class="row">
        <%-- Something more to add --%>
      </div>
      <!--/row-->

    </div>
    <!--/span-->
  </div>
  <!--/row-->
</div>
<!--/container-->

</body>

</html>
