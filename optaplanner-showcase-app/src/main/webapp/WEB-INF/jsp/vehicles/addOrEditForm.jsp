<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ include file="/WEB-INF/jsp/admin/beanProperties.jspf" %>

<spring:url value="/resources/js/form.js" var="formJsUrl"/>

<html>
<head>
  <title>Vehicle</title>
  <script type="text/javascript" src="${formJsUrl}"></script>
</head>
<body>

<div class="container">
  <div class="row row-offcanvas row-offcanvas-right">

    <%@include file="../admin/sidebar.jsp" %>

    <div class="col-xs-12 col-sm-9">
      <p class="pull-right visible-xs">
        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
      </p>
      <%-- If entity has persistent representation, it should be edited; otherwise it should be added --%>
      <c:choose>
        <c:when test="${vehicle['transient']}"><c:set var="method" value="post"/></c:when>
        <c:otherwise><c:set var="method" value="put"/></c:otherwise>
      </c:choose>
      <h2>
        <c:choose>
          <c:when test="${vehicle['transient']}"><spring:message code="vehicle.add"/></c:when>
          <c:otherwise><spring:message code="vehicle.edit"/></c:otherwise>
        </c:choose>
      </h2>

      <div class="well">
        <%--For now only post is used to get expected validation behaviour, but there should be a workaround for this.--%>
        <%--<form:form modelAttribute="vehicle" method="${method}" class="form-horizontal" id="vehicle-form">--%>
        <form:form modelAttribute="vehicle" method="post" class="form-horizontal" id="vehicle-form">

          <div id="control-group-lic" class="form-group">
            <label class="col-xs-2 control-label" for="lic">${licMessage}:</label>
            <div class="col-xs-8">
              <form:input id="lic" class="form-control"
                          placeholder="License plate" path="licensePlate"/>
              <form:errors id="error-lic" path="licensePlate" cssClass="help-block"/>
            </div>
          </div>
          <div id="control-group-capacity" class="form-group">
            <label class="col-xs-2 control-label" for="capacity">${capacityMessage}:</label>
            <div class="col-xs-8">
              <form:input type="number" id="capacity" class="form-control"
                          placeholder="Capacity" path="capacity"/>
              <form:errors id="error-capacity" path="capacity" cssClass="help-block"/>
            </div>
          </div>

          <div class="action-buttons">
            <button id="save-button" type="submit" class="btn btn-primary"><spring:message code="label.save"/></button>
            <c:choose>
              <c:when test="${method == 'post'}"><c:set var="cancelUrl" value="${vehicle.id}"/></c:when>
              <c:otherwise><c:set var="cancelUrl" value="${vehicle.id}/cancel"/></c:otherwise>
            </c:choose>
            <a href='<spring:url value="/vehicles/${cancelUrl}" htmlEscape="true"/>' class="btn btn-default">
              <spring:message code="label.cancel"/>
            </a>
          </div>
        </form:form>

      </div>
      <!--/well-->

    </div>
    <!--/span-->
  </div>
  <!--/row-->
</div>
<!--/container-->

</body>

</html>
