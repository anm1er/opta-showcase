<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="datatables" uri="http://github.com/dandelion/datatables" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ include file="/WEB-INF/jsp/admin/beanProperties.jspf" %>

<html>
<head>
  <title>Vehicles</title>
</head>

<body>

<div class="container">
  <div class="row row-offcanvas row-offcanvas-right">

    <%@include file="../admin/sidebar.jsp" %>

    <div class="col-xs-12 col-sm-9">
      <p class="pull-right visible-xs">
        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
      </p>
      <c:if test="${not empty feedbackMessage}">
        <div class="alert alert-danger">${feedbackMessage}</div>
      </c:if>
      <h1><spring:message code="vehicle.header"/></h1>
      <p>
        <a href='<spring:url value="/vehicles/add" htmlEscape="true"/>' class="btn btn-success">
          <spring:message code="label.add.button"/>
        </a>
      </p>

      <datatables:table id="vehicles" data="${vehicles}" cdn="true" row="vehicle" theme="bootstrap2"
                        cssClass="table table-striped">
      <spring:url value="/vehicles/{vehicleId}.html" var="vehicleUrl">
        <spring:param name="vehicleId" value="${vehicle.id}"/>
      </spring:url>
      <spring:url value="/vehicles/{vehicleId}/deliveries.html" var="deliveriesUrl">
        <spring:param name="vehicleId" value="${vehicle.id}"/>
      </spring:url>

        <%-- Columns --%>
        <datatables:column title="${licMessage}">
          <a href="${vehicleUrl}"><c:out value="${vehicle.licensePlate}"/></a>
        </datatables:column>
        <datatables:column title="${deliveriesMessage}">
<%--          <c:choose>
            <c:when test="${not empty vehicle.timedDeliveries}"><a href="${deliveriesUrl}">
              <c:out value="${viewDeliveriesMessage}"/></a>
            </c:when>
            <c:otherwise><c:out value="${noDeliveriesMessage}"/></c:otherwise>
          </c:choose>--%>
          <a href="${deliveriesUrl}"><c:out value="${viewDeliveriesMessage}"/></a>
        </datatables:column>
        <datatables:column title="${capacityMessage}">
          <c:out value="${vehicle.capacity}"/>
        </datatables:column>
        <datatables:column title="${depotMessage}">
          <c:out value="${vehicle.depot.location.latitude} ${vehicle.depot.location.longitude}"/>
        </datatables:column>

      </datatables:table>
    </div>
    <!--/span-->
  </div>
  <!--/row-->
</div>
<!--/container-->

</body>

</html>
