<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="datatables" uri="http://github.com/dandelion/datatables" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ include file="/WEB-INF/jsp/admin/beanProperties.jspf" %>

<body>
<div class="container">
  <div class="row row-offcanvas row-offcanvas-right">

    <%@include file="../admin/sidebar.jsp" %>

    <div class="col-xs-12 col-sm-9">
      <h1><spring:message code="vehicle.deliveries"/></h1>

      <datatables:table id="deliveries" data="${vehicle.timedDeliveries}" cdn="true" row="delivery" theme="bootstrap2"
                        cssClass="table table-striped" paginate="false" info="false">
        <spring:url value="/customers/{customerId}.html" var="customerUrl">
          <spring:param name="customerId" value="${delivery.customer.id}"/>
        </spring:url>

        <%-- Columns --%>
        <datatables:column title="${customerMessage}" cssStyle="width: 150px;" display="html">
          <a href="${customerUrl}">
            <c:out value="${delivery.customer.location.latitude} ${delivery.customer.location.longitude}"/>
          </a>
        </datatables:column>
        <datatables:column title="${readyTimeMessage}">
          <joda:format value="${delivery.customer.timeWindow.readyTime}" pattern="yyyy/MM/dd H:mm"/>
        </datatables:column>
        <datatables:column title="${dueTimeMessage}">
          <joda:format value="${delivery.customer.timeWindow.dueTime}" pattern="yyyy/MM/dd H:mm"/>
        </datatables:column>
        <datatables:column title="${arrivalTimeMessage}" cssStyle="color: #c62104;">
          <joda:format value="${delivery.arrivalTime}" pattern="yyyy/MM/dd H:mm"/>
        </datatables:column>

      </datatables:table>
    </div>
    <!--/span-->
  </div>
  <!--/row-->
</div>
<!--/container-->

</body>

</html>
