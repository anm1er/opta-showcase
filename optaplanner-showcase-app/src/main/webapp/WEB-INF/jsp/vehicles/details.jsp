<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="datatables" uri="http://github.com/dandelion/datatables" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/admin/beanProperties.jspf" %>

<html>
<head>
  <title>Vehicle Details</title>
</head>

<body>

<div class="container">
  <div class="row row-offcanvas row-offcanvas-right">

    <%@include file="../admin/sidebar.jsp" %>

    <div class="col-xs-12 col-sm-9">
      <p class="pull-right visible-xs">
        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
      </p>

      <div id="vehicle-id" class="hidden">${vehicle.id}</div>
      <c:if test="${not empty feedbackMessage}">
        <div class="alert alert-success">${feedbackMessage}</div>
      </c:if>
      <h1><spring:message code="vehicle.view"/></h1>

      <div class="well">

        <table class="table" style="width:600px;">
          <tr>
            <th>${licMessage}:</th>
            <td><c:out value="${vehicle.licensePlate}"/></td>
          </tr>
          <tr>
            <th>${capacityMessage}:</th>
            <td><c:out value="${vehicle.capacity}"/></td>
          </tr>
          <tr>
            <th>${depotMessage}:</th>
            <td><c:out value="${vehicle.depot.location.latitude} ${vehicle.depot.location.longitude}"/></td>
          </tr>
        </table>

        <%-- buttons --%>
        <div class="action-buttons">
          <a href='<spring:url value="/vehicles/${vehicle.id}/edit" htmlEscape="true"/>' class="btn btn-primary">
            <spring:message code="label.edit"/>
          </a>
          <a href='<spring:url value="/vehicles/${vehicle.id}/delete" htmlEscape="true"/>' class="btn btn-primary">
            <spring:message code="label.delete"/>
          </a>
          <a href='<spring:url value="/vehicles" htmlEscape="true"/>' class="btn btn-default">
            <spring:message code="label.cancel"/>
          </a>
        </div>

      </div>
      <!--/well-->

    </div>
    <!--/span-->
  </div>
  <!--/row-->
</div>
<!--/container-->

</body>

</html>
