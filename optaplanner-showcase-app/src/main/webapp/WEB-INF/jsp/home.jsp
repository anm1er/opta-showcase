<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
  <title>Home</title>
</head>
<body>

<div class="jumbotron">
  <h1>More business with less resources</h1>

  <h2>What is OptaPlanner?</h2>

  <p class="lead"><a href="http://www.optaplanner.org">OptaPlanner</a> is
    <strong>a lightweight, embeddable planning engine</strong>.It is <strong>open source software</strong>,
    released under the Apache Software License, written in 100% pure Java™, running on any JVM
    and available in <a href="http://www.optaplanner.org/download/download.html">the Maven Central repository</a>.</p>.
  <p>
    <a class="btn btn-lg btn-success centred"
       href="http://docs.jboss.org/optaplanner/release/6.2.0.Final/optaplanner-docs/html_single/index.html"
       role="button">Read documentation</a>
  </p>
</div>

<div class="row">
  <div class="col-lg-4">
    <h2>Resource optimization in latest JBoss BPM Suite </h2>

    <p>Red Hat Helps Enterprises Optimize Complex Planning and Scheduling Challenges with New Business Resource Planner </p>

    <p>
      <a class="btn btn-primary"
         href="http://www.redhat.com/en/about/press-releases/red-hat-helps-enterprises-optimize-complex-planning-and-scheduling-challenges-new-business-resource-planner"
         role="button">View details &raquo;</a>
    </p>
  </div>

  <div class="col-lg-4">
    <h2>Open benchmarks for the win </h2>

    <p>Sharing our benchmarks and enabling others to easily reproduce them, is part of a bigger vision: </p>

    <p>
      <a class="btn btn-primary"
         href="http://www.optaplanner.org/blog/2014/11/07/OpenBenchmarksForTheWin.html" role="button">View details &raquo;</a>
    </p>
  </div>

  <div class="col-lg-4">
    <h2>How good are human planners? </h2>

    <p>Are we smarter than machines when it comes to planning? Or can automated planning beat humans? </p>

    <p>
      <a class="btn btn-primary"
         href="http://www.optaplanner.org/blog/2015/06/03/HowGoodAreHumanPlanners.html" role="button">View details &raquo;</a>
    </p>
  </div>
</div>

</body>
</html>
