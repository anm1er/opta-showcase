<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
  <title>Demo</title>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?language=en"></script>
</head>
<body>

<div class="row">
  <p class="pull-right visible-xs">
    <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
  </p>

  <div class="jumbotron">
    <h1>Planner demo</h1>

    <p>A basic OptaPlanner demonstration for Vehicle Routing Problem with Time Windows</p>
  </div>

  <div class="well">
    <p>
      <strong>VRP with Time Windows</strong> (VRPTW) is a problem with a fixed fleet of delivery vehicles servicing
      known customer demands for a single commodity from a common depot at minimum transit cost,
      with the additional restriction that a time window is associated with each customer and the depot.
    </p>
    <p><strong>Try your domain problems here:</strong> to edit problem go to
      <a href="<spring:url value="/admin" htmlEscape="true" />">Administration</a>.
    </p>
    <p><strong>You can check the scheduled deliveries for each vehicle</strong> on the
      <a href="<spring:url value="/vehicles" htmlEscape="true" />">Vehicles page</a>.
    </p>
  </div>

  <div class="row">
    <div class="col-md-3">
      <div class="btn-group btn-group-vertical">
        <button id="solveButton" class="btn btn-default" type="submit" onclick="solve()">Solve</button>
        <button id="terminateEarlyButton" class="btn btn-default" type="submit" onclick="terminateEarly()"
                disabled="disabled">Terminate
        </button>
      </div>
    </div>

    <div class="col-md-9">
      <div id="map-div" style="height: 600px; width: 100%; margin-top: 10px; margin-bottom: 10px;"></div>
      <p>This visualization does not work offline and uses proprietary Google software.</p>
    </div>
  </div>
  <!--/row-->
</div>

<script type="text/javascript">
  var map;
  var vehicleRouteLines;
  var intervalTimer;

  var colorSeries = ['#DC143C', '#E066FF', '#1C86EE', '#3D9140', '#FFA54F', '#808069',
    '#FF82AB', '#9B30FF', '#00B2EE', '#00FA9A', '#9C661F', '#FFD700'];
  var routeColors;

  function initMap() {
    //Enabling new cartography and themes
    var mapElement = document.getElementById('map-div');
    var mapOptions = {
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(mapElement, mapOptions);
    loadSolution();
  }

  loadSolution = function () {
    $.ajax({
      url: "<%=application.getContextPath()%>/demo/schedule",
      type: "GET",
      dataType: "json",
      success: function (solutionDTO) {
        var zoomBounds = new google.maps.LatLngBounds();
        $.each(solutionDTO.customerDTOs, function (index, customerDTO) {
          var latLng = new google.maps.LatLng(customerDTO.latitude, customerDTO.longitude);
          zoomBounds.extend(latLng);
          var marker = new google.maps.Marker({
            position: latLng,
            title: customerDTO.name + ": Deliver " + customerDTO.demand + " items.",
            map: map
          });
          google.maps.event.addListener(marker, 'click', function () {
            new google.maps.InfoWindow({
              content: customerDTO.name + "</br>Deliver " + customerDTO.demand + " items."
            }).open(map, marker);
          })
        });
        map.fitBounds(zoomBounds);
      }, error: function (jqXHR, textStatus, errorThrown) {
        alert("Error: " + errorThrown);
      }
    });
  };

  updateSolution = function () {
    $.ajax({
      url: "<%=application.getContextPath()%>/demo/schedule",
      type: "GET",
      dataType: "json",
      success: function (solutionDTO) {
        if (vehicleRouteLines != undefined) {
          for (var i = 0; i < vehicleRouteLines.length; i++) {
            vehicleRouteLines[i].setMap(null);
          }
        }
        vehicleRouteLines = [];
        routeColors = [];
        var routeCount = 0;
        $.each(solutionDTO.vehicleDTOs, function (i, vehicleDTO) {
          var locations = [new google.maps.LatLng(vehicleDTO.depotLatitude, vehicleDTO.depotLongitude)];
          $.each(vehicleDTO.customerDTOs, function (j, customerDTO) {
            locations.push(new google.maps.LatLng(customerDTO.latitude, customerDTO.longitude));
          });
          locations.push(new google.maps.LatLng(vehicleDTO.depotLatitude, vehicleDTO.depotLongitude));
          routeColors.push(colorSeries[routeCount]);
          if (locations.length > 2)
            routeCount++;
          if (routeCount == colorSeries.length)
            routeCount = 0;
          var line = new google.maps.Polyline({
            path: locations,
            geodesic: true,
            strokeColor: routeColors[i],
            strokeOpacity: 0.8,
            strokeWeight: 4
          });
          line.setMap(map);
          vehicleRouteLines.push(line);
        });
      }, error: function (jqXHR, textStatus, errorThrown) {
        alert("Error: " + errorThrown);
      }
    });
  };

  solve = function () {
    $('#solveButton').attr("disabled", "disabled");
    $.ajax({
      url: "<%=application.getContextPath()%>/demo/start",
      type: "POST",
      dataType: "text",
      data: "",
      success: function (message) {
        console.log(message);
        intervalTimer = setInterval(function () {
          updateSolution()
        }, 2000);
        $('#terminateEarlyButton').removeAttr("disabled");
      }, error: function (jqXHR, textStatus, errorThrown) {
        alert("Error: " + errorThrown);
      }
    });
  };

  terminateEarly = function () {
    $('#terminateEarlyButton').attr("disabled", "disabled");
    window.clearInterval(intervalTimer);
    $.ajax({
      url: "<%=application.getContextPath()%>/demo/terminate",
      type: "POST",
      data: "",
      dataType: "text",
      success: function (message) {
        console.log(message);
        updateSolution();
        $('#solveButton').removeAttr("disabled");
      }, error: function (jqXHR, textStatus, errorThrown) {
        alert("Error: " + jqXHR.responseText);
      }
    });
  };

  function sleep(milliseconds) {
    console.log("Sleeping " + milliseconds + " ms");
    // Don't hang the browser page like this
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > milliseconds) {
        break;
      }
    }
  }

  google.maps.event.addDomListener(window, 'load', initMap);

</script>

</body>

</html>
