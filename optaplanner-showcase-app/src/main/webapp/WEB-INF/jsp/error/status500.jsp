<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <title>Error</title>
</head>

<body>
<div class="container">

  <h2><spring:message code="label.internal_server_error.page.title"/></h2>

  <div class="page-content">
    <p><spring:message code="label.internal_server_error.page.message"/></p>
  </div>

  <!--
    Exception:  ${exception.message}
        <c:forEach items="${exception.stackTrace}" var="ste">
            ${ste}
        </c:forEach>
    -->

</div>
</body>

</html>

