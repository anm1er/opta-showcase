<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <title>404</title>
</head>

<body>
<div class="container">

  <h2><spring:message code="label.404.page.title"/></h2>

  <div class="page-content">
    <p><spring:message code="label.404.page.message"/></p>
  </div>

</div>
</body>

</html>
