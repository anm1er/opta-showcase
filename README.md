# OptaShowcase

OptaShowcase is a basic demonstration of [OptaPlanner], a lightweight, embeddable planning engine (known as jBPM-OptaPlanner
and before 6.0.0.Beta1 as Drools-Planner), to optimize *vehicle routing problem with time windows*. (It is in fact an extension
of `org.optaplanner.examples.vehiclerouting`, so most credit should go to the brilliant [OptaPlanner team])

The showcase was partly inspired by [PLANNER-159] and some room for experimentation with the problem planning model.
However, I ended up with a quite simple spring web app to manipulate domain data and a basic playground for trying out
known VRP instances (both *Capacitated VRP* and *Capacitated VRP with Time Windows*).

Check out [vrp instances] for file format details, problem instances and the corresponding optimal solutions.

## VRPTW

**VRP with Time Windows (VRPTW)** is a combinatorial optimization and integer programming problem seeking to service known
customer demands for a single commodity with a fixed fleet of delivery vehicles at minimum transit cost, with the additional
restriction that a time window is associated with each customer and the depot.

You may find the formal problem description in the papers (*TODO*).

## Domain Model and Solver Configuration
Most of the `showcase.model.*` code relies on the 6.2.0 `optaplanner-examples`. I'm still to find out whether any alternatives
to the original planning model for this and other flavours of VRP are worth further research, but I currently stick with
`org.optaplanner.examples.vehiclerouting.domain` implementation.
The solver configuration is set to use `vehicleRoutingScoreRules.drl` for score calculation.

## Running the Build
```
$ cd opta-showcase
$ mvn clean install

```

## TODO
A few things to mention:

* Fix mapping domain deliveries to solved problem (to avoid starting solving afresh when the preset solution exists)
* Make UI more explicit about met constraints, score, travel distance, etc
* Add a new dataset format that would support both [road distances] and time window constraints
* Switch to Thymeleaf

[vrp instances]: http://neo.lcc.uma.es/vrp/vrp-instances/capacitated-vrp-instances/
[OptaPlanner]: http://www.optaplanner.org
[OptaPlanner team]: http://www.optaplanner.org/community/team.html
[PLANNER-159]: https://issues.jboss.org/browse/PLANNER-159
[road distances]: http://www.optaplanner.org/blog/2014/09/02/VehicleRoutingWithRealRoadDistances.html