package org.optaplanner.showcase.model.vehiclerouting.impl.domain.timewindowed.problem;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem.TimeWindowedVehicleRoutingProblem;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem.TimeWindowedVehicleRoutingProblemFactory;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem.TimeWindowedVehicleRoutingProblemBuilder;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

public class DefaultVehicleRoutingProblemFactory implements TimeWindowedVehicleRoutingProblemFactory {

    // For convenience includes an option to construct a problem from scratch with builder
    @Override
    public Builder getProblemBuilder() {
        return new Builder();
    }

    @Override
    public TimeWindowedVehicleRoutingProblem createProblem(final VehicleRoutingSolution solution) {
        return new DefaultVehicleRoutingProblemImpl(solution);
    }

    public static class Builder extends TimeWindowedVehicleRoutingProblemBuilder {

        @Override
        public TimeWindowedVehicleRoutingProblem createProblem() {
            return new DefaultVehicleRoutingProblemImpl();
        }

        @Override
        public Builder from(TimeWindowedVehicleRoutingProblem originalProblem) {
            built.setCustomerList(originalProblem.getCustomerList());
            built.setVehicleList(originalProblem.getVehicleList());
            built.setDepotList(originalProblem.getDepotList());
            built.setLocationList(originalProblem.getLocationList());
            built.setDistanceType(originalProblem.getDistanceType());
            built.setScore(originalProblem.getScore());
            built.setName(originalProblem.getName());
            return this;
        }
    }

}
