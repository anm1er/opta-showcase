package org.optaplanner.showcase.model.vehiclerouting.api.domain.location;

import org.optaplanner.showcase.model.common.PlannerIdentifiable;

public interface PlanningLocation extends PlannerIdentifiable {

    String getName();

    void setName(String name);

    double getLatitude();

    void setLatitude(double latitude);

    double getLongitude();

    void setLongitude(double longitude);

    /**
     * The unit of measurement depends on the planning domain model implementation.
     * @param otherLocation never null
     * @return a positive number, distance to {@code otherLocation}
     */
    int getDistanceTo(PlanningLocation otherLocation);

    /**
     * Returns the angle relative to the direction EAST.
     * @param otherLocation never null
     * @return angle in Cartesian coordinates
     */
    double getAngle(PlanningLocation otherLocation);

    DistanceType getDistanceType();

}
