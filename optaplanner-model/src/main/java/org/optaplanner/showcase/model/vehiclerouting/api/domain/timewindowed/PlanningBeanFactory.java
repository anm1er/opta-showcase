package org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;

public interface PlanningBeanFactory {

    PlanningLocation createLocation();

    PlanningLocation createUniqueLocation(String name, double latitude, double longitude);

    PlanningVehicle createVehicle();

    PlanningTimeWindowedCustomer createCustomer();

    PlanningTimeWindowedDepot createDepot();

}
