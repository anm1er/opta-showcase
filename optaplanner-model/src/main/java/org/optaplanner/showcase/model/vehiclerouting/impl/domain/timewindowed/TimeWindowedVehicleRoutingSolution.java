package org.optaplanner.showcase.model.vehiclerouting.impl.domain.timewindowed;

import java.util.Objects;

import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("VrpTimeWindowedVehicleRoutingSolution")
public class TimeWindowedVehicleRoutingSolution extends VehicleRoutingSolution {

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode());
    }

}
