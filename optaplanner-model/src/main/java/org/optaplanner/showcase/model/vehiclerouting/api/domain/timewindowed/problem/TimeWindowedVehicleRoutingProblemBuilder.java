package org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem;

import java.util.List;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.DistanceType;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;

public abstract class TimeWindowedVehicleRoutingProblemBuilder {

    protected TimeWindowedVehicleRoutingProblem built;

    protected TimeWindowedVehicleRoutingProblemBuilder() {
        built = createProblem();
    }

    public abstract TimeWindowedVehicleRoutingProblem createProblem();

    public abstract TimeWindowedVehicleRoutingProblemBuilder from(TimeWindowedVehicleRoutingProblem problem);

    public TimeWindowedVehicleRoutingProblemBuilder getBuilder() {
        return this;
    }

    public TimeWindowedVehicleRoutingProblemBuilder withName(String name) {
        built.setName(name);
        return this;
    }

    public TimeWindowedVehicleRoutingProblemBuilder withDistanceType(DistanceType distanceType) {
        built.setDistanceType(distanceType);
        return this;
    }

    public TimeWindowedVehicleRoutingProblemBuilder withLocations(List<PlanningLocation> locations) {
        built.setLocationList(locations);
        return this;
    }

    public TimeWindowedVehicleRoutingProblemBuilder withCustomers(List<PlanningTimeWindowedCustomer> customers) {
        built.setCustomerList(customers);
        return this;
    }

    public TimeWindowedVehicleRoutingProblemBuilder withDepots(List<PlanningTimeWindowedDepot> depots) {
        built.setDepotList(depots);
        return this;
    }

    public TimeWindowedVehicleRoutingProblemBuilder withVehicles(List<PlanningVehicle> vehicles) {
        built.setVehicleList(vehicles);
        return this;
    }

    public TimeWindowedVehicleRoutingProblem build() {
        return built;
    }

}
