package org.optaplanner.showcase.model.common;

/**
 * Guarantees that any planning bean - <em>planning entity</em> or <em>problem fact</em> - provides ID.
 */
public interface PlannerIdentifiable {

    Long getId();

    void setId(Long id);
}
