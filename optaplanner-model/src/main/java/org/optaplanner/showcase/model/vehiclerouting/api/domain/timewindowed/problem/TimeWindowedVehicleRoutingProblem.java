package org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.problem.VehicleRoutingProblem;

/**
 * Represents vehicle routing problem with time windows view to be used outside the planning engine.
 */
public interface TimeWindowedVehicleRoutingProblem extends VehicleRoutingProblem {

}
