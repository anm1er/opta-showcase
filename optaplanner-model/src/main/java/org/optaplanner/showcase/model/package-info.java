/**
 * The classes in this package represent the planning model of the vehicle routing problem.
 * It is mostly the copy of the {@link org.optaplanner.examples.vehiclerouting.domain} classes with a few touches.
 * There could be alternatives to model implementation, but this sample makes use of the original
 * {@link org.optaplanner.examples} solution.
 *
 */
package org.optaplanner.showcase.model;

