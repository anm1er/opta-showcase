package org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.problem.VehicleRoutingProblemFactory;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

public interface TimeWindowedVehicleRoutingProblemFactory extends VehicleRoutingProblemFactory {

    TimeWindowedVehicleRoutingProblem createProblem(VehicleRoutingSolution solution);

    TimeWindowedVehicleRoutingProblemBuilder getProblemBuilder();

}
