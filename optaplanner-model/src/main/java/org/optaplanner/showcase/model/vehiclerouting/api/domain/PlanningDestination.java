package org.optaplanner.showcase.model.vehiclerouting.api.domain;

import org.optaplanner.showcase.model.common.PlannerIdentifiable;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;

/**
 *  Represents a single destination point - a customer or a depot - on a route of a vehicle routing problem instance.
 */
public interface PlanningDestination extends PlannerIdentifiable {

    PlanningLocation getLocation();

    void setLocation(PlanningLocation location);

}
