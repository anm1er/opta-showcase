package org.optaplanner.showcase.model.vehiclerouting.api.domain.problem;

import java.util.List;

import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningDepot;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.DistanceType;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

/**
 * Represents vehicle routing problem view to be used outside the planning engine.
 * It encapsulates the actual 'solution' (the one to hold the scheduled planning facts) for interactions with planner.
 */
public interface VehicleRoutingProblem {

    HardSoftScore getScore();

    void setScore(HardSoftScore score);

    String getName();

    void setName(String name);

    DistanceType getDistanceType();

    void setDistanceType(DistanceType distanceType);

    List<PlanningLocation> getLocationList();

    void setLocationList(List<? extends PlanningLocation> locationList);

    List<? extends PlanningCustomer> getCustomerList();

    void setCustomerList(List<? extends PlanningCustomer> customerList);

    List<? extends PlanningDepot> getDepotList();

    void setDepotList(List<? extends PlanningDepot> depotList);

    List<PlanningVehicle> getVehicleList();

    void setVehicleList(List<? extends PlanningVehicle> vehicleList);

    void setCustomer(int position, PlanningCustomer customer);

    void addCustomer(PlanningCustomer customer);

    void removeCustomer(int position);

    void setDepot(int position, PlanningDepot depot);

    void addDepot(PlanningDepot depot);

    void removeDepot(int position);

    void setVehicle(int position, PlanningVehicle vehicle);

    void addVehicle(PlanningVehicle vehicle);

    void removeVehicle(int position);

    VehicleRoutingSolution getPlannerSolution();

    int getCustomersNumber();

    int getVehiclesNumber();

    int getDepotsNumber();

    int getLocationsNumber();

    /**
     * Clears the encapsulated VehicleRoutingSolution so that no scheduled data
     * is associated with this problem instance.
     */
    void clearSolution();

}
