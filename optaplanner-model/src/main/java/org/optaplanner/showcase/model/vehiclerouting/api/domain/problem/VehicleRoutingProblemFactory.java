package org.optaplanner.showcase.model.vehiclerouting.api.domain.problem;

import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;

public interface VehicleRoutingProblemFactory {

    VehicleRoutingProblem createProblem(VehicleRoutingSolution solution);

}
