/*
 * Copyright 2012 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.showcase.model.vehiclerouting.impl.domain;

import java.util.Objects;

import org.optaplanner.showcase.model.common.AbstractPersistable;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningDepot;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.location.Location;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.timewindowed.TimeWindowedDepot;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamInclude;

@XStreamAlias("VrpDepot")
@XStreamInclude({
        TimeWindowedDepot.class
})
public class Depot extends AbstractPersistable implements PlanningDepot {

    protected Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(PlanningLocation location) {
        this.location = (Location) location;
    }

    @Override
    public String toString() {
        return location.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Depot depot = (Depot) o;
        return Objects.equals(location, depot.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(location);
    }

}
