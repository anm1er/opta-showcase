/*
 * Copyright 2012 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.showcase.model.vehiclerouting.impl.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.impl.score.buildin.hardsoft.HardSoftScoreDefinition;
import org.optaplanner.persistence.xstream.impl.score.XStreamScoreConverter;
import org.optaplanner.showcase.model.common.AbstractPersistable;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.DistanceType;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.location.Location;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.timewindowed.TimeWindowedVehicleRoutingSolution;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamInclude;

@PlanningSolution
@XStreamAlias("VrpVehicleRoutingSolution")
@XStreamInclude({
        TimeWindowedVehicleRoutingSolution.class
})
public class VehicleRoutingSolution extends AbstractPersistable implements Solution<HardSoftScore> {

    protected List<Location> locationList = new ArrayList<>();
    protected List<Customer> customerList = new ArrayList<>();
    protected List<Vehicle> vehicleList = new ArrayList<>();
    protected List<Depot> depotList = new ArrayList<>();

    private String name;
    private DistanceType distanceType;
    private String distanceUnitOfMeasurement;

    @XStreamConverter(value = XStreamScoreConverter.class, types = {HardSoftScoreDefinition.class})
    private HardSoftScore score;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DistanceType getDistanceType() {
        return distanceType;
    }

    public void setDistanceType(DistanceType distanceType) {
        this.distanceType = distanceType;
    }

    public String getDistanceUnitOfMeasurement() {
        return distanceUnitOfMeasurement;
    }

    public void setDistanceUnitOfMeasurement(String distanceUnitOfMeasurement) {
        this.distanceUnitOfMeasurement = distanceUnitOfMeasurement;
    }

    public List<Location> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<Location> locationList) {
        this.locationList = locationList;
    }

    @PlanningEntityCollectionProperty
    @ValueRangeProvider(id = "customerRange")
    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    @PlanningEntityCollectionProperty
    @ValueRangeProvider(id = "vehicleRange")
    public List<Vehicle> getVehicleList() {
        return vehicleList;
    }

    public void setVehicleList(List<Vehicle> vehicleList) {
        this.vehicleList = vehicleList;
    }

    public List<Depot> getDepotList() {
        return depotList;
    }

    public void setDepotList(List<Depot> depotList) {
        this.depotList = depotList;
    }

    @Override
    public HardSoftScore getScore() {
        return score;
    }

    @Override
    public void setScore(HardSoftScore score) {
        this.score = score;
    }

    @Override
    public Collection<?> getProblemFacts() {
        List<Object> facts = new ArrayList<>();
        facts.addAll(getLocationList());
        facts.addAll(getDepotList());
        // Do not add the planning entities (vehicleList, customerList) because that will be done automatically
        return facts;
    }

    // Note: equals() and hashCode() added to original VehicleRoutingSolution

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VehicleRoutingSolution solution = (VehicleRoutingSolution) o;
        return listsEqualWithIgnoredOrder(customerList, solution.customerList)
                && listsEqualWithIgnoredOrder(vehicleList, solution.vehicleList)
                && listsEqualWithIgnoredOrder(depotList, solution.depotList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(locationList, customerList, vehicleList, depotList);
    }

    private boolean listsEqualWithIgnoredOrder(List<?> list1, List<?> list2) {
        return new HashSet(list1).equals(new HashSet(list2));
    }

}
