package org.optaplanner.showcase.model.vehiclerouting.impl.domain.timewindowed.problem;

import java.util.ArrayList;
import java.util.List;

import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningDepot;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.DistanceType;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.problem.TimeWindowedVehicleRoutingProblem;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.Customer;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.Depot;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.Vehicle;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.VehicleRoutingSolution;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.location.Location;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.timewindowed.TimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.timewindowed.TimeWindowedDepot;

import com.google.common.base.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkArgument;

public class DefaultVehicleRoutingProblemImpl implements TimeWindowedVehicleRoutingProblem {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultVehicleRoutingProblemImpl.class);

    private VehicleRoutingSolution plannerSolution;

    public DefaultVehicleRoutingProblemImpl() {
        plannerSolution = new VehicleRoutingSolution();
    }

    public DefaultVehicleRoutingProblemImpl(VehicleRoutingSolution plannerSolution) {
        this.plannerSolution = plannerSolution;
    }

    private void setDistanceUnitOfMeasurement(String distanceUnitOfMeasurement) {
        plannerSolution.setDistanceUnitOfMeasurement(distanceUnitOfMeasurement);
    }

    @Override
    public VehicleRoutingSolution getPlannerSolution() {
        return plannerSolution;
    }

    @Override
    public HardSoftScore getScore() {
        return plannerSolution.getScore();
    }

    @Override
    public void setScore(HardSoftScore score) {
        plannerSolution.setScore(score);
    }

    @Override
    public String getName() {
        return plannerSolution.getName() == null ? "" : plannerSolution.getName();
    }

    @Override
    public void setName(String name) {
        plannerSolution.setName(name);
    }

    @Override
    public DistanceType getDistanceType() {
        return plannerSolution.getDistanceType() == null ? DistanceType.UNDETERMINED : plannerSolution.getDistanceType();
    }

    @Override
    public void setDistanceType(DistanceType distanceType) {
        plannerSolution.setDistanceType(distanceType);
    }

    @Override
    public List<PlanningLocation> getLocationList() {
        List<PlanningLocation> locations = new ArrayList<>();
        locations.addAll(plannerSolution.getLocationList());
        return locations;
    }

    @Override
    public void setLocationList(List<? extends PlanningLocation> locationList) {
        List<Location> locations = new ArrayList<>(locationList.size());
        for (PlanningLocation planningLocation : locationList) {
            verifyEntityActualType(Location.class, planningLocation);
            locations.add((Location) planningLocation);
        }
        plannerSolution.setLocationList(locations);
    }

    @Override
    public List<PlanningTimeWindowedCustomer> getCustomerList() {
        List<PlanningTimeWindowedCustomer> planningCustomers = new ArrayList<>(getCustomersNumber());
        for (Customer customer : plannerSolution.getCustomerList()) {
            verifyEntityActualType(TimeWindowedCustomer.class, customer);
            planningCustomers.add((PlanningTimeWindowedCustomer) customer);
        }
        return planningCustomers;
    }

    @Override
    public void setCustomerList(List<? extends PlanningCustomer> customerList) {
        List<Customer> customers = new ArrayList<>(customerList.size());
        for (PlanningCustomer planningCustomer : customerList) {
            verifyEntityActualType(TimeWindowedCustomer.class, planningCustomer);
            customers.add((TimeWindowedCustomer) planningCustomer);
        }
        plannerSolution.setCustomerList(customers);
    }

    @Override
    public List<PlanningTimeWindowedDepot> getDepotList() {
        List<PlanningTimeWindowedDepot> planningDepots = new ArrayList<>(getDepotsNumber());
        for (Depot depot : plannerSolution.getDepotList()) {
            verifyEntityActualType(TimeWindowedDepot.class, depot);
            planningDepots.add((PlanningTimeWindowedDepot) depot);
        }
        return planningDepots;
    }

    @Override
    public void setDepotList(List<? extends PlanningDepot> depotList) {
        List<Depot> depots = new ArrayList<>(depotList.size());
        for (PlanningDepot planningDepot : depotList) {
            verifyEntityActualType(TimeWindowedDepot.class, planningDepot);
            depots.add((TimeWindowedDepot) planningDepot);
        }
        plannerSolution.setDepotList(depots);
    }

    @Override
    public List<PlanningVehicle> getVehicleList() {
        List<PlanningVehicle> planningVehicles = new ArrayList<>(getVehiclesNumber());
        planningVehicles.addAll(plannerSolution.getVehicleList());
        return planningVehicles;
    }

    @Override public void setVehicleList(List<? extends PlanningVehicle> vehicleList) {
        List<Vehicle> vehicles = new ArrayList<>(vehicleList.size());
        for (PlanningVehicle planningVehicle : vehicleList) {
            verifyEntityActualType(Vehicle.class, planningVehicle);
            vehicles.add((Vehicle) planningVehicle);
        }
        plannerSolution.setVehicleList(vehicles);
    }

    @Override
    public void setCustomer(int position, PlanningCustomer customer) {
        int customersNumber = getCustomersNumber();
        checkArgument(position < customersNumber, "position should be less than " + customersNumber);
        verifyEntityActualType(Customer.class, customer);
        plannerSolution.getCustomerList().set(position, (Customer) customer);
    }

    @Override
    public void addCustomer(PlanningCustomer customer) {
        verifyEntityActualType(Customer.class, customer);
        plannerSolution.getCustomerList().add((Customer) customer);
    }

    @Override
    public void removeCustomer(int position) {
        int customersNumber = getCustomersNumber();
        checkArgument(position < customersNumber, "position should be less than " + customersNumber);
        plannerSolution.getCustomerList().remove(position);
    }

    @Override
    public void setDepot(int position, PlanningDepot depot) {
        int depotsNumber = getDepotsNumber();
        checkArgument(position < depotsNumber, "position should be less than " + depotsNumber);
        verifyEntityActualType(Depot.class, depot);
        plannerSolution.getDepotList().set(position, (Depot) depot);
    }

    @Override
    public void addDepot(PlanningDepot depot) {
        verifyEntityActualType(Depot.class, depot);
        plannerSolution.getDepotList().add((Depot) depot);
    }

    @Override
    public void removeDepot(int position) {
        int depotsNumber = getDepotsNumber();
        checkArgument(position < depotsNumber, "position should be less than " + depotsNumber);
        plannerSolution.getDepotList().remove(position);
    }

    @Override
    public void setVehicle(int position, PlanningVehicle vehicle) {
        int vehiclesNumber = getVehiclesNumber();
        checkArgument(position < vehiclesNumber, "position should be less than " + vehiclesNumber);
        verifyEntityActualType(Vehicle.class, vehicle);
        plannerSolution.getVehicleList().set(position, (Vehicle) vehicle);
    }

    @Override
    public void addVehicle(PlanningVehicle vehicle) {
        verifyEntityActualType(Vehicle.class, vehicle);
        plannerSolution.getVehicleList().add((Vehicle) vehicle);
    }

    @Override
    public void removeVehicle(int position) {
        int vehiclesNumber = getVehiclesNumber();
        checkArgument(position < vehiclesNumber, "position should be less than " + vehiclesNumber);
        plannerSolution.getVehicleList().remove(position);
    }

    @Override
    public void clearSolution() {
        for (Vehicle vehicle : plannerSolution.getVehicleList()) {
            Customer customer = vehicle.getNextCustomer();
            while (customer != null) {
                Customer thisCustomer = customer;
                customer = customer.getNextCustomer();
                thisCustomer.setNextCustomer(null);
                thisCustomer.setPreviousStandstill(null);
                if (thisCustomer instanceof TimeWindowedCustomer) {
                    ((TimeWindowedCustomer) thisCustomer).setArrivalTime(0);
                }
                thisCustomer = null;
            }
            vehicle.setNextCustomer(null);
        }
    }

    @Override
    public int getCustomersNumber() {
        return plannerSolution.getCustomerList().size();
    }

    @Override
    public int getVehiclesNumber() {
        return plannerSolution.getVehicleList().size();
    }

    @Override
    public int getDepotsNumber() {
        return plannerSolution.getDepotList().size();
    }

    @Override
    public int getLocationsNumber() {
        return plannerSolution.getLocationList().size();
    }

    private void verifyEntityActualType(Class<?> clazz, Object obj) {
        checkArgument(clazz.isInstance(obj), "Expected object of type %s, but was %s",
                clazz, (obj != null ? obj.getClass().getName() : "null"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DefaultVehicleRoutingProblemImpl problem = (DefaultVehicleRoutingProblemImpl) o;
        return java.util.Objects.equals(plannerSolution, problem.plannerSolution);
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(plannerSolution);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("name", getName())
                .add("customers number", getCustomersNumber())
                .add("vehicles number", getVehiclesNumber())
                .add("depots number", getDepotsNumber())
                .add("distanceType", getDistanceType())
                .toString();
    }

}
