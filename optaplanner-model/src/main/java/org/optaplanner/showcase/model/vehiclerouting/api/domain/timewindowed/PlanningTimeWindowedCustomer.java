package org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningCustomer;

public interface PlanningTimeWindowedCustomer extends PlanningCustomer, TimeWindow {

    int getServiceDuration();

    void setServiceDuration(int serviceDuration);

    int getScheduledTime();

}
