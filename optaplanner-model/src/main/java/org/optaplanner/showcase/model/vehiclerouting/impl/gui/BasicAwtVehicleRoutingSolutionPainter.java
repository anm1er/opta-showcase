package org.optaplanner.showcase.model.vehiclerouting.impl.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.CubicCurve2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.ImageIcon;

import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.examples.common.swingui.TangoColorFactory;
import org.optaplanner.examples.common.swingui.latitudelongitude.LatitudeLongitudeTranslator;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningDepot;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.problem.VehicleRoutingProblem;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;
import org.optaplanner.showcase.model.vehiclerouting.api.gui.VehicleRoutingSolutionPainter;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.location.AirLocation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This painter is a modified version of the original
 * {@link org.optaplanner.examples.vehiclerouting.swingui.VehicleRoutingSolutionPainter}.
 * It is not thread-safe and serves the demonstrative purposes only.
 */
public class BasicAwtVehicleRoutingSolutionPainter implements VehicleRoutingSolutionPainter {

    public static final int DEPOT_MARKER_WIDTH = 5;
    public static final int DEPOT_MARKER_HEIGHT = 5;
    public static final int LEGEND_STRING_POSITION_X = 15;
    public static final int CUSTOMER_CLOCK_VERTICAL_OFFSET = 5;
    public static final int STRING_VERTICAL_PADDING = 10;
    public static final int HEIGHT_OFFSET_FOR_TRANSLATOR = 10;
    // To avoid floating point arithmetic rounding errors
    public static final double FACTOR = 1000.0;
    private static final int TEXT_SIZE = 12;
    private static final int TIME_WINDOW_DIAMETER = 26;
    private static final NumberFormat NUMBER_FORMAT = new DecimalFormat("#,##0.00");
    private static final int CUSTOMER_MARKER_HEIGHT = 3;
    private static final int CUSTOMER_MARKER_WIDTH = 3;
    private static final String IMAGE_PATH_PREFIX = "/org/optaplanner/showcase/model/vehiclerouting/gui/";
    private static final Logger LOGGER = LoggerFactory.getLogger(BasicAwtVehicleRoutingSolutionPainter.class);

    private final ImageIcon depotImageIcon;
    private final ImageIcon[] vehicleImageIcons;

    private BufferedImage canvas = null;
    private LatitudeLongitudeTranslator translator = null;

    public BasicAwtVehicleRoutingSolutionPainter() {
        depotImageIcon = new ImageIcon(getClass().getResource(IMAGE_PATH_PREFIX + "depot.png"));
        vehicleImageIcons = new ImageIcon[]{
                new ImageIcon(getClass().getResource(IMAGE_PATH_PREFIX + "vehicleChameleon.png")),
                new ImageIcon(getClass().getResource(IMAGE_PATH_PREFIX + "vehicleButter.png")),
                new ImageIcon(getClass().getResource(IMAGE_PATH_PREFIX + "vehicleSkyBlue.png")),
                new ImageIcon(getClass().getResource(IMAGE_PATH_PREFIX + "vehicleChocolate.png")),
                new ImageIcon(getClass().getResource(IMAGE_PATH_PREFIX + "vehiclePlum.png")),
        };
        if (vehicleImageIcons.length != TangoColorFactory.SEQUENCE_1.length) {
            throw new IllegalStateException("The vehicleImageIcons length (" + vehicleImageIcons.length
                    + ") should be equal to the TangoColorFactory.SEQUENCE length ("
                    + TangoColorFactory.SEQUENCE_1.length + ").");
        }
    }

    public BufferedImage getCanvas() {
        return canvas;
    }

    public LatitudeLongitudeTranslator getTranslator() {
        return translator;
    }

    @Override
    public void reset(VehicleRoutingProblem problem, Dimension size, ImageObserver imageObserver) {
        translator = new LatitudeLongitudeTranslator();
        for (PlanningLocation location : problem.getLocationList()) {
            translator.addCoordinates(location.getLatitude(), location.getLongitude());
        }
        double width = size.getWidth();
        double height = size.getHeight();
        translator.prepareFor(width, height - HEIGHT_OFFSET_FOR_TRANSLATOR - TEXT_SIZE);

        Graphics2D g = createCanvas(width, height);
        g.setFont(g.getFont().deriveFont((float) TEXT_SIZE));
        g.setStroke(TangoColorFactory.NORMAL_STROKE);
        drawCustomerDetails(g, problem);
        drawDepotDetails(g, imageObserver, problem);

        int colorIndex = 0;
        boolean singleCustomerChain = false;
        // Instead of N^2 iteration over all vehicles and all customers this one is an iteration over customer linked list
        // iff the given vehicle is scheduled for transportation
        for (PlanningVehicle vehicle : problem.getVehicleList()) {
            if (vehicle.getDepot() == null) {
                LOGGER.error("Vehicle has null depot reference",
                        new IllegalStateException("Invalid VR problem instance: vehicle " + vehicle
                                + " has no depot reference"));
            } else {
                PlanningLocation depotLocation = vehicle.getDepot().getLocation();
                g.setColor(TangoColorFactory.SEQUENCE_2[colorIndex]);
                PlanningCustomer vehicleInfoCustomer = null;
                int longestNonDepotDistance = -1;
                int load = 0;
                PlanningCustomer customer = vehicle.getFirstCustomerToService();
                if (customer != null) {
                    drawRoute(g, depotLocation, customer.getLocation(),
                            customer.getLocation() instanceof AirLocation);
                    while (customer.getNextCustomer() != null) {
                        load += customer.getDemand();
                        PlanningCustomer nextCustomer = customer.getNextCustomer();
                        PlanningLocation nextLocation = customer.getNextCustomer().getLocation();
                        drawRoute(g, customer.getLocation(), nextLocation, nextLocation instanceof AirLocation);
                        // Determines where to draw the vehicle info
                        int distance = customer.getLocation().getDistanceTo(customer.getNextCustomer().getLocation());
                        if (longestNonDepotDistance < distance) {
                            longestNonDepotDistance = distance;
                            vehicleInfoCustomer = customer;
                        }
                        customer = nextCustomer;
                    }
                    if (load == 0) {
                        // If we haven't moved any further, we make a single-customer chain
                        load += customer.getDemand();
                        singleCustomerChain = true;
                        vehicleInfoCustomer = customer;
                    }
                    // Draws line back to the vehicle depot
                    g.setStroke(TangoColorFactory.FAT_DASHED_STROKE);
                    drawRoute(g, depotLocation, customer.getLocation(), customer.getLocation() instanceof AirLocation);
                    g.setStroke(TangoColorFactory.NORMAL_STROKE);
                }
                if (vehicleInfoCustomer != null) {
                    if (load > vehicle.getCapacity()) {
                        g.setColor(TangoColorFactory.SCARLET_2);
                    }
                    if (singleCustomerChain) {
                        drawVehicleDetails(g, vehicleImageIcons[colorIndex], imageObserver, depotLocation,
                                customer.getLocation(), load, vehicle.getCapacity());
                    } else {
                        drawVehicleDetails(g, vehicleImageIcons[colorIndex], imageObserver,
                                vehicleInfoCustomer, load, vehicle.getCapacity());
                    }
                    // The next line was moved from the outer loop:
                    // to reduce probability that several vehicles are painted in one color
                    colorIndex = (colorIndex + 1) % TangoColorFactory.SEQUENCE_2.length;
                }
            }
        }
        drawLegend(g, problem, width, height);
        drawSoftScore(g, problem, width, height);
    }

    @Override
    public void reset(VehicleRoutingProblem problem, Dimension size) {
        reset(problem, size, null);
    }

    private void drawCustomerDetails(Graphics2D g, VehicleRoutingProblem solution) {
        int maxWindowTime = determineMaximumTimeWindowTime(solution);
        for (PlanningCustomer customer : solution.getCustomerList()) {
            PlanningLocation location = customer.getLocation();
            int x = translator.translateLongitudeToX(location.getLongitude());
            int y = translator.translateLatitudeToY(location.getLatitude());
            g.setColor(TangoColorFactory.ALUMINIUM_6);
            g.fillRect(x - 1, y - 1, CUSTOMER_MARKER_WIDTH, CUSTOMER_MARKER_HEIGHT);
            String demandString = Integer.toString(customer.getDemand());
            g.drawString(customer.getId().toString() + '/' + demandString,
                    x - (g.getFontMetrics().stringWidth(demandString) / 2), y - TEXT_SIZE / 2);
            // Adds time windowed customer details
            if (customer instanceof PlanningTimeWindowedCustomer) {
                PlanningTimeWindowedCustomer timeWindowedCustomer = (PlanningTimeWindowedCustomer) customer;
                g.setColor(TangoColorFactory.ALUMINIUM_3);
                int circleX = x - (TIME_WINDOW_DIAMETER / 2);
                int circleY = y + CUSTOMER_CLOCK_VERTICAL_OFFSET;
                g.drawOval(circleX, circleY, TIME_WINDOW_DIAMETER, TIME_WINDOW_DIAMETER);
                g.fillArc(circleX, circleY, TIME_WINDOW_DIAMETER, TIME_WINDOW_DIAMETER,
                        getAngleForTimeWindowDegree(90, maxWindowTime, timeWindowedCustomer.getReadyTime()),
                        getAngleForTimeWindowDegrees(maxWindowTime, timeWindowedCustomer.getReadyTime(),
                                timeWindowedCustomer.getDueTime()));
                if (timeWindowedCustomer.getScheduledTime() > 0) {
                    if (timeWindowedCustomer.getScheduledTime() > timeWindowedCustomer.getDueTime()) {
                        g.setColor(TangoColorFactory.SCARLET_2);
                    } else if (timeWindowedCustomer.getScheduledTime() > 0
                            && timeWindowedCustomer.getScheduledTime() < timeWindowedCustomer.getReadyTime()) {
                        g.setColor(TangoColorFactory.ORANGE_2);
                    } else {
                        g.setColor(TangoColorFactory.ALUMINIUM_6);
                    }
                    g.setStroke(TangoColorFactory.THICK_STROKE);
                    int circleCenterY = y + CUSTOMER_CLOCK_VERTICAL_OFFSET + TIME_WINDOW_DIAMETER / 2;
                    int angle = calculateTimeWindowDegree(maxWindowTime, timeWindowedCustomer.getScheduledTime());
                    g.drawLine(x, circleCenterY,
                            x + (int) (Math.sin(Math.toRadians(angle)) * (TIME_WINDOW_DIAMETER / 2 + 3)),
                            circleCenterY - (int) (Math.cos(Math.toRadians(angle)) * (TIME_WINDOW_DIAMETER / 2 + 3)));
                    g.setStroke(TangoColorFactory.NORMAL_STROKE);
                }
            }
        }
    }

    private void drawVehicleDetails(Graphics2D g, ImageIcon icon, ImageObserver imageObserver,
            PlanningCustomer fromCustomer, int load, int capacity) {
        if (fromCustomer.getNextCustomer() == null) {
            LOGGER.warn("Error getting location from the next customer in chain: nextCustomer is null");
        } else {
            PlanningLocation nextLocation = fromCustomer.getNextCustomer().getLocation();
            PlanningLocation location = fromCustomer.getLocation();
            double longitude = (nextLocation.getLongitude() + location.getLongitude()) / 2.0;
            int x = translator.translateLongitudeToX(longitude);
            double latitude = (nextLocation.getLatitude() + location.getLatitude()) / 2.0;
            int y = translator.translateLatitudeToY(latitude);
            boolean ascending = (nextLocation.getLongitude() < location.getLongitude())
                    ^ (nextLocation.getLatitude() < location.getLatitude());
            int vehicleInfoHeight = icon.getIconHeight() + 2 + TEXT_SIZE;
            g.drawImage(icon.getImage(),
                    x + 1, (ascending ? y - vehicleInfoHeight - 1 : y + 1), imageObserver);
            g.drawString(load + " / " + capacity,
                    x + 1, (ascending ? y - 1 : y + vehicleInfoHeight + 1));
        }
    }

    private void drawVehicleDetails(Graphics2D g, ImageIcon icon, ImageObserver imageObserver,
            PlanningLocation fromLocation, PlanningLocation toLocation, int load, int capacity) {
        double longitude = (toLocation.getLongitude() + fromLocation.getLongitude()) / 2.0;
        int x = translator.translateLongitudeToX(longitude);
        double latitude = (toLocation.getLatitude() + fromLocation.getLatitude()) / 2.0;
        int y = translator.translateLatitudeToY(latitude);
        boolean ascending = (toLocation.getLongitude() < fromLocation.getLongitude())
                ^ (toLocation.getLatitude() < fromLocation.getLatitude());
        int vehicleInfoHeight = icon.getIconHeight() + 2 + TEXT_SIZE;
        g.drawImage(icon.getImage(),
                x + 1, (ascending ? y - vehicleInfoHeight - 1 : y + 1), imageObserver);
        g.drawString(load + " / " + capacity,
                x + 1, (ascending ? y - 1 : y + vehicleInfoHeight + 1));
    }

    private void drawDepotDetails(Graphics2D g, ImageObserver imageObserver, VehicleRoutingProblem solution) {
        g.setColor(TangoColorFactory.ALUMINIUM_3);
        for (PlanningDepot depot : solution.getDepotList()) {
            int x = translator.translateLongitudeToX(depot.getLocation().getLongitude());
            int y = translator.translateLatitudeToY(depot.getLocation().getLatitude());
            g.fillRect(x - 2, y - 2, DEPOT_MARKER_WIDTH, DEPOT_MARKER_HEIGHT);
            g.drawImage(depotImageIcon.getImage(),
                    x - depotImageIcon.getIconWidth() / 2, y - 2 - depotImageIcon.getIconHeight(), imageObserver);
        }
    }

    private void drawLegend(Graphics2D g, VehicleRoutingProblem solution, double width, double height) {
        g.setColor(TangoColorFactory.ALUMINIUM_3);
        g.fillRect(5, (int) height - 12 - TEXT_SIZE - (TEXT_SIZE / 2), 5, 5);
        g.drawString("Depot", LEGEND_STRING_POSITION_X, (int) height - STRING_VERTICAL_PADDING - TEXT_SIZE);
        String vehiclesSizeString = solution.getVehiclesNumber() + " vehicles";
        g.drawString(vehiclesSizeString,
                ((int) width - g.getFontMetrics().stringWidth(vehiclesSizeString)) / 2,
                (int) height - STRING_VERTICAL_PADDING - TEXT_SIZE);
        g.setColor(TangoColorFactory.ALUMINIUM_4);
        g.fillRect(6, (int) height - 6 - (TEXT_SIZE / 2), 3, 3);
        g.drawString("Customer: demand", LEGEND_STRING_POSITION_X, (int) height - 5);
        String customersSizeString = solution.getCustomersNumber() + " customers";
        g.drawString(customersSizeString,
                ((int) width - g.getFontMetrics().stringWidth(customersSizeString)) / 2, (int) height - 5);
    }

    private void drawSoftScore(Graphics2D g, VehicleRoutingProblem solution, double width, double height) {
        g.setColor(TangoColorFactory.ORANGE_3);
        HardSoftScore score = solution.getScore();
        if (score != null) {
            String fuelString;
            if (!score.isFeasible()) {
                fuelString = "Not feasible";
            } else {
                double fuel = ((double) -score.getSoftScore()) / FACTOR;
                fuelString = NUMBER_FORMAT.format(fuel) + " fuel";
            }
            g.setFont(g.getFont().deriveFont(Font.BOLD, (float) TEXT_SIZE * 2));
            g.drawString(fuelString,
                    (int) width - g.getFontMetrics().stringWidth(fuelString) - 10, (int) height - 10 - TEXT_SIZE);
        }
    }

    private void drawRoute(Graphics2D g, int x1, int y1, int x2, int y2, boolean straight) {
        if (straight) {
            g.drawLine(x1, y1, x2, y2);
        } else {
            double xDistPart = (x2 - x1) / 3.0;
            double yDistPart = (y2 - y1) / 3.0;
            double ctrlx1 = x1 + xDistPart + yDistPart;
            double ctrly1 = y1 - xDistPart + yDistPart;
            double ctrlx2 = x2 - xDistPart - yDistPart;
            double ctrly2 = y2 + xDistPart - yDistPart;
            g.draw(new CubicCurve2D.Double(x1, y1, ctrlx1, ctrly1, ctrlx2, ctrly2, x2, y2));
        }
    }

    private void drawRoute(Graphics2D g, PlanningLocation location1, PlanningLocation location2, boolean straight) {
        int x1 = translator.translateLongitudeToX(location1.getLongitude());
        int y1 = translator.translateLatitudeToY(location1.getLatitude());
        int x2 = translator.translateLongitudeToX(location2.getLongitude());
        int y2 = translator.translateLatitudeToY(location2.getLatitude());
        if (straight) {
            g.drawLine(x1, y1, x2, y2);
        } else {
            double xDistPart = (x2 - x1) / 3.0;
            double yDistPart = (y2 - y1) / 3.0;
            double ctrlx1 = x1 + xDistPart + yDistPart;
            double ctrly1 = y1 - xDistPart + yDistPart;
            double ctrlx2 = x2 - xDistPart - yDistPart;
            double ctrly2 = y2 + xDistPart - yDistPart;
            g.draw(new CubicCurve2D.Double(x1, y1, ctrlx1, ctrly1, ctrlx2, ctrly2, x2, y2));
        }
    }

    private int determineMaximumTimeWindowTime(VehicleRoutingProblem solution) {
        int maximumTimeWindowTime = 0;
        for (PlanningDepot depot : solution.getDepotList()) {
            if (depot instanceof PlanningTimeWindowedDepot) {
                int timeWindowTime = ((PlanningTimeWindowedDepot) depot).getDueTime();
                if (timeWindowTime > maximumTimeWindowTime) {
                    maximumTimeWindowTime = timeWindowTime;
                }
            }
        }
        for (PlanningCustomer customer : solution.getCustomerList()) {
            if (customer instanceof PlanningTimeWindowedCustomer) {
                int timeWindowTime = ((PlanningTimeWindowedCustomer) customer).getDueTime();
                if (timeWindowTime > maximumTimeWindowTime) {
                    maximumTimeWindowTime = timeWindowTime;
                }
            }
        }
        return maximumTimeWindowTime;
    }

    private int calculateTimeWindowDegree(int maximumTimeWindowTime, int timeWindowTime) {
        return (360 * timeWindowTime / maximumTimeWindowTime);
    }

    private int getAngleForTimeWindowDegree(int startDegree, int maximumTimeWindow, int timeWindowBound) {
        return startDegree - calculateTimeWindowDegree(maximumTimeWindow, timeWindowBound);
    }

    private int getAngleForTimeWindowDegrees(int maximumTimeWindow, int lowerBound, int upperBound) {
        return calculateTimeWindowDegree(maximumTimeWindow, lowerBound)
                - calculateTimeWindowDegree(maximumTimeWindow, upperBound);
    }

    private Graphics2D createCanvas(double width, double height) {
        int canvasWidth = (int) Math.ceil(width) + 1;
        int canvasHeight = (int) Math.ceil(height) + 1;
        canvas = new BufferedImage(canvasWidth, canvasHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = canvas.createGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, canvasWidth, canvasHeight);
        return g;
    }

}
