/*
 * Copyright 2012 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.showcase.model.vehiclerouting.impl.domain.location;

import java.util.Objects;

import org.optaplanner.showcase.model.common.AbstractPersistable;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.DistanceType;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.location.segmented.RoadSegmentLocation;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamInclude;

@XStreamAlias("VrpLocation")
@XStreamInclude({
        AirLocation.class,
        RoadLocation.class
})
public abstract class Location extends AbstractPersistable implements PlanningLocation {

    protected static final double SAFE_FACTOR = 1000.0;
    protected static final double SAFE_SHIFT = 0.5;

    protected String name;
    protected double latitude;
    protected double longitude;

    public Location() {
    }

    public Location(long id, double latitude, double longitude) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public DistanceType getDistanceType() {
        if (this instanceof AirLocation) {
            return DistanceType.AIR_DISTANCE;
        } else if (this instanceof RoadLocation) {
            return DistanceType.ROAD_DISTANCE;
        } else if (this instanceof RoadSegmentLocation) {
            return DistanceType.SEGMENTED_ROAD_DISTANCE;
        } else {
            return DistanceType.UNDETERMINED;
        }
    }

    @Override
    public String toString() {
        if (name == null || name.equals("")) {
            return String.valueOf(id);
        } else {
            return id + "-" + name;
        }
    }

    public String getName() {
        if (name == null) {
            return "Location id: " + id;
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * The angle relative to the direction EAST.
     * @param location never null
     * @return in Cartesian coordinates
     */
    public double getAngle(PlanningLocation location) {
        // Euclidean distance (Pythagorean theorem) - not correct when the surface is a sphere
        double latitudeDifference = location.getLatitude() - latitude;
        double longitudeDifference = location.getLongitude() - longitude;
        return Math.atan2(latitudeDifference, longitudeDifference);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Location location = (Location) o;
        return Objects.equals(latitude, location.latitude)
                && Objects.equals(longitude, location.longitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }

    public abstract static class Builder<T extends Location, B extends Builder<T, B>> {

        protected T built;

        private B builder;

        protected Builder() {
            built = createLocation();
            builder = getBuilder();
        }

        public B withId(Long id) {
            built.setId(id);
            return builder;
        }

        public B withLatitude(double latitude) {
            built.setLatitude(latitude);
            return builder;
        }

        public B withLongitude(double longitude) {
            built.setLongitude(longitude);
            return builder;
        }

        public B withName(String name) {
            built.setName(name);
            return builder;
        }

        public T build() {
            return built;
        }

        protected abstract T createLocation();

        protected abstract B getBuilder();

    }

}
