package org.optaplanner.showcase.model.vehiclerouting.api.domain;

import org.optaplanner.showcase.model.common.PlannerIdentifiable;

public interface PlanningCustomer extends PlannerIdentifiable, PlanningDestination {

    int getDemand();

    void setDemand(int demand);

    /**
     * Gets the next customer in the delivery chain.
     * @return  next customer in chain
     */
    PlanningCustomer getNextCustomer();

}
