package org.optaplanner.showcase.model.vehiclerouting.impl.domain.timewindowed;

import java.util.Objects;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.Depot;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("VrpTimeWindowedDepot")
public class TimeWindowedDepot extends Depot implements PlanningTimeWindowedDepot {

    // Times are multiplied by 1000 to avoid floating point arithmetic rounding errors
    private int readyTime;
    private int dueTime;

    /**
     * @return a positive number, the time multiplied by 1000 to avoid floating point arithmetic rounding errors
     */
    public int getReadyTime() {
        return readyTime;
    }

    public void setReadyTime(int readyTime) {
        this.readyTime = readyTime;
    }

    /**
     * @return a positive number, the time multiplied by 1000 to avoid floating point arithmetic rounding errors
     */
    public int getDueTime() {
        return dueTime;
    }

    public void setDueTime(int dueTime) {
        this.dueTime = dueTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode());
    }

}
