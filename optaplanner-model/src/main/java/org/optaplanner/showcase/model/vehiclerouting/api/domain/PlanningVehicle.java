package org.optaplanner.showcase.model.vehiclerouting.api.domain;

import org.optaplanner.showcase.model.common.PlannerIdentifiable;

public interface PlanningVehicle<T extends PlanningDepot> extends PlannerIdentifiable {

    int getCapacity();

    void setCapacity(int capacity);

    T getDepot();

    void setDepot(T depot);

    PlanningCustomer getFirstCustomerToService();

    String getLicensePlate();

    void setLicensePlate(String licensePlate);

}
