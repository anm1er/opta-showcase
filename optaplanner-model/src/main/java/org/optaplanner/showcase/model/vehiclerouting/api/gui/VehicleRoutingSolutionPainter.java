package org.optaplanner.showcase.model.vehiclerouting.api.gui;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.problem.VehicleRoutingProblem;

public interface VehicleRoutingSolutionPainter {

    BufferedImage getCanvas();

    void reset(VehicleRoutingProblem problem, Dimension size, ImageObserver imageObserver);

    void reset(VehicleRoutingProblem problem, Dimension size);

}
