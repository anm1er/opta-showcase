/**
 *
 * Classes in this package are the copies of the original org.optaplanner.examples.vehiclerouting.persistence.
 *
 * @see <a href="http://www.optaplanner.org">http://www.optaplanner.org</a>
 *
 */
package org.optaplanner.showcase.model.vehiclerouting.impl.persistence;

