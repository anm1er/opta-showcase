/**
 *
 * Interfaces in this package represent contracts to decouple the client of OptaPlanner
 * from the concrete planning model implementation for vehicle routing problem with time windows.
 *
 * @see <a href="http://www.optaplanner.org">http://www.optaplanner.org</a>
 *
 */
package org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed;

