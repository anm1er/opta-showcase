package org.optaplanner.showcase.model.vehiclerouting.impl.domain.timewindowed;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningVehicle;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.DistanceType;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningBeanFactory;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedCustomer;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed.PlanningTimeWindowedDepot;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.Vehicle;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.location.AirLocation;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.location.Location;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.location.RoadLocation;
import org.optaplanner.showcase.model.vehiclerouting.impl.domain.location.segmented.RoadSegmentLocation;

public final class DefaultPlanningBeanFactory implements PlanningBeanFactory {

    private DistanceType distanceType;

    private DefaultPlanningBeanFactory(DistanceType distanceType) {
        this.distanceType = distanceType;
    }

    /**
     * Returns {@code PlanningBeanFactory} instance to calculate the cost between 2 locations along a straight line:
     * <em>"the euclidean distance between their GPS coordinates"</em>.
     * <p/>
     * <p/>
     * See OptaPlanner docs for more information on distance type options.
     * <p/>
     * @return {@code TimeWindowedPlanningEntityMapper} instance with reasonable defaults for mapping
     */
    public static DefaultPlanningBeanFactory getInstanceForAirDistance() {
        return new DefaultPlanningBeanFactory(DistanceType.AIR_DISTANCE);
    }

    /**
     * Returns {@code PlanningBeanFactory} instance to calculate the cost between 2 locations <em>"precalculatedon a real
     * road network route"</em>.
     * <p/>
     * <p/>
     * See OptaPlanner docs for more information on distance type options.
     * <p/>
     * @return {@code TimeWindowedPlanningEntityMapper} instance with reasonable defaults for mapping
     */
    public static DefaultPlanningBeanFactory getInstanceForRoadDistance() {
        return new DefaultPlanningBeanFactory(DistanceType.ROAD_DISTANCE);
    }

    /**
     * Returns {@code PlanningBeanFactory} instance to calculate the cost between 2 locations for high scale problems to
     * avoid keeping the entire cost matrix in memory.
     * <p/>
     * <p/>
     * See OptaPlanner docs for more information on distance type options.
     * <p/>
     * @return {@code TimeWindowedPlanningEntityMapper} instance with reasonable defaults for mapping
     */
    public static DefaultPlanningBeanFactory getInstanceForRoadSegmentDistance() {
        return new DefaultPlanningBeanFactory(DistanceType.SEGMENTED_ROAD_DISTANCE);
    }

    @Override
    public PlanningLocation createLocation() {
        return createUniqueLocation("", 0, 0);
    }

    @Override
    public PlanningLocation createUniqueLocation(String name, double latitude, double longitude) {
        // Factory should not preserve state - create builder each time creation method is called
        return getDistanceTypeSpecificBuilder()
                .withName(name)
                .withLatitude(latitude)
                .withLongitude(longitude)
                .build();
    }

    private Location.Builder getDistanceTypeSpecificBuilder() {
        Location.Builder builder;
        if (distanceType == DistanceType.AIR_DISTANCE) {
            builder = AirLocation.getBuilder();
        } else if (distanceType == DistanceType.ROAD_DISTANCE) {
            builder = RoadLocation.getBuilder();
        } else {
            builder = RoadSegmentLocation.getBuilder();
        }
        return builder;
    }

    @Override
    public PlanningTimeWindowedCustomer createCustomer() {
        return new TimeWindowedCustomer();
    }

    @Override
    public PlanningTimeWindowedDepot createDepot() {
        return new TimeWindowedDepot();
    }

    @Override
    public PlanningVehicle createVehicle() {
        return new Vehicle();
    }

}
