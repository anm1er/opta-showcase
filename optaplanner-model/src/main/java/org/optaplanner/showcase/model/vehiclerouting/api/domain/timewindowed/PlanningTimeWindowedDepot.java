package org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.PlanningDepot;

public interface PlanningTimeWindowedDepot extends PlanningDepot, TimeWindow {
}
