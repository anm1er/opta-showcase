/*
 * Copyright 2014 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.showcase.model.vehiclerouting.impl.domain.location;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.DistanceType;
import org.optaplanner.showcase.model.vehiclerouting.api.domain.location.PlanningLocation;

/**
 * The cost between 2 locations is a straight line: the euclidean distance between their GPS coordinates.
 * Used with {@link DistanceType#AIR_DISTANCE}.
 */
@XStreamAlias("VrpAirLocation")
public class AirLocation extends Location {

    public AirLocation() {
    }

    public AirLocation(long id, double latitude, double longitude) {
        super(id, latitude, longitude);
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    /**
     * The distance's unit of measurement depends on the {@link DistanceType}.
     * It can be in miles or km, but for most cases it's in the TSPLIB's unit of measurement.
     * @param location never null
     * @return a positive number, the distance multiplied by 1000 to avoid floating point arithmetic rounding errors
     */
    @Override
    public int getDistanceTo(PlanningLocation location) {
        // Implementation specified by TSPLIB http://www2.iwr.uni-heidelberg.de/groups/comopt/software/TSPLIB95/
        // Euclidean distance (Pythagorean theorem) - not correct when the surface is a sphere
        double latitudeDifference = location.getLatitude() - latitude;
        double longitudeDifference = location.getLongitude() - longitude;
        double distance = Math.sqrt(
                (latitudeDifference * latitudeDifference) + (longitudeDifference * longitudeDifference));
        // Multiplied by 1000 to avoid floating point arithmetic rounding errors
        return (int) (distance * SAFE_FACTOR + SAFE_SHIFT);
    }

    public static class Builder extends Location.Builder<AirLocation, Builder> {

        @Override
        protected AirLocation createLocation() {
            return new AirLocation();
        }

        @Override
        protected Builder getBuilder() {
            return this;
        }

    }

}
