package org.optaplanner.showcase.model.vehiclerouting.api.domain.timewindowed;

public interface TimeWindow {

    int getReadyTime();

    void setReadyTime(int readyTime);

    int getDueTime();

    void setDueTime(int dueTime);

}
